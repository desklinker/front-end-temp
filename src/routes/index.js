import React, {useEffect, useState } from 'react';
import { Redirect, Route } from 'react-router-dom';

import * as routes from '../constants/routes';
import { isUserAuthenticated } from '../helpers/authUtils';
import { EXECUTIVE, SCREENER, ADMIN, COORDINATOR, PROFESSIONAL, CLIENT } from '../constants';

// lazy load all the views
// auth
const Login = React.lazy(() => import('../pages/auth/Login'));
const Logout = React.lazy(() => import('../pages/auth/Logout'));
const Register = React.lazy(() => import('../pages/auth/Register'));
const ForgetPassword = React.lazy(() => import('../pages/auth/ForgetPassword'));
const Confirm = React.lazy(() => import('../pages/auth/Confirm'));
const PhoneNumberVerify = React.lazy(() => import('../pages/auth/PhoneNumberVerify'));

// dashboard
const Dashboard = React.lazy(() => import('../pages/dashboards'));

// pages
const MyProjectsPage = React.lazy(() => import('../pages/myProjects'));
const ProjectPage = React.lazy(() => import('../pages/projects'));
const ProjectView = React.lazy(() => import('../pages/projects/ProjectView'));
const Threads = React.lazy(() => import('../pages/threads'));
const Contacts = React.lazy(() => import('../pages/contacts'));
const Finance = React.lazy(() => import('../pages/finance'));
const Earnings = React.lazy(() => import('../pages/earnings'));
const FileManager = React.lazy(() => import('../pages/fileManager'));
const Profile = React.lazy(() => import('../pages/profile/ViewProfile'));
const CreateProfile = React.lazy(() => import('../pages/profile/CreateProfile'));

// error pages
const ErrorPageNotFound = React.lazy(() => import('../pages/error/PageNotFound'));
const ServerError = React.lazy(() => import('../pages/error/ServerError'));

// handle auth and authorization
const PrivateRoute = ({ component: Component, roles, loggedInUser, ...rest }) => {
  const [isMounted, setMounted] = useState(true);

  useEffect(() => {
    setMounted(false);
  }, []);
  return (
    <Route
      {...rest}
      render={(props) => {
        if (!isUserAuthenticated()) {
          // not logged in so redirect to login page with the return url
          return <Redirect to={{ pathname: routes.LOGIN, state: { from: props.location } }} />;
        }

        // check if route is restricted by role
        if (roles && roles.indexOf(loggedInUser.role) === -1) {
          // role not authorised so redirect to home page
          return <Redirect to={{ pathname: routes.ROOT, state: { from: props.location } }} />;
        }

        if (!loggedInUser.userConfirmed && isMounted && (loggedInUser.role === PROFESSIONAL || loggedInUser.role === CLIENT)) {
          // role not authorised so redirect to home page
          return <Redirect to={{ pathname: routes.PROFILE_CREATE, state: { from: props.location } }} />;
        }

        // authorised so return component
        return <Component {...props} />;
      }}

      // component={Component}
    />
  );
};

// root routes
const rootRoute = {
  path: routes.ROOT,
  exact: true,
  component: () => <Redirect to={routes.DASHBOARD} />,
  route: PrivateRoute,
};

// executive dashboard
const dashboardRoutes = {
  path: routes.DASHBOARD,
  name: 'Dashboard',
  icon: 'uil-home-alt',
  exact: true,
  roles: [EXECUTIVE, SCREENER, ADMIN, COORDINATOR, PROFESSIONAL, CLIENT],
  component: Dashboard,
  route: PrivateRoute,
  display: true,
};

// profile
const profileRoutes = {
  path: routes.PROFILE,
  name: 'Profile',
  roles: [COORDINATOR, PROFESSIONAL, CLIENT, ADMIN],
  display: false,
  children: [
    {
      path: routes.PROFILE_VIEW,
      name: 'Profile',
      component: Profile,
      route: PrivateRoute,
    },
    {
      path: routes.PROFILE_CREATE,
      name: 'Create Profile',
      component: CreateProfile,
      route: PrivateRoute,
    },
  ],
};

// my projects
const myProjectsRoutes = {
  path: routes.MY_PROJECT,
  name: 'MyProjects',
  icon: 'uil-bag',
  roles: [COORDINATOR, PROFESSIONAL, CLIENT],
  component: MyProjectsPage,
  route: PrivateRoute,
  display: true,
};

// threads
const threadsRoute = {
  path: routes.THREAD,
  name: 'Threads',
  icon: 'uil-comments',
  roles: [COORDINATOR, PROFESSIONAL, CLIENT, ADMIN],
  component: Threads,
  route: PrivateRoute,
  display: true,
};

// file managers
const fileManagerRoute = {
  path: routes.FILE_MANAGER,
  name: 'File Manager',
  icon: 'uil-file',
  roles: [ADMIN],
  component: FileManager,
  route: PrivateRoute,
  display: true,
};

//finance
const financeRoutes = {
  path: routes.FINANCE,
  name: 'Finance',
  icon: 'uil-money-bill',
  roles: [ADMIN],
  component: Finance,
  route: PrivateRoute,
  display: true,
};

// project view
const projectRoutes = {
  path: routes.PROJECT,
  name: 'Project',
  roles: [COORDINATOR, PROFESSIONAL, CLIENT, SCREENER, ADMIN],
  component: ProjectPage,
  route: PrivateRoute,
  display: false,
  children: [
    {
      path: routes.PROJECT_VIEW,
      name: 'Single Project',
      component: ProjectView,
      route: PrivateRoute,
    },
  ],
};

// earnings
const earningsRoute = {
  path: routes.EARNING,
  name: 'Earnings',
  roles: [COORDINATOR, PROFESSIONAL],
  component: Earnings,
  icon: 'uil-dollar-alt',
  route: PrivateRoute,
  display: true,
};

// contacts
const contactsRoutes = {
  path: routes.CONTACT,
  name: 'Contacts',
  roles: [COORDINATOR, ADMIN, CLIENT],
  component: Contacts,
  route: PrivateRoute,
  icon: 'uil-users-alt',
  display: true,
};

const appRoutes = [
  profileRoutes,
  myProjectsRoutes,
  threadsRoute,
  fileManagerRoute,
  projectRoutes,
  earningsRoute,
  contactsRoutes,
  financeRoutes,
];

// pages
const pageRoutes = {
  name: 'Pages',
  icon: 'uil-copy-alt',
  header: 'Custom',
  children: [
    {
      path: routes.PAGE_NOT_FOUND,
      name: 'Error - 404',
      component: ErrorPageNotFound,
      route: PrivateRoute,
    },
    {
      path: routes.SERVER_ERROR,
      name: 'Error - 500',
      component: ServerError,
      route: PrivateRoute,
    },
  ],
};

// auth
const authRoutes = {
  path: routes.ACCOUNT,
  name: 'Auth',
  children: [
    {
      path: routes.LOGIN,
      name: 'Login',
      component: Login,
      route: Route,
    },
    {
      path: routes.LOGOUT,
      name: 'Logout',
      component: Logout,
      route: Route,
    },
    {
      path: routes.REGISTER,
      name: 'Register',
      component: Register,
      route: Route,
    },
    {
      path: routes.CONFIRM,
      name: 'Confirm',
      component: Confirm,
      route: Route,
    },
    {
      path: routes.PHONE_VERIFY,
      name: 'Phone Number Confirm',
      component: PhoneNumberVerify,
      route: Route,
    },
    {
      path: routes.FORGET_PASSWORD,
      name: 'Forget Password',
      component: ForgetPassword,
      route: Route,
    },
  ],
};

// flatten the list of all nested routes
const flattenRoutes = (routes) => {
  let flatRoutes = [];

  routes = routes || [];
  routes.forEach((item) => {
    flatRoutes.push(item);

    if (typeof item.children !== 'undefined') {
      flatRoutes = [...flatRoutes, ...flattenRoutes(item.children)];
    }
  });
  return flatRoutes;
};

// All routes
const allRoutes = [rootRoute, dashboardRoutes, ...appRoutes, authRoutes, pageRoutes];

const authProtectedRoutes = [dashboardRoutes, ...appRoutes, pageRoutes];

const allFlattenRoutes = flattenRoutes(allRoutes);

export { allRoutes, authProtectedRoutes, allFlattenRoutes };
