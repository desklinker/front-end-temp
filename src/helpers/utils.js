import cad from '../assets/images/projects/cad.jpg';
import computer from '../assets/images/projects/design.jpg';
import design2 from '../assets/images/projects/design2.jpg';
import architecture from '../assets/images/projects/architecture.jpg';

const queryString = require('query-string');

export function projectImage(fieldId) {
  switch (fieldId) {
    case 1:
      return computer;
    case 2:
      return cad;
    case 3:
      return architecture;
    case 4:
      return computer;
    default:
      return design2;
  }
}

export function queryStringify(obj) {
  return queryString.stringify(obj, { arrayFormat: 'comma' });
}

export function downloadBlob(blob, filename) {
  const url = URL.createObjectURL(blob);
  const a = document.createElement('a');
  a.href = url;
  a.download = filename || 'download';
  const clickHandler = () => {
    setTimeout(() => {
      URL.revokeObjectURL(url);
      a.removeEventListener('click', clickHandler);
    }, 150);
  };
  a.addEventListener('click', clickHandler, false);
  a.click();
  return a;
}

export function extensionBgColor(extension) {
  switch (extension) {
    case 'docx':
      return 'bg-secondary';
    case 'pdf':
      return 'bg-danger';
    case 'jpg':
    case 'jpeg':
    case 'png':
      return 'bg-primary';
    default:
      return '';
  }
}
