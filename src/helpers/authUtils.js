import { CognitoUserPool, CognitoUser, AuthenticationDetails } from 'amazon-cognito-identity-js';
import { region, userPoolId, poolData } from '../config/env';
import * as AWS from 'aws-sdk/global';
import { head } from 'lodash-es';

/**
 * Checks if user is authenticated
 */
const isUserAuthenticated = () => {
  const { isAWSAuthenticate } = getCurrentCognitoUser();
  return isAWSAuthenticate;
};

const getAuthUser = () => {
  const { authUser } = getCurrentCognitoUser();
  return authUser;
};

const getCurrentCognitoUser = () => {
  let isAWSAuthenticate = false;
  let authUser;

  const userPool = new CognitoUserPool(poolData);
  const cognitoUser = userPool.getCurrentUser();
  const url = `cognito-idp.${region}.amazonaws.com/${userPoolId}`;

  if (cognitoUser != null) {
    cognitoUser.getSession(function (err, result) {
      if (result) {
        authUser = formattedResponse(result, true);

        const refresh_token = result.getRefreshToken(); // receive session from calling cognitoUser.getSession()
        if (AWS.config.credentials.needsRefresh()) {
          cognitoUser.refreshSession(refresh_token, (err, session) => {
            if (err) {
              console.log(err);
            } else {
              AWS.config.credentials.params.Logins[url] = session.getIdToken().getJwtToken();
              AWS.config.credentials.refresh((err) => {
                if (err) {
                  console.log(err);
                } else {
                  isAWSAuthenticate = true;
                }
              });
            }
          });
        } else {
          isAWSAuthenticate = true;
        }

        if (!authUser.phoneNumberVerified || !authUser.emailVerified) {
          isAWSAuthenticate = false;
        }
      } else {
        isAWSAuthenticate = false;
      }
    });
  }

  return { isAWSAuthenticate, authUser };
};

const formattedResponse = (result, isAuthUser) => {
  const payload = result.getIdToken().decodePayload();
  var accessToken = result.getAccessToken().getJwtToken();
  let idToken = result.getIdToken().getJwtToken();

  const response = {
    id: payload.aud,
    username: payload && payload['cognito:username'],
    role: head(payload['cognito:groups']),
    email: payload.email,
    emailVerified: payload.email_verified,
    phoneNumberVerified: payload.phone_number_verified,
  };

  if (!isAuthUser) {
    return response;
  } else {
    return {
      ...response,
      accessToken: accessToken,
      idToken: idToken,
      phoneNumber: payload.phone_number,
    };
  }
};

const getCognitoAuthConfig = ({ username, password }) => {
  const authenticationData = {
    Username: username,
    Password: password,
  };

  const userPool = new CognitoUserPool(poolData);
  const userData = {
    Username: username,
    Pool: userPool,
  };

  const cognitoUser = new CognitoUser(userData);
  const authenticationDetails = new AuthenticationDetails(authenticationData);

  return { cognitoUser, authenticationDetails };
};

const formattedResponseUser = (response, action) => {
  const { uid, userConfirmed, firstName, lastName } = response.data;
  const userDetail = {
    userId: uid,
    userConfirmed: userConfirmed,
    firstName: firstName,
    lastName: lastName,
  };

  const result = {
    ...action.payload,
    ...userDetail,
  };
  return result;
};

export { isUserAuthenticated, formattedResponse, getCognitoAuthConfig, formattedResponseUser, getAuthUser };
