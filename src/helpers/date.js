import moment from 'moment';

export const years = (isOngoing, minDate) => {
  const years = [];
  if (isOngoing) {
    years.push({
      value: 'Ongoing',
      label: 'Ongoing',
    });
  }
  const dateStart = moment();
  const dateEnd = moment().subtract(60, 'y');
  while (dateStart.diff(dateEnd, 'years') > 0) {
    years.push({
      value: dateStart.format('YYYY'),
      label: dateStart.format('YYYY'),
      isDisabled: minDate ? dateStart.format('YYYY') < minDate.value : false,
    });
    dateStart.subtract(1, 'year');
  }
  return years;
};
