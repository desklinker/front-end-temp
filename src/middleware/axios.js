import { LOGOUT, SERVER_ERROR } from '../constants/routes';
import { push } from 'connected-react-router';
import { getAuthUser } from '../helpers/authUtils';

/**
 * Axios request identifier
 * @type {string}
 */
export const HTTP_REQUEST = 'HTTP_REQUEST';

/**
 * Axios middleware
 * Create request using given client
 * Use action identifier options to process request
 */
function createMiddleware(client) {
  return ({ dispatch, getState }) => (next) => (action) => {
    const { [HTTP_REQUEST]: options, ...nextAction } = action;

    if (!options) {
      return next(action);
    }

    let authorization;
    if (options.useAuth !== false) {
      const authUser = getAuthUser();
      if (authUser) {
        authorization = authUser.idToken;
      }
    }

    // Add request interceptor
    const { onSuccess, onError, onUploadProgress, onDownloadProgress, ...requestConfig } = options;
    if (authorization) {
      client.defaults.headers.common['DL_AUTH'] = authorization;
    }

    if (action.type) {
      next(nextAction);
    }

    if (onUploadProgress) {
      requestConfig.onUploadProgress = (event) => {
        const progressAction = onUploadProgress(event, getState, dispatch);
        if (progressAction) {
          dispatch(progressAction);
        }
      };
    }

    if (onDownloadProgress) {
      requestConfig.onDownloadProgress = (event) => {
        const progressAction = onDownloadProgress(event, getState, dispatch);
        if (progressAction) {
          dispatch(progressAction);
        }
      };
    }

    return client
      .request(requestConfig)
      .then((response) => {
        if (options.onSuccess) {
          dispatch(onSuccess(response, getState, dispatch));
        }
        return response;
      })
      .catch((error) => {
        if (error.message === 'Unauthenticated.') {
          dispatch(push(LOGOUT));
        } else if (error.message === 'This action is unauthorized.') {
          dispatch(push(SERVER_ERROR));
        }

        if (options.onError) {
          dispatch(onError(error, getState, dispatch));
        }

        return Promise.reject(error);
      });
  };
}

export default createMiddleware;
