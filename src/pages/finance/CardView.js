import React from 'react';
import { Col, Row } from 'reactstrap';
import ProjectList from './ProjectList';
import ProjectView from './ProjectView';

function CardView({ onProjectSelect, selectedProject }) {
    return (
        <>
            <Row>
                <Col xl={3} lg={3} md={12} className="order-lg-1 order-xl-1">
                    <ProjectList onProjectSelect={onProjectSelect} />
                </Col>

                <Col xl={9} lg={9} md={12} className="order-lg-2 order-xl-1">
                    <ProjectView project={selectedProject} />
                </Col>
            </Row>
        </>
    );
}

export default CardView;
