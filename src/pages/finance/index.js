import React, { useState } from 'react';
import PageTitle from '../../components/PageTitle';
import { Collapse } from 'reactstrap';
import FilterForm from './FilterForm';
import CardView from './CardView';
import ListView from './ListView';

function Finance() {
    const [selectedProject, setSelectedProject] = useState();
    const [view, setView] = useState('card');
    const [isFiltersOpen, setFiltersOpen] = useState(false);

    const onProjectSelect = (project) => {
        setSelectedProject(project);
    };

    const search = (text) => {
        //
    };

    const toggle = () => setFiltersOpen(!isFiltersOpen);

    return (
        <>
            <PageTitle
                breadCrumbItems={[{ label: 'Finance', path: '/finance', active: true }]}
                title={'Finance'}
                isFilter
                toggle={toggle}
                isSearch
                search={search}
                view={view}
                setView={setView}
            />

            <Collapse isOpen={isFiltersOpen}>
                <FilterForm setFilterOpen={setFiltersOpen} />
            </Collapse>

            {view === 'card' ? (
                <CardView onProjectSelect={onProjectSelect} selectedProject={selectedProject} />
            ) : (
                <ListView />
            )}
        </>
    );
}

export default Finance;
