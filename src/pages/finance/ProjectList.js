import React, { useEffect, useState } from 'react';
import { Card, CardBody, ButtonGroup, Button } from 'reactstrap';
import SimpleBar from 'simplebar-react';

import { projects as allProjects } from './data';
import ProjectItem from './ProjectItem';

function ProjectList({ onProjectSelect }) {
    const [projects, setProjects] = useState(allProjects);
    const [selectedProject, setSelectedProject] = useState(allProjects[0]);
    const [isOngoing, setOngoing] = useState(true);
    const [isPast, setPast] = useState(false);

    const search = (text) => {
        if (text) {
            setProjects(allProjects.filter((u) => u.title.toLowerCase().indexOf(text.toLowerCase()) >= 0));
        } else {
            setProjects(allProjects);
        }
    };

    const activateProject = (project) => {
        setSelectedProject(project);
        if (onProjectSelect) {
            onProjectSelect(project);
        }
    };

    const handleProjectFilterBtnClick = (e) => {
        if (e.target.value === 'onGoing') {
            setOngoing(true);
            setPast(false);
        } else if (e.target.value === 'past') {
            setOngoing(false);
            setPast(true);
        }
    };

    useEffect(() => {
        if (onProjectSelect) {
            onProjectSelect(allProjects[0]);
        }
    }, [onProjectSelect, projects]);

    return (
        <React.Fragment>
            <Card>
                <CardBody className="p-0">
                    <div className="tab-content">
                        <div className="tab-pane show active p-3">
                            <div className="app-search">
                                <div className="form-group position-relative">
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder="Search projects"
                                        onKeyUp={(e) => search(e.target.value)}
                                    />
                                    <span className="mdi mdi-magnify"></span>
                                </div>
                                <ButtonGroup className="threadFilterButton">
                                    <Button
                                        color="light"
                                        onClick={handleProjectFilterBtnClick}
                                        className={isOngoing ? 'activeBtn' : ''}
                                        value="onGoing">
                                        Ongoing
                                    </Button>
                                    <Button
                                        color="light"
                                        onClick={handleProjectFilterBtnClick}
                                        className={isPast ? 'activeBtn' : ''}
                                        value="past">
                                        Past
                                    </Button>
                                </ButtonGroup>
                            </div>

                            <SimpleBar style={{ maxHeight: '550px', width: '100%' }}>
                                {projects &&
                                    projects.map((project, index) => {
                                        if (isOngoing && project.status === 'Ongoing') {
                                            return (
                                                <ProjectItem
                                                    index={`ongoing_${index}`}
                                                    activateProject={activateProject}
                                                    project={project}
                                                    selectedProject={selectedProject}
                                                />
                                            );
                                        } else if (isPast && project.status === 'Past') {
                                            return (
                                                <ProjectItem
                                                    index={`past_${index}`}
                                                    activateProject={activateProject}
                                                    project={project}
                                                    selectedProject={selectedProject}
                                                />
                                            );
                                        } else {
                                            return null;
                                        }
                                    })}
                            </SimpleBar>
                        </div>
                    </div>
                </CardBody>
            </Card>
        </React.Fragment>
    );
}

export default ProjectList;
