import React from 'react';
import Card from 'reactstrap/es/Card';
import CardBody from 'reactstrap/es/CardBody';
import { Badge, Table } from 'reactstrap';
import Invoice from './Invoice';
import startCase from 'lodash-es/startCase';
import lowerCase from 'lodash-es/lowerCase';
import map from 'lodash-es/map';
import Threads from './Threads';

function ProjectView({ project }) {
    return (
        <>
            <Card>
                <CardBody>
                    <h3 className="mb-0">{project && `${project.title}`}</h3>
                    <Badge color="secondary mb-3">{project ? project.status : 'Ongoing'}</Badge>

                    <div className="">
                        <Table responsive className="mb-0 table-centered table-nowrap" size="sm" borderless>
                            <tbody>
                                <tr>
                                    <td className="font-weight-bold">Posted Time</td>
                                    <td>
                                        <span className="mr-2 font-weight-bold">: </span>
                                        {project && project.postedTime}
                                    </td>
                                    <td className="font-weight-bold">Completion Date</td>
                                    <td>
                                        <span className="mr-2 font-weight-bold">: </span>
                                        {project && project.completionDate}
                                    </td>

                                    <td className="font-weight-bold">Output File Status</td>
                                    <td>
                                        <span className="mr-2 font-weight-bold">: </span>
                                        {project && project.outputFileStatus}
                                    </td>
                                </tr>
                                <tr>
                                    {project && project.client && (
                                        <>
                                            <td className="font-weight-bold">Client Budget</td>
                                            <td>
                                                <span className="mr-2 font-weight-bold">: </span>
                                                {project.client.budget}
                                            </td>
                                            <td className="font-weight-bold">Client ID</td>
                                            <td>
                                                <span className="mr-2 font-weight-bold">: </span>
                                                {project.client.id}
                                            </td>
                                            <td className="font-weight-bold">Client Name</td>
                                            <td>
                                                <span className="mr-2 font-weight-bold">: </span>
                                                {project.client.name}
                                            </td>
                                        </>
                                    )}
                                </tr>
                                <tr>
                                    {project && project.scree && (
                                        <>
                                            <td className="font-weight-bold">Screen Budget</td>
                                            <td>
                                                <span className="mr-2 font-weight-bold">: </span>
                                                {project.scree.budget}
                                            </td>
                                            <td className="font-weight-bold">Screen ID</td>
                                            <td>
                                                <span className="mr-2 font-weight-bold">: </span>
                                                {project.scree.id}
                                            </td>
                                            <td className="font-weight-bold">Screen Name</td>
                                            <td>
                                                <span className="mr-2 font-weight-bold">: </span>
                                                {project.scree.name}
                                            </td>
                                        </>
                                    )}
                                </tr>
                                <tr>
                                    {project && project.codi && (
                                        <>
                                            <td className="font-weight-bold">Codi Budget</td>
                                            <td>
                                                <span className="mr-2 font-weight-bold">: </span>
                                                {project.codi.budget}
                                            </td>
                                            <td className="font-weight-bold">Codi ID</td>
                                            <td>
                                                <span className="mr-2 font-weight-bold">: </span>
                                                {project.codi.id}
                                            </td>
                                            <td className="font-weight-bold">Codi Name</td>
                                            <td>
                                                <span className="mr-2 font-weight-bold">: </span>
                                                {project.codi.name}
                                            </td>
                                        </>
                                    )}
                                </tr>
                                {project &&
                                    project.profDivision &&
                                    map(project.profDivision, (budget, key) => {
                                        return (
                                            <tr key={key}>
                                                <td className="font-weight-bold pl-3">{`${startCase(
                                                    lowerCase(key)
                                                )} Budget`}</td>
                                                <td>
                                                    <span className="mr-2 font-weight-bold">: </span>
                                                    {budget}
                                                </td>
                                            </tr>
                                        );
                                    })}
                            </tbody>
                        </Table>
                    </div>
                </CardBody>
            </Card>
            <Card>
                <CardBody>
                    <h4 className="header-title">Revision or Scope Change Note</h4>
                    <p>{project && project.revisionNote}</p>
                </CardBody>
            </Card>

            <Threads title="Message from Client / Codi / Prof" />

            <Card>
                <CardBody>
                    <Invoice project={project} />
                </CardBody>
            </Card>

            <Threads title="Message to Client" />
            <Threads title="Message to Coordinator / Professionals" />
        </>
    );
}

export default ProjectView;
