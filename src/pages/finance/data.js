const countries = [
    { value: 'sin', label: 'Singapore' },
    { value: 'aus', label: 'Australia' },
];

const projectStatus = [
    { value: '1', label: 'Just posted' },
    { value: '2', label: 'Screened' },
    { value: '3', label: 'Bid on progress' },
    { value: '4', label: 'Project ongoing' },
    { value: '5', label: 'Project completed' },
    { value: '6', label: 'Pending for payment' },
    { value: '7', label: 'Revision on progress' },
    { value: '8', label: 'Project completed > 14 days' },
    { value: '9', label: 'Project completed < 14 days' },
    { value: '10', label: 'Terminated projects' },
];

const categories = [
    { value: '1', label: 'Civil Engineering' },
    { value: '2', label: 'Mechanical Engineering' },
    { value: '3', label: 'Computer Science' },
];

const outputDeliverStatus = [
    { value: '1', label: 'Drafts passed' },
    { value: '2', label: 'Pending client confirmation' },
    { value: '3', label: 'Final output passed' },
    { value: '4', label: 'Revisions on progress' },
    { value: '5', label: 'Scope changes on progress' },
];

const paymentStatus = [
    { value: '1', label: 'Pending' },
    { value: '2', label: 'Success' },
    { value: '3', label: 'Failed' },
];
const skills = [
    { value: 'cad', label: 'CAD' },
    { value: 'design', label: 'Design' },
    { value: 'arch', label: 'Architecture' },
    { value: 'draft', label: 'Draft' },
];

const coordinators = [
    { value: '1', label: 'Brandon Smith (2)' },
    { value: '2', label: 'Thomazin Ursula (6)' },
    { value: '3', label: 'Isabell Esabell (2)' },
    { value: '4', label: 'Jennat Marion (1)' },
];

const projects = [
    {
        id: 1,
        title: 'Steel Project #2',
        status: 'Ongoing',
        shortDesc: 'With supporting text below as a natural lead-in to additional contenposuere erat a ante',
        field: 'Civil Engineering',
        client: {
            id: 'Cli-00024',
            name: 'Dominic Keller',
            budget: 'S$ 4000',
        },
        scree: {
            id: 'Scr-00024',
            name: 'Brandon Smith',
            budget: 'S$ 4000',
        },
        codi: {
            id: 'Codi-000214',
            name: 'Brandon Smith',
            budget: 'S$3800',
        },
        profDivision: {
            designer: 'S$ 500',
            drafter: 'S$ 100',
            architect: 'S$ 200',
        },
        postedTime: '01/02/2020, 14:50',
        completionDate: '01/03/2020',
        outputFileStatus: 'Ongoing',
        revisionNote: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
    },
    {
        id: 2,
        title: 'Timber Project #2',
        state: 'Planned',
        status: 'Ongoing',
        shortDesc:
            'This card has supporting text below as a natural lead-in to additional content is a little bit longer',
        field: 'Civil Engineering',
        duration: '6 Weeks',
        executives: [
            {
                id: 1,
                fullName: 'Kimberly',
                shortName: 'Kimberly',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
            {
                id: 2,
                fullName: 'Madeleine',
                shortName: 'Madeleine',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
            {
                id: 3,
                fullName: 'Rebecca',
                shortName: 'Rebecca',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
            {
                id: 5,
                fullName: 'Rachel',
                shortName: 'Rachel',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
        ],
        files: [
            {
                name: 'Client-design.zip',
                size: '2.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Dashboard-design.jpg',
                size: '3.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Admin-design.jpg',
                size: '2.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Admin-bug-report.mp4',
                size: '7.05 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Codi-design.jpg',
                size: '6.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Dashboard-bug-report.mp4',
                size: '7.05 MB',
                postedOn: '2020-01-03, 15:03',
            },
        ],
    },
    {
        id: 3,
        title: 'Concrete Project #2',
        state: 'Planned',
        status: 'Past',
        shortDesc: 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid',
        progress: '40',
        field: 'Civil Engineering',
        duration: '6 Weeks',
        executives: [
            {
                id: 3,
                fullName: 'Rebecca',
                shortName: 'Rebecca',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
            {
                id: 5,
                fullName: 'Rachel',
                shortName: 'Rachel',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
                username: 'rachel',
                password: '123456',
                isInformed: false,
            },
        ],
        files: [
            {
                name: 'Client-design.zip',
                size: '2.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Dashboard-design.jpg',
                size: '3.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Admin-design.jpg',
                size: '2.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Admin-bug-report.mp4',
                size: '7.05 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Codi-design.jpg',
                size: '6.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
        ],
    },
    {
        id: 4,
        title: 'Steel Project #1',
        state: 'Planned',
        status: 'Ongoing',
        shortDesc: "Some quick example text to build on the card title and make up the bulk of the card's content",
        field: 'Civil Engineering',
        duration: '6 Weeks',
        executives: [
            {
                id: 1,
                fullName: 'Kimberly',
                shortName: 'Kimberly',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
            {
                id: 2,
                fullName: 'Madeleine',
                shortName: 'Madeleine',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
        ],
        files: [
            {
                name: 'Dashboard-design.jpg',
                size: '3.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Admin-design.jpg',
                size: '2.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Admin-bug-report.mp4',
                size: '7.05 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Codi-design.jpg',
                size: '6.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Dashboard-bug-report.mp4',
                size: '7.05 MB',
                postedOn: '2020-01-03, 15:03',
            },
        ],
    },
    {
        id: 5,
        title: 'Concrete Project #1',
        state: 'Planned',
        status: 'Past',
        shortDesc: "Some quick example text to build on the card title and make up the bulk of the card's content",
        field: 'Civil Engineering',
        duration: '6 Weeks',
        executives: [
            {
                id: 1,
                fullName: 'Kimberly',
                shortName: 'Kimberly',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
            {
                id: 2,
                fullName: 'Madeleine',
                shortName: 'Madeleine',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
            {
                id: 3,
                fullName: 'Rebecca',
                shortName: 'Rebecca',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
        ],
        files: [
            {
                name: 'Client-design.zip',
                size: '2.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Dashboard-design.jpg',
                size: '3.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Admin-design.jpg',
                size: '2.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Admin-bug-report.mp4',
                size: '7.05 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Codi-design.jpg',
                size: '6.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Dashboard-bug-report.mp4',
                size: '7.05 MB',
                postedOn: '2020-01-03, 15:03',
            },
        ],
    },
];



export { countries, paymentStatus, outputDeliverStatus, categories, projectStatus, skills, coordinators, projects};
