import React from 'react';
import { Row, Col } from 'reactstrap';

import logoImg from '../../assets/images/logo.png';
import orderBarcodeImg from '../../assets/images/barcode.png';

function Invoice() {
    const item = {
        customer: 'Cooper Hobson',
        notes:
            'Please find below a cost-breakdown for the recent work completed. Please make payment at your earliest convenience, and do not hesitate to contact me with any questions.',
        billed_date: 'Jan 17, 2020',
        bill_no: '123456',
        bill_barcode: orderBarcodeImg,
        projectTitle: 'Landscape Design',
        timeOfDelivery: 'Jan 06, 2020',
        numberOfRevisions: 2,
        items: [
            {
                id: 1,
                description: 'Payment adjustments (scope changes)',
                duration: null,
                rate: null,
                total: '$00.00',
            },
            {
                id: 2,
                description: 'Basic amount',
                duration: '6 months',
                rate: '$1000.00',
                total: '$6000.00',
            },
        ],
        sub_total: '$6000.00',
        tax: '$515.00',
        total: '$6515.00',
    };
    return (
        <div className="project-widget">
            <div className="clearfix">
                <div className="float-left">
                    <img src={logoImg} alt="logo" height="24" />
                </div>
                <div className="float-right">
                    <h4 className="m-0 d-print-none">Invoice</h4>
                </div>
            </div>

            <Row>
                <Col sm={6}>
                    <div className="float-left mt-3">
                        <p>
                            <b>Hello, {item.customer}</b>
                        </p>
                        <p className="text-muted font-13">{item.notes}</p>
                    </div>
                </Col>

                <Col sm={6}>
                    <div className="mt-3 float-sm-right">
                        <p className="font-13">
                            <strong>Billed Date: </strong> &nbsp;&nbsp;&nbsp; {item.billed_date}
                        </p>
                        <p className="font-13">
                            <strong>Bill No: </strong> <span className="float-right">#{item.bill_no}</span>
                        </p>
                    </div>
                </Col>
            </Row>

            <Row className="mt-3">
                <Col sm={8}>
                    <p>
                        <strong>Project Title :</strong>
                        <span className="ml-2">{item.projectTitle}</span>
                    </p>
                    <p>
                        <strong>Time of delivery :</strong>
                        <span className="ml-2">{item.timeOfDelivery}</span>
                    </p>
                    <p>
                        <strong>Number of revisions :</strong>
                        <span className="ml-2">{item.numberOfRevisions}</span>
                    </p>
                </Col>

                <Col sm={4}>
                    <div className="text-sm-right">
                        <img src={item.bill_barcode} alt="" className="img-fluid mr-2" />
                    </div>
                </Col>
            </Row>

            <Row>
                <Col>
                    <div className="table-responsive">
                        <table className="table bg-transparent mt-3 table-sm">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Description</th>
                                    <th>Duration</th>
                                    <th>Rate</th>
                                    <th className="text-right">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                {item.items.map((item, idx) => {
                                    return (
                                        <tr key={idx}>
                                            <td>{idx + 1}</td>
                                            <td>{item.description}</td>
                                            <td>{item.duration}</td>
                                            <td>{item.rate}</td>
                                            <td className="text-right">{item.total}</td>
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </table>
                    </div>
                </Col>
            </Row>

            <Row>
                <Col sm={6}>
                    <div className="clearfix pt-3">
                        <h6 className="text-muted">Notes:</h6>
                        <small>
                            All accounts are to be paid within 7 days from receipt of invoice. To be paid by cheque or
                            credit card or direct payment online. If account is not paid within 7 days the credits
                            details supplied as confirmation of work undertaken will be charged the agreed quoted fee
                            noted above.
                        </small>
                    </div>
                </Col>
                <Col sm={6}>
                    <div className="float-right mt-3 mt-sm-0">
                        <p>
                            <b>Sub-total:</b> <span className="float-right">{item.sub_total}</span>
                        </p>
                        <p>
                            <b>Tax:</b> <span className="float-right">{item.tax}</span>
                        </p>
                        <h3>{item.total} USD</h3>
                    </div>
                    <div className="clearfix"></div>
                </Col>
            </Row>
        </div>
    );
}

export default Invoice;
