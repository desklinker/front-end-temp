import React from 'react';
import PaginateTable from '../../components/PaginateTable';

const records = [
    {
        id: 1,
        projectID: 'P001',
        clientID: 'CLI-001',
        project: 'Project #1',
        clientBudget: '$300',
        screenerBudget: '$300',
        coordinatorBudget: '$100',
        projectCompletionDate: '2020-01-05',
        biddingStatus: 'completed',
        outputFileStatus: 'revisionsOnProgress',
        partialPayment: 'completed',
        paymentStatus: 'completed',
        notification: '4'
    },
    {
        id: 2,
        projectID: 'P002',
        clientID: 'CLI-002',
        project: 'Project #2',
        clientBudget: '$300',
        screenerBudget: '$300',
        coordinatorBudget: '$300',
        projectCompletionDate: '2020-01-05',
        biddingStatus: 'ongoing',
        outputFileStatus: 'pendingClientConfirmation',
        partialPayment: 'ongoing',
        paymentStatus: 'completed',
        notification: '4'
    },
    {
        id: 3,
        projectID: 'P003',
        clientID: 'CLI-003',
        project: 'Project #3',
        clientBudget: '$300',
        screenerBudget: '$300',
        coordinatorBudget: '$350',
        projectCompletionDate: '2020-01-08',
        biddingStatus: 'ongoing',
        outputFileStatus: 'pendingClientConfirmation',
        partialPayment: 'completed',
        paymentStatus: 'ongoing',
        notification: '4'
    },
    {
        id: 4,
        projectID: 'P004',
        clientID: 'CLI-004',
        project: 'Project #4',
        clientBudget: '$300',
        screenerBudget: '$300',
        coordinatorBudget: '$250',
        projectCompletionDate: '2020-02-05',
        biddingStatus: 'completed',
        outputFileStatus: 'revisionsOnProgress',
        partialPayment: 'completed',
        paymentStatus: 'ongoing',
        notification: '4'
    },
    {
        id: 5,
        projectID: 'P005',
        clientID: 'CLI-005',
        project: 'Project #5',
        clientBudget: '$300',
        screenerBudget: '$300',
        coordinatorBudget: '$200',
        projectCompletionDate: '2020-01-05',
        biddingStatus: 'ongoing',
        outputFileStatus: 'pendingClientConfirmation',
        partialPayment: 'ongoing',
        paymentStatus: 'ongoing',
        notification: '4'
    },
    {
        id: 6,
        projectID: 'P006',
        clientID: 'CLI-006',
        project: 'Project #6',
        clientBudget: '$300',
        screenerBudget: '$300',
        coordinatorBudget: '$250',
        projectCompletionDate: '2020-03-05',
        biddingStatus: 'ongoing',
        outputFileStatus: 'pendingClientConfirmation',
        partialPayment: 'ongoing',
        paymentStatus: 'completed',
        notification: '4'
    },
];

const columns = [
    {
        dataField: 'project',
        text: 'Project Name',
        sort: true,
    },
    {
        dataField: 'projectID',
        text: 'Project ID',
        sort: true,
    },
    {
        dataField: 'clientID',
        text: 'Client ID',
        sort: true,
    },
    {
        dataField: 'clientBudget',
        text: 'Client Budget',
        sort: true,
    },
    {
        dataField: 'screenerBudget',
        text: 'Screener Budget',
        sort: true,
    },
    {
        dataField: 'coordinatorBudget',
        text: 'Coordinator Budget',
        sort: true,
    },
    {
        dataField: 'projectCompletionDate',
        text: 'Project Completion Date',
        sort: true,
    },
    {
        dataField: 'outputFileStatus',
        text: 'Output File Status',
        sort: true,
        formatter: 'statusColumn',
    },
    {
        dataField: 'partialPayment',
        text: 'Partial Payment',
        sort: true,
        formatter: 'statusColumn',
    },
    {
        dataField: 'paymentStatus',
        text: 'Payment Status',
        sort: true,
        formatter: 'statusColumn',
    },
    {
        dataField: 'notification',
        text: 'Notification',
        sort: true,
        align: 'center',
        formatter: 'badgeColumn',
    },
];

function ListView() {
    return (
        <>
            <PaginateTable data={records} columns={columns} />
        </>
    );
}

export default ListView;
