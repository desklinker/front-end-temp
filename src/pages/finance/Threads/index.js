import React from 'react';
import { Col, Row } from 'reactstrap';
import Comments from './Comments';

function Threads({ onUserChange, title }) {
    return (
        <Row>
            <Col md={12} className="order-lg-2 order-xl-1">
                <Comments title={title} />
            </Col>
        </Row>
    );
}

export default Threads;
