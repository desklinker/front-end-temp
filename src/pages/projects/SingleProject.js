import React from 'react';
import { Row, Col, Card, CardBody, Button, Input, UncontrolledPopover, PopoverHeader, PopoverBody } from 'reactstrap';
import classNames from 'classnames';

import PageTitle from '../../components/PageTitle';
import Comments from './Comments';
import Files from './Files';

import './styles/projects.css';

import designImg from '../../assets/images/projects/design.jpg';
import GroupBidApply from './GroupBidApply';
import SubProjectsTable from './SubProjectsTable';
import { connect } from 'react-redux';

import ProjectPaymentsTable from './ProjectPaymentsTable';
import GroupBidsTable from './GroupBidsTable';
import BidsTable from './BidsTable';
import { CLIENT, COORDINATOR, PROFESSIONAL } from '../../constants';

class SingleProject extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      bidValue: 0,
      role: props.user.role,
    };
  }

  componentDidMount() {
    // this.getPathParam();
  }

  handleRange = (e) => {
    this.setState({ bidValue: e.target.value });
  };

  render() {
    // const toggle = () => this.setState({ modal: !this.state.modal });

    // let { id } = useParams();
    // console.log(id);

    const project = {
      title: 'Minton v3.0 - Redesign',
      mainProject: 'Main Project Name',
      shortDesc:
        'This card has supporting text below as a natural lead-in to additional content is a little bit longer',
      state: 'Bidding on process',
      totalTasks: 81,
      totalComments: 103,
      totalMembers: 6,
      startDate: '17 March 2018',
      startTime: '1:00 PM',
      duration: '45',
      totalBudget: '$800',
    };

    return (
      <React.Fragment>
        <PageTitle
          breadCrumbItems={[
            { label: 'Projects', path: '/apps/projects' },
            { label: 'Details', path: '/apps/projects/detail', active: true },
          ]}
          title={'Single Project Details'}
        />

        <Row>
          <Col md={8}>
            <Card className="d-block">
              <CardBody>
                <Row>
                  <Col md={6}>
                    <img className="project-img" src={designImg} alt="project-img" />
                  </Col>
                  <Col md={6}>
                    <h3 className="mt-0">{project.title}</h3>
                    <div
                      className={classNames(
                        'badge',
                        {
                          'badge-success': project.state === 'Completed',
                          'badge-info': project.state === 'Ongoing',
                          'badge-secondary': project.state === 'Bidding on process',
                        },
                        'mb-1'
                      )}>
                      {project.state}
                    </div>
                    {this.state.role === PROFESSIONAL && (
                      <p className="mt-0">
                        Coordinator Email: <span className="text-muted">codi1996@konet.com</span>
                      </p>
                    )}

                    {this.state.role === COORDINATOR && (
                      <div>
                        <Button size="sm" color="secondary" outline className="mr-1 mb-1">
                          Edit project
                        </Button>
                        <Button size="sm" color="success" outline className="mr-1 mb-1">
                          Coordinate project
                        </Button>
                        <Button size="sm" color="danger" outline className="mr-1 mb-1" id="AdminReportPop">
                          Report
                        </Button>
                        <Button size="sm" color="danger" outline className="mr-1 mb-1">
                          Terminate
                        </Button>
                        <Button size="sm" color="danger" outline className="mr-1 mb-1">
                          Start revision
                        </Button>

                        <UncontrolledPopover
                          placement="bottom"
                          trigger="legacy"
                          target="AdminReportPop"
                          style={{ width: '400px' }}>
                          <PopoverHeader>Report to Admin</PopoverHeader>
                          <PopoverBody>
                            <Row>
                              <Col md={12}>
                                <Input type="text" placeholder="Subject" className="mb-1" />
                              </Col>
                              <Col md={12}>
                                <Input type="textarea" placeholder="Description" className="mb-1" />
                              </Col>
                              <Col md={12} className="text-right">
                                <Button size="sm" color="danger" className="mb-1">
                                  Report
                                </Button>
                              </Col>
                            </Row>
                          </PopoverBody>
                        </UncontrolledPopover>
                      </div>
                    )}
                  </Col>
                  <Col md={12} className="mt-3">
                    <h5>{this.state.role === CLIENT ? 'Outline:' : 'Literature:'}</h5>

                    <p className="text-muted mb-2">
                      With supporting text below as a natural lead-in to additional contenposuere erat a ante.
                      Voluptates, illo, iste itaque voluptas corrupti ratione reprehenderit magni similique? Tempore,
                      quos delectus asperiores libero voluptas quod perferendis! Voluptate, quod illo rerum? Lorem ipsum
                      dolor sit amet.
                    </p>

                    <p className="text-muted mb-4">
                      Voluptates, illo, iste itaque voluptas corrupti ratione reprehenderit magni similique? Tempore,
                      quos delectus asperiores libero voluptas quod perferendis! Voluptate, quod illo rerum? Lorem ipsum
                      dolor sit amet. With supporting text below as a natural lead-in to additional contenposuere erat a
                      ante.
                    </p>
                  </Col>
                  {this.state.role !== CLIENT && (
                    <Col md={12} className="mt-1">
                      <h5>Synopsis:</h5>

                      <p className="text-muted mb-2">
                        With supporting text below as a natural lead-in to additional contenposuere erat a ante.
                        Voluptates, illo, iste itaque voluptas corrupti ratione reprehenderit magni similique? Tempore,
                        quos delectus asperiores libero voluptas quod perferendis! Voluptate, quod illo rerum? Lorem
                        ipsum dolor sit amet.
                      </p>

                      <p className="text-muted mb-4">
                        Voluptates, illo, iste itaque voluptas corrupti ratione reprehenderit magni similique? Tempore,
                        quos delectus asperiores libero voluptas quod perferendis! Voluptate, quod illo rerum? Lorem
                        ipsum dolor sit amet. With supporting text below as a natural lead-in to additional
                        contenposuere erat a ante.
                      </p>
                    </Col>
                  )}
                </Row>
                <Row>
                  <Col md={4}>
                    <div className="mb-4">
                      <h5>Start Date</h5>
                      <p>
                        {project.startDate} {/* <small className="text-muted">{project.startTime}</small> */}
                      </p>
                    </div>
                  </Col>
                  <Col md={4}>
                    <div className="mb-4">
                      <h5>Duration</h5>
                      <p>
                        {project.duration} <small className="text-muted">days</small>
                      </p>
                    </div>
                  </Col>
                  {this.state.role === COORDINATOR && (
                    <Col md={4}>
                      <div className="mb-4">
                        <h5>Budget</h5>
                        <p>{project.totalBudget}</p>
                      </div>
                    </Col>
                  )}
                </Row>
                {this.state.role !== CLIENT && (
                  <Row>
                    <Col md={12}>
                      <SubProjectsTable />
                    </Col>
                  </Row>
                )}

                {/* <TeamMembers /> */}
              </CardBody>
            </Card>

            {this.state.role !== CLIENT && (
              <>
                <Card>
                  <CardBody>
                    <GroupBidsTable />
                    <br />
                    <BidsTable />
                  </CardBody>
                </Card>

                <Comments />
              </>
            )}
          </Col>

          <Col md={4}>
            {this.state.role === PROFESSIONAL && <GroupBidApply />}
            <Files title="Project files" />
            <Files title="Output files" />
            {this.state.role === COORDINATOR && <ProjectPaymentsTable />}
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.Auth.user,
});

export default connect(mapStateToProps, null)(SingleProject);
