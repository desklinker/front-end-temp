// @flow
import React from 'react';
import { upperCase } from 'lodash/string';
import { Card, CardBody, Label, Row } from 'reactstrap';
import { extensionBgColor } from '../../helpers/utils';

function Files({ title, files, downloadUrl, removeFile, driveLink }) {
  return (
    <Card>
      <CardBody>
        <h5 className="card-title mb-3">{title}</h5>

        {files?.length > 0 ? (
          files.map((file, key) => {
            const fileName = file.filePath.split('/').pop();
            const extension = fileName.split('.').pop();
            const size = file.fileSize ? (parseFloat(file.fileSize) / (1024 * 1024)).toFixed(2) : 0.0;

            return (
              <Card className="mb-1 shadow-none border" key={`file_${key}`}>
                <div className="p-2">
                  <Row className="align-items-center">
                    <div className="col-auto">
                      <div className="avatar-sm">
                        <span className={`avatar-title rounded ${extensionBgColor(extension)}`}>
                          {upperCase(extension)}
                        </span>
                      </div>
                    </div>
                    <div className="col pl-0">
                      <div
                        onClick={() => downloadUrl(file.filePath)}
                        className="text-muted font-weight-bold cursor-pointer">
                        {fileName}
                      </div>
                      <p className="mb-0">{`${size} MB`}</p>
                    </div>
                    <div className="col-auto">
                      <span className="text-muted cursor-pointer" onClick={() => removeFile(file)}>
                        <i className="dripicons-trash"></i>
                      </span>
                      <span
                        onClick={() => downloadUrl(file.filePath, fileName)}
                        className="btn btn-link btn-lg text-muted">
                        <i className="dripicons-download"></i>
                      </span>
                    </div>
                  </Row>
                </div>
              </Card>
            );
          })
        ) : (
          <div className="text-muted"> There is no files</div>
        )}

        {driveLink && (
          <div className="mt-2">
            <Label>Drive Link</Label> <br />
            <a href="##" className="badge badge-pill badge-info" target="_blank">
              {driveLink}
            </a>
          </div>
        )}
      </CardBody>
    </Card>
  );
}

export default Files;
