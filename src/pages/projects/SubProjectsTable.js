// @flow
import React from 'react';
import { Row, Table, Col, FormGroup, Label, Input, Button } from 'reactstrap';
import Select from 'react-select';

import HyperDatepicker from '../../components/Datepicker';

class SubProjectsTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            skills: [
                { value: 'cad', label: 'CAD' },
                { value: 'architecture', label: 'Architecture' },
                { value: 'design', label: 'Design' },
            ],
            timeFrameOptions: [
                { value: 'hours', label: 'Hours' },
                { value: 'days', label: 'Days' },
                { value: 'months', label: 'Months' },
            ],
        };
    }
    render() {
        const subProjects = [
            { name: 'Sub Project 1', startDate: '02/05/2020', skill: 'CAD', duration: '15 days', budget: '$250' },
            { name: 'Sub Project 1', startDate: '02/05/2020', skill: 'CAD', duration: '15 days', budget: '$250' },
            { name: 'Sub Project 1', startDate: '02/05/2020', skill: 'CAD', duration: '15 days', budget: '$250' },
            { name: 'Sub Project 1', startDate: '02/05/2020', skill: 'CAD', duration: '15 days', budget: '$250' },
        ];

        return (
            <div>
                <h5 className="card-title mb-3">Sub projects</h5>

                <Row>
                    <Col md="4">
                        <FormGroup>
                            <Label className="text-muted">Sub project name</Label>
                            <Input type="text" name="sub-project-name" />
                        </FormGroup>
                    </Col>
                    <Col md="3">
                        <FormGroup>
                            <Label className="text-muted">Skill</Label>
                            <Select className="react-select" options={this.state.skills} />
                        </FormGroup>
                    </Col>
                    <Col md="2">
                        <FormGroup>
                            <Label className="text-muted">Budget %</Label>
                            <Input type="text" name="sub-project-name" />
                        </FormGroup>
                    </Col>
                    <Col md="3">
                        <FormGroup>
                            <Label className="text-muted">Start date</Label>
                            <HyperDatepicker />
                        </FormGroup>
                    </Col>
                    <Col md={3}>
                        <FormGroup>
                            <Label for="exampleNumber" className="text-muted">
                                Duration
                            </Label>
                            <Input type="text" name="bid-duration" id="bid-duration" placeholder="Duration" />
                        </FormGroup>
                    </Col>
                    <Col md={3} style={{ marginTop: '30px' }}>
                        <FormGroup>
                            <Select className="react-select" options={this.state.timeFrameOptions} />
                        </FormGroup>
                    </Col>
                    <Col md="3">
                        <Button className="secondary" style={{ marginTop: '30px' }}>
                            + Add sub project
                        </Button>
                    </Col>
                </Row>

                <Table className="mb-0" size="sm" bordered>
                    <thead className="thead-dark">
                        <tr>
                            <th>Sub Project</th>
                            <th className="text-center">Skill</th>
                            <th className="text-center">Start date</th>
                            <th className="text-center">Duration</th>
                            <th className="text-center">Budget</th>
                        </tr>
                    </thead>
                    <tbody>
                        {subProjects.map((sp, index) => {
                            return (
                                <tr key={index}>
                                    <td>
                                        <a href="/project/sub">{sp.name}</a>
                                    </td>
                                    <td className="text-center">{sp.skill}</td>
                                    <td className="text-center">{sp.startDate}</td>
                                    <td className="text-center">{sp.duration}</td>
                                    <td className="text-center">{sp.budget}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default SubProjectsTable;
