import { useCallback, useState, useEffect } from 'react';
import { loadUploadFiles, removeUploadFile } from '../../../../actions/project';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';

import { downloadBlob } from '../../../../helpers/utils';
import Storage from '@aws-amplify/storage';

export function useLoadUploadFiles({ item }) {
  const dispatch = useDispatch();
  const [files, setFiles] = useState(null);

  const onLoadUploadFiles = useCallback(async () => {
    try {
      const res = await dispatch(loadUploadFiles(item.pid));
      setFiles(res.uploads);
    } catch (e) {
      //
    }
  }, [dispatch, item.pid]);

  useEffect(() => {
    onLoadUploadFiles();
  }, [onLoadUploadFiles]);

  return { files, onLoadUploadFiles };
}

export function useDownloadUrl() {
  const downloadUrl = useCallback(async (fileKey, fileName) => {
    await Storage.get(fileKey, { expires: 10, download: true }).then((res) => downloadBlob(res.Body, fileName));
  }, []);

  return { downloadUrl };
}

export function useRemoveFile({ onLoadUploadFiles }) {
  const dispatch = useDispatch();
  let { id } = useParams();

  const removeFile = useCallback(
    async (file) => {
      await dispatch(removeUploadFile(id, file.puid));
      await Storage.remove(file.filePath)
        .then((result) => {
          onLoadUploadFiles();
        })
        .catch((err) => console.log(err));
    },
    [dispatch, id, onLoadUploadFiles]
  );

  return { removeFile };
}
