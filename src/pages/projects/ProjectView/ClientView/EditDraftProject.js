import React from 'react';
import { Row, Col, FormGroup } from 'reactstrap';
import ProjectInfoForm from '../../CreateProject/ProjectInfoForm';

function EditDraftProject({ isEdit, toggleEdit, ...props }) {
  return (
    <React.Fragment>
      <Row>
        <Col>
          <FormGroup>
            <button type="button" className="close" aria-label="Close" onClick={toggleEdit}>
              <span aria-hidden="true">&times;</span>
            </button>
          </FormGroup>

          <h3>Edit Project</h3>
        </Col>
      </Row>
      <ProjectInfoForm {...props} />
    </React.Fragment>
  );
}

export default EditDraftProject;
