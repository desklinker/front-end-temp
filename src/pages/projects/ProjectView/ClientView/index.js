import React from 'react';
import { Row, Col, Card, CardBody } from 'reactstrap';
import DraftView from './DraftView';
import Files from '../../Files';
import '../styles/projects.css';
import { useSelector } from 'react-redux';
import { useLoadUploadFiles, useRemoveFile, useDownloadUrl } from './hooks';
import { useUpdateProject, useRemoveProject } from '../../CreateProject/hooks';
import ProjectInfo from './ProjectInfo';

function ClientView({ item }) {
  const user = useSelector((state) => state.Auth.user);
  const { files, onLoadUploadFiles } = useLoadUploadFiles({ item });
  const { removeFile } = useRemoveFile({ onLoadUploadFiles });
  const { downloadUrl } = useDownloadUrl();
  const { onUpdateProject } = useUpdateProject({ user });
  const { onRemoveProject } = useRemoveProject();

  return (
    <>
      {item.projectStatus?.status === 'DRAFT' ? (
        <DraftView
          item={item}
          files={files}
          downloadUrl={downloadUrl}
          removeFile={removeFile}
          onSubmit={onUpdateProject}
          onRemove={onRemoveProject}
        />
      ) : (
        <Row>
          <Col md={8}>
            <Card className="d-block">
              <CardBody>
                <ProjectInfo item={item} />
              </CardBody>
            </Card>
          </Col>

          <Col md={4}>
            <Files
              title="Project files"
              files={files}
              downloadUrl={downloadUrl}
              removeFile={removeFile}
              driveLink={item.driveLink}
            />
            {item.projectStatus?.status !== 'PENDING_APPROVAL' && <Files title="Output files" />}
          </Col>
        </Row>
      )}
    </>
  );
}

export default ClientView;
