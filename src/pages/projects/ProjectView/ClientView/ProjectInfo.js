import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import moment from 'moment';
import get from 'lodash/get';
import { Row, Col, Button } from 'reactstrap';
import { startCase, camelCase } from 'lodash/string';
import { projectImage } from '../../../../helpers';

function ProjectInfo({ item, toggleEdit }) {
  const duration = useMemo(
    () => moment.unix(item.endDateTime).startOf('day').diff(moment.unix(item.startDateTime).startOf('day'), 'days'),
    [item]
  );

  return (
    <>
      <Row>
        <Col md={6}>
          <img className="project-img" src={projectImage(item.fieldId)} alt="project-img" />
        </Col>
        <Col md={6}>
          {item.projectStatus?.status === 'DRAFT' && (
            <Button className="close" aria-label="Close" onClick={toggleEdit}>
              <i className="mdi mdi-square-edit-outline"></i>
            </Button>
          )}

          <h3 className="mt-0">{item.title}</h3>
          <div
            className={classNames(
              'badge',
              {
                'badge-success': item.projectStatus?.status === 'Completed',
                'badge-info': item.projectStatus?.status === 'PENDING_APPROVAL',
                'badge-secondary': item.projectStatus?.status === 'DRAFT',
              },
              'mb-1'
            )}>
            {startCase(camelCase(item.projectStatus?.status))}
          </div>
        </Col>
      </Row>
      <Row>
        <Col md={12} className="mt-3">
          <h5>Outline</h5>
          <p className="text-muted mb-2">{item.overview}</p>
        </Col>
      </Row>
      <Row>
        <Col md={4}>
          <div className="mb-4">
            <h5>Category</h5>
            <p>{item.field?.name}</p>
          </div>
        </Col>
        <Col md={4}>
          <div className="mb-4">
            <h5>Start Date</h5>
            <p>{moment.unix(item.startDateTime).format('YYYY-MM-DD')}</p>
          </div>
        </Col>
        <Col md={4}>
          <div className="mb-4">
            <h5>Duration</h5>
            <p>
              {duration} <small className="text-muted">Days</small>
            </p>
          </div>
        </Col>
      </Row>
      <Row>
        <Col md={4}>
          <div className="mb-4">
            <h5>Expected Output</h5>
            <p>
              {item.expectedOutputs &&
                item.expectedOutputs.map((output, key) => (
                  <span className="badge badge-pill badge-light mr-1" key={key}>
                    {output.output}
                  </span>
                ))}
            </p>
          </div>
        </Col>
        <Col md={4}>
          <div className="mb-4">
            <h5>Budget</h5>
            <p>{`${item.budget} ${item.currency && item.currency.currency}`}</p>
          </div>
        </Col>
        <Col md={4}>
          <div className="mb-4">
            <h5>Team Members</h5>
            <p>
              {item.clientExecutives &&
                item.clientExecutives.map((executive, key) => (
                  <span className="badge badge-pill badge-light mr-1" key={key}>
                    {startCase(`${get(executive, 'user.firstName')} ${get(executive, 'user.lastName')}`)}
                  </span>
                ))}
            </p>
          </div>
        </Col>
      </Row>
    </>
  );
}

ProjectInfo.propTypes = {
  item: PropTypes.object.isRequired,
  toggleEdit: PropTypes.func,
};

export default ProjectInfo;
