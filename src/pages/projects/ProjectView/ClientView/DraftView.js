import React, { useState, useMemo, useEffect } from 'react';
import moment from 'moment';
import get from 'lodash/get';
import { map } from 'lodash/map';
import { Formik, Form } from 'formik';
import { useSelector } from 'react-redux';
import {
  Row,
  Col,
  Card,
  CardBody,
  UncontrolledButtonDropdown,
  Button,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Label,
  FormGroup,
  Badge,
} from 'reactstrap';
import Files from '../../Files';
import '../../styles/projects.css';
import FileUploader from '../../../../components/FileUploader';
import EditDraftProject from './EditDraftProject';
import Comments from './Comments';
import TextField from '../../../../components/formFields/TextField';
import { validationSchema, initialValues } from '../../CreateProject/utils';
import ProjectInfo from './ProjectInfo';

function DraftView({ item, files, downloadUrl, removeFile, onSubmit, onRemove }) {
  const [isEdit, setEdit] = useState(false);
  const toggleEdit = () => setEdit(!isEdit);

  const user = useSelector((state) => state.Auth.user);

  const handleSave = (values, actions) => {
    onSubmit(
      {
        ...values,
        clientId: user.userId,
        startDateTime: moment(values.startDateTime, 'YYYY-MM-DD').unix(),
        endDateTime: moment(values.startDateTime, 'YYYY-MM-DD').add(values.duration, 'd').unix(),
        fieldId: values.field?.id,
        projectExecutives: map(values.executives, 'id'),
        expectedOutputs: map(values.expectedOutputs, 'value'),
        projectLinks: map(values.projectLinks, 'id'),
        currencyId: values.currency?.id,
        projectExternalUploads: [values.driveLink],
      },
      actions
    );
    setEdit(false);
  };

  useEffect(() => {
    return () => {
      setEdit(false);
    };
  }, []);

  const formattedItem = useMemo(() => {
    return {
      title: item.title,
      budget: item.budget,
      currency: { id: item.currency?.cid, title: item.currency?.currency },
      startDateTime: moment.unix(item.startDateTime)._d,
      duration: moment
        .unix(item.endDateTime)
        .startOf('day')
        .diff(moment.unix(item.startDateTime).startOf('day'), 'days'),
      overview: item.overview,
      isQuotationRequired: item.isQuotationRequired,
      expectedOutputs: item.expectedOutputs
        ? item.expectedOutputs.map((output) => ({ label: output.output, value: output.output }))
        : [],
      field: { id: item.field?.fid, title: item.field?.name },
      files: [],
      executives: item.clientExecutives
        ? item.clientExecutives.map((executive) => ({ id: executive.ceid, title: get(executive, 'user.firstName') }))
        : [],
      projectLinks: [],
      isDraft: false,
      isLinked: false,
    };
  }, [item]);

  return (
    <React.Fragment>
      <Formik
        validateOnBlur={false}
        enableReinitialize
        initialValues={!item ? initialValues : formattedItem}
        validationSchema={validationSchema}
        onSubmit={handleSave}>
        {(props) => (
          <Form className="form-custom" onSubmit={props.handleSubmit}>
            <Row>
              <Col md={8}>
                <Card className="d-block">
                  <CardBody>
                    {!isEdit ? (
                      <ProjectInfo item={item} toggleEdit={toggleEdit} />
                    ) : (
                      <EditDraftProject toggleEdit={toggleEdit} {...props} isEdit={isEdit} />
                    )}
                  </CardBody>
                </Card>
              </Col>

              <Col md={4}>
                <Card>
                  <CardBody>
                    <FormGroup>
                      <Label>Attachments</Label>
                      <FileUploader
                        onFileUpload={(files) => {
                          props.setFieldValue('files', files);
                        }}
                        description={'Recommended files less than 250 Mb'}
                      />
                    </FormGroup>

                    <TextField
                      name="driveLink"
                      placeholder="Insert if you have any Drive attachments"
                      label="Drive Link"
                    />
                  </CardBody>
                </Card>
                <Files
                  title="Uploaded files"
                  files={files}
                  downloadUrl={downloadUrl}
                  removeFile={removeFile}
                  driveLink={item.driveLink}
                />
              </Col>
            </Row>

            <Row className="mb-3 border-top pt-2">
              <Col md={8}>
                <Card>
                  <CardBody>
                    <h3 className="mb-1">{item.title}</h3>
                    <div className="mb-2">
                      <Badge color="secondary">{item.status}</Badge>
                    </div>
                    <Comments />
                  </CardBody>
                </Card>
              </Col>
              <Col md={4} className="text-right">
                <Row className="mt-2">
                  <Col className="text-right">
                    <UncontrolledButtonDropdown>
                      <Button
                        id="removeButton"
                        color="danger"
                        type="button"
                        className="mr-2"
                        onClick={() => {
                          props.onRemove();
                        }}>
                        Delete Draft
                      </Button>

                      <Button
                        id="submitButton"
                        color="success"
                        type="button"
                        onClick={() => {
                          props.setFieldValue('isDraft', false);
                          props.handleSubmit();
                        }}>
                        Submit
                      </Button>
                      <DropdownToggle caret color="success" size="xs" />
                      <DropdownMenu>
                        <DropdownItem
                          id="draftButton"
                          onClick={() => {
                            props.setFieldValue('isDraft', true);
                            props.handleSubmit();
                          }}>
                          Save as draft
                        </DropdownItem>
                      </DropdownMenu>
                    </UncontrolledButtonDropdown>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Form>
        )}
      </Formik>
      <Row className="mb-3 pt-2"></Row>
    </React.Fragment>
  );
}

export default DraftView;
