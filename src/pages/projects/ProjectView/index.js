import React, { useMemo } from 'react';
import { connect } from 'react-redux';
import './styles/projects.css';
import PageTitle from '../../../components/PageTitle';
import ScreenerView from './ScreenerView';
import AdminView from './AdminView';
import ClientView from './ClientView';
import SubProject from '../SubProject';
import { projects as screeProjects } from '../../dashboards/ScreenerDashboard/data';
import { projects as adminProjects } from '../../dashboards/AdminDashboard/ListView/data';
import { projects as clientProjects } from '../../dashboards/ClientDashboard/data';
import { SCREENER, CLIENT, ADMIN } from '../../../constants';
import { useLoadItem } from './hooks';

function ProjectView({ user, match }) {
  const { item, onLoadItem } = useLoadItem({ match });

  //want to remove
  const project = useMemo(() => {
    if (match.params) {
      let projects;
      switch (user.role) {
        case SCREENER:
          projects = screeProjects;
          break;

        case CLIENT:
          projects = clientProjects;
          break;

        default:
          projects = adminProjects;
      }

      return projects.find((project) => project.id === parseInt(match.params.id));
    }
  }, [match.params, user.role]);

  if (!item) {
    return null;
  }

  return (
    <React.Fragment>
      <PageTitle
        breadCrumbItems={[
          { label: 'Projects', path: '/apps/projects' },
          { label: 'Details', path: '/apps/projects/detail', active: true },
        ]}
        title={item.title ? item.title : 'Single Project Details'}
      />

      {user.role === SCREENER ? (
        <ScreenerView formData={project} />
      ) : user.role === ADMIN ? (
        <AdminView />
      ) : user.role === CLIENT ? (
        <ClientView item={item} onLoadItem={onLoadItem} />
      ) : (
        <SubProject />
      )}
    </React.Fragment>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.Auth.user,
  };
};

export default connect(mapStateToProps)(ProjectView);
