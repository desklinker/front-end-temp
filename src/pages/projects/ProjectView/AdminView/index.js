import React, { useState } from 'react';
import { Col, Row } from 'reactstrap';
import ProjectDetailsWidget from '../../../dashboards/AdminDashboard/ProjectDetailsWidget';
import BidDetailsWidget from '../../../dashboards/AdminDashboard/BidDetailsWidget';
import DeliveryDetailsWidget from '../../../dashboards/AdminDashboard/DeliveryDetailsWidget';
import EditBidWidget from '../../../dashboards/AdminDashboard/EditBidWidget';
import EditDeliveryWidget from '../../../dashboards/AdminDashboard/EditDeliveryWidget';
import EditProjectWidget from '../../../dashboards/AdminDashboard/EditProjectWidget';

function AdminView() {
  const [formOpen, setFormOpen] = useState(null);

  const handleToggle = (value) => {
    setFormOpen(value);
  };

  return (
    <>
      <Row>
        <Col md={12}>
          {formOpen === 'client' && <EditProjectWidget editToggle={() => handleToggle(null)} />}
          <ProjectDetailsWidget isEdit={formOpen !== 'client'} toggle={() => handleToggle('client')} />
        </Col>
      </Row>
      <Row>
        <Col md={12}>
          {formOpen === 'bid' ? (
            <EditBidWidget editToggle={() => handleToggle(null)} />
          ) : (
            <BidDetailsWidget isEdit toggle={() => handleToggle('bid')} />
          )}
        </Col>
      </Row>
      <Row>
        <Col md={12}>
          {formOpen === 'delivery' ? (
            <EditDeliveryWidget editToggle={(e) => handleToggle(e)} />
          ) : (
            <DeliveryDetailsWidget isEdit toggle={() => handleToggle('delivery')} />
          )}
        </Col>
      </Row>
    </>
  );
}

export default AdminView;
