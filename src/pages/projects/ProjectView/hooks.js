import { useCallback, useState, useEffect } from 'react';
import { loadProject } from '../../../actions/project';
import { useDispatch } from 'react-redux';

export function useLoadItem({ match }) {
  const dispatch = useDispatch();
  const [item, setItem] = useState(null);

  const onLoadItem = useCallback(async () => {
    try {
      const res = await dispatch(loadProject(match.params.id));
      setItem(res);
    } catch (e) {
      //
    }
  }, [dispatch, match.params.id]);

  useEffect(() => {
    onLoadItem();
  }, [onLoadItem]);

  return { item, onLoadItem };
}


