// @flow
import React from 'react';
import { Card, CardBody, Table } from 'reactstrap';

class ProjectPaymentsTable extends React.Component {
    render() {
        const payments = [
            { payment: 'Phase 1', amount: '$100', status: 'Completed' },
            { payment: 'Phase 1', amount: '$100', status: 'Pending' },
            { payment: 'Phase 1', amount: '$150', status: 'Pending' },
        ];

        return (
            <Card>
                <CardBody>
                    <h5 className="card-title mb-3">Payments</h5>

                    <Table className="mb-0" size="sm" bordered>
                        <thead className="thead-dark">
                            <tr>
                                <th>Payment</th>
                                <th className="text-center">Amount</th>
                                <th className="text-center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            {payments.map((p, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{p.payment}</td>
                                        <td className="text-center">{p.amount}</td>
                                        <td className="text-center">{p.status}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </Table>
                </CardBody>
            </Card>
        );
    }
}

export default ProjectPaymentsTable;
