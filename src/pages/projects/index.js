import React from 'react';
import './styles/projects.css';
import { withRouter } from 'react-router';
import SubProject from './SubProject';
import SingleProject from './SingleProject';
import MultiProject from './MultiProject';

class ProjectPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            projectType: null,
        };
    }

    componentDidMount() {
        if (this.props.location.pathname === '/projects/sub') {
            this.setState({
                projectType: 'sub',
            });
        } else if (this.props.location.pathname === '/projects/single') {
            this.setState({
                projectType: 'single',
            });
        } else if (this.props.location.pathname === '/project/multi') {
            this.setState({
                projectType: 'multi',
            });
        }
    }

    render() {
        let renderProject;

        if (this.state.projectType === 'sub') {
            renderProject = <SubProject />;
        } else if (this.state.projectType === 'single') {
            renderProject = <SingleProject />;
        } else if (this.state.projectType === 'multi') {
            renderProject = <MultiProject />;
        } else {
            // 404
        }

        return <div>{renderProject}</div>;
    }
}

export default withRouter(ProjectPage);
