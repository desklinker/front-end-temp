import React, { Component } from 'react';
import moment from 'moment';
import {
    Row,
    Col,
    Card,
    CardBody,
    FormGroup,
    Label,
    Button,
    CustomInput,
    ButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
} from 'reactstrap';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import Flatpickr from 'react-flatpickr';
import Select from 'react-select';

import FileUploader from '../../components/FileUploader';

class CreateProjectForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            startDate: new Date(),
            endDate: new Date(),
            timeFrameOptions: [
                { value: 'hours', label: 'Hours' },
                { value: 'days', label: 'Days' },
                { value: 'months', label: 'Months' },
            ],
            currencyOptions: [
                { value: 'USD', label: 'USD' },
                { value: 'SGD', label: 'SGD' },
                { value: '£', label: '£' },
                { value: 'AUD', label: 'AUD' },
            ],
            isRequested: false,
            isLinked: false,
            isOpen: false,
        };

        this.handleValidSubmit = this.handleValidSubmit.bind(this);
        this.updateValues = this.updateValues.bind(this);
    }

    /**
     * Update values
     */
    updateValues = (field, fieldValue) => {
        const state = { ...this.state.state };
        state[field] = fieldValue;
        this.setState(state);
    };

    /**
     * Handle the form submission
     */
    handleValidSubmit = (e, values) => {
        console.log({ ...values, ...this.state });
    };

    render() {
        return (
            <React.Fragment>
                <Card>
                    <CardBody>
                        <Row>
                            <Col>
                                <AvForm onValidSubmit={this.handleValidSubmit}>
                                    <Row>
                                        <Col>
                                            <h3>Create Project</h3>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <AvField
                                                name="name"
                                                label="Name"
                                                required
                                                placeholder="Enter project name"
                                            />

                                            <FormGroup>
                                                <Label>Project category</Label>
                                                <Select
                                                    className="react-select"
                                                    classNamePrefix="react-select"
                                                    onChange={(members) => {
                                                        this.updateValues('members', members);
                                                    }}
                                                    options={[
                                                        { value: '1', label: 'Civil Engineering' },
                                                        { value: '2', label: 'Mechanical Engineering' },
                                                        { value: '3', label: 'Computer Science' },
                                                    ]}
                                                />
                                            </FormGroup>

                                            <AvField
                                                name="overview"
                                                label="Overview"
                                                placeholder="Enter some brief about project.."
                                                type="textarea"
                                                rows="5"
                                            />

                                            <Row>
                                                <Col md={6}>
                                                    <AvField
                                                        name="budget"
                                                        label="Budget"
                                                        required
                                                        placeholder="Enter project budget in selected currency"
                                                        type="number"
                                                        min={0}
                                                    />
                                                </Col>
                                                <Col md={6}>
                                                    <FormGroup>
                                                        <Label>Currency</Label>
                                                        <Select
                                                            className="react-select"
                                                            options={this.state.currencyOptions}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={12}>
                                                    <FormGroup>
                                                        <CustomInput
                                                            id="isRequested"
                                                            label="Request for quotation"
                                                            type="checkbox"
                                                            className="text-secondary"
                                                        />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={3}>
                                                    <FormGroup>
                                                        <AvField
                                                            label="Duration"
                                                            name="project-duration"
                                                            id="project-duration"
                                                            placeholder="Duration"
                                                        />
                                                    </FormGroup>
                                                </Col>
                                                <Col md={3}>
                                                    <FormGroup style={{ marginTop: '31px' }}>
                                                        <Select
                                                            className="react-select"
                                                            options={this.state.timeFrameOptions}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                                <Col md={6}>
                                                    <FormGroup>
                                                        <Label>Start Date</Label>
                                                        <Flatpickr
                                                            className="form-control"
                                                            value={this.state.startDate}
                                                            onChange={(date) => {
                                                                this.updateValues('startDate', date);
                                                            }}
                                                            options={{ minDate: moment().format('YYYY-MM-DD') }}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={12}>
                                                    <FormGroup>
                                                        <Label>Expected output</Label>
                                                        <Select
                                                            isMulti={true}
                                                            className="react-select"
                                                            classNamePrefix="react-select"
                                                            onChange={(members) => {
                                                                this.updateValues('members', members);
                                                            }}
                                                            options={[
                                                                { value: '1', label: 'CAD' },
                                                                { value: '2', label: 'Design' },
                                                                { value: '3', label: 'Architecture' },
                                                                { value: '4', label: 'Survey' },
                                                            ]}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={12}>
                                                    <FormGroup>
                                                        <CustomInput
                                                            id="isLinked"
                                                            label="Linked to previous projects"
                                                            type="checkbox"
                                                            className="text-secondary"
                                                            onChange={(e) =>
                                                                this.setState({ isLinked: e.target.checked })
                                                            }
                                                        />
                                                    </FormGroup>
                                                </Col>
                                            </Row>

                                            {this.state.isLinked && (
                                                <Row>
                                                    <Col md={12}>
                                                        <FormGroup>
                                                            <Select
                                                                isMulti={true}
                                                                className="react-select"
                                                                classNamePrefix="react-select"
                                                                onChange={(projects) => {
                                                                    this.updateValues('previous-projects', projects);
                                                                }}
                                                                options={[
                                                                    { value: '1', label: 'Project #1' },
                                                                    { value: '2', label: 'Project #2' },
                                                                    { value: '3', label: 'Project #3' },
                                                                ]}
                                                            />
                                                        </FormGroup>
                                                    </Col>
                                                </Row>
                                            )}

                                            <Row>
                                                <Col md={12}>
                                                    <FormGroup>
                                                        <Label>Team Members</Label>
                                                        <Select
                                                            isMulti={true}
                                                            className="react-select"
                                                            classNamePrefix="react-select"
                                                            onChange={(members) => {
                                                                this.updateValues('members', members);
                                                            }}
                                                            options={[
                                                                { value: '1', label: 'Mary Scott' },
                                                                { value: '2', label: 'Shreyu N' },
                                                                { value: '3', label: 'Greeva Y' },
                                                            ]}
                                                        />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col>
                                            <FormGroup>
                                                <button
                                                    type="button"
                                                    className="close"
                                                    aria-label="Close"
                                                    onClick={this.props.toggleForm}>
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </FormGroup>

                                            <FormGroup>
                                                <Label>Attachments</Label>
                                                <p className="text-muted font-14">Recommended files less than 250 Mb</p>
                                                <FileUploader
                                                    onFileUpload={(files) => {
                                                        this.updateValues('avatar', files);
                                                    }}
                                                />
                                            </FormGroup>

                                            <AvField
                                                name="driveLink"
                                                label="Drive Link"
                                                placeholder="Insert if you have any Drive attachments"
                                                type="text"
                                            />
                                            <Row className="mt-2">
                                                <Col className="text-right">
                                                    <ButtonDropdown
                                                        isOpen={this.state.isOpen}
                                                        toggle={() => this.setState({ isOpen: !this.state.isOpen })}>
                                                        <Button id="caret" color="success" type="submit">
                                                            Submit
                                                        </Button>
                                                        <DropdownToggle caret color="success" size="xs" />
                                                        <DropdownMenu>
                                                            <DropdownItem>Save as draft</DropdownItem>
                                                        </DropdownMenu>
                                                    </ButtonDropdown>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </AvForm>
                            </Col>
                        </Row>
                    </CardBody>
                </Card>
            </React.Fragment>
        );
    }
}

export default CreateProjectForm;
