// @flow
import React from 'react';
import { Table, Button, UncontrolledPopover, PopoverHeader, PopoverBody } from 'reactstrap';

class GroupBidsTable extends React.Component {
    render() {
        const bids = [
            { id: 'GBID:3245', value: '$ 150', duration: '15 days' },
            { id: 'GBID:3245', value: '$ 150', duration: '15 days' },
            { id: 'GBID:3245', value: '$ 150', duration: '15 days' },
            { id: 'GBID:3245', value: '$ 150', duration: '15 days' },
        ];

        return (
            <div>
                <h5 className="card-title mb-3">Group Bids</h5>

                <Table className="mb-0 bg-transparent" size="sm" bordered>
                    <thead className="thead-dark">
                        <tr>
                            <th className="text-center">Bid ID</th>
                            <th className="text-center">Bid Value</th>
                            <th className="text-center">Duration</th>
                            <th className="text-center">View details</th>
                            <th className="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {bids.map((b, index) => {
                            return (
                                <tr key={index}>
                                    <td className="text-center">{b.id}</td>
                                    <td className="text-center">{b.value}</td>
                                    <td className="text-center">{b.duration}</td>
                                    <td className="text-center">
                                        <Button size="sm" color="success">
                                            Accept
                                        </Button>
                                    </td>
                                    <td className="text-center">
                                        <Button size="sm" color="secondary" id="BidPop1" outline>
                                            Details
                                        </Button>
                                        <UncontrolledPopover placement="right" target="BidPop1" trigger="focus">
                                            <PopoverHeader>Group Bid Details</PopoverHeader>
                                            <PopoverBody>
                                                Project 1 - BID:2132 <br />
                                                Project 1 - BID:2132 <br />
                                                Project 1 - BID:2132 <br />
                                            </PopoverBody>
                                        </UncontrolledPopover>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default GroupBidsTable;
