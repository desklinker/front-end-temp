// @flow
import React from 'react';
import { Table, Button } from 'reactstrap';

class BidsTable extends React.Component {
    render() {
        const bids = [
            { id: 'BID:3245', pid: 'PID:4251', value: '$ 150', duration: '15 days' },
            { id: 'BID:3245', pid: 'PID:4251', value: '$ 150', duration: '15 days' },
            { id: 'BID:3245', pid: 'PID:4251', value: '$ 150', duration: '15 days' },
            { id: 'BID:3245', pid: 'PID:4251', value: '$ 150', duration: '15 days' },
        ];

        return (
            <div>
                <h5 className="card-title mb-3">Individual Bids</h5>

                <Table className="mb-0 bg-transparent" size="sm" bordered>
                    <thead className="thead-dark">
                        <tr>
                            <th className="text-center">Bid ID</th>
                            <th className="text-center">Professional ID</th>
                            <th className="text-center">Bid Value</th>
                            <th className="text-center">Duration</th>
                            <th className="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {bids.map((b, index) => {
                            return (
                                <tr key={index}>
                                    <td className="text-center">{b.id}</td>
                                    <td className="text-center">
                                        <a href="/profile">{b.pid}</a>
                                    </td>
                                    <td className="text-center">{b.value}</td>
                                    <td className="text-center">{b.duration}</td>
                                    <td className="text-center">
                                        <Button size="sm" color="success">
                                            Accept
                                        </Button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default BidsTable;
