import React from 'react';
import { CardBody, Card, Row, Col, FormGroup, Label,  Button } from 'reactstrap';
import Select from 'react-select';
import 'nouislider/distribute/nouislider.css';

class GroupBidApply extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bidValue: '0',
            timeFrameOptions: [],
        };
    }

    componentDidMount() {
        this.setState({
            timeFrameOptions: [
                { value: 'prof:2312', label: 'prof:2312' },
                { value: 'prof:1420', label: 'prof:1420' },
                { value: 'prof:9871', label: 'prof:9871' },
            ],
            bidValue: 250,
        });
    }

    handleRange = (e) => {
        this.setState({ bidValue: e.target.value });
    };

    onSlide = (value) => {
        this.setState({ bidValue: value[0].toFixed(0) });
    };

    render() {
        return (
            <Card>
                <CardBody>
                    <h5 className="card-title mb-3">Apply for a Group Bid</h5>
                    <Row>
                        <Col>
                            <FormGroup row>
                                <Label sm={5}>Sub Project Name 1</Label>
                                <Col sm={7}>
                                    <Select
                                        className="react-select"
                                        placeholder="Select your colleague's bid"
                                        options={this.state.timeFrameOptions}
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={5}>Sub Project Name 2</Label>
                                <Col sm={7}>
                                    <Select
                                        className="react-select"
                                        placeholder="Select your colleague's bid"
                                        options={this.state.timeFrameOptions}
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={5}>Sub Project Name 3</Label>
                                <Col sm={7}>
                                    <Select
                                        className="react-select"
                                        placeholder="Select your colleague's bid"
                                        options={this.state.timeFrameOptions}
                                    />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label sm={5}>Sub Project Name 4</Label>
                                <Col sm={7}>
                                    <Select
                                        className="react-select"
                                        placeholder="Select your colleague's bid"
                                        options={this.state.timeFrameOptions}
                                    />
                                </Col>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Row form>
                                <Col md={12} className="text-right mt-2">
                                    <Button color="success">Send Request</Button>
                                    <Button color="success" className="ml-3">
                                        Apply
                                    </Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </CardBody>
            </Card>
        );
    }
}

export default GroupBidApply;
