import React from 'react';
import { CardBody, Card, Row, Col, FormGroup, Label, Input, Button } from 'reactstrap';
import Select from 'react-select';
import Nouislider from 'nouislider-react';
import 'nouislider/distribute/nouislider.css';

class BidApply extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bidValue: '0',
            timeFrameOptions: [],
        };
    }

    componentDidMount() {
        this.setState({
            timeFrameOptions: [
                { value: 'hours', label: 'Hours' },
                { value: 'days', label: 'Days' },
                { value: 'months', label: 'Months' },
            ],
            bidValue: 250,
        });
    }

    handleRange = (e) => {
        this.setState({ bidValue: e.target.value });
    };

    onSlide = (value) => {
        this.setState({ bidValue: value[0].toFixed(0) });
    };

    render() {
        return (
            <Card>
                <CardBody>
                    <h5 className="card-title mb-3">Apply for a Bid</h5>
                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="exampleCustomRange">Bid value - $ {this.state.bidValue}</Label>
                                <Nouislider
                                    range={{ min: 250, max: 1000 }}
                                    start={[250]}
                                    step={25}
                                    connect
                                    onSlide={(render, handle, value, un, percent) => this.onSlide(value)}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Row form>
                                <Col md={12}>
                                    <Label for="exampleNumber">Duration</Label>
                                </Col>
                                <Col md={6}>
                                    <FormGroup>
                                        <Input
                                            type="text"
                                            name="bid-duration"
                                            id="bid-duration"
                                            placeholder="Duration"
                                        />
                                    </FormGroup>
                                </Col>
                                <Col md={6}>
                                    <FormGroup>
                                        <Select className="react-select" options={this.state.timeFrameOptions} />
                                    </FormGroup>
                                </Col>
                                <Col md={6}>
                                    <a href="/" className="text-muted">
                                        Apply for a group bid
                                    </a>
                                </Col>
                                <Col md={6} className="text-right">
                                    <Button color="success">Apply</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </CardBody>
            </Card>
        );
    }
}

export default BidApply;
