import React from 'react';
import { Formik, Form } from 'formik';
import { object, string, number } from 'yup';
import TextField from '../../../components/formFields/TextField';
import {
  Row,
  Col,
  Button,
  CustomInput,
  Modal,
  ModalBody,
  ModalHeader,
  FormGroup,
  Input,
  FormFeedback,
  Label,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from 'reactstrap';
import PhoneField from '../../../components/formFields/PhoneField';
import { useCheckAvailability } from '../../auth/Register/hooks';
import lowerCase from 'lodash-es/lowerCase';

const validationSchema = object().shape({
  firstName: string().required('First name field is required'),
  lastName: string().required('Last name field is required'),
  username: string().required('Username field is required'),
  email: string().email().required('Email field is required'),
  phoneNumber: string().required('Phone number field is required'),
  password: string().required('Password is required'),
  company: object().shape({
    name: string().required('Company name field is required'),
    phoneNumber: number('Company phone number field must be number')
      .typeError('Company phone number field must be number')
      .required('Company phone number field is required'),
  }),
});

function TeamMemberForm({ isOpen, user, toggle, onSubmit }) {
  const { onCheckAvailability, emailExist, usernameExist, phoneExist } = useCheckAvailability();

  const handleCheckAvailability = (type, value) => {
    onCheckAvailability(type, value);
  };

  const validate = (values, props) => {
    const errors = {};

    if (usernameExist) {
      errors.username = 'Already the username exist';
    }
    if (emailExist) {
      errors.email = 'Already the email exist';
    }
    if (phoneExist) {
      errors.phoneNumber = 'Already the phone number exist';
    }

    return errors;
  };

  const handleSubmit = (values, actions) => {
    onSubmit(
      {
        ...values,
        username: `${lowerCase(user.firstName)}_${values.username}`,
      },
      actions
    );
  };

  return (
    <Modal isOpen={isOpen} toggle={toggle} className="modal-dialog-centered" size="lg">
      <ModalHeader toggle={toggle}>Create Team Member</ModalHeader>
      <ModalBody>
        <Formik
          validateOnBlur={false}
          enableReinitialize
          initialValues={{
            firstName: '',
            lastName: '',
            phoneNumber: '',
            email: '',
            username: '',
            password: '',
            company: {
              name: '',
              email: '',
              phoneNumber: '',
              streetAddress1: '',
              streetAddress2: '',
            },
          }}
          validate={validate}
          validationSchema={validationSchema}
          onSubmit={handleSubmit}>
          {({ errors, values, handleChange, handleFocus, touched, isSubmitting, setFieldValue }) => (
            <Form className="form-custom">
              <Row form>
                <Col sm={6}>
                  <TextField name="firstName" placeholder="Enter first name" label="First Name" />
                </Col>
                <Col sm={6}>
                  <TextField name="lastName" placeholder="Enter last name" label="Last Name" />
                </Col>
              </Row>

              <Row form>
                <Col sm={6}>
                  <PhoneField name="phoneNumber" label="Phone Number" onCheckAvailability={handleCheckAvailability} />
                </Col>
                <Col sm={6}>
                  <FormGroup>
                    <label htmlFor="email">Email</label>
                    <Input
                      name="email"
                      type="email"
                      placeholder="Email address"
                      onChange={(e) => {
                        handleChange(e);
                        handleCheckAvailability('email', e.target.value);
                      }}
                      value={values.email}
                    />
                    {errors.email && touched.email && <FormFeedback className="d-block">{errors.email}</FormFeedback>}
                  </FormGroup>
                </Col>
              </Row>

              <hr />
              <Row form>
                <Col sm={6}>
                  <TextField name="company.name" type="text" placeholder="Enter company name" label="Company Name" />
                </Col>
                <Col sm={6}>
                  <TextField
                    name="company.phoneNumber"
                    type="text"
                    placeholder="Enter company phone number"
                    label="Company Phone Number"
                  />
                </Col>
              </Row>

              <Row form>
                <Col md={12}>
                  <TextField name="company.address" label="Company Address" placeholder="Enter company address" />
                </Col>
              </Row>
              <hr />

              <Row>
                <Col md={6}>
                  <FormGroup>
                    <Label>Username</Label>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>{`${user.firstName}_`}</InputGroupText>
                      </InputGroupAddon>
                      <Input
                        name="username"
                        type="text"
                        placeholder="Username"
                        onChange={(e) => {
                          handleChange(e);
                          handleCheckAvailability('username', e.target.value);
                        }}
                        onFocus={handleFocus}
                        value={values.username}
                      />
                    </InputGroup>

                    {errors.username && touched.username && (
                      <FormFeedback className="d-block">{errors.username}</FormFeedback>
                    )}
                  </FormGroup>
                </Col>

                <Col md={6}>
                  <TextField
                    name="password"
                    label="Password"
                    type="password"
                    placeholder="Enter password"
                    passGenerator
                  />
                </Col>
              </Row>

              <Row>
                <Col md={12}>
                  <CustomInput
                    id="isInformed"
                    label="Inform executive username and password through whatsapp"
                    type="checkbox"
                    className="text-secondary"
                  />
                </Col>
              </Row>

              <Row className="mt-2">
                <Col className="text-right">
                  <Button color="primary" type="submit">
                    {isSubmitting ? 'Loading...' : 'Submit'}
                  </Button>
                </Col>
              </Row>
            </Form>
          )}
        </Formik>
      </ModalBody>
    </Modal>
  );
}

export default TeamMemberForm;
