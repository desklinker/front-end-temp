import React, { useEffect, useCallback } from 'react';
import { Collapse } from 'reactstrap';
import { useDispatch } from 'react-redux';
import ProjectForm from './ProjectForm';
import { uploadFile } from '../../../redux/actions';
import { DASHBOARD } from '../../../constants/routes';
import { useStoreProject } from './hooks';
import { modalService } from '../../../services/modal.service';

function CreatProject({ toggleProgressbar, history, setShow, isShow, user, isSubmitting, onLoad }) {
  const { onStoreProject } = useStoreProject({ user, onLoad });

  const dispatch = useDispatch();
  const handleUploadFile = useCallback(() => dispatch(uploadFile(50, {})), [dispatch]);

  useEffect(() => {
    history.listen((location) => {
      if (location.pathname && location.pathname !== DASHBOARD && isSubmitting) {
        handleUploadFile();
      }
    });
  }, [history, handleUploadFile, isSubmitting]);

  const toggleProjectForm = () => {
    if (!isSubmitting) {
      modalService.setModal({
        header: 'Cofirm to close',
        text: 'Are you sure want to close this project form?',
        onConfirm: () => setShow(false),
      });
    }
  };

  const handleProjectSubmit = (values, actions) => {
    onStoreProject(values, actions);
    setShow(false);
  };

  return (
    <>
      <Collapse isOpen={isShow}>
        <ProjectForm
          toggle={toggleProjectForm}
          toggleProgressbar={toggleProgressbar}
          user={user}
          onSubmit={handleProjectSubmit}
        />
      </Collapse>
    </>
  );
}
export default CreatProject;
