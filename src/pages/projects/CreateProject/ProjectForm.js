import React from 'react';
import moment from 'moment';

import { Formik, Form } from 'formik';
import {
  Row,
  Col,
  Card,
  CardBody,
  FormGroup,
  Label,
  Button,
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';
import FileUploader from '../../../components/FileUploader';
import TextField from '../../../components/formFields/TextField';
import { map } from 'lodash';
import ProjectInfoForm from './ProjectInfoForm';
import { validationSchema, initialValues } from './utils';

function ProjectForm({ user, onSubmit, ...props }) {
  const handleSave = (values, actions) => {
    onSubmit(
      {
        ...values,
        clientId: user.userId,
        startDateTime: moment(values.startDateTime, 'YYYY-MM-DD').unix(),
        endDateTime: moment(values.startDateTime, 'YYYY-MM-DD').add(values.duration, 'd').unix(),
        fieldId: values.field?.id,
        projectExecutives: map(values.executives, 'id'),
        expectedOutputs: map(values.expectedOutputs, 'value'),
        projectLinks: map(values.projectLinks, 'id'),
        currencyId: values.currency?.id,
        projectExternalUploads: [values.driveLink],
      },
      actions
    );
    props.toggleProgressbar();
  };

  return (
    <React.Fragment>
      <Card>
        <CardBody>
          <Row>
            <Col>
              <FormGroup>
                <button type="button" className="close" aria-label="Close" onClick={props.toggle}>
                  <span aria-hidden="true">&times;</span>
                </button>
              </FormGroup>

              <h3>Create Project</h3>
            </Col>
          </Row>
          <Row>
            <Col>
              <Formik
                validateOnBlur={false}
                enableReinitialize
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={handleSave}>
                {(props) => {
                  return (
                    <Form className="form-custom" onSubmit={props.handleSubmit}>
                      <Row>
                        <Col>
                          <ProjectInfoForm {...props} />
                        </Col>

                        <Col>
                          <FormGroup>
                            <Label>Attachments</Label>
                            <FileUploader
                              onFileUpload={(files) => {
                                props.setFieldValue('files', files);
                              }}
                              description={'Recommended files less than 250 Mb'}
                            />
                          </FormGroup>

                          <TextField
                            name="driveLink"
                            placeholder="Insert if you have any Drive attachments"
                            label="Drive Link"
                          />

                          <Row className="mt-2">
                            <Col className="text-right project-submit">
                              <UncontrolledButtonDropdown>
                                <Button
                                  id="caret"
                                  color="success"
                                  type="button"
                                  onClick={() => {
                                    props.setFieldValue('isDraft', false);
                                    props.handleSubmit();
                                  }}>
                                  Submit
                                </Button>
                                <DropdownToggle caret color="success" size="xs" />
                                <DropdownMenu>
                                  <DropdownItem
                                    id="secondButton"
                                    onClick={() => {
                                      props.setFieldValue('isDraft', true);
                                      props.handleSubmit();
                                    }}>
                                    Save as draft
                                  </DropdownItem>
                                </DropdownMenu>
                              </UncontrolledButtonDropdown>
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                    </Form>
                  );
                }}
              </Formik>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </React.Fragment>
  );
}

export default ProjectForm;
