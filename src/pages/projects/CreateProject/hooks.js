import { useState, useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { CognitoUserPool, CognitoUserAttribute } from 'amazon-cognito-identity-js';
import Storage from '@aws-amplify/storage';
import { useHistory, useParams } from 'react-router-dom';

import { create } from '../../../actions/executives';
import { region, userPoolId, poolData } from '../../../config/env';
import { storeProject, updateProject, removeProject } from '../../../actions/project';
import { queryStringify } from '../../../helpers';
import { DASHBOARD, PROJECT_VIEW } from '../../../constants/routes';

const aws = require('aws-sdk');
const userPool = new CognitoUserPool(poolData);
const cognitoIdServiceProvider = new aws.CognitoIdentityServiceProvider({
  apiVersion: '2016-04-18',
  region: region,
});

//register new executive
export function useRegisterExecutive(user) {
  const [error, setError] = useState(null);
  const dispatch = useDispatch();
  const { userId } = user;

  const onRegisterExecutive = useCallback(
    async (values, actions) => {
      actions.setSubmitting(true);
      const { username, email, password, phoneNumber, groupName } = values;
      let attributeList = [];

      let dataEmail = {
        Name: 'email',
        Value: email,
      };

      let dataPhoneNumber = {
        Name: 'phone_number',
        Value: phoneNumber,
      };

      const params = {
        UserPoolId: userPoolId,
        Username: username,
      };

      const userAttibuteParams = {
        ...params,
        UserAttributes: [
          {
            Name: 'email_verified',
            Value: 'true',
          },
          {
            Name: 'phone_number_verified',
            Value: 'true',
          },
        ],
      };

      let attributeEmail = new CognitoUserAttribute(dataEmail);
      let attributePhoneNumber = new CognitoUserAttribute(dataPhoneNumber);

      attributeList.push(attributeEmail);
      attributeList.push(attributePhoneNumber);

      try {
        // Create new executive in cognito user pool with confirmation
        await userPool.signUp(username, password, attributeList, null, async (err, result) => {
          if (err) {
            alert(err.message || JSON.stringify(err));
            return;
          }

          await cognitoIdServiceProvider.adminConfirmSignUp(params, async (err, data) => {
            if (err) {
              setError(err.message);
              return;
            }
            await cognitoIdServiceProvider.adminUpdateUserAttributes(userAttibuteParams, async (err, result) => {
              if (err) {
                setError(err.message);
                return;
              }
              await postConfirmationTrigger(username, groupName);
            });
          });
        });

        //Create new executive in app database
        await dispatch(create(userId, values));
      } catch (err) {
        actions.setFieldError('general', err.message);
      }

      actions.setSubmitting(false);
    },
    [dispatch, userId]
  );

  return { onRegisterExecutive, error };
}

//create new project
export function useStoreProject({ user, onLoad }) {
  const dispatch = useDispatch();
  const { userId } = user;

  const onStoreProject = useCallback(
    async (values, actions) => {
      actions.setSubmitting(true);

      try {
        //upload files to s3
        const projectUploads = await uploadFiles(values, userId);
        const query = queryStringify({ isDraft: values.isDraft });

        //dispatch store api
        const res = await dispatch(
          storeProject(
            {
              ...values,
              projectUploads: projectUploads,
            },
            query
          )
        );

        //load dashboard projects
        if (res) onLoad();
        actions.setSubmitting(false);
      } catch (e) {
        actions.setErrors(e.errors);
        actions.setSubmitting(false);
      }
    },
    [dispatch, onLoad, userId]
  );
  return { onStoreProject };
}

//update project
export function useUpdateProject({ user }) {
  let history = useHistory();
  let { id } = useParams();

  const dispatch = useDispatch();
  const { userId } = user;

  const onUpdateProject = useCallback(
    async (values, actions) => {
      actions.setSubmitting(true);

      try {
        //upload project files to s3
        const projectUploads = await uploadFiles(values, userId);
        const query = queryStringify({ isDraft: values.isDraft });

        //dispatch update api
        const res = await dispatch(
          updateProject(
            id,
            {
              ...values,
              projectUploads: projectUploads,
            },
            query
          )
        );

        //load dashboard projects
        if (res) {
          history.push('/temp');
          history.push(PROJECT_VIEW.replace(':id', id));
        }

        actions.setSubmitting(false);
      } catch (e) {
        actions.setErrors(e.errors);
        actions.setSubmitting(false);
      }
    },
    [dispatch, history, id, userId]
  );
  return { onUpdateProject };
}

//cognito post confirm
function postConfirmationTrigger(username, groupName) {
  const params = {
    GroupName: groupName,
    UserPoolId: userPoolId,
    Username: username,
  };

  cognitoIdServiceProvider.adminAddUserToGroup(params).promise();
}

const uploadFiles = async (values, userId) => {
  let projectUploads = [];

  if (values.files && values.files.length > 0) {
    await Promise.all(
      values.files.map(async (file) => {
        const { key } = await Storage.put(`projects/${file.name}`, file, {
          contentType: file.type,
        });
        projectUploads.push({
          filePath: key,
          fileSize: file.size,
          fileTypeExtension: file.type,
          userId: userId,
        });
      })
    );
  }

  return projectUploads;
};

export function useRemoveProject() {
  let history = useHistory();
  let { id } = useParams();
  const dispatch = useDispatch();

  const onRemoveProject = useCallback(async () => {
    try {
      await dispatch(removeProject(id));
      history.push(DASHBOARD);
    } catch (e) {
      //
    }
  }, [dispatch, history, id]);

  return { onRemoveProject };
}
