import React, { useState } from 'react';
import Flatpickr from 'react-flatpickr';
import { useSelector } from 'react-redux';

import TextField from '../../../components/formFields/TextField';
import Dropdown from '../../../components/formFields/Dropdown';

import { useRegisterExecutive } from './hooks';
import {
  Row,
  Col,
  FormGroup,
  Label,
  Button,
  InputGroup,
  CustomInput,
  Input,
  InputGroupText,
  FormFeedback,
  InputGroupAddon,
} from 'reactstrap';
import { roles } from '../../../constants/roles';
import { configMgtUrl, projectMgtUrl, userMgtUrl } from '../../../config/env';
import TeamMemberForm from './TeamMemberForm';

function ProjectInfoForm(props) {
  const user = useSelector((state) => state.Auth.user);
  const [isFormOpen, setFormOpen] = useState(false);
  const { onRegisterExecutive } = useRegisterExecutive(user);

  const toggleForm = () => {
    setFormOpen(!isFormOpen);
  };

  const handleExecutiveSubmit = (values, actions) => {
    onRegisterExecutive(
      {
        ...values,
        phoneNumber: `+${values.phoneNumber}`,
        userRoleId: roles.executive?.id,
        groupName: roles.executive?.value,
      },
      actions
    );
    toggleForm();
  };

  const { errors, values, handleChange, setFieldValue, touched, handleFocus } = props;
  return (
    <>
      <TextField name="title" label="Project Name" placeholder="Enter project name" />
      <Dropdown
        label="Project category"
        name="field"
        placeholder="Select..."
        mgtUrl={configMgtUrl}
        url="app-configs/fields"
        resource="fields"
        listKey="fid"
        listLabel="name"
        clearField={() => setFieldValue('expectedOutputs', '')}
      />
      <TextField
        name="overview"
        type="textarea"
        label="Overview"
        placeholder="Enter some brief about project.."
        rows="5"
      />

      {!values.isQuotationRequired && (
        <Row>
          <Col md={6}>
            <TextField
              name="budget"
              type="number"
              label="Budget"
              step={10}
              min={0}
              placeholder="Enter project budget in selected currency"
            />
          </Col>
          <Col md={6}>
            <Dropdown
              label="Currency"
              name="currency"
              placeholder="Select..."
              mgtUrl={configMgtUrl}
              url="app-configs/currencies"
              resource="currencies"
              listKey="cid"
              listLabel="currency"
            />
          </Col>
        </Row>
      )}

      <Row>
        <Col md={12}>
          <FormGroup>
            <CustomInput
              className="text-muted font-weight-normal"
              type="checkbox"
              id="isQuotationRequired"
              label="Request for quotation"
              checked={values.isQuotationRequired}
              onChange={() => {
                setFieldValue('isQuotationRequired', !values.isQuotationRequired);
              }}
              inline
            />
          </FormGroup>
        </Col>
      </Row>

      <Row>
        <Col md={6}>
          <FormGroup>
            <Label>Start Date</Label>
            <Flatpickr
              className="form-control bg-white"
              value={values.startDateTime}
              placeholder="Start Date"
              onChange={(date) => {
                setFieldValue('startDateTime', date[0]);
              }}
              options={{ dateFormat: 'Y-m-d', minDate: 'today' }}
            />
            {touched.startDateTime && errors.startDateTime && (
              <FormFeedback className="d-block">{errors.startDateTime}</FormFeedback>
            )}
          </FormGroup>
        </Col>
        <Col md={6}>
          <FormGroup>
            <Label>Duration</Label>
            <InputGroup>
              <Input
                name="duration"
                placeholder="Enter duration in days"
                value={values.duration}
                onFocus={handleFocus}
                onChange={handleChange}
              />
              <InputGroupAddon addonType="append">
                <InputGroupText>Days</InputGroupText>
              </InputGroupAddon>
            </InputGroup>
            {touched.duration && errors.duration && <FormFeedback className="d-block">{errors.duration}</FormFeedback>}
          </FormGroup>
        </Col>
      </Row>

      <Row>
        <Col md={12}>
          <Dropdown
            isMulti
            label="Expected output"
            name="expectedOutputs"
            placeholder="Select..."
            mgtUrl={projectMgtUrl}
            url="projects/expectedOutputs/fields"
            resource="expectedOutputs"
            listKey="output"
            listLabel="output"
            relation="fieldId"
            relationId={values.field?.id}
            isDisabled={!values.field?.id}
            isCreatable
          />
        </Col>
      </Row>

      <Row>
        <Col md={12}>
          <FormGroup>
            <CustomInput
              id="isLinked"
              label="Linked to previous projects"
              type="checkbox"
              className="text-secondary"
              onChange={(e) => setFieldValue('isLinked', e.target.checked)}
            />
          </FormGroup>
        </Col>
      </Row>

      {values.isLinked && (
        <Row>
          <Col md={12}>
            <Dropdown
              isMulti
              name="projectLinks"
              placeholder="Select..."
              mgtUrl={projectMgtUrl}
              resource="projectList"
              listKey="PID"
              listLabel="title"
              relation="clientId"
              relationId={user?.userId}
              url={`projects/list`}
            />
          </Col>
        </Row>
      )}

      <Row>
        <Col sm={11}>
          <Dropdown
            isMulti
            label="Team Members"
            name="executives"
            placeholder="Select..."
            mgtUrl={userMgtUrl}
            resource="executives"
            listKey="ceid"
            listLabel="user.firstName"
            url={`clients/${user?.userId}/executives`}
          />
        </Col>
        <Col sm={1} className="d-flex align-items-end">
          <FormGroup>
            <Button color="secondary" className="btn-icon" size="md" onClick={toggleForm}>
              <span className="font-16">
                <i className="mdi mdi-plus"></i>
              </span>
            </Button>
          </FormGroup>
        </Col>
      </Row>

      {isFormOpen && (
        <TeamMemberForm isOpen={isFormOpen} toggle={toggleForm} onSubmit={handleExecutiveSubmit} user={user} />
      )}
    </>
  );
}

export default ProjectInfoForm;
