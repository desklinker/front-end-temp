import { object, string, array, number } from 'yup';

//project validation schema 
export const validationSchema = object().shape({
    title: string().required('Project name field is required'),
    field: string().required('Field selection is required'),
    expectedOutputs: array().required('Expected output field is required').nullable(),
    duration: number('Duration field must be number')
      .typeError('Duration field must be number')
      .positive('Please enter duration greater than 0')
      .required('Duration field is required'),
    budget: number().when('isQuotationRequired', {
      is: false,
      then: number('Budget field must be number')
        .required('Budget field is required')
        .positive('Please enter budget greater than 0')
        .typeError('Budget field must be number'),
    }),
    currency: object().when('isQuotationRequired', {
      is: false,
      then: object().required('Currency field is required').nullable(),
    }),
    startDateTime: string().required('Start date field is required'),
  });
  
  //project initialValues
  export const initialValues = {
    title: '',
    budget: '',
    currency: { id: 1, title: 'USD' },
    startDateTime: new Date(),
    overview: '',
    duration: '',
    isQuotationRequired: false,
    expectedOutputs: [],
    field: '',
    files: [],
    executives: [],
    projectLinks: [],
    isDraft: false,
    isLinked: false,
  }