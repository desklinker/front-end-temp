import React from 'react';
import { Row, Col, Button, Card, CardBody } from 'reactstrap';

import cad from '../../assets/images/projects/cad.jpg';
import design from '../../assets/images/projects/design.jpg';
import design2 from '../../assets/images/projects/design2.jpg';
import architecture from '../../assets/images/projects/architecture.jpg';

import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

import './styles/myProjects.css';
import ListView from './ListView';
import PageTitle from '../../components/PageTitle';
import TableView from './TableView';

class MyProjectsPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            projectView: 'list',
        };
    }

    handleViewButtonClick = (e) => {
        this.setState({ projectView: e.target.id }, () => {
            console.log(this.state.projectView);
        });
    };

    render() {
        const plannedProjects = [
            {
                id: 1,
                title: 'Ubold v3.0 - Redesign',
                state: 'Planned',
                image: cad,
                shortDesc: 'With supporting text below as a natural lead in to additional contenposuere erat a ante',
                totalTasks: null,
                totalComments: null,
                totalMembers: null,
                progress: '50',
                field: 'Civil Engineering',
                skill: 'CAD',
                minBid: '$250',
                maxBid: '$500',
                duration: '6 Weeks',
            },
            {
                id: 2,
                title: 'Minton v3.0 - Redesign',
                state: 'Planned',
                image: design,
                shortDesc:
                    'This card has supporting text below as a natural lead in to additional content is a little bit longer',
                totalTasks: null,
                totalComments: null,
                totalMembers: null,
                progress: '30',
                field: 'Civil Engineering',
                skill: 'Design',
                minBid: '$250',
                maxBid: '$500',
                duration: '6 Weeks',
            },
            {
                id: 3,
                title: 'Hyper v2.1 - Angular and Django',
                state: 'Planned',
                image: design2,
                shortDesc:
                    'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid',
                totalTasks: null,
                totalComments: null,
                totalMembers: null,
                progress: '40',
                field: 'Civil Engineering',
                skill: 'CAD',
                minBid: '$250',
                maxBid: '$500',
                duration: '6 Weeks',
            },
            {
                id: 4,
                title: 'Hyper v2.1 - React, Webpack',
                state: 'Planned',
                image: architecture,
                shortDesc:
                    "Some quick example text to build on the card title and make up the bulk of the card's content",
                totalTasks: null,
                totalComments: null,
                totalMembers: null,
                progress: '70',
                field: 'Civil Engineering',
                skill: 'Design',
                minBid: '$250',
                maxBid: '$500',
                duration: '6 Weeks',
            },
            {
                id: 5,
                title: 'Hyper v2.1 - React, Webpack',
                state: 'Planned',
                image: architecture,
                shortDesc:
                    "Some quick example text to build on the card title and make up the bulk of the card's content",
                totalTasks: null,
                totalComments: null,
                totalMembers: null,
                progress: '90',
                field: 'Civil Engineering',
                skill: 'Design',
                minBid: '$250',
                maxBid: '$500',
                duration: '6 Weeks',
            },
        ];

        const projects = [
            {
                id: 1,
                title: 'Ubold v3.0 - Redesign',
                state: 'Ongoing',
                image: cad,
                shortDesc: 'With supporting text below as a natural lead in to additional contenposuere erat a ante',
                totalTasks: null,
                totalComments: null,
                totalMembers: null,
                progress: '50',
                field: 'Civil Engineering',
                skill: 'CAD',
                minBid: '$250',
                maxBid: '$500',
                duration: '6 Weeks',
            },
            {
                id: 2,
                title: 'Minton v3.0 - Redesign',
                state: 'Ongoing',
                image: design,
                shortDesc:
                    'This card has supporting text below as a natural lead in to additional content is a little bit longer',
                totalTasks: null,
                totalComments: null,
                totalMembers: null,
                progress: '30',
                field: 'Civil Engineering',
                skill: 'Design',
                minBid: '$250',
                maxBid: '$500',
                duration: '6 Weeks',
            },
            {
                id: 3,
                title: 'Hyper v2.1 - Angular and Django',
                state: 'Ongoing',
                image: design2,
                shortDesc:
                    'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid',
                totalTasks: null,
                totalComments: null,
                totalMembers: null,
                progress: '40',
                field: 'Civil Engineering',
                skill: 'CAD',
                minBid: '$250',
                maxBid: '$500',
                duration: '6 Weeks',
            },
            {
                id: 4,
                title: 'Hyper v2.1 - React, Webpack',
                state: 'Ongoing',
                image: architecture,
                shortDesc:
                    "Some quick example text to build on the card title and make up the bulk of the card's content",
                totalTasks: null,
                totalComments: null,
                totalMembers: null,
                progress: '70',
                field: 'Civil Engineering',
                skill: 'Design',
                minBid: '$250',
                maxBid: '$500',
                duration: '6 Weeks',
            },
            {
                id: 5,
                title: 'Hyper v2.1 - React, Webpack',
                state: 'Ongoing',
                image: architecture,
                shortDesc:
                    "Some quick example text to build on the card title and make up the bulk of the card's content",
                totalTasks: null,
                totalComments: null,
                totalMembers: null,
                progress: '90',
                field: 'Civil Engineering',
                skill: 'Design',
                minBid: '$250',
                maxBid: '$500',
                duration: '6 Weeks',
            },
        ];

        const projectsCompleted = [
            {
                id: 1,
                title: 'Ubold v3.0 - Redesign',
                state: 'Completed',
                image: cad,
                shortDesc: 'With supporting text below as a natural lead in to additional contenposuere erat a ante',
                totalTasks: null,
                totalComments: null,
                totalMembers: null,
                progress: '100',
                field: 'Civil Engineering',
                skill: 'CAD',
                minBid: '$250',
                maxBid: '$500',
                duration: '6 Weeks',
            },
            {
                id: 2,
                title: 'Minton v3.0 - Redesign',
                state: 'Completed',
                image: design,
                shortDesc:
                    'This card has supporting text below as a natural lead in to additional content is a little bit longer',
                totalTasks: null,
                totalComments: null,
                totalMembers: null,
                progress: '100',
                field: 'Civil Engineering',
                skill: 'Design',
                minBid: '$250',
                maxBid: '$500',
                duration: '6 Weeks',
            },
            {
                id: 3,
                title: 'Hyper v2.1 - Angular and Django',
                state: 'Completed',
                image: design2,
                shortDesc:
                    'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid',
                totalTasks: null,
                totalComments: null,
                totalMembers: null,
                progress: '100',
                field: 'Civil Engineering',
                skill: 'CAD',
                minBid: '$250',
                maxBid: '$500',
                duration: '6 Weeks',
            },
            {
                id: 4,
                title: 'Hyper v2.1 - React, Webpack',
                state: 'Completed',
                image: architecture,
                shortDesc:
                    "Some quick example text to build on the card title and make up the bulk of the card's content",
                totalTasks: null,
                totalComments: null,
                totalMembers: null,
                progress: '100',
                field: 'Civil Engineering',
                skill: 'Design',
                minBid: '$250',
                maxBid: '$500',
                duration: '6 Weeks',
            },
            {
                id: 5,
                title: 'Hyper v2.1 - React, Webpack',
                state: 'Completed',
                image: architecture,
                shortDesc:
                    "Some quick example text to build on the card title and make up the bulk of the card's content",
                totalTasks: null,
                totalComments: null,
                totalMembers: null,
                progress: '100',
                field: 'Civil Engineering',
                skill: 'Design',
                minBid: '$250',
                maxBid: '$500',
                duration: '6 Weeks',
            },
        ];
        return (
            <React.Fragment>
                <PageTitle
                    breadCrumbItems={[
                        { label: 'My Projects', path: '/my-projects' },
                        { label: 'List', path: '/apps/projects', active: true },
                    ]}
                    title={'Projects'}
                />
                <Row className="mb-2">
                    <Col sm={4}></Col>
                    <Col sm={8}>
                        <div className="text-sm-right switch-project-view">
                            <div className="btn-group mb-3 ml-2 d-none d-sm-inline-block">
                                <Button
                                    color={this.state.projectView === 'list' ? 'secondary' : 'link'}
                                    onClick={this.handleViewButtonClick}>
                                    <i className="dripicons-view-apps" id="list"></i>
                                </Button>
                            </div>
                            <div className="btn-group mb-3 d-none d-sm-inline-block">
                                <Button
                                    color={this.state.projectView === 'table' ? 'secondary' : 'text-muted'}
                                    onClick={this.handleViewButtonClick}>
                                    <i className="dripicons-checklist" id="table"></i>
                                </Button>
                            </div>
                        </div>
                    </Col>
                </Row>
                {this.state.projectView === 'list' ? (
                    <ListView
                        projects={projects}
                        projectsCompleted={projectsCompleted}
                        plannedProjects={plannedProjects}
                    />
                ) : (
                    <Card>
                        <CardBody>
                            <TableView projects={projects} filters={true} />
                        </CardBody>
                    </Card>
                )}
            </React.Fragment>
        );
    }
}

export default MyProjectsPage;
