// @flow
import React from 'react';
import { Row, Col } from 'reactstrap';

import SingleProject from '../../components/SingleProject';

import './styles/myProjects.css';
import ReactOwlCarousel from 'react-owl-carousel2';

class ListView extends React.Component {
    constructor(props) {
        super(props);
        this.state = { counter: 0 };
    }

    render() {
        const projects = this.props.projects;
        const plannedProjects = this.props.plannedProjects;
        const projectsCompleted = this.props.projectsCompleted;

        const options = {
            // items: 6,
            rewind: true,
            responsiveClass: "true",
            responsive: {
                0: { items: 1, nav: true },
                768: { items: 2, nav: true },
                1000: { items: 4, nav: true },
                1500: { items: 4, nav: true },
            },
            navText: ['<i class="uil-angle-left" ></i>', '<i class="uil-angle-right" ></i>'],
        };

        return (
            <React.Fragment>
                <Row className="mb-2">
                    <Col md={12}>
                        <div className="page-title-box">
                            <h4>Bidding on process</h4>
                        </div>
                    </Col>
                    <ReactOwlCarousel options={options}>
                        {plannedProjects.map((project, i) => {
                            return (
                                <Col md={12} xl={12} key={'proj-' + project.id} className="d-flex">
                                    <SingleProject project={project} />
                                </Col>
                            );
                        })}
                    </ReactOwlCarousel>
                </Row>
                <Row className="mb-2">
                    <Col md={12}>
                        <div className="page-title-box">
                            <h4>Ongoing</h4>
                        </div>
                    </Col>
                    <ReactOwlCarousel options={options}>
                        {projects.map((project, i) => {
                            return (
                                <Col md={12} xl={12} key={'proj-' + project.id} className="d-flex">
                                    <SingleProject project={project} />
                                </Col>
                            );
                        })}
                    </ReactOwlCarousel>
                </Row>
                <Row className="mb-2">
                    <Col md={12}>
                        <div className="page-title-box">
                            <h4>Completed</h4>
                        </div>
                    </Col>
                    <ReactOwlCarousel options={options}>
                        {projectsCompleted.map((project, i) => {
                            return (
                                <Col md={12} xl={12} key={'proj-' + project.id}>
                                    <SingleProject project={project} />
                                </Col>
                            );
                        })}
                    </ReactOwlCarousel>
                </Row>
            </React.Fragment>
        );
    }
}

export default ListView;
