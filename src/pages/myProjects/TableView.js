import React from 'react';
import { Row, Col, ButtonGroup, Button } from 'reactstrap';

import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

import './styles/myProjects.css';
import BootstrapTable from 'react-bootstrap-table-next';
import Select from 'react-select';

// const ListView = (props) => {
class TableView extends React.Component {
    constructor(props) {
        super(props);
        this.state = { counter: 0 };
    }

    render() {
        const projects = this.props.projects;

        const columns = [
            {
                dataField: 'projectTitle',
                text: 'Project Title',
                sort: true,
            },
            {
                dataField: 'date',
                text: 'Date',
                sort: true,
            },
            {
                dataField: 'skill',
                text: 'Skill',
                sort: false,
            },
            {
                dataField: 'bid',
                text: 'Bid',
                sort: true,
            },
            {
                dataField: 'status',
                text: 'Status',
                sort: true,
            },
            {
                dataField: 'payment',
                text: 'Payment',
                sort: true,
            },
        ];

        const defaultSorted = [
            {
                dataField: 'date',
                order: 'asc',
            },
        ];

        const records = [];

        projects.map((project, index) =>
            records.push({
                id: project.id,
                projectTitle: project.title,
                date: '21/03/2020',
                skill: project.skill,
                bid: '$800',
                status: project.state,
                payment: 'pending',
            })
        );

        return (
            <React.Fragment>
                {this.props.filters && (
                    <Row className="mb-2">
                        <Col md={6}></Col>
                        <Col md={4} className="text-right">
                            <ButtonGroup>
                                <Button color="light">
                                    <i className="uil-signal-alt-3"></i>
                                </Button>
                                <Button color="light">
                                    <i className="uil-constructor"></i>
                                </Button>
                                <Button color="light">
                                    <i className="uil-check-circle"></i>
                                </Button>
                            </ButtonGroup>
                        </Col>
                        <Col md={2}>
                            <Select
                                className="react-select"
                                options={[{ value: 'cad', label: 'CAD' }]}
                                placeholder="Skill"
                            />
                        </Col>
                    </Row>
                )}
                <BootstrapTable
                    bootstrap4
                    keyField="id"
                    data={records}
                    columns={columns}
                    defaultSorted={defaultSorted}
                    // pagination={paginationFactory(paginationOptions)}
                    wrapperClasses="table-responsive"
                />
            </React.Fragment>
        );
    }
}

export default TableView;
