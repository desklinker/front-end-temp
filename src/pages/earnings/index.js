import React, { Component } from 'react';
import { Row, Col, Card, CardBody } from 'reactstrap';
import PageTitle from '../../components/PageTitle';
import EarningsTable from './EarningsTable';


class Earnings extends Component {
    render() {
        return (
            <React.Fragment>
                <PageTitle
                    breadCrumbItems={[
                        { label: 'Pages', path: '/pages/invoice' },
                        { label: 'Earnings', active: true },
                    ]}
                    title={'Earnings'}
                />

                <Row className="mb-2">
                    <Col md={3}>
                        <Card className="tilebox-one">
                            <CardBody>
                                <Row>
                                    <Col xs={7}>
                                        <h3>Total Earnings</h3>
                                    </Col>
                                    <Col xs={5} className="text-right text-muted ">
                                        <h3>$2500</h3>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col md={3}>
                        <Card className="tilebox-one">
                            <CardBody>
                                <Row>
                                    <Col xs={7}>
                                        <h3>Pending</h3>
                                    </Col>
                                    <Col xs={5} className="text-right text-muted ">
                                        <h3>$600</h3>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card className="tilebox-one">
                            <CardBody>
                                <EarningsTable />
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

export default Earnings;
