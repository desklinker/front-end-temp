import React from 'react';
import { Button, UncontrolledPopover, PopoverHeader, PopoverBody, Row, Col, Input } from 'reactstrap';
import BootstrapTable from 'react-bootstrap-table-next';

class EarningsTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            reportSubject: '',
            reportDesc: '',
        };
    }
    render() {
        const records = [
            {
                id: 1,
                projectName: 'Design of a Steel building',
                completionDate: '13/04/2020',
                amount: '$800',
                status: 'Completed',
            },
            {
                id: 2,
                projectName: 'Design of a Steel building',
                completionDate: '13/04/2020',
                amount: '$800',
                status: 'Pending',
            },
            {
                id: 3,
                projectName: 'Design of a Steel building',
                completionDate: '13/04/2020',
                amount: '$800',
                status: 'Pending',
            },
            {
                id: 4,
                projectName: 'Design of a Steel building',
                completionDate: '13/04/2020',
                amount: '$800',
                status: 'Completed',
            },
            {
                id: 5,
                projectName: 'Design of a Steel building',
                completionDate: '13/04/2020',
                amount: '$800',
                status: 'Completed',
            },
        ];

        function reportBtnFormatter(cell, row, rowIndex, formatExtraData) {
            return (
                <i
                    className="uil-exclamation-triangle font-15"
                    id="AdminReportPop"
                    style={{ position: 'inherit', color: '#888', opacity: '1', cursor: 'pointer' }}></i>
            );
        }

        const columns = [
            {
                dataField: 'id',
                text: '#',
                sort: false,
                align: 'center',
                headerAlign: 'center',
            },
            {
                dataField: 'projectName',
                text: 'Project Name',
                sort: false,
            },
            {
                dataField: 'completionDate',
                text: 'Completed on',
                sort: true,
                align: 'center',
                headerAlign: 'center',
            },
            {
                dataField: 'amount',
                text: 'Amount',
                sort: true,
                align: 'center',
                headerAlign: 'center',
            },
            {
                dataField: 'status',
                text: 'Payment Status',
                sort: true,
                align: 'center',
                headerAlign: 'center',
            },
            {
                dataField: 'edit',
                text: '',
                formatter: reportBtnFormatter,
                sort: false,
                align: 'center',
                headerAlign: 'center',
            },
        ];

        return (
            <div>
                <BootstrapTable
                    bootstrap4
                    keyField="id"
                    data={records}
                    columns={columns}
                    // defaultSorted={defaultSorted}
                    // pagination={paginationFactory(paginationOptions)}
                    wrapperClasses="table-responsive"
                />
                <UncontrolledPopover
                    placement="bottom"
                    trigger="legacy"
                    target="AdminReportPop"
                    style={{ width: '400px' }}>
                    <PopoverHeader>Report to Admin</PopoverHeader>
                    <PopoverBody>
                        <Row>
                            <Col md={12}>
                                <Input
                                    type="text"
                                    placeholder="Subject"
                                    className="mb-1"
                                    onChange={(e) => this.setState({ reportSubject: e.target.value })}
                                    value={this.state.reportSubject}
                                />
                            </Col>
                            <Col md={12}>
                                <Input
                                    type="textarea"
                                    placeholder="Description"
                                    className="mb-1"
                                    onChange={(e) => this.setState({ reportDesc: e.target.value })}
                                    value={this.state.reportDesc}
                                />
                            </Col>
                            <Col md={12} className="text-right">
                                <Button size="sm" color="secondary" className="mb-1" outline>
                                    Send
                                </Button>
                            </Col>
                        </Row>
                    </PopoverBody>
                </UncontrolledPopover>
            </div>
        );
    }
}

export default EarningsTable;
