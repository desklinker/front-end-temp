import React, { useState } from 'react';
import { Button, ModalHeader, ModalBody, ModalFooter, Modal } from 'reactstrap';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import ConfirmEdit from './ConfirmEdit';

function EditPersonalDetails(props) {
    const [isOpen, setOpen] = useState(false);

    const handleValidSubmit = (e, values) => {
        setOpen(true);
        //
    };

    const { formData, isEdit, setEdit } = props;
    const { lastName, firstName, role, phone, country, email } = formData;

    const defaultValues = {
        lastName: lastName,
        firstName: firstName,
        role: role,
        phone: phone,
        email: email,
        country: country,
    };

    return (
        <React.Fragment>
            <Modal isOpen={isEdit} size="md" fade={false} className="modal-dialog-centered">
                <ModalHeader>Edit Phone Number</ModalHeader>
                <AvForm onValidSubmit={handleValidSubmit} model={isEdit ? defaultValues : {}}>
                    <ModalBody>
                        <AvField name="phone" label="Phone Number" placeholder="Enter phone number" />
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={() => setEdit(false)}>
                            Cancel
                        </Button>
                        <Button
                            type="submit"
                            color="primary"
                            onClick={() => {
                                setOpen(true);
                            }}>
                            Update
                        </Button>
                    </ModalFooter>
                </AvForm>
            </Modal>

            <ConfirmEdit isOpen={isOpen} setOpen={setOpen} setEdit={setEdit} />
        </React.Fragment>
    );
}

export default EditPersonalDetails;
