import React from 'react';
import { Card, CardBody, Col, Row } from 'reactstrap';
import profileImg from '../../../../assets/images/users/avatar-1.jpg';

function ViewPersonalDetails({ setEdit, data }) {
    return (
        <Card className="w-100">
            <CardBody className="profile-user-box">
                <Row>
                    <Col lg={9} md={12}>
                        <div className="media">
                            <Row>
                                <Col md={3} className="text-center">
                                    <span className="">
                                        <img
                                            src={profileImg}
                                            style={{ width: '150px' }}
                                            alt=""
                                            className="rounded-circle img-thumbnail"
                                        />
                                    </span>
                                </Col>
                                <Col>
                                    <div className="media-body">
                                        <h4 className="mt-1 mb-1">{`${data.firstName} ${data.lastName}`}</h4>
                                        <p className="text-dark-50"> {data.role}</p>

                                        <hr className="" />

                                        <p className="mb-1">
                                            <strong>
                                                <i className="uil uil-location"></i> Country:
                                            </strong>
                                            <span className="ml-2">Singapore</span>
                                        </p>

                                        <p className="mt-2 mb-1">
                                            <strong>
                                                <i className="uil uil-at"></i> Email:
                                            </strong>
                                            <span className="ml-2">support@coderthemes.com</span>
                                        </p>

                                        <p className="mt-2 mb-1">
                                            <strong>
                                                <i className="uil uil-phone"></i> Phone Number:
                                            </strong>
                                            <span className="ml-2 cursor-pointer" onClick={() => setEdit(true)}>
                                                +1 456 9595 9594
                                            </span>
                                        </p>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
            </CardBody>
        </Card>
    );
}

export default ViewPersonalDetails;
