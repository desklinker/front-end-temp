import React, { useState } from 'react';
import EditPersonalDetails from './EditPersonalDetails';
import ViewPersonalDetails from './ViewPersonalDetails';

function PersonalDetails({ user }) {
    const [isEdit, setEdit] = useState(false);

    const data = {
        firstName: user.firstName,
        lastName: user.lastName,
        role: 'client',
        phone: '+1 456 9595 9594',
        email: 'supportmail@email.com',
        country: 'Singapore',
    };

    return (
        <>
            <ViewPersonalDetails setEdit={setEdit} isEdit={isEdit} data={data} />
            <EditPersonalDetails setEdit={setEdit} isEdit={isEdit} formData={data} />
        </>
    );
}

export default PersonalDetails;
