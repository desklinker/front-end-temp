import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { AvForm } from 'availity-reactstrap-validation';
import OTPInput from '../../../../components/OTPInput';

function ConfirmEdit(props) {
    const handleValidSubmit = (e, values) => {
        //
    };

    const { isOpen, setOpen, setEdit } = props;

    return (
        <Modal isOpen={isOpen} size="md" fade={false} className="modal-dialog-centered">
            <ModalHeader>Confirm Edit</ModalHeader>
            <AvForm onValidSubmit={handleValidSubmit}>
                <ModalBody>
                    <OTPInput
                        length={6}
                        className="otpContainer"
                        inputClassName="otpInput"
                        isNumberInput
                        autoFocus
                        onChangeOTP={(otp) => console.log('Number OTP: ', otp)}
                    />
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={() => setOpen(false)}>
                        Cancel
                    </Button>
                    <Button
                        color="success"
                        onClick={() => {
                            setOpen(false);
                            setEdit(false);
                        }}>
                        Confirm
                    </Button>
                </ModalFooter>
            </AvForm>
        </Modal>
    );
}

export default ConfirmEdit;
