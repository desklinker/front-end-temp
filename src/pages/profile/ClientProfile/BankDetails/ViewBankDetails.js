import React from 'react';
import { Button, Card, CardBody } from 'reactstrap';

function ViewBankDetails({ setEdit }) {
    return (
        <Card className="w-100">
            <CardBody>
                <Button className="close" aria-label="Close" onClick={() => setEdit(true)}>
                    <i className="mdi mdi-square-edit-outline"></i>
                </Button>
                <h4 className="header-title mt-0">Bank Details</h4>
                <hr />

                <div className="text-left">
                    <p>
                        <strong>Country Code: </strong>
                        <span className="ml-2">AUS </span>
                    </p>

                    <p>
                        <strong>Bank Name :</strong>
                        <span className="ml-2"> Commercial </span>
                    </p>

                    <p>
                        <strong>Branch Name:</strong>
                        <span className="ml-2"> Wellawatte</span>
                    </p>
                    <p>
                        <strong>Account Type :</strong>
                        <span className="ml-2"> Saving Account</span>
                    </p>
                    <p>
                        <strong>Account Number :</strong>
                        <span className="ml-2"> 878717457</span>
                    </p>

                    <p>
                        <strong>Account Name :</strong>
                        <span className="ml-2"> Vincent Mil</span>
                    </p>
                </div>
            </CardBody>
        </Card>
    );
}

export default ViewBankDetails;
