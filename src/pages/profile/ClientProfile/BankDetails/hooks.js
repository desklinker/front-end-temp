import { useCallback, useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import {
  storeBankDetails,
  loadBankDetails,
  updateBankDetails,
  loadBillingDetails,
  storeBillingDetails,
  updateBillingDetails,
} from '../../../../actions/clientProfile';
import get from 'lodash/get';

//store bank details
export function useStoreBankDetails({ user }) {
  const dispatch = useDispatch();
  const { userId } = user;

  const onStoreBankDetails = useCallback(
    async (values, actions) => {
      actions.setSubmitting(true);
      const { bank, billing } = values;
      const bankFormData = { ...bank, bankId: bank?.bank?.id };
      const billingFormData = { ...billing, cityId: billing?.city?.id };

      try {
        if (values.isEdit) {
          await dispatch(updateBankDetails(userId, bankFormData));
          await dispatch(updateBillingDetails(userId, billingFormData));
          
        } else {
          await dispatch(storeBankDetails(userId, bankFormData));
          await dispatch(storeBillingDetails(userId, billingFormData));
        }
      } catch (e) {
        actions.setErrors(e.errors);
      }
      actions.setSubmitting(false);
    },
    [dispatch, userId]
  );
  return { onStoreBankDetails };
}

export function useLoadBankDetail(userId) {
  const dispatch = useDispatch();
  const [items, setItems] = useState(null);

  const onLoadBankDetail = useCallback(async () => {
    try {
      const res = await dispatch(loadBankDetails(userId));
      const billRes = await dispatch(loadBillingDetails(userId));

      const data = {
        isEdit: true,
        bank: formattedBankData(res),
        billing: formattedBillingData(billRes),
      };
      setItems(data);
    } catch (e) {
      //
    }
  }, [dispatch, userId]);

  useEffect(() => {
    onLoadBankDetail();
  }, [onLoadBankDetail]);
  return { items };
}

const formattedBankData = (item) => {
  return {
    country: {
      id: get(item, 'bank.city.country.cid'),
      title: get(item, 'bank.city.country.name'),
    },
    bank: {
      id: item.bank?.bid,
      title: item.bank?.bankName,
    },
    accountNumber: item.accountNumber,
    branch: item.branch,
  };
};

const formattedBillingData = (item) => {
  return {
    streetAddress1: item.streetAddress1,
    streetAddress2: item.streetAddress2,
    zipCode: item ? item.zipCode : '',
    country: {
      id: get(item, 'city.country.cid'),
      title: get(item, 'city.country.name'),
    },
    city: {
      id: get(item, 'city.cid'),
      title: get(item, 'city.name'),
    },
  };
};
