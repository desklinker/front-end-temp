import React from 'react';
import { Formik, Form } from 'formik';
import { object, string } from 'yup';
import { Button, Row, Col, FormGroup, CustomInput } from 'reactstrap';
import TextField from '../../../../components/formFields/TextField';
import Dropdown from '../../../../components/formFields/Dropdown';
import { useLoadBankDetail } from './hooks';
import SubmitButton from '../../components/SubmitButton';
import { configMgtUrl, userMgtUrl } from '../../../../config/env';

const validationSchema = object().shape({
  bank: object().shape({
    bank: object().required('Bank name field is required').nullable(),
    branch: string().required('Branch name field is required'),
    country: object().required('Country field is required').nullable(),
    accountNumber: string().required('Account number field is required'),
  }),
  billing: object().shape({
    streetAddress1: string().required('Company address field is required'),
    zipCode: string().required('Zip code field is required'),
  }),
});

function BankDetailsForm({ isEdit, title, next, user, skip, previous, onSubmit }) {
  const { items } = useLoadBankDetail(user.userId);

  const handleSubmit = (values, actions) => {
    onSubmit(values, actions);
    next();
  };

  return (
    <div>
      <h4 className="header-title mt-0">{title ? title : 'Bank Details'}</h4>
      <hr />

      <Formik
        validateOnBlur={false}
        enableReinitialize
        initialValues={
          !items
            ? {
                bank: {
                  country: '',
                  bank: '',
                  branch: '',
                  accountNumber: '',
                },
                billing: {
                  country: '',
                  zipCode: '',
                  streetAddress1: '',
                  streetAddress2: '',
                },
              }
            : items
        }
        validationSchema={validationSchema}
        onSubmit={handleSubmit}>
        {(props) => (
          <Form className="form-custom">
            <Row form>
              <Col sm={4}>
                <Dropdown
                  name="bank.country"
                  placeholder="Country"
                  mgtUrl={configMgtUrl}
                  url="app-configs/countries"
                  resource="countries"
                  listKey="cid"
                  listLabel="name"
                  clearField={() => props.setFieldValue('bank.bank', '')}
                />
              </Col>
              <Col sm={8}>
                <Dropdown
                  name="bank.bank"
                  placeholder="Bank Name"
                  mgtUrl={userMgtUrl}
                  url="users/banks"
                  resource="banks"
                  listKey="bid"
                  listLabel="bankName"
                  relation="cityId"
                  relationId={props.values.bank?.country?.id}
                  isDisabled={!props.values.bank?.country}
                />
              </Col>
            </Row>

            <Row form>
              <Col sm={4}>
                <TextField name="bank.branch" type="text" placeholder="Branch Name" />
              </Col>
              <Col sm={8}>
                <TextField name="bank.accountNumber" type="text" placeholder="Account Number" />
              </Col>
            </Row>

            <p className="text-muted mt-2">
              <strong>Billing Details :</strong>
            </p>

            <FormGroup>
              <CustomInput
                className="text-muted font-weight-normal"
                type="checkbox"
                id="usedCompanyAddress"
                label="Use company address as billing address"
                checked={props.values?.usedCompanyAddress}
                onChange={() => {
                  props.setFieldValue('usedCompanyAddress', !props.values.usedCompanyAddress);
                }}
              />
            </FormGroup>

            <Row form>
              <Col sm={4}>
                <Dropdown
                  name="billing.country"
                  placeholder="Country"
                  mgtUrl={configMgtUrl}
                  url="app-configs/countries"
                  resource="countries"
                  listKey="cid"
                  listLabel="name"
                  clearField={() => props.setFieldValue('billing.city', '')}
                />
              </Col>
              {/* <Col sm={4}>
                <Dropdown
                  name="state"
                  placeholder="State/ Province"
                  mgtUrl={configMgtUrl}
                  url="app-configs/states"
                  resource="states"
                  listKey="sid"
                  listLabel="name"
                  relation="countryId"
                  relationId={props.values.country?.id}
                  clearField={() => props.setFieldValue('city', '')}
                  isDisabled={!props.values.country}
                />
              </Col> */}
              <Col sm={4}>
                <Dropdown
                  name="billing.city"
                  placeholder="City"
                  mgtUrl={configMgtUrl}
                  url="app-configs/cities"
                  resource="cities"
                  listKey="cid"
                  listLabel="name"
                  relation="countryId"
                  relationId={props.values.billing?.country?.id}
                  isDisabled={!props.values.billing?.country}
                />
              </Col>
              <Col sm={4}>
                <TextField name="billing.zipCode" type="text" placeholder="Zip Code" />
              </Col>
            </Row>

            <Row form>
              <Col md={12}>
                <TextField name="billing.streetAddress1" type="text" placeholder="Street Address 1" />
              </Col>
            </Row>

            <Row form>
              <Col md={12}>
                <TextField name="billing.streetAddress2" type="text" placeholder="Street Address 2" />
              </Col>
            </Row>

            <Row className="mt-2">
              <Col className="left-right">
                {previous && (
                  <Button onClick={previous} color="info">
                    Previous
                  </Button>
                )}
              </Col>
              <Col className="text-right">
                {skip && (
                  <Button color="secondary" className="mr-1" onClick={() => next()}>
                    Setup Later
                  </Button>
                )}

                <SubmitButton isSubmitting={props.isSubmitting} next={next} isEdit={isEdit} isItems={!!items} />
              </Col>
            </Row>
          </Form>
        )}
      </Formik>
    </div>
  );
}

export default BankDetailsForm;
