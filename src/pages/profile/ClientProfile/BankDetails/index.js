import React, { useState } from 'react';
import EditBankDetails from './EditBankDetails';
import ViewBankDetails from './ViewBankDetails';

function BankDetails() {
    const [isEdit, setEdit] = useState(false);

    const data = {
        countryCode: 'AUS',
        bankName: 'Commercial',
        branchName: 'Sydney',
        accountType: 'Saving',
        accountNumber: '878717457',
        accountName: 'Vincent Mil',
    };

    if (!isEdit) {
        return <ViewBankDetails setEdit={setEdit} isEdit={isEdit} data={data} />;
    } else {
        return <EditBankDetails setEdit={setEdit} isEdit={isEdit} formData={data} />;
    }
}

export default BankDetails;
