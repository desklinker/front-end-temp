import React from 'react';
import { Button, Card, CardBody } from 'reactstrap';

function ViewCompanyDetails({ isEdit, setEdit }) {
    return (
        <Card className="w-100">
            <CardBody>
                <Button className="close" aria-label="Close" onClick={() => setEdit(true)}>
                    <i className="mdi mdi-square-edit-outline"></i>
                </Button>
                <h4 className="header-title mt-0">Your Company Details</h4>
                <hr />

                <div className="text-left">
                    <p>
                        <strong>Company Name :</strong>
                        <span className="ml-2"> ABC (Pvt)Ltd</span>
                    </p>

                    <p>
                        <strong>Company Address :</strong>
                        <span className="ml-2"> 3940 Richison Drive, Singapore</span>
                    </p>

                    <p>
                        <strong>Telephone Number :</strong>
                        <span className="ml-2">+1 456 9595 9594 </span>
                    </p>

                    <p>
                        <strong>Fax Number :</strong>
                        <span className="ml-2">+1 456 9595 9594 </span>
                    </p>

                    <p className="text-muted">
                        <strong>Customer service contact details :</strong>
                    </p>

                    <p className="ml-3">
                        <strong>Name :</strong>
                        <span className="ml-2">Rachel </span>
                    </p>

                    <p className="ml-3">
                        <strong>Telephone Number :</strong>
                        <span className="ml-2">+1 456 9595 9594 </span>
                    </p>
                </div>
            </CardBody>
        </Card>
    );
}

export default ViewCompanyDetails;
