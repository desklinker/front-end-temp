import React, { useState } from 'react';
import EditCompanyDetails from './EditCompanyDetails';
import ViewCompanyDetails from './ViewCompanyDetails';

function CompanyDetails() {
    const [isEdit, setEdit] = useState(false);

    const data = {
        companyName: 'ABC (Pvt)Ltd',
        companyAddress: '3940 Richison Drive, Singapore',
        telephoneNumber: '+1 456 9595 9594',
        fax: '+1 456 9595 9594 ',
        serviceContactName: 'Rachel',
        serviceContactPhone: '+1 456 9595 9594',
    };

    if (!isEdit) {
        return <ViewCompanyDetails setEdit={setEdit} isEdit={isEdit} data={data} />;
    } else {
        return <EditCompanyDetails setEdit={setEdit} isEdit={isEdit} formData={data} />;
    }
}

export default CompanyDetails;
