import { useCallback, useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { loadCompanyDetails, updateCompanyDetails, storeCompanyDetails } from '../../../../actions/clientProfile';
import get from 'lodash/get';

//Load client company details
export function useLoadCompanyDetails(userId) {
  const dispatch = useDispatch();
  const [items, setItems] = useState(null);

  const onLoadCompanyDetails = useCallback(async () => {
    try {
      const res = await dispatch(loadCompanyDetails(userId));
      if (res) {
        setItems(formattedData(res));
      }
    } catch (e) {
      //
    }
  }, [dispatch, userId]);

  useEffect(() => {
    onLoadCompanyDetails();
  }, [onLoadCompanyDetails]);
  return { items };
}

//store client company details
export function useStoreCompanyDetails({ user }) {
  const dispatch = useDispatch();
  const { userId } = user;

  const onStoreCompanyDetails = useCallback(
    async (values, actions) => {
      actions.setSubmitting(true);

      try {
        if (values.isEdit) {
          await dispatch(updateCompanyDetails(userId, values));
        } else {
          await dispatch(storeCompanyDetails(userId, values));
        }
      } catch (e) {
        actions.setErrors(e.errors);
      }
      actions.setSubmitting(false);
    },
    [dispatch, userId]
  );

  return { onStoreCompanyDetails };
}

const formattedData = (item) => {
  return {
    isEdit: true,
    name: item.name,
    faxNumber: item.faxNumber ? item.faxNumber : '',
    email: item.email,
    phoneNumber: item.phoneNumber,
    streetAddress1: item.streetAddress1,
    streetAddress2: item.streetAddress2,
    zipCode: item.zipCode,
    country: {
      id: get(item, 'city.country.cid'),
      title: get(item, 'city.country.name'),
    },
    city: {
      id: get(item, 'city.cid'),
      title: get(item, 'city.name'),
    },
  };
};
