import React from 'react';
import { Card, CardBody, FormGroup, Button } from 'reactstrap';
import CompanyDetailsForm from './CompanyDetailsForm';

function EditCompanyDetails({ formData, isEdit, setEdit, ...props }) {
  const handleSubmit = (e, values) => {
    //
  };

  return (
    <Card className="w-100">
      <CardBody>
        <FormGroup>
          {isEdit && (
            <Button className="close" onClick={() => setEdit(false)}>
              <span aria-hidden="true">&times;</span>
            </Button>
          )}
        </FormGroup>

        <CompanyDetailsForm onSubmit={handleSubmit} isEdit={isEdit} title="Edit Company Details" item={formData} />
      </CardBody>
    </Card>
  );
}

export default EditCompanyDetails;
