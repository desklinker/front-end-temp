import React from 'react';
import { Formik, Form } from 'formik';
import { object, string } from 'yup';
import Dropdown from '../../../../components/formFields/Dropdown';

import { Button, Row, Col } from 'reactstrap';
import TextField from '../../../../components/formFields/TextField';
import SubmitButton from '../../components/SubmitButton';
import { useLoadCompanyDetails } from './hooks';
import { configMgtUrl } from '../../../../config/env';

const validationSchema = object().shape({
  name: string().required('Company name field is required'),
  streetAddress1: string().required('Company address field is required'),
  zipCode: string().required('Zip code field is required'),
  phoneNumber: string().required('Phone number field is required'),
  faxNumber: string().nullable(),
  email: string().email().nullable(),
});

function CompanyDetailsForm({ isEdit, title, next, skip, onSubmit, user }) {
  const { items } = useLoadCompanyDetails(user.userId);

  const handleSubmit = (values, actions) => {
    onSubmit(
      {
        ...values,
        cityId: values.city?.id,
      },
      actions
    );
    next();
  };

  return (
    <div>
      <h4 className="header-title mt-0">{title ? title : 'Company Details'}</h4>
      <hr />

      <Formik
        validateOnBlur={false}
        enableReinitialize
        initialValues={
          !items
            ? {
                name: '',
                zipCode: '',
                streetAddress1: '',
                streetAddress2: '',
                phoneNumber: '',
                fax: '',
                email: '',
              }
            : items
        }
        validationSchema={validationSchema}
        onSubmit={handleSubmit}>
        {(props) => (
          <Form className="form-custom">
            <Row form>
              <Col md={6}>
                <TextField name="name" type="text" placeholder="Company Name" />
              </Col>
              <Col md={6}>
                <TextField name="phoneNumber" type="text" placeholder="Phone Number" />
              </Col>
            </Row>
            <Row form>
              <Col md={6}>
                <TextField name="faxNumber" type="text" placeholder="Fax Number" />
              </Col>
              <Col md={6}>
                <TextField name="email" type="email" placeholder="Email" />
              </Col>
            </Row>
            <Row form>
              <Col sm={4}>
                <Dropdown
                  name="country"
                  placeholder="Country"
                  mgtUrl={configMgtUrl}
                  url="app-configs/countries"
                  resource="countries"
                  listKey="cid"
                  listLabel="name"
                  clearField={() => props.setFieldValue('city', '')}
                />
              </Col>
              {/* <Col sm={4}>
                <Dropdown
                  name="state"
                  placeholder="State/ Province"
                  mgtUrl={configMgtUrl}
                  url="app-configs/states"
                  resource="states"
                  listKey="sid"
                  listLabel="name"
                  relation="countryId"
                  relationId={props.values.country?.id}
                  clearField={() => props.setFieldValue('city', '')}
                  isDisabled={!props.values.country}
                />
              </Col> */}
              <Col sm={4}>
                <Dropdown
                  name="city"
                  placeholder="City"
                  mgtUrl={configMgtUrl}
                  url="app-configs/cities"
                  resource="cities"
                  listKey="cid"
                  listLabel="name"
                  relation="countryId"
                  relationId={props.values.country?.id}
                  isDisabled={!props.values.country}
                />
              </Col>
              <Col sm={4}>
                <TextField name="zipCode" type="text" placeholder="Zip Code" />
              </Col>
            </Row>

            <Row form>
              <Col md={12}>
                <TextField name="streetAddress1" type="text" placeholder="Street Address 1" />
              </Col>
            </Row>

            <Row form>
              <Col md={12}>
                <TextField name="streetAddress2" type="text" placeholder="Street Address 2" />
              </Col>
            </Row>

            <Row className="mt-2">
              <Col className="text-right">
                {skip && (
                  <Button color="secondary" className="mr-1" onClick={() => next()}>
                    Setup Later
                  </Button>
                )}

                <SubmitButton isSubmitting={props.isSubmitting} next={next} isEdit={isEdit} isItems={!!items} />
              </Col>
            </Row>
          </Form>
        )}
      </Formik>
    </div>
  );
}

export default CompanyDetailsForm;
