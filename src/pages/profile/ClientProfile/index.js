import React from 'react';
import { Card, CardBody, Row, Col } from 'reactstrap';

import TemplateAttachment from './TemplateAttachment';
import Templates from './Templates';
import BankDetails from './BankDetails';
import CompanyDetails from './CompanyDetails';
import PersonalDetails from './PersonalDetails';

function ClientProfile(props) {
    const { user } = props;

    return (
        <Row>
            <Col lg={10}>
                <Row>
                    <Col className="d-flex">
                        <PersonalDetails user={user} />
                    </Col>
                    <Col sm={3}>
                        <Card className="tilebox-one">
                            <CardBody>
                                <i className="uil-bag float-right text-muted"></i>
                                <h6 className="text-muted text-uppercase mt-0">On Going Projects</h6>
                                <h3 className="m-b-20">2</h3>
                            </CardBody>
                        </Card>
                        <Card className="tilebox-one">
                            <CardBody>
                                <i className="uil-bag float-right text-muted"></i>
                                <h6 className="text-muted text-uppercase mt-0">Completed Projects</h6>
                                <h3 className="m-b-20">5</h3>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col className="d-flex" md={6}>
                        <CompanyDetails />
                    </Col>
                    <Col className="d-flex" md={6}>
                        <BankDetails />
                    </Col>
                </Row>

                <hr />

                <Row>
                    <Col className="d-flex" md={6}>
                        <Templates title="Templates" />
                    </Col>
                    <Col className="d-flex" md={6}>
                        <TemplateAttachment />
                    </Col>
                </Row>
            </Col>
        </Row>
    );
}

export default ClientProfile;
