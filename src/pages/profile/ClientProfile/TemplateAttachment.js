import React, { Component } from 'react';
import { Button, Card, CardBody, Col, Row } from 'reactstrap';
import { AvForm } from 'availity-reactstrap-validation';
import FileUploader from '../../../components/FileUploader';

class TemplateAttachment extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.handleValidSubmit = this.handleValidSubmit.bind(this);
        this.updateValues = this.updateValues.bind(this);
    }

    /**
     * Update values
     */
    updateValues = (field, fieldValue) => {
        const state = { ...this.state.state };
        state[field] = fieldValue;
        this.setState(state);
    };

    /**
     * Handle the form submission
     */
    handleValidSubmit = (e, values) => {
        console.log({ ...values, ...this.state });
    };

    render() {
        return (
            <Card className="w-100">
                <CardBody>
                    <Row>
                        <Col>
                            <AvForm onValidSubmit={this.handleValidSubmit}>
                                <Row>
                                    <Col>
                                        <FileUploader
                                            onFileUpload={(files) => {
                                                this.updateValues('avatar', files);
                                            }}
                                        />

                                        <Row className="mt-1">
                                            <Col className="text-right">
                                                <Button id="caret" color="success" type="submit">
                                                    Submit
                                                </Button>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </AvForm>
                        </Col>
                    </Row>
                </CardBody>
            </Card>
        );
    }
}

export default TemplateAttachment;
