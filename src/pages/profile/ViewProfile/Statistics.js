// @flow
import React from 'react';
import { Card, CardBody, Row, Col } from 'reactstrap';

const Statistics = () => {
    return (
        <Row>
            <Col sm={3}>
                <Card className="tilebox-one">
                    <CardBody>
                        <i className="uil-bag float-right text-muted"></i>
                        <h6 className="text-muted text-uppercase mt-0">On Going Projects</h6>
                        <h2 className="m-b-20">2</h2>
                        {/* <span className="badge badge-primary"> +11% </span>{' '}
                        <span className="text-muted">From previous period</span> */}
                    </CardBody>
                </Card>
            </Col>

            <Col sm={3}>
                <Card className="tilebox-one">
                    <CardBody>
                        <i className="uil-bag float-right text-muted"></i>
                        <h6 className="text-muted text-uppercase mt-0">Completed Projects</h6>
                        <h2 className="m-b-20">5</h2>
                        {/* <span className="badge badge-primary"> +11% </span>{' '}
                        <span className="text-muted">From previous period</span> */}
                    </CardBody>
                </Card>
            </Col>

            <Col sm={3}>
                <Card className="tilebox-one">
                    <CardBody>
                        <i className="uil-dollar-sign float-right text-muted"></i>
                        <h6 className="text-muted text-uppercase mt-0">Total Earnings</h6>
                        <h2 className="m-b-20">
                            $<span>4500</span>
                        </h2>
                        {/* <span className="badge badge-danger"> -29% </span>{' '}
                        <span className="text-muted">From previous period</span> */}
                    </CardBody>
                </Card>
            </Col>

            <Col sm={3}>
                <Card className="tilebox-one">
                    <CardBody>
                        <i className="uil-star float-right text-muted"></i>
                        <h6 className="text-muted text-uppercase mt-0">Rating</h6>
                        <h2 className="m-b-20">4.7</h2>
                        {/* <span className="badge badge-success"> +89% </span>{' '}
                        <span className="text-muted">From previous period</span> */}
                    </CardBody>
                </Card>
            </Col>
        </Row>
    );
};

export default Statistics;
