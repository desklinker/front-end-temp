// @flow
import React from 'react';
import { Card, CardBody, Form, FormGroup, Label, Input, Row, Col, Button, CustomInput } from 'reactstrap';
import MaskedInput from 'react-text-mask';
import Select from 'react-select';
import { Wizard, Steps, Step } from 'react-albus';
import FileUploader from '../../../components/FileUploader';
import HyperDatepicker from '../../../components/Datepicker';

class EditProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isStudent: true,
            isProfessional: false,
            isCurrentJob: false,
            isPastJob: true,
        };
    }

    handleCancelBtn(e) {
        this.props.handleEditProfileBtn(false);
    }

    handleStudProfChange = (e) => {
        if (e.target.value === 'student') {
            this.setState({
                isStudent: true,
                isProfessional: false,
            });
        } else if (e.target.value === 'professional') {
            console.log('Professional changeed');
            this.setState({
                isProfessional: true,
                isStudent: false,
            });
        }
    };

    handleJobStatusChange = (e) => {
        if (e.value === 'current') {
            this.setState(
                {
                    isCurrentJob: true,
                    isPastJob: false,
                },
                () => {
                    console.log(this.state.isCurrentJob + ' - Current Job');
                }
            );
            this.forceUpdate();
        } else if (e.value === 'past') {
            this.setState(
                {
                    isPastJob: true,
                    isCurrentJob: false,
                },
                () => {
                    console.log(this.state.isPastJob + ' - Past Job');
                }
            );
            this.forceUpdate();
        }
    };

    render() {
        return (
            <Card>
                <CardBody>
                    <Wizard>
                        <Steps>
                            <Step
                                id="basic"
                                render={({ next }) => (
                                    <Form>
                                        <h4 className="header-title mb-3">Basic Information</h4>

                                        <Row>
                                            <Col md={6}>
                                                <FormGroup>
                                                    <Label for="text">First name</Label>
                                                    <Input
                                                        type="text"
                                                        name="fname"
                                                        id="fname"
                                                        placeholder="with a placeholder"
                                                    />
                                                </FormGroup>
                                            </Col>
                                            <Col md={6}>
                                                <FormGroup>
                                                    <Label for="text">Last name</Label>
                                                    <Input
                                                        type="text"
                                                        name="lname"
                                                        id="lname"
                                                        placeholder="with a placeholder"
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col md={6}>
                                                <FormGroup>
                                                    <Label for="email">Email</Label>
                                                    <Input
                                                        type="email"
                                                        name="email"
                                                        id="email"
                                                        placeholder="with a placeholder"
                                                    />
                                                </FormGroup>
                                            </Col>
                                            <Col md={6}>
                                                <FormGroup>
                                                    <Label for="text">Phone number</Label>
                                                    <MaskedInput
                                                        mask={[
                                                            '(',
                                                            /[1-9]/,
                                                            /\d/,
                                                            ')',
                                                            ' ',
                                                            /\d/,
                                                            /\d/,
                                                            /\d/,
                                                            '-',
                                                            /\d/,
                                                            /\d/,
                                                            /\d/,
                                                            /\d/,
                                                        ]}
                                                        placeholder="(__) ____-____"
                                                        className="form-control"
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col md={6}>
                                                <FormGroup>
                                                    <Label for="field">Field category</Label>
                                                    <Select
                                                        className="react-select"
                                                        options={[
                                                            { value: 'civil', label: 'Civil Engineering' },
                                                            { value: 'mechanical', label: 'Mechanical Engineering' },
                                                            { value: 'cs', label: 'Computer Science' },
                                                        ]}></Select>
                                                </FormGroup>
                                            </Col>
                                            <Col md={6}>
                                                <FormGroup>
                                                    <Label for="country">Country</Label>
                                                    <Select
                                                        className="react-select"
                                                        options={[
                                                            { value: 'sl', label: 'Sri Lanka' },
                                                            { value: 'sg', label: 'Singapore' },
                                                        ]}></Select>
                                                </FormGroup>
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col md={12}>
                                                <FormGroup>
                                                    <Label for="bio">Bio</Label>
                                                    <Input type="textarea" name="bio" id="bio" rows="5" />
                                                </FormGroup>
                                            </Col>
                                        </Row>

                                        <ul className="list-inline wizard mb-0">
                                            <li className="previous list-inline-item">
                                                <Button onClick={this.handleCancelBtn.bind(this)} color="info">
                                                    Cancel
                                                </Button>
                                            </li>
                                            <li className="next list-inline-item float-right">
                                                <Button onClick={next} color="success">
                                                    Next
                                                </Button>
                                            </li>
                                        </ul>
                                    </Form>
                                )}
                            />

                            <Step
                                id="qualifications"
                                render={({ next, previous }) => (
                                    <Form>
                                        <h4 className="header-title mb-3">Qualifications</h4>

                                        <Row>
                                            <Col md={6}>
                                                <FormGroup>
                                                    <Row>
                                                        <Col md={2}>
                                                            <Label for="exampleCheckbox">I am a</Label>
                                                        </Col>
                                                        <Col md={3}>
                                                            <CustomInput
                                                                type="radio"
                                                                id="exampleCustomRadio"
                                                                name="customRadio"
                                                                label="Student"
                                                                value="student"
                                                                onChange={this.handleStudProfChange}
                                                            />
                                                        </Col>
                                                        <Col md={3}>
                                                            <CustomInput
                                                                type="radio"
                                                                id="exampleCustomRadio3"
                                                                name="customRadio"
                                                                label="Professional"
                                                                value="professional"
                                                                onChange={this.handleStudProfChange}
                                                            />
                                                        </Col>
                                                    </Row>
                                                </FormGroup>
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col md={3}>
                                                <FormGroup>
                                                    <Label for="degree">Degree</Label>
                                                    <Input
                                                        type="text"
                                                        name="degree"
                                                        id="degree"
                                                        placeholder="with a placeholder"
                                                    />
                                                </FormGroup>
                                            </Col>
                                            <Col md={3}>
                                                <FormGroup>
                                                    <Label for="uni">University</Label>
                                                    <Input
                                                        type="text"
                                                        name="uni"
                                                        id="uni"
                                                        placeholder="with a placeholder"
                                                    />
                                                </FormGroup>
                                            </Col>
                                            <Col md={2}>
                                                <FormGroup>
                                                    <Label for="startedYear">Started year</Label>
                                                    <HyperDatepicker hideAddon={true} dateFormat="yyyy" />
                                                </FormGroup>
                                            </Col>
                                            <Col md={2}>
                                                <FormGroup>
                                                    <Label for="status">Status</Label>
                                                    <Select
                                                        className="react-select"
                                                        options={[
                                                            { value: 'completed', label: 'Completed' },
                                                            { value: 'ongoing', label: 'Pending' },
                                                        ]}></Select>
                                                </FormGroup>
                                            </Col>
                                            {this.state.isStudent ? (
                                                <Col md={1}>
                                                    <FormGroup>
                                                        <Label for="uni">GPA</Label>
                                                        <Input
                                                            type="text"
                                                            name="gpa"
                                                            id="gpa"
                                                            placeholder="with a placeholder"
                                                        />
                                                    </FormGroup>
                                                </Col>
                                            ) : (
                                                <div></div>
                                            )}
                                            <Col md={1}>
                                                <FormGroup>
                                                    <Label></Label>
                                                    <Button style={{ marginTop: '30px' }} color="info">
                                                        +
                                                    </Button>
                                                </FormGroup>
                                            </Col>
                                        </Row>

                                        {this.state.isProfessional ? (
                                            <Row>
                                                <Col md={3}>
                                                    <FormGroup>
                                                        <Label for="job-title">Job title</Label>
                                                        <Input
                                                            type="text"
                                                            name="job-title"
                                                            id="job-title"
                                                            placeholder="with a placeholder"
                                                        />
                                                    </FormGroup>
                                                </Col>
                                                <Col md={2}>
                                                    <FormGroup>
                                                        <Label for="from">From</Label>
                                                        <HyperDatepicker hideAddon={true} dateFormat="MM/yyyy" />
                                                    </FormGroup>
                                                </Col>
                                                {this.state.isPastJob ? (
                                                    <Col md={2}>
                                                        <FormGroup>
                                                            <Label for="to">To</Label>
                                                            <HyperDatepicker hideAddon={true} dateFormat="MM/yyyy" />
                                                        </FormGroup>
                                                    </Col>
                                                ) : (
                                                    <div></div>
                                                )}
                                                <Col md={3}>
                                                    <FormGroup>
                                                        <Label for="bio">Status</Label>
                                                        <Select
                                                            className="react-select"
                                                            options={[
                                                                { value: 'past', label: 'Past experience' },
                                                                { value: 'current', label: 'Currently work here' },
                                                            ]}
                                                            onChange={this.handleJobStatusChange}></Select>
                                                    </FormGroup>
                                                </Col>
                                                <Col md={1}>
                                                    <FormGroup>
                                                        <Label></Label>
                                                        <Button style={{ marginTop: '30px' }} color="info">
                                                            +
                                                        </Button>
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                        ) : (
                                            <div></div>
                                        )}

                                        <Row>
                                            <Col md={12}>
                                                <FormGroup>
                                                    <Label for="bio">Skills</Label>
                                                    <Select
                                                        className="react-select"
                                                        isMulti={true}
                                                        options={[
                                                            { value: 'drafting', label: 'Drafting' },
                                                            { value: 'design', label: 'Design' },
                                                            { value: 'architecture', label: 'Architecture' },
                                                            { value: 'surveying', label: 'Surveying' },
                                                        ]}></Select>
                                                </FormGroup>
                                            </Col>
                                            <Col md={12}>
                                                <FormGroup>
                                                    <Label>Supportive documents</Label>
                                                    <FileUploader
                                                        onFileUpload={(files) => {
                                                            console.log(files);
                                                        }}
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>

                                        <ul className="list-inline wizard mb-0">
                                            <li className="previous list-inline-item">
                                                <Button onClick={previous} color="info">
                                                    Previous
                                                </Button>
                                            </li>
                                            <li className="next list-inline-item float-right">
                                                <Button onClick={next} color="success">
                                                    Next
                                                </Button>
                                            </li>
                                        </ul>
                                    </Form>
                                )}
                            />

                            <Step
                                id="bank"
                                render={({ next, previous }) => (
                                    <Form>
                                        <h4 className="header-title mb-3">Bank Details</h4>

                                        <Row>
                                            <Col md={4}>
                                                <FormGroup>
                                                    <Label for="bank-name">Bank name</Label>
                                                    <Input
                                                        type="text"
                                                        name="bank-name"
                                                        id="bank-name"
                                                        placeholder="with a placeholder"
                                                    />
                                                </FormGroup>
                                            </Col>
                                            <Col md={4}>
                                                <FormGroup>
                                                    <Label for="branch">Branch</Label>
                                                    <Input
                                                        type="text"
                                                        name="branch"
                                                        id="branch"
                                                        placeholder="with a placeholder"
                                                    />
                                                </FormGroup>
                                            </Col>
                                            <Col md={4}>
                                                <FormGroup>
                                                    <Label for="account-number">Account number</Label>
                                                    <Input
                                                        type="text"
                                                        name="account-number"
                                                        id="account-number"
                                                        placeholder="with a placeholder"
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>

                                        <ul className="list-inline wizard mb-0">
                                            <li className="previous list-inline-item">
                                                <Button onClick={previous} color="info">
                                                    Previous
                                                </Button>
                                            </li>

                                            <li className="next list-inline-item float-right">
                                                <Button color="success">Submit</Button>
                                            </li>
                                        </ul>
                                    </Form>
                                )}
                            />
                        </Steps>
                    </Wizard>
                </CardBody>
            </Card>
        );
    }
}

export default EditProfile;
