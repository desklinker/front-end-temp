import React from 'react';
import { Card, CardBody, Row, Col, Button } from 'reactstrap';
import profileImg from '../../../assets/images/users/avatar-1.jpg';
import Rating from 'react-rating';

function UserBox({ type, setEdit }) {
    return (
        <Card className="">
            <CardBody className="profile-user-box">
                {type !== 'client' && (
                    <Button className="close" aria-label="Close" onClick={() => setEdit(true)}>
                        <i className="mdi mdi-square-edit-outline"></i>
                    </Button>
                )}

                <Row>
                    <Col lg={9} md={12}>
                        <div className="media">
                            <Row>
                                <Col md={3} className="text-center">
                                    <span className="">
                                        <img
                                            src={profileImg}
                                            style={{ width: '150px' }}
                                            alt=""
                                            className="rounded-circle img-thumbnail"
                                        />
                                    </span>
                                </Col>
                                <Col>
                                    <div className="media-body">
                                        <h4 className="mt-1 mb-1 mr-2">Vigneshan Seshamany</h4>
                                        <p className="text-dark-50">
                                            <span className="mr-2">Civil Engineer</span>
                                            <Rating
                                                stop={5}
                                                initialRating={4.5}
                                                readonly
                                                emptySymbol="mdi mdi-star-outline font-16 text-muted"
                                                fullSymbol="mdi mdi-star font-16 text-warning"
                                            />
                                            <br />
                                            {type !== 'client' && (
                                                <>
                                                    Active No of Projects :
                                                    <span className="ml-1 font-weight-semibold"> 04 </span>
                                                    <span className="mx-1 text-muted">|</span> Total Earnings :
                                                    <span className="ml-1 font-weight-semibold">$400 </span>
                                                    <span className="mx-1 text-muted">|</span> Total Project Budget:{' '}
                                                    <span className="ml-1 font-weight-semibold">$1000 </span>
                                                </>
                                            )}
                                        </p>
                                        <p className="text-muted">
                                            Hye, I’m Michael Franklin residing in this beautiful world. I create
                                            websites and mobile apps with great UX and UI design. I have done work with
                                            big companies like Nokia, Google and Yahoo. Meet me or Contact me for any
                                            queries. One Extra line for filling space.
                                        </p>
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>
            </CardBody>
        </Card>
    );
}

export default UserBox;
