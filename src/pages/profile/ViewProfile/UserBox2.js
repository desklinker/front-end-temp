// @flow
import React from 'react';
import { Card, CardBody, Row, Col } from 'reactstrap';

import profileImg from '../../../assets/images/users/avatar-1.jpg';

// const UserBox = () => {
class UserBox2 extends React.Component {
    // constructor(props) {
    //     super(props);
    //     // this.state = {
    //     //     a: true
    //     // };
    // }

    handleEditProfileBtn(e) {
        this.props.handleEditProfileBtn(true);
    }

    render() {
        return (
            <Card className="">
                <CardBody className="profile-user-box">
                    <Row>
                        <Col sm={12} className="text-center">
                            <span className="text-center m-2 mr-4">
                                <img
                                    src={profileImg}
                                    style={{ height: '100px' }}
                                    alt=""
                                    className="rounded-circle img-thumbnail"
                                />
                            </span>
                        </Col>
                        <Col sm={12}>
                            <div className="media-body">
                                <h4 className="mt-1 mb-1 text-center">Vigneshan Seshamany</h4>
                                <p className="text-dark-50 text-center"> Civil Engineer</p>

                                <p className="text-muted text-center">
                                    Hye, I’m Michael Franklin residing in this beautiful world. I create websites and
                                    mobile apps with great UX and UI design. I have done work with big companies like
                                    Nokia, Google and Yahoo. Meet me or Contact me for any queries. One Extra line for
                                    filling space.
                                </p>
                            </div>
                        </Col>
                    </Row>
                </CardBody>
            </Card>
        );
    }
}

export default UserBox2;
