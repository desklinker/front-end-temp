// @flow
import React from 'react';
import { Row, Col, Card, CardBody } from 'reactstrap';

import PageTitle from '../../../components/PageTitle';
import { CLIENT } from '../../../constants/roles';
import cad from '../../../assets/images/projects/cad.jpg';
import design from '../../../assets/images/projects/design.jpg';
import design2 from '../../../assets/images/projects/design2.jpg';
import architecture from '../../../assets/images/projects/architecture.jpg';

import UserInfoBox from './UserInfoBox';
import Statistics from './Statistics';
import UserBox from './UserBox';
import TableView from '../../myProjects/TableView';
import { connect } from 'react-redux';
import { default as ClientProfileView } from '../ClientProfile';

// const Profile = () => {
class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showEditProfile: false,
      user: props.user,
    };
  }

  handleEditProfileBtn = () => {
    this.setState({
      showEditProfile: !this.state.showEditProfile,
    });
  };

  
  render() {
    const projectsCompleted = [
      {
        id: 1,
        title: 'Ubold v3.0 - Redesign',
        state: 'Ongoing',
        image: cad,
        shortDesc: 'With supporting text below as a natural lead-in to additional contenposuere erat a ante',
        totalTasks: null,
        totalComments: null,
        totalMembers: null,
        progress: '50',
        field: 'Civil Engineering',
        skill: 'CAD',
        minBid: '$250',
        maxBid: '$500',
        duration: '6 Weeks',
      },
      {
        id: 2,
        title: 'Minton v3.0 - Redesign',
        state: 'Finished',
        image: design,
        shortDesc:
          'This card has supporting text below as a natural lead-in to additional content is a little bit longer',
        totalTasks: null,
        totalComments: null,
        totalMembers: null,
        progress: '30',
        field: 'Civil Engineering',
        skill: 'Design',
        minBid: '$250',
        maxBid: '$500',
        duration: '6 Weeks',
      },
      {
        id: 3,
        title: 'Hyper v2.1 - Angular and Django',
        state: 'Planning',
        image: design2,
        shortDesc: 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid',
        totalTasks: null,
        totalComments: null,
        totalMembers: null,
        progress: '40',
        field: 'Civil Engineering',
        skill: 'CAD',
        minBid: '$250',
        maxBid: '$500',
        duration: '6 Weeks',
      },
      {
        id: 4,
        title: 'Hyper v2.1 - React, Webpack',
        state: 'Ongoing',
        image: architecture,
        shortDesc: "Some quick example text to build on the card title and make up the bulk of the card's content",
        totalTasks: null,
        totalComments: null,
        totalMembers: null,
        progress: '70',
        field: 'Civil Engineering',
        skill: 'Design',
        minBid: '$250',
        maxBid: '$500',
        duration: '6 Weeks',
      },
      {
        id: 5,
        title: 'Hyper v2.1 - React, Webpack',
        state: 'Finished',
        image: architecture,
        shortDesc: "Some quick example text to build on the card title and make up the bulk of the card's content",
        totalTasks: null,
        totalComments: null,
        totalMembers: null,
        progress: '90',
        field: 'Civil Engineering',
        skill: 'Design',
        minBid: '$250',
        maxBid: '$500',
        duration: '6 Weeks',
      },
    ];

    return (
      <React.Fragment>
        <PageTitle
          breadCrumbItems={[
            { label: 'Pages', path: '/pages/profile' },
            { label: 'Profile', path: '/pages/profile', active: true },
          ]}
          title={'Profile'}
        />

        {this.state.user.role === CLIENT ? (
          <ClientProfileView user={this.state.user} />
        ) : (
          <Row>
            <Col md={12}>
              <UserBox setEdit={this.handleEditProfileBtn} />
            </Col>
            <Col md={4}>
              <UserInfoBox />
            </Col>

            <Col md={8}>
              <Statistics />
              <Card>
                <CardBody>
                  <TableView projects={projectsCompleted} filters={false} />
                </CardBody>
              </Card>
            </Col>
          </Row>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.Auth.user,
  };
};
export default connect(mapStateToProps)(Profile);
