// @flow
import React from 'react';
import { useSelector } from 'react-redux';
import { Card, CardBody, Col, Row } from 'reactstrap';
import { ADMIN } from '../../../constants';
import '../styles/profile.css';


const UserInfoBox2 = () => {
    const user = useSelector(state => state.Auth.user);

    return (
        <Card>
            <CardBody>
                <Row>
                    <Col md={4}>
                        <h4 className="header-title mt-0 mb-3">Basic Information</h4>
                        <div className="text-left">
                            <p className="text-muted">
                                <strong>Mobile :</strong>
                                <span className="ml-2">(+94) 77 123 4567</span>
                            </p>

                            <p className="text-muted">
                                <strong>Email :</strong> <span className="ml-2">vikki@gmail.com</span>
                            </p>

                            <p className="text-muted">
                                <strong>Country :</strong> <span className="ml-2">Singapore</span>
                            </p>

                            <p className="text-muted">
                                <strong>Skills :</strong>
                                <span className="ml-2"> CAD, Design, Architecture </span>
                            </p>
                        </div>
                    </Col>
                    <Col md={4}>
                        <h4 className="header-title mt-0 mb-3">Educational Qualifications</h4>
                        <div className="text-left">
                            <p className="text-muted">
                                <strong>Qualification :</strong>
                                <span className="ml-2">BTech. Civil Engineering</span>
                            </p>

                            <p className="text-muted">
                                <strong>University :</strong>{' '}
                                <span className="ml-2">The Open University of Sri Lanka</span>
                            </p>

                            <p className="text-muted">
                                <strong>Year of Completion :</strong> <span className="ml-2">2015</span>
                            </p>

                            <p className="text-muted">
                                <strong>GPA :</strong> <span className="ml-2">3.4</span>
                            </p>

                            <p className="text-muted">
                                <strong>Skills :</strong>
                                <span className="ml-2"> CAD, Design, Architecture </span>
                            </p>
                        </div>
                    </Col>
                    <Col md={4}>
                        <h4 className="header-title mt-0 mb-3">Experience</h4>
                        <div className="text-left">
                            <p className="text-muted">
                                <strong>Qualification :</strong>
                                <span className="ml-2">Design Engineerin</span>
                            </p>

                            <p className="text-muted">
                                <strong>From :</strong> <span className="ml-2 mr-4">2016</span>
                                <strong>To :</strong> <span className="ml-2">Present</span>
                            </p>
                        </div>
                    </Col>
                    {user.role === ADMIN && (
                        <Col md={4}>
                            <h4 className="header-title mt-0 mb-3">Bank Details</h4>
                            <div className="text-left">
                                <p className="text-muted">
                                    <strong>Bank Name :</strong>
                                    <span className="ml-2">Commercial Bank (Pvt) Ltd</span>
                                </p>

                                <p className="text-muted">
                                    <strong>Branch :</strong> <span className="ml-2">Wellawatte</span>
                                </p>

                                <p className="text-muted">
                                    <strong>Account Number :</strong> <span className="ml-2">00212545212</span>
                                </p>
                            </div>
                        </Col>
                    )}
                </Row>
            </CardBody>
        </Card>
    );
};

export default UserInfoBox2;
