import React from 'react';
import { FormFeedback } from 'reactstrap';
import get from 'lodash/get';

function FieldError({ props, name }) {
  const error = get(props.errors, name);

  if (!get(props.touched, name)) {
    return null;
  }
  return <FormFeedback className="d-block">{error}</FormFeedback>;
}

export default FieldError;
