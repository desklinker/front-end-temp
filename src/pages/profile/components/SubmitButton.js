import React from 'react';
import { Button } from 'reactstrap';

function SubmitButton({ isEdit, isItems, next, isSubmitting }) {
  return (
    <Button type="submit" color={isEdit || isItems ? 'primary' : 'success'} className="mr-1">
      {isSubmitting
        ? 'Loading...'
        : next
        ? !isItems
          ? 'Save & Continue'
          : 'Update & Continue'
        : !isEdit
        ? 'Submit'
        : 'Update'}
    </Button>
  );
}

export default SubmitButton;
