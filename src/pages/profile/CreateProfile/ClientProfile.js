import React from 'react';
import CompanyDetailsForm from '../ClientProfile/CompanyDetails/CompanyDetailsForm';
import BankDetailsForm from '../ClientProfile/BankDetails/BankDetailsForm';
import { useStoreCompanyDetails } from '../ClientProfile/CompanyDetails/hooks';
import { useStoreBankDetails } from '../ClientProfile/BankDetails/hooks';
import { Row, Col, Button, Progress } from 'reactstrap';
import { Wizard, Steps, Step } from 'react-albus';
import { useConfirmUserDetails } from './hooks';

function ClientProfile({ history, user, match: { url } }) {
  const { onStoreCompanyDetails } = useStoreCompanyDetails({ user });
  const { onStoreBankDetails } = useStoreBankDetails({ user });
  const { onConfirmUserDetails } = useConfirmUserDetails(history);

  const handleSubmit = () => {
    if (user) {
      onConfirmUserDetails(user);
    }
  };

  return (
    <>
      <Wizard
        history={history}
        basename={url}
        render={({ step, steps }) => {
          return (
            <React.Fragment>
              <Progress
                animated
                striped
                color="info"
                value={((steps.indexOf(step) + 1) / steps.length) * 100}
                className="mb-3 progress-sm"
              />

              <Steps>
                <Step
                  id="clientCompany"
                  render={({ next }) => (
                    <CompanyDetailsForm
                      onSubmit={onStoreCompanyDetails}
                      title="Representing Company Details"
                      next={next}
                      skip
                      user={user}
                    />
                  )}
                />
                <Step
                  id="clientBank"
                  render={({ next, previous }) => (
                    <BankDetailsForm
                      onSubmit={onStoreBankDetails}
                      title="Create Bank Details"
                      next={next}
                      skip
                      user={user}
                      previous={previous}
                    />
                  )}
                />
                <Step
                  id="success"
                  render={({ previous }) => (
                    <Row>
                      <Col sm={12}>
                        <div className="text-center">
                          <h2 className="mt-0">
                            <i className="mdi mdi-check-all"></i>
                          </h2>
                          <h3 className="mt-0">Thank you !</h3>

                          <p className="w-75 mb-2 mx-auto">
                            Quisque nec turpis at urna dictum luctus. Suspendisse convallis dignissim eros at volutpat.
                            In egestas mattis dui. Aliquam mattis dictum aliquet.
                          </p>
                        </div>
                      </Col>

                      <Col sm={12}>
                        <ul className="list-inline wizard mb-0">
                          <li className="next list-inline-item float-right">
                            <Button color="success" onClick={handleSubmit}>
                              Close
                            </Button>
                          </li>
                        </ul>
                      </Col>
                    </Row>
                  )}
                />
              </Steps>
            </React.Fragment>
          );
        }}
      />
    </>
  );
}

export default ClientProfile;
