import React from 'react';
import { Row, Col, Button, Progress } from 'reactstrap';
import { Wizard, Steps, Step } from 'react-albus';

import BankDetailsForm from '../ProfessionalProfile/BankDetails/BankDetailsForm';
import QualificationDetailsForm from '../ProfessionalProfile/QualificationDetails/QualificationDetailsForm';
import ExperienceDetailsForm from '../ProfessionalProfile/ExperienceDetails/ExperienceDetailsForm';
import SkillDetailsForm from '../ProfessionalProfile/SkillDetails/SkillDetailsForm';

import { useStoreExperienceDetails } from '../ProfessionalProfile/ExperienceDetails/hooks';
import { useStoreQualificationDetails } from '../ProfessionalProfile/QualificationDetails/hooks';
import { useStoreBankDetails } from '../ProfessionalProfile/BankDetails/hooks';
import { useStoreSkillDetails } from '../ProfessionalProfile/SkillDetails/hooks';
import { DASHBOARD } from '../../../constants';

function ProfessionalProfile({ history, user, match: { url } }) {
  const { onStoreSkillDetails } = useStoreSkillDetails({ user });
  const { onStoreBankDetails } = useStoreBankDetails({ user });
  const { onStoreQualificationDetails } = useStoreQualificationDetails({ user });
  const { onStoreExperienceDetails } = useStoreExperienceDetails({ user });

  return (
    <>
      <Wizard
        history={history}
        basename={url}
        render={({ step, steps }) => {
          return (
            <React.Fragment>
              <Progress
                animated
                striped
                color="info"
                value={((steps.indexOf(step) + 1) / steps.length) * 100}
                className="mb-3 progress-sm"
              />

              <Steps>
                <Step
                  id="skill"
                  render={({ next }) => (
                    <SkillDetailsForm
                      onSubmit={onStoreSkillDetails}
                      title="Create Field & Skills Details"
                      next={next}
                      user={user}
                    />
                  )}
                />
                <Step
                  id="qualification"
                  render={({ next, previous }) => (
                    <QualificationDetailsForm
                      onSubmit={onStoreQualificationDetails}
                      title="Create Qualification Details"
                      next={next}
                      skip
                      previous={previous}
                      user={user}
                    />
                  )}
                />
                <Step
                  id="experience"
                  render={({ next, previous }) => (
                    <ExperienceDetailsForm
                      onSubmit={onStoreExperienceDetails}
                      title="Create Work Experience Details"
                      user={user}
                      next={next}
                      skip
                      previous={previous}
                    />
                  )}
                />
                <Step
                  id="profBank"
                  render={({ next, previous }) => (
                    <BankDetailsForm
                      onSubmit={onStoreBankDetails}
                      title="Create Bank Details"
                      next={next}
                      skip
                      user={user}
                      previous={previous}
                    />
                  )}
                />
                <Step
                  id="completed"
                  render={({ previous }) => (
                    <Row>
                      <Col sm={12}>
                        <div className="text-center">
                          <h2 className="mt-0">
                            <i className="mdi mdi-check-all"></i>
                          </h2>
                          <h3 className="mt-0">Thank you !</h3>

                          <p className="w-75 mb-2 mx-auto">
                            Quisque nec turpis at urna dictum luctus. Suspendisse convallis dignissim eros at volutpat.
                            In egestas mattis dui. Aliquam mattis dictum aliquet.
                          </p>
                        </div>
                      </Col>

                      <Col sm={12}>
                        <ul className="list-inline wizard mb-0">
                          <li className="next list-inline-item float-right">
                            <Button color="success" onClick={() => history.push(DASHBOARD)}>
                              Close
                            </Button>
                          </li>
                        </ul>
                      </Col>
                    </Row>
                  )}
                />
              </Steps>
            </React.Fragment>
          );
        }}
      />
    </>
  );
}

export default ProfessionalProfile;
