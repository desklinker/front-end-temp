import React from 'react';
import { useSelector } from 'react-redux';
import ClientProfile from './ClientProfile';
import { Card, CardBody, Row, Col } from 'reactstrap';
import { PROFESSIONAL } from '../../../constants/roles';
import ProfessionalProfile from './ProfessionalProfile';

function CreateProfile({ history, match }) {
  const user = useSelector((state) => state.Auth.user);

  if (user && user.role === PROFESSIONAL) {
    return (
      <Row>
        <Col md={{ size: 8, offset: 2 }}>
          <Card>
            <CardBody>
              <ProfessionalProfile history={history} user={user} match={match} />
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  } else {
    return (
      <Row>
        <Col md={{ size: 6, offset: 3 }}>
          <Card>
            <CardBody>
              <ClientProfile history={history} user={user} match={match} />
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

export default CreateProfile;
