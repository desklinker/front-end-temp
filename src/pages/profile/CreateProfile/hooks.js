import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { DASHBOARD } from '../../../constants/routes';
import { confirmUserDetails, loadUserDetails } from '../../../actions/users';

export function useConfirmUserDetails(history) {
  const dispatch = useDispatch();

  const onConfirmUserDetails = useCallback(
    async (user) => {
      const { userId } = user;
      try {
        await dispatch(confirmUserDetails(userId));
        await dispatch(loadUserDetails(user));
        history.push(DASHBOARD);
      } catch (e) {
        //
      }
    },
    [dispatch, history]
  );
  return { onConfirmUserDetails };
}
