import { useCallback, useState, useEffect } from 'react';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import { userServiceUrl } from '../../../../config/env';
import { useDispatch } from 'react-redux';
import {
  storeUniversity,
  loadQualification,
  updateQualificationDetails,
  storeQualificationDetails,
  removeQualification,
} from '../../../../actions/professionalProfile';
import moment from 'moment';
import get from 'lodash/get';
import { getAuthUser } from '../../../../helpers/authUtils';

const queryString = require('query-string');

//store university details
export function useStoreUniversity({ setOpen }) {
  const dispatch = useDispatch();

  const onStoreUniversity = useCallback(
    async (values, actions) => {
      actions.setSubmitting(true);
      try {
        await dispatch(storeUniversity(values));
        setOpen(false);
      } catch (e) {
        actions.setErrors(e.errors);
      }
      actions.setSubmitting(false);
    },
    [dispatch, setOpen]
  );
  return { onStoreUniversity };
}

export function useLoadQualification(userId) {
  const dispatch = useDispatch();
  const [items, setItems] = useState(null);

  const onLoadQualification = useCallback(async () => {
    try {
      const res = await dispatch(loadQualification(userId));
      if (res?.qualificationDetails.length) {
        setItems(formattedData(res.qualificationDetails));
      }
    } catch (e) {
      //
    }
  }, [dispatch, userId]);

  useEffect(() => {
    onLoadQualification();
  }, [onLoadQualification]);
  return { items };
}

//store user qualification details
export function useStoreQualificationDetails({ user }) {
  const dispatch = useDispatch();
  const { userId } = user;

  const onStoreQualificationDetails = useCallback(
    async (values, actions) => {
      actions.setSubmitting(true);
      values.forEach(async (element) => {
        let proofDocumentUrl = null;
        const formData = {
          fromDate: moment(element.fromDate, 'YYYY').unix(),
          toDate: moment(element.toDate, 'YYYY').unix(),
          qualificationPrefixId: element.qualification?.id,
          specialization: element.specialization,
        };

        let universityId;
        try {
          if (element.universityDetailsId) {
            universityId = element.universityDetailsId;
          } else {
            const res = await dispatch(
              storeUniversity({
                cityId: element.newUniversity.city.id,
                name: element.newUniversity.name,
              })
            );
            universityId = res ? res.udid : 1;
          }
          if (element.file) {
            const query = queryString.stringify({ objectKey: uuidv4() });
            const authUser = getAuthUser();
            const idToken = authUser ? authUser.idToken : null;

            // first get the pre-signed URL
            await axios
              .get(`${userServiceUrl}/v1/uploads/presignedUrl?${query}`, { headers: { DL_AUTH: idToken } })
              .then((response) => {
                const signedUrl = response.data.url;

                return axios.put(signedUrl, element.file, { headers: { 'Content-Type': element.file.type } });
              })
              .then((result) => {
                proofDocumentUrl = result.config?.url;
              })
              .catch((err) => {
                console.log(err);
              });
          }
          if (element.isEdit) {
            await dispatch(
              updateQualificationDetails(userId, element.qdid, {
                ...formData,
                universityDetailsId: universityId,
                proofDocumentUrl: proofDocumentUrl,
              })
            );
          } else {
            await dispatch(
              storeQualificationDetails(userId, {
                ...formData,
                universityDetailsId: universityId,
                proofDocumentUrl: proofDocumentUrl,
              })
            );
          }
        } catch (e) {
          actions.setErrors(e.errors);
        }
      });
      actions.setSubmitting(false);
    },
    [dispatch, userId]
  );

  return { onStoreQualificationDetails };
}

//remove user qualification
export function useRemoveQualification(userId) {
  const dispatch = useDispatch();

  const onRemoveQualification = useCallback(
    async (qdid) => {
      try {
        await dispatch(removeQualification(userId, qdid));
      } catch (e) {
        //
      }
    },
    [dispatch, userId]
  );

  return { onRemoveQualification };
}

const formattedData = (data) => {
  let fields = [];
  data &&
    data.map((item) => {
      return fields.push({
        isEdit: true,
        qdid: item.qdid,
        country: {
          id: get(item, 'universityDetails.city.country.cid'),
          title: get(item, 'universityDetails.city.country.name'),
        },
        qualification: {
          id: item.qualificationPrefix?.qpid,
          title: item.qualificationPrefix?.prefix,
        },
        university: {
          id: item.universityDetails?.udid,
          title: item.universityDetails?.name,
        },
        specialization: item.specialization,
        fromDate: {
          label: moment.unix(item.fromDate).format('YYYY'),
          value: moment.unix(item.fromDate).format('YYYY'),
        },
        toDate: {
          label: item.toDate ? moment.unix(item.toDate).format('YYYY') : 'Ongoing',
          value: item.toDate ? moment.unix(item.toDate).format('YYYY') : 'Ongoing',
        },
      });
    });
  return fields;
};
