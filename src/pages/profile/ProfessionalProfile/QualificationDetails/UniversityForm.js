import React from 'react';
import { Row, Col, FormGroup } from 'reactstrap';
import Dropdown from '../../../../components/formFields/Dropdown';
import TextField from '../../../../components/formFields/TextField';
import { configMgtUrl } from '../../../../config/env';

function UniversityForm({ fieldName, field, closeForm }) {
  return (
    <FormGroup className="dashed-border">
      <div className="d-flex flex-row-reverse">
        <p className="add-action-icon rounded-circle font-weight-light mb-0" onClick={() => closeForm()}>
          <i className="uil uil-multiply text-primary font-18"></i>
        </p>
      </div>
      <Row className="mx-1">
        <Col sm={7}>
          <TextField name={`${fieldName}.newUniversity.name`} type="text" placeholder="University Name" />
        </Col>
        <Col sm={5}>
          <Dropdown
            name={`${fieldName}.newUniversity.city`}
            placeholder="City"
            mgtUrl={configMgtUrl}
            url="app-configs/cities"
            resource="cities"
            listKey="cid"
            listLabel="name"
            relation="countryId"
            relationId={field.country?.id}
          />
        </Col>
      </Row>
    </FormGroup>
  );
}

export default UniversityForm;
