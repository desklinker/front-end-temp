import React, { Fragment, useState } from 'react';
import { Formik, Form } from 'formik';
import { object, string, array } from 'yup';
import Select from 'react-select';
import { Button, Row, Col, Collapse } from 'reactstrap';
import TextField from '../../../../components/formFields/TextField';
import Dropdown from '../../../../components/formFields/Dropdown';
import { years } from '../../../../helpers/date';
import UniversityForm from './UniversityForm';
import { filter, includes } from 'lodash';
import DocumentUploader from '../../../../components/DocumentUploder';
import { useLoadQualification, useRemoveQualification } from './hooks';
import SubmitButton from '../../components/SubmitButton';
import FieldError from '../../components/FieldError';
import { configMgtUrl, userMgtUrl } from '../../../../config/env';

const validationSchema = object().shape({
  fields: array().of(
    object().shape({
      fromDate: string().required('Start year field is required'),
      toDate: string().required('End year field is required'),
      specialization: string().required('Specialization field is required'),
      qualification: object().required('Qualification field is required').nullable(),
      university: object().required('University field is required').nullable(),
      country: object().required('Country field is required').nullable(),
    })
  ),
});

function QualificationDetailsForm({ isEdit, title, next, skip, previous, user, onSubmit }) {
  const [settings, setSettings] = useState([]);
  const { items } = useLoadQualification(user.userId);
  const { onRemoveQualification } = useRemoveQualification(user.userId);

  const handleAddInput = (props) => {
    const fieldValues = [...props.values.fields];
    fieldValues.push({
      country: '',
      qualification: '',
      university: '',
      stream: '',
      specialization: '',
      fromDate: '',
      toDate: '',
      file: '',
    });
    props.setFieldValue('fields', fieldValues);
  };

  const handleRemoveInput = (key, props) => {
    const fieldValues = [...props.values.fields];
    fieldValues.splice(key, 1);
    props.setFieldValue('fields', fieldValues);
  };

  const handleSettings = (fieldName, remove) => {
    if (remove) {
      const temData = filter(settings, (item) => {
        return item !== fieldName;
      });
      setSettings(temData);
    } else if (!activeForm(fieldName)) {
      setSettings([...settings, fieldName]);
    }
  };

  const activeForm = (fieldName) => {
    return includes(settings, fieldName);
  };

  const handleSubmit = (values, actions) => {
    const formattedValues = values.fields.map((field) => {
      return {
        ...field,
        universityDetailsId: field.university?.id,
        toDate: field.toDate?.value !== 'Ongoing' ? field.toDate?.value : '',
        fromDate: field.fromDate?.value,
      };
    });

    onSubmit(formattedValues, actions);
    next();
  };

  const closeUniversityForm = (fieldName, props) => {
    handleSettings(fieldName, true);
    props.setFieldValue(`${fieldName}.newUniversity.name`, '');
    props.setFieldValue(`${fieldName}.newUniversity.city`, []);
    props.setFieldValue(`${fieldName}.university`, '');
  };

  return (
    <div>
      <Formik
        validateOnBlur={false}
        initialValues={{
          fields: !items
            ? [
                {
                  country: '',
                  qualification: '',
                  university: '',
                  specialization: '',
                  fromDate: '',
                  toDate: '',
                },
              ]
            : items,
        }}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
        enableReinitialize>
        {(props) => (
          <Form className="form-custom">
            <div className="d-flex justify-content-between mb-1">
              <h4 className="header-title mt-0">{title ? title : 'Experience Details'}</h4>
              <p
                className="my-0 add-action-icon rounded-circle font-weight-light"
                onClick={() => handleAddInput(props)}>
                <i className="uil uil-plus text-primary font-24"></i>
              </p>
            </div>
            {props.values &&
              props.values.fields.map((field, key) => {
                const fieldName = `fields.${key}`;
                return (
                  <Fragment key={`${field}-${key}`}>
                    {key !== 0 && (
                      <div className="border-top d-flex flex-row-reverse">
                        <p
                          className="my-1 add-action-icon rounded-circle font-weight-light"
                          onClick={() => {
                            handleRemoveInput(key, props);
                            if (field.qdid) {
                              onRemoveQualification(field.qdid);
                            }
                          }}>
                          <i className="uil uil-multiply text-primary font-18"></i>
                        </p>
                      </div>
                    )}

                    <Row form>
                      <Col md={3}>
                        <Dropdown
                          name={`${fieldName}.country`}
                          placeholder="Country"
                          mgtUrl={configMgtUrl}
                          url="app-configs/countries"
                          resource="countries"
                          listKey="cid"
                          listLabel="name"
                          clearField={() => props.setFieldValue(`${fieldName}.university`, '')}
                        />
                      </Col>
                      <Col md={9}>
                        <Dropdown
                          name={`${fieldName}.university`}
                          placeholder="University"
                          mgtUrl={userMgtUrl}
                          url="users/universities"
                          resource="universities"
                          listKey="udid"
                          listLabel="name"
                          relation="countryId"
                          relationId={field.country?.id}
                          isDisabled={!field.country}
                          setOpen={() => handleSettings(fieldName)}
                          clearField={() => closeUniversityForm(fieldName, props)}
                          addNew
                        />
                      </Col>
                    </Row>

                    <Collapse isOpen={activeForm(fieldName)} className="navbar-collapse" id="topnav-menu-content">
                      <UniversityForm
                        fieldName={fieldName}
                        field={field}
                        closeForm={() => closeUniversityForm(fieldName, props)}
                      />
                    </Collapse>

                    <Row form>
                      <Col md={4}>
                        <Dropdown
                          name={`${fieldName}.qualification`}
                          placeholder="Qualification"
                          mgtUrl={userMgtUrl}
                          resource="qualificationPrefixes"
                          listKey="qpid"
                          listLabel="prefix"
                          url="users/qualifications/prefixes"
                        />
                      </Col>

                      <Col md={4}>
                        <TextField name={`${fieldName}.specialization`} type="text" placeholder="Specialization" />
                      </Col>

                      <Col md={2}>
                        <Select
                          placeholder="Start Year"
                          value={field.fromDate}
                          className="react-select"
                          classNamePrefix="react-select"
                          onChange={(value) => {
                            props.setFieldValue(`${fieldName}.fromDate`, value);
                          }}
                          options={years(false)}
                        />
                        <FieldError props={props} name={`${fieldName}.fromDate`} />
                      </Col>
                      <Col md={2}>
                        <Select
                          placeholder="End Year"
                          value={field.toDate}
                          className="react-select"
                          classNamePrefix="react-select"
                          onChange={(value) => {
                            props.setFieldValue(`${fieldName}.toDate`, value);
                          }}
                          options={years(true, field.fromDate)}
                        />

                        <FieldError props={props} name={`${fieldName}.toDate`} />
                      </Col>
                    </Row>
                    <DocumentUploader
                      onUpload={(files) => {
                        props.setFieldValue(`${fieldName}.file`, files);
                      }}
                    />
                  </Fragment>
                );
              })}

            <Row className="mt-2">
              <Col className="text-left">
                {previous && (
                  <Button onClick={previous} color="info">
                    Previous
                  </Button>
                )}
              </Col>
              <Col className="text-right">
                {skip && (
                  <Button color="secondary" className="mr-1" onClick={() => next()}>
                    Setup Later
                  </Button>
                )}

                <SubmitButton isSubmitting={props.isSubmitting} next={next} isEdit={isEdit} isItems={!!items} />
              </Col>
            </Row>
          </Form>
        )}
      </Formik>
    </div>
  );
}

export default QualificationDetailsForm;
