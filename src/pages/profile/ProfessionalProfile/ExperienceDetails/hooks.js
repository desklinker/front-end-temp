import { useCallback, useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import {
  loadExperience,
  updateCompanyDetails,
  updateExperienceDetails,
  storeCompanyDetails,
  storeExperienceDetails,
  removeExperienceDetails
} from '../../../../actions/professionalProfile';
import moment from 'moment';
import get from 'lodash/get';

//Store and edit experience details
export function useStoreExperienceDetails({ user }) {
  const dispatch = useDispatch();
  const { userId } = user;

  const onStoreExperienceDetails = useCallback(
    async (values, actions) => {
      actions.setSubmitting(true);

      values.fields.forEach(async (element) => {
        const formData = {
          fromDate: moment(element.fromDate, 'YYYY-m-d').unix(),
          toDate: moment(element.toDate, 'YYYY-m-d').unix(),
          position: element.position,
        };

        try {
          if (element.isEdit) {
            await dispatch(
              updateCompanyDetails(element.companyId, { cityId: element.city?.id, name: element.companyName })
            );
            await dispatch(
              updateExperienceDetails(userId, element.wedid, {
                ...formData,
                workExperienceCompanyDetailsId: element.wecdid,
              })
            );
          } else {
            const res = await dispatch(storeCompanyDetails({ cityId: element.city?.id, name: element.companyName }));
            if (res) {
              await dispatch(
                storeExperienceDetails(userId, {
                  ...formData,
                  workExperienceCompanyDetailsId: res.wecdid,
                })
              );
            }
          }
        } catch (e) {
          actions.setErrors(e.errors);
        }
      });

      actions.setSubmitting(false);
    },
    [dispatch, userId]
  );
  return { onStoreExperienceDetails };
}

//Load experience details
export function useLoadExperience({ user }) {
  const dispatch = useDispatch();
  const { userId } = user;

  const [item, setItem] = useState(null);

  const onLoadExperience = useCallback(async () => {
    try {
      const res = await dispatch(loadExperience(userId));
      if (res?.workExperiences.length) {
        setItem(formattedData(res.workExperiences));
      }
    } catch (e) {
      //
    }
  }, [dispatch, userId]);

  useEffect(() => {
    onLoadExperience();
  }, [onLoadExperience]);
  return { item };
}

//Remove user experience details
export function useRemoveExperienceDetails(userId) {
  const dispatch = useDispatch();

  const onRemoveExperienceDetails = useCallback(
    async (wedid) => {
      try {
        await dispatch(removeExperienceDetails(userId, wedid));
      } catch (e) {
        //
      }
    },
    [dispatch, userId]
  );

  return { onRemoveExperienceDetails };
}


//Format response data
const formattedData = (data) => {
  let fields = [];
  data &&
    data.map((item) => {
      return fields.push({
        isEdit: true,
        wedid: item.wedid,
        country: {
          id: get(item, 'companyDetails.city.country.cid'),
          title: get(item, 'companyDetails.city.country.name'),
        },
        city: {
          id: get(item, 'companyDetails.city.cid'),
          title: get(item, 'companyDetails.city.name'),
        },
        companyName: item.companyDetails?.name,
        companyId: item.companyDetails?.wecdid,
        position: item.position,
        fromDate: moment.unix(item.fromDate)._d,
        toDate: item.toDate ? moment.unix(item.toDate)._d : "Ongoing",
      });
    });
  return fields;
};
