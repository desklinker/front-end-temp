import React from 'react';
import { Form, Formik } from 'formik';
import { object, string, array, boolean } from 'yup';
import { Button, Row, Col, CustomInput, FormGroup, FormFeedback } from 'reactstrap';
import TextField from '../../../../components/formFields/TextField';
import moment from 'moment';
import Flatpickr from 'react-flatpickr';
import Dropdown from '../../../../components/formFields/Dropdown';
import get from 'lodash/get';
import { useLoadExperience, useRemoveExperienceDetails } from './hooks';
import SubmitButton from '../../components/SubmitButton';
import { configMgtUrl } from '../../../../config/env';

const validationSchema = object().shape({
  isWorking: boolean().required(),
  fields: array().of(
    object().shape({
      position: string().required('Position field is required'),
      fromDate: string().required('From date field is required'),
      toDate: string().required('To date field is required'),
      companyName: string().required('Company name field is required'),
      country: object().required('Company country field is required'),
      city: object().required('Company city field is required'),
    })
  ),
});

function ExperienceDetailsForm({ isEdit, title, user, next, skip, previous, onSubmit }) {
  const { item } = useLoadExperience({ user });
  const { onRemoveExperienceDetails } = useRemoveExperienceDetails(user.userId);

  const handleAddInput = (props) => {
    const fieldValues = [...props.values.fields];
    fieldValues.push({
      position: '',
      fromDate: '',
      toDate: '',
      companyName: '',
      country: '',
      city: '',
    });
    props.setFieldValue('fields', fieldValues);
  };

  const handleRemoveInput = (key, props) => {
    const fieldValues = [...props.values.fields];
    fieldValues.splice(key, 1);
    props.setFieldValue('fields', fieldValues);
  };

  const handleSubmit = (values, actions) => {
    onSubmit(values, actions);
    next();
  };

  return (
    <div>
      <h4 className="header-title mt-0">{title ? title : 'Add Work Experience'}</h4>
      <hr />
      <Formik
        validateOnBlur={false}
        enableReinitialize
        initialValues={{
          isWorking: item ? !!item[0].toDate : true,
          fields: !item
            ? [
                {
                  position: '',
                  fromDate: '',
                  toDate: 'Ongoing',
                  companyName: '',
                  country: '',
                  city: '',
                },
              ]
            : item,
        }}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}>
        {(props) => (
          <Form className="form-custom">
            <FormGroup>
              <CustomInput
                className="text-muted font-weight-normal"
                type="checkbox"
                id="working"
                label="I am currently working in this role"
                checked={props.values?.isWorking}
                onChange={() => {
                  props.setFieldValue('isWorking', !props.values.isWorking);
                  if (!props.values.isWorking) {
                    props.setFieldValue('fields.0.toDate', 'Ongoing');
                  } else {
                    props.setFieldValue('fields.0.toDate', '');
                  }
                }}
              />
            </FormGroup>

            {props.values &&
              props.values.fields.map((field, key) => {
                const fieldName = `fields.${key}`;
                const isShowField = (key === 0 && !props.values.isWorking) || key > 0;

                return (
                  <React.Fragment key={`${field}-${key}`}>
                    {key !== 0 && (
                      <div className="border-top d-flex flex-row-reverse">
                        <p
                          className="my-1 add-action-icon rounded-circle font-weight-light"
                          onClick={() => {
                            handleRemoveInput(key, props);
                            if (field.wedid) {
                              onRemoveExperienceDetails(field.wedid);
                            }
                          }}>
                          <i className="uil uil-multiply text-primary font-18"></i>
                        </p>
                      </div>
                    )}
                    <Row form>
                      <Col md={6}>
                        <TextField name={`${fieldName}.position`} type="text" placeholder="Position" />
                      </Col>
                      <Col md={3}>
                        <Flatpickr
                          className="form-control bg-white"
                          value={field.fromDate}
                          placeholder="From Date"
                          onChange={(date) => {
                            props.setFieldValue(`${fieldName}.fromDate`, date[0]);
                          }}
                          options={{ dateFormat: 'Y-m-d', maxDate: moment()._d }}
                        />
                        {get(props.touched, `${fieldName}.fromDate`) && get(props.errors, `${fieldName}.fromDate`) && (
                          <FormFeedback className="d-block">{get(props.errors, `${fieldName}.fromDate`)}</FormFeedback>
                        )}
                      </Col>

                      <Col md={3}>
                        {isShowField ? (
                          <Flatpickr
                            className="form-control bg-white"
                            value={field.toDate}
                            placeholder="To Date"
                            onChange={(date) => {
                              props.setFieldValue(`${fieldName}.toDate`, date[0]);
                            }}
                            options={{
                              dateFormat: 'Y-m-d',
                              minDate: moment(field.fromDate)._d,
                              maxDate: moment()._d,
                            }}
                          />
                        ) : (
                          <FormGroup>
                            To Date: <span className="pl-1">Present </span>
                          </FormGroup>
                        )}
                        {get(props.errors, `${fieldName}.toDate`) && (
                          <FormFeedback className="d-block">{get(props.errors, `${fieldName}.toDate`)}</FormFeedback>
                        )}
                      </Col>
                    </Row>

                    <Row form>
                      <Col md={6}>
                        <TextField name={`${fieldName}.companyName`} type="text" placeholder="Company Name" />
                      </Col>
                      <Col md={3}>
                        <Dropdown
                          name={`${fieldName}.country`}
                          placeholder="Country"
                          mgtMgt={configMgtUrl}
                          url="app-configs/countries"
                          resource="countries"
                          listKey="cid"
                          listLabel="name"
                          clearField={() => props.setFieldValue(`${fieldName}.city`, '')}
                        />
                      </Col>
                      <Col md={3}>
                        <Dropdown
                          name={`${fieldName}.city`}
                          placeholder="City"
                          mgtMgt={configMgtUrl}
                          url="app-configs/cities"
                          resource="cities"
                          listKey="cid"
                          listLabel="name"
                          relation="countryId"
                          relationId={field.country?.id}
                          isDisabled={!field.country}
                        />
                      </Col>
                    </Row>
                  </React.Fragment>
                );
              })}

            <Button color="primary" size="sm" className="mr-1 mb-3" onClick={() => handleAddInput(props)}>
              Add Past Experience
            </Button>
            <Row className="mt-2">
              <Col className="left-right">
                {previous && (
                  <Button onClick={previous} color="info">
                    Previous
                  </Button>
                )}
              </Col>
              <Col className="text-right">
                {skip && (
                  <Button color="secondary" className="mr-1" onClick={() => next()}>
                    Setup Later
                  </Button>
                )}

                <SubmitButton isSubmitting={props.isSubmitting} next={next} isEdit={isEdit} isItems={!!item} />
              </Col>
            </Row>
          </Form>
        )}
      </Formik>
    </div>
  );
}

export default ExperienceDetailsForm;
