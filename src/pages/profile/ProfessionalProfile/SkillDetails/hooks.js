import { useCallback, useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import map from 'lodash-es/map';
import {
  fetchFields,
  storeSkillDetails,
  updateFieldDetails,
  loadFieldAndSkills,
} from '../../../../actions/professionalProfile';
import { confirmUserDetails, loadUserDetails } from '../../../../actions/users';

//fetch all fields
export function useFetchFields() {
  const dispatch = useDispatch();
  const [fields, setFields] = useState(null);

  const onfetchFields = useCallback(async () => {
    try {
      const res = await dispatch(fetchFields());
      setFields(res.fields);
    } catch (e) {
      //
    }
  }, [dispatch]);

  useEffect(() => {
    let mounted = true;

    if (mounted) {
      onfetchFields();
    }
    return () => (mounted = false);
  }, [onfetchFields]);
  return { fields };
}

//store user skill details
export function useStoreSkillDetails({ user }) {
  const dispatch = useDispatch();
  const { userId } = user;

  const onStoreSkillDetails = useCallback(
    async (values, actions) => {
      actions.setSubmitting(true);
      const skills = map(values.skill, 'id');
      try {
        await dispatch(storeSkillDetails(userId, { skillsIdList: skills }));
        await dispatch(updateFieldDetails(userId, { fieldId: values.field.fid }));
        await dispatch(confirmUserDetails(userId));
        await dispatch(loadUserDetails(user));
      } catch (e) {
        actions.setErrors(e.errors);
      }
      actions.setSubmitting(false);
    },
    [dispatch, user, userId]
  );

  return { onStoreSkillDetails };
}

export function useLoadFieldAndSkills(userId) {
  const dispatch = useDispatch();
  const [items, setItems] = useState(null);

  const onLoadFieldAndSkills = useCallback(async () => {
    try {
      const res = await dispatch(loadFieldAndSkills(userId));
      if (res) {
        setItems(formattedData(res));
      }
    } catch (e) {
      //
    }
  }, [dispatch, userId]);

  useEffect(() => {
    onLoadFieldAndSkills();
  }, [onLoadFieldAndSkills]);
  return { items };
}

const formattedData = (item) => {
  return {
    isEdit: true,
    field: item.field,
    skill: item.skills.map((skill) => {
      return {
        id: skill.sid,
        title: skill.name,
      };
    }),
  };
};
