import React from 'react';
import { Formik, Form } from 'formik';
import { object, string, array } from 'yup';
import Dropdown from '../../../../components/formFields/Dropdown';

import { Button, Row, Col, FormGroup, CustomInput, Label, FormFeedback } from 'reactstrap';
import { useFetchFields, useLoadFieldAndSkills } from './hooks';
import SubmitButton from '../../components/SubmitButton';
import { configMgtUrl } from '../../../../config/env';

const validationSchema = object().shape({
  field: string().required('Field selection is required'),
  skill: array().required('Skill field is required').nullable(),
});

function SkillDetailsForm({ isEdit, title, user, next, skip, onSubmit }) {
  const { fields } = useFetchFields();
  const { items } = useLoadFieldAndSkills(user.userId);
  const handleSubmit = (values, actions) => {
    onSubmit(values, actions);
    next();
  };

  return (
    <div>
      <h4 className="header-title mt-0">{title ? title : 'Bank Details'}</h4>
      <hr />

      <Formik
        enableReinitialize
        initialValues={
          !items
            ? {
                field: '',
                skills: [],
              }
            : items
        }
        validationSchema={validationSchema}
        onSubmit={handleSubmit}>
        {(props) => (
          <Form className="form-custom">
            <Label for="field">Field</Label>

            <FormGroup row>
              {fields &&
                fields.map((field, key) => (
                  <Col sm={6} key={`field-${key}`}>
                    <FormGroup className="border p-2 rounded">
                      <CustomInput
                        className="text-muted font-weight-normal"
                        type="radio"
                        id={field.fid}
                        label={field.name}
                        checked={props.values.field?.fid === field.fid}
                        onChange={() => {
                          props.setFieldValue('field', field);
                          props.setFieldValue('skill', []);
                        }}
                        inline
                      />
                    </FormGroup>
                  </Col>
                ))}
              {props.touched.field && props.errors.field && (
                <FormFeedback className="d-block ml-2">{props.errors.field}</FormFeedback>
              )}
            </FormGroup>

            <Dropdown
              isMulti
              label="Skills"
              name="skill"
              placeholder="Select..."
              mgtUrl={configMgtUrl}
              url="app-configs/skills"
              resource="skills"
              listKey="sid"
              listLabel="name"
              relation="fieldId"
              relationId={props.values.field?.fid}
              isDisabled={!props.values.field?.fid}
            />

            <Row className="mt-2">
              <Col className="text-right">
                {skip && (
                  <Button color="secondary" className="mr-1" onClick={() => next()}>
                    Skip
                  </Button>
                )}

                <SubmitButton isSubmitting={props.isSubmitting} next={next} isEdit={isEdit} isItems={!!items} />
              </Col>
            </Row>
          </Form>
        )}
      </Formik>
    </div>
  );
}

export default SkillDetailsForm;
