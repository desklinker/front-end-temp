import React from 'react';
import { Formik, Form } from 'formik';
import { object, string } from 'yup';
import { Button, Row, Col } from 'reactstrap';
import TextField from '../../../../components/formFields/TextField';
import Dropdown from '../../../../components/formFields/Dropdown';
import { useLoadBankDetail } from './hooks';
import SubmitButton from '../../components/SubmitButton';
import { configMgtUrl, userMgtUrl } from '../../../../config/env';

const validationSchema = object().shape({
  branch: string().required('Branch name field is required'),
  country: object().required('Country field is required').nullable(),
  accountNumber: string().required('Account number field is required'),
  bank: object().required('Bank name field is required').nullable(),
});

function BankDetailsForm({ isEdit, title, next, user, skip, previous, onSubmit }) {
  const { items } = useLoadBankDetail(user.userId);

  const handleSubmit = (values, actions) => {
    onSubmit(
      {
        ...values,
        bankId: values.bank?.id,
      },
      actions
    );
    next();
  };

  return (
    <div>
      <h4 className="header-title mt-0">{title ? title : 'Bank Details'}</h4>
      <hr />

      <Formik
        validateOnBlur={false}
        enableReinitialize
        initialValues={
          !items
            ? {
                country: '',
                bank: '',
                branch: '',
                accountNumber: '',
              }
            : items
        }
        validationSchema={validationSchema}
        onSubmit={handleSubmit}>
        {(props) => (
          <Form className="form-custom">
            <Row form>
              <Col sm={4}>
                <Dropdown
                  name="country"
                  placeholder="Country"
                  mgtUrl={configMgtUrl}
                  url="app-configs/countries"
                  resource="countries"
                  listKey="cid"
                  listLabel="name"
                  clearField={() => props.setFieldValue('bank', '')}
                />
              </Col>
              <Col sm={8}>
                <Dropdown
                  name="bank"
                  placeholder="Bank Name"
                  mgtUrl={userMgtUrl}
                  url="users/banks"
                  resource="banks"
                  listKey="bid"
                  listLabel="bankName"
                  relation="cityId"
                  relationId={props.values.country?.id}
                  isDisabled={!props.values.country}
                />
              </Col>
            </Row>

            <Row form>
              <Col sm={4}>
                <TextField name="branch" type="text" placeholder="Branch Name" />
              </Col>
              <Col sm={8}>
                <TextField name="accountNumber" type="text" placeholder="Account Number" />
              </Col>
            </Row>

            <Row className="mt-2">
              <Col className="left-right">
                {previous && (
                  <Button onClick={previous} color="info">
                    Previous
                  </Button>
                )}
              </Col>
              <Col className="text-right">
                {skip && (
                  <Button color="secondary" className="mr-1" onClick={() => next()}>
                    Setup Later
                  </Button>
                )}

                <SubmitButton isSubmitting={props.isSubmitting} next={next} isEdit={isEdit} isItems={!!items} />
              </Col>
            </Row>
          </Form>
        )}
      </Formik>
    </div>
  );
}

export default BankDetailsForm;
