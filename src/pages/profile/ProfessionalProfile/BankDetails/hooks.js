import { useCallback, useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { storeBankDetails, loadBankDetail, updateBankDetails } from '../../../../actions/professionalProfile';
import get from 'lodash/get';

//store bank details
export function useStoreBankDetails({ user }) {
  const dispatch = useDispatch();
  const { userId } = user;

  const onStoreBankDetails = useCallback(
    async (values, actions) => {
      actions.setSubmitting(true);
      try {
        if (values.isEdit) {
          await dispatch(updateBankDetails(userId, values));
        } else {
          await dispatch(storeBankDetails(userId, values));
        }
      } catch (e) {
        actions.setErrors(e.errors);
      }
      actions.setSubmitting(false);
    },
    [dispatch, userId]
  );
  return { onStoreBankDetails };
}

export function useLoadBankDetail(userId) {
  const dispatch = useDispatch();
  const [items, setItems] = useState(null);

  const onLoadBankDetail = useCallback(async () => {
    try {
      const res = await dispatch(loadBankDetail(userId));

      if (res) {
        setItems(formattedData(res));
      }
    } catch (e) {
      //
    }
  }, [dispatch, userId]);

  useEffect(() => {
    onLoadBankDetail();
  }, [onLoadBankDetail]);
  return { items };
}

const formattedData = (item) => {
  return {
    isEdit: true,
    country: {
      id: get(item, 'bank.city.country.cid'),
      title: get(item, 'bank.city.country.name'),
    },
    bank: {
      id: item.bank?.bid,
      title: item.bank?.bankName,
    },

    accountNumber: item.accountNumber,
    branch: item.branch,
  };
};
