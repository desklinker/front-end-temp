import React from 'react';

function FilterButton({ filter, toggle }) {
    return (
        <div className="grid-filter-wrap">
            <span
                onClick={() => toggle('clientDetail')}
                className={`filter-item ${filter === 'clientDetail' ? 'active' : ''}`}>
                1
            </span>
            <span
                onClick={() => toggle('codiDetail')}
                className={`filter-item ${filter === 'codiDetail' ? 'active' : ''}`}>
                2
            </span>
            <span
                onClick={() => toggle('deliverDetail')}
                className={`filter-item ${filter === 'deliverDetail' ? 'active' : ''}`}>
                3
            </span>
            <span onClick={() => toggle('thread')} className={`filter-item ${filter === 'thread' ? 'active' : ''}`}>
                <i className="mdi mdi-message-text"></i>
            </span>
            <span onClick={() => toggle('all')} className={`filter-item ${filter === 'all' ? 'active' : ''}`}>
                <i className="mdi mdi-expand-all"></i>
            </span>
        </div>
    );
}

export default FilterButton;
