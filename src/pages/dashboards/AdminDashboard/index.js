import React, { Fragment, useState } from 'react';
import { Row, Col, Collapse, Button } from 'reactstrap';
import { items } from './data';
import FilterButton from './FilterButton';
import WidgetFilter from './WidgetFilter';
import FilterForm from './WidgetFilter/FilterForm';
import CardView from './CardView';
import ListView from './ListView';

function AdminDashboard() {
    const [filter, setFilter] = useState(null);
    const [widgetFilter, setWidgetFilter] = useState(null);
    const [widgetView, setWidgetView] = useState('card');
    const [isOpen, setOpen] = useState(false);
    const [sort, setSort] = useState('asc');

    const handleToggle = (value) => {
        if (filter === value) {
            setFilter(null);
        } else {
            setFilter(value);
        }
    };

    const handleWidgetToggle = () => {
        setOpen(!isOpen);
    };

    return (
        <Fragment>
            <Row>
                <Col lg={5} md={2} >
                    <div className="page-title-box mt-2">
                        <h4 className="page-title mb-0 d-inline-block">Dashboard</h4>
                    </div>
                </Col>
                <Col lg={3} md={5} className="align-self-center">
                    <div className="page-title-box mt-2">
                        {widgetView === 'card' && (
                            <FilterButton filter={filter} toggle={(value) => handleToggle(value)} />
                        )}
                    </div>
                </Col>
                <Col lg={2} md={3} className="align-self-center">
                    <div className="page-title-box mt-2">
                        <WidgetFilter
                            toggle={() => handleWidgetToggle()}
                            sort={sort}
                            setSort={setSort}
                            filter={widgetFilter}
                            setFilter={setWidgetFilter}
                        />
                    </div>
                </Col>
                <Col lg={2} md={2} className="align-self-center text-right">
                    <div className="page-title-box mt-2">
                        <div className="btn-group ml-2">
                            <Button
                                color={widgetView === 'card' ? 'secondary' : 'link'}
                                onClick={() => setWidgetView('card')}>
                                <i className="dripicons-view-apps"></i>
                            </Button>
                        </div>
                        <div className="btn-group d-inline-block">
                            <Button
                                color={widgetView === 'list' ? 'secondary' : 'text-muted'}
                                onClick={() => setWidgetView('list')}>
                                <i className="dripicons-checklist"></i>
                            </Button>
                        </div>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col>
                    <Collapse isOpen={isOpen}>
                        <FilterForm setFilter={setWidgetFilter} setOpen={setOpen} filter={widgetFilter} />
                    </Collapse>
                </Col>
            </Row>
            <Row>
                <Col>
                    {widgetView === 'card' ? (
                        <CardView items={items} filter={filter} toggle={(value) => handleToggle(value)} />
                    ) : (
                        <ListView />
                    )}
                </Col>
            </Row>
        </Fragment>
    );
}

export default AdminDashboard;
