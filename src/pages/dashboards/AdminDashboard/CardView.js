import React from 'react';
import ProjectWidget from './ProjectWidget';

function CardView({ items, filter, toggle }) {
    return (
        <>
            <ProjectWidget item={items[0]} filter={filter} toggle={(value) => toggle(value)} />
        </>
    );
}

export default CardView;
