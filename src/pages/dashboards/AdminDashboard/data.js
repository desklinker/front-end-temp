export const items = [
    {
        client: {
            name: 'Dominic Keller',
            rating: 4,
            noOfExe: 5,
        },
        project: {
            name: 'Minton v3.0 - Redesign',
            category: 'Civil',
        },
        budget: 'S$ 4000',
        duration: '36 months',
        screenBudget: 'S$ 3950',
        codiBudget: 'S$ 4000',
        postedTime: '01/02/2020, 14:50',
        scrPosted: {
            id: 'Scr-00024',
            time: '01/02/2020, 14:50',
        },
        cordiPosted: {
            id: 'Codi-000214',
            time: '01/02/2020, 14:50',
        },
        bid: {
            status: 'Ongoing',
            codi: {
                name: 'Brandon Smith',
                rating: 4.5,
                completedProjects: 2,
            },
            profDivision: {
                total: 'S$300',
                designer: {
                    budget: 'S$ 100',
                    duration: '2 months',
                },
                drafter: {
                    budget: 'S$ 100',
                    duration: '2 months',
                },
                architect: {
                    budget: 'S$ 100',
                    duration: '2 months',
                },
            },
        },
        outputDelivery: {
            status: 'Ongoing',
            updatedAt: '03/06/2020, 12:25',
            paymentStatus: 'Pending',
            revision:
                'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
        },
    },
];

export const professionals = [
    { value: 1, label: 'Judith D Swilley' },
    { value: 2, label: 'McGowan' },
    { value: 3, label: 'Judith D Swilley' },
    { value: 4, label: 'McGowan' },
];

export const timeFrameOptions = [
    { value: 'hours', label: 'Hours' },
    { value: 'days', label: 'Days' },
    { value: 'months', label: 'Months' },
];

export const project = {
    title: 'Minton v3.0 - Redesign',
    mainProject: 'Main Project Name',
    shortDesc: 'This card has supporting text below as a natural lead-in to additional content is a little bit longer',
    state: 'Bidding on process',
    totalTasks: 81,
    totalComments: 103,
    totalMembers: 6,
    startDate: '17 March 2018',
    startTime: '1:00 PM',
    duration: '45',
    budget: '$800',
};

export const projectFormData = {
    project: 'Minton v3.0 - Redesign',
    projectCategory: { value: '2', label: 'Mechanical Engineering' },
    mainProject: 'Main Project Name',
    shortDesc: 'This card has supporting text below as a natural lead-in to additional content is a little bit longer',
    state: 'Bidding on process',
    totalTasks: 81,
    totalComments: 103,
    totalMembers: 6,
    startDate: '17 March 2018',
    startTime: '1:00 PM',
    duration: '45',
    totalBudget: '$800',
};

export const currencyOptions = [
    { value: 'USD', label: 'USD' },
    { value: 'SGD', label: 'SGD' },
    { value: '£', label: '£' },
    { value: 'AUD', label: 'AUD' },
];

export const skills = [
    { value: 'cad', label: 'CAD' },
    { value: 'design', label: 'Design' },
    { value: 'arch', label: 'Architecture' },
    { value: 'draft', label: 'Draft' },
];

export const ongoingStatus = [
    { value: '1', label: 'Ongoing' },
    { value: '2', label: 'Completed' },
];

export const coordinators = [
    { value: '1', label: 'Brandon Smith (2)' },
    { value: '2', label: 'Thomazin Ursula (6)' },
    { value: '3', label: 'Isabell Esabell (2)' },
    { value: '4', label: 'Jennat Marion (1)' },
];

export const countries = [
    { value: 'sin', label: 'Singapore' },
    { value: 'aus', label: 'Australia' },
];

export const projectStatus = [
    { value: '1', label: 'Just posted' },
    { value: '2', label: 'Screened' },
    { value: '3', label: 'Bid on progress' },
    { value: '4', label: 'Project ongoing' },
    { value: '5', label: 'Project completed' },
    { value: '6', label: 'Pending for payment' },
    { value: '7', label: 'Revision on progress' },
    { value: '8', label: 'Project completed > 14 days' },
    { value: '9', label: 'Project completed < 14 days' },
    { value: '10', label: 'Terminated projects' },
];

export const projectCategories = [
    { value: '1', label: 'Civil Engineering' },
    { value: '2', label: 'Mechanical Engineering' },
    { value: '3', label: 'Computer Science' },
];

export const outputDeliverStatus = [
    { value: '1', label: 'Drafts passed' },
    { value: '2', label: 'Pending client confirmation' },
    { value: '3', label: 'Final output passed' },
    { value: '4', label: 'Revisions on progress' },
    { value: '5', label: 'Scope changes on progress' },
];

export const paymentStatus = [
    { value: '1', label: 'Pending' },
    { value: '2', label: 'Success' },
    { value: '3', label: 'Failed' },
];
