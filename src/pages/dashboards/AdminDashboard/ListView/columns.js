import React from 'react';
import { Link } from 'react-router-dom';
import startCase from 'lodash-es/startCase';
import lowerCase from 'lodash-es/lowerCase';

const badges = {
    pendingClientConfirmation: 'badge-danger-lighten',
    draftsPassed: 'badge-info-lighten',
    finalOutputPassed: 'badge-success-lighten',
    revisionsOnProgress: 'badge-secondary-lighten',
    scopeChangesOnProgress: 'badge-primary-lighten',
    pending: 'badge-info-lighten',
    success: 'badge-success-lighten',
    failed: 'badge-danger-lighten',
    ongoing: 'badge-info-lighten',
    completed: 'badge-success-lighten',
};

const ProjectColumn = (cell, row, rowIndex, extraData) => {
    return (
        <Link to={`projects/${row.id}`} className="text-body font-weight-semibold" target="_blank">
            {cell}
        </Link>
    );
};

const statusColumn = (cell, row, rowIndex, extraData) => {
    return <span className={`badge ${badges[cell]}`}>{startCase(lowerCase(cell))}</span>;
};

const ActionColumn = (cell, row, rowIndex, extraData) => {
    return (
        <>
            <span>
                <i className="mdi mdi-message-text-outline"></i>
            </span>
            <span className="ml-2">
                <i className="mdi mdi-bell-outline"></i>
            </span>
        </>
    );
};

export const columns = [
    {
        dataField: 'budget',
        text: 'Budget',
        sort: true,
    },
    {
        dataField: 'title',
        text: 'Project Name',
        sort: true,
        formatter: ProjectColumn,
    },
    {
        dataField: 'coordinator',
        text: 'Coordinator Name',
        sort: true,
    },
    {
        dataField: 'duration',
        text: 'Duration',
        sort: true,
    },
    {
        dataField: 'noOfProfessionals',
        text: 'No of Professionals',
        sort: true,
    },
    {
        dataField: 'biddingStatus',
        text: 'Bidding Status',
        sort: true,
        formatter: statusColumn,
    },
    {
        dataField: 'outputDeliveryStatus',
        text: 'Output Delivery Status',
        sort: true,
        formatter: statusColumn,
    },
    {
        dataField: 'paymentStatus',
        text: 'Payment Status',
        sort: true,
        formatter: statusColumn,
    },
    {
        dataField: 'action',
        isDummyColumn: true,
        text: '',
        sort: false,
        classes: 'table-action',
        formatter: ActionColumn,
    },
];
