import React from 'react';
import { Row, Col, Card, CardBody, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import paginationFactory, {
    PaginationProvider,
    SizePerPageDropdownStandalone,
    PaginationTotalStandalone,
    PaginationListStandalone,
} from 'react-bootstrap-table2-paginator';
import classNames from 'classnames';
import { projects } from './data';
import { columns } from './columns';

function ListView() {
    const paginationOptions = {
        paginationSize: 5,
        pageStartIndex: 1,
        withFirstAndLast: false,
        showTotal: true,
        sizePerPageList: [
            {
                text: '10',
                value: 10,
            },
            {
                text: '25',
                value: 25,
            },
            {
                text: '50',
                value: 50,
            },
        ],
    };

    const customTotal = (from, to, size) => (
        <label className="react-bootstrap-table-pagination-total ml-2">
            Showing {from} to {to} of {size}
        </label>
    );

    const sizePerPageRenderer = ({ options, currSizePerPage, onSizePerPageChange }) => (
        <React.Fragment>
            <label className="d-inline mr-1">Display</label>
            <UncontrolledDropdown className="d-inline">
                <DropdownToggle caret tag="button" type="button" className="btn btn-outline-secondary btn-sm">
                    {currSizePerPage}
                </DropdownToggle>
                <DropdownMenu>
                    {options.map((option, idx) => (
                        <DropdownItem
                            key={idx}
                            type="button"
                            className={classNames({ active: currSizePerPage + '' === option.page + '' })}
                            onClick={() => onSizePerPageChange(option.page)}>
                            {option.text}
                        </DropdownItem>
                    ))}
                </DropdownMenu>
            </UncontrolledDropdown>
            <label className="d-inline ml-1">projects</label>
        </React.Fragment>
    );

    const { SearchBar } = Search;

    return (
        <>
            <Row>
                <Col>
                    <Card>
                        <CardBody>
                            <PaginationProvider
                                bootstrap4
                                pagination={paginationFactory({
                                    ...paginationOptions,
                                    paginationTotalRenderer: customTotal,
                                    custom: true,
                                    sizePerPageRenderer: sizePerPageRenderer,
                                })}
                                keyField="id"
                                data={projects}
                                columns={columns}>
                                {({ paginationProps, paginationTableProps }) => (
                                    <ToolkitProvider keyField="id" data={projects} columns={columns} search>
                                        {(props) => (
                                            <React.Fragment>
                                                <Row className="mt-2">
                                                    <Col md={6}>
                                                        <SizePerPageDropdownStandalone {...paginationProps} />
                                                    </Col>
                                                    <Col md={6} className="text-sm-right mt-2 mt-sm-0">
                                                        Search: <SearchBar {...props.searchProps} />
                                                    </Col>
                                                </Row>

                                                <BootstrapTable
                                                    {...props.baseProps}
                                                    striped
                                                    bordered={false}
                                                    wrapperClasses="table-responsive"
                                                    {...paginationTableProps}
                                                />
                                                <Row>
                                                    <Col>
                                                        <PaginationTotalStandalone
                                                            {...paginationProps}
                                                            dataSize={projects.length}
                                                        />
                                                    </Col>
                                                    <Col className="react-bootstrap-table-pagination-list">
                                                        <PaginationListStandalone {...paginationProps} />
                                                    </Col>
                                                </Row>
                                            </React.Fragment>
                                        )}
                                    </ToolkitProvider>
                                )}
                            </PaginationProvider>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </>
    );
}

export default ListView;
