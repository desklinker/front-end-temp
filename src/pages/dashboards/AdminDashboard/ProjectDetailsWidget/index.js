import React, { useState } from 'react';
import { project } from '../data';
import ProjectDetail from './ProjectDetail';
import { Card, CardBody } from 'reactstrap';
import designImg from '../../../../assets/images/projects/design.jpg';

function ProjectDetailsWidget({ isEdit, toggle }) {
    const [activeEdit, setActiveEdit] = useState(null);

    const handleToggle = (value) => {
        if (value === activeEdit) {
            setActiveEdit(null);
        } else {
            setActiveEdit(value);
        }
    };

    return (
        <Card className="project-widget">
            <CardBody>
                {isEdit && (
                    <div className="text-right">
                        <span onClick={toggle} className="font-24 text-primary icon-arrow cursor-pointer">
                            <i className="mdi mdi-square-edit-outline"></i>
                        </span>
                    </div>
                )}
                <ProjectDetail type="client" project={project} img={designImg} isEdit={false} />
                <ProjectDetail
                    type="screener"
                    project={project}
                    img={designImg}
                    isEdit={true}
                    activeEdit={activeEdit}
                    toggle={(value) => handleToggle(value)}
                />
                <ProjectDetail
                    type="coordinator"
                    project={project}
                    img={designImg}
                    isEdit={true}
                    activeEdit={activeEdit}
                    toggle={() => handleToggle('coordinator')}
                />
            </CardBody>
        </Card>
    );
}

export default ProjectDetailsWidget;
