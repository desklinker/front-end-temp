import React from 'react';
import { Col, Row } from 'reactstrap';
import EditProjectDetail from '../EditProjectDetail';
import FileWidget from '../../../../components/FileWidget';

function ProjectDetail({ img, project, type, isEdit, activeEdit, toggle }) {
    return (
        <Row
            className={`${type !== 'coordinator' ? 'pb-2 border-bottom' : ''} ${
                type !== 'client' ? 'pt-2' : ''
            } project-widget`}>
            <Col md={3}>
                <img className="project-img" src={img} alt={img} />
                <div className="card-img-overlay">
                    <div className="badge badge-secondary p-1">by {type}</div>
                </div>
            </Col>
            <Col md={6}>
                {activeEdit !== type ? (
                    <>
                        <div className="project-detail-heading">
                            <h4 className="mt-0">{project.title}</h4>
                            <span className="ml-2">
                                ( <strong>Start Date :</strong>
                            </span>
                            <span className="ml-2">{project.startDate}</span>
                            <strong className="ml-3">Duration :</strong>
                            <span className="ml-2">
                                {project.duration} <small className="text-muted">days</small> )
                            </span>
                        </div>

                        <div className="mt-2">
                            <p>
                                <strong>Outline : </strong>
                                <span className="text-muted ml-2">
                                    With supporting text below as a natural lead-in to additional contenposuere erat a
                                    ante. Voluptates, illo, iste itaque voluptas corrupti ratione reprehenderit magni
                                    similique? Tempore, quos delectus asperiores libero voluptas quod perferendis!
                                    Voluptate, quod illo rerum? Lorem ipsum dolor sit amet. Voluptates, illo, iste
                                    itaque voluptas corrupti ratione reprehenderit magni similique? Tempore, quos
                                    delectus asperiores libero.
                                </span>
                            </p>
                        </div>
                        {isEdit && (
                            <span
                                className={`text-right position-absolute fixed-top mr-2 font-18 text-muted icon-arrow cursor-pointer`}
                                onClick={() => toggle(type)}>
                                <i className="mdi mdi-square-edit-outline"></i>
                            </span>
                        )}
                    </>
                ) : (
                    <EditProjectDetail toggle={toggle} type={type} />
                )}
            </Col>
            <Col md={3}>
                <FileWidget maxHeight="125px" />
            </Col>
        </Row>
    );
}

export default ProjectDetail;
