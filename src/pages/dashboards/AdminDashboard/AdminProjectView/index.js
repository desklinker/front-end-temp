import React, { useState } from 'react';
import { Card, CardBody, Col, Row } from 'reactstrap';
import ProjectDetailsWidget from '../ProjectDetailsWidget';
import BidDetailsWidget from '../BidDetailsWidget';
import DeliveryDetailsWidget from '../DeliveryDetailsWidget';
import EditBidWidget from '../EditBidWidget';
import EditDeliveryWidget from '../EditDeliveryWidget';
import EditProjectWidget from '../EditProjectWidget';

function AdminProjectView() {
    const [formOpen, setFormOpen] = useState(null);

    const handleToggle = (value) => {
        setFormOpen(value);
    };

    return (
        <Card>
            <CardBody>
                <Row>
                    <Col md={12}>
                        {formOpen === 'client' && <EditProjectWidget editToggle={() => handleToggle(null)} />}
                        <ProjectDetailsWidget isEdit={!(formOpen === 'client')} toggle={() => handleToggle('client')} />
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        {formOpen === 'bid' ? (
                            <EditBidWidget editToggle={() => handleToggle(null)} />
                        ) : (
                            <BidDetailsWidget isEdit toggle={() => handleToggle('bid')} />
                        )}
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        {formOpen === 'delivery' ? (
                            <EditDeliveryWidget editToggle={(e) => handleToggle(e)} />
                        ) : (
                            <DeliveryDetailsWidget isEdit toggle={() => handleToggle('delivery')} />
                        )}
                    </Col>
                </Row>
            </CardBody>
        </Card>
    );
}

export default AdminProjectView;
