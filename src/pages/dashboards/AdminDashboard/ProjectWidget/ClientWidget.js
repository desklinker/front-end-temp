import React from 'react';
import Rating from 'react-rating';

function ClientWidget({ item, toggle, isOpen, toggleEdit, isOpenEdit }) {
    return (
        <div>
            <div className="text-left w-100">
                <p>
                    <strong> Client :</strong>
                    <span className="mx-2">{item.client.name}</span>
                    <Rating
                        initialRating={item.client.rating || 0}
                        readonly
                        emptySymbol="mdi mdi-star-outline font-16 text-muted"
                        fullSymbol="mdi mdi-star font-16 text-warning"
                    />
                    <strong className="pl-xl-2 d-none d-xl-inline"> No.of exe :</strong>
                    <span className="d-none d-xl-inline"> {item.client.noOfExe}</span>
                    <span
                        className={`text-right position-absolute fixed-top mr-2 font-18 icon-arrow cursor-pointer ${
                            isOpenEdit ? 'text-dark font-weight-bold' : 'text-muted'
                        }`}
                        onClick={() => toggleEdit()}>
                        <i className="mdi mdi-square-edit-outline"></i>
                    </span>
                </p>

                <p>
                    <strong className="d-xl-none"> No.of exe :</strong>
                    <span className="ml-2 d-xl-none"> {item.client.noOfExe}</span>
                </p>
                <p>
                    <strong> Project :</strong>
                    <span className="ml-2">{item.project.name}</span>
                    <span className="ml-2">( {item.project.category} )</span>
                </p>
                <div className="table-widget table-responsive">
                    <table className="mb-0 bg-transparent">
                        <tbody>
                            <tr>
                                <td className="font-weight-bold">Screen</td>
                                <td>
                                    <span className="mr-2 font-weight-bold">: </span>
                                    {item.screenBudget}
                                </td>
                            </tr>
                            <tr>
                                <td className="font-weight-bold">Codi</td>
                                <td>
                                    <span className="mr-2 font-weight-bold">: </span>
                                    {item.codiBudget}
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <table className="mb-0 bg-transparent">
                        <tbody>
                            <tr>
                                <td className="font-weight-bold">{item.scrPosted.id} time</td>
                                <td>
                                    <span className="mr-2 font-weight-bold">: </span>
                                    {item.scrPosted.time}
                                </td>
                            </tr>
                            <tr>
                                <td className="font-weight-bold">{item.cordiPosted.id} time</td>
                                <td>
                                    <span className="mr-2 font-weight-bold">: </span>
                                    {item.cordiPosted.time}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div
                    className="text-right position-absolute fixed-bottom mr-2 font-24 text-primary icon-arrow cursor-pointer"
                    onClick={() => toggle()}>
                    <i className={`mdi ${isOpen ? 'mdi-chevron-up' : 'mdi-chevron-down'}`}></i>
                </div>
            </div>
        </div>
    );
}

export default ClientWidget;
