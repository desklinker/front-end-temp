import React from 'react';
import { Card, CardBody, Row, Col } from 'reactstrap';
import ProjectDetailsWidget from '../ProjectDetailsWidget';
import BidDetailsWidget from '../BidDetailsWidget';
import DeliveryDetailsWidget from '../DeliveryDetailsWidget';

function DefaultDetailsWidget() {
    return (
        <Card>
            <CardBody>
                <Row>
                    <Col md={12}>
                        <ProjectDetailsWidget />
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <BidDetailsWidget />
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <DeliveryDetailsWidget />
                    </Col>
                </Row>
            </CardBody>
        </Card>
    );
}

export default DefaultDetailsWidget;
