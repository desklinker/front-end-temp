import React, { useEffect, useState } from 'react';
import { Card, CardBody, Row, Col, Collapse } from 'reactstrap';
import CardHeader from 'reactstrap/es/CardHeader';
import ClientWidget from './ClientWidget';
import BidWidget from './BidWidget';
import DeliveryWidget from './DeliveryWidget';
import DeliveryDetailsWidget from '../DeliveryDetailsWidget';
import ProjectDetailsWidget from '../ProjectDetailsWidget';
import BidDetailsWidget from '../BidDetailsWidget';
import DefaultDetailsWidget from './DefaultDetailsWidget';
import ThreadDetailsWidget from './ThreadDetailsWidget';
import EditBidWidget from '../EditBidWidget';
import EditProjectWidget from '../EditProjectWidget';
import EditDeliveryWidget from '../EditDeliveryWidget';

function ProjectWidget({ item, filter, toggle }) {
    const [active, setActive] = useState(null);
    const [activeEdit, setActiveEdit] = useState(null);

    const handleCollapse = () => {
        setActiveEdit(null);

        if (filter) {
            return null;
        }
        if (active) {
            setActive(null);
        } else {
            handleToggle('all');
        }
    };

    useEffect(() => {
        if (filter) {
            setActiveEdit(null);
            setActive(filter);
        } else {
            setActive(null);
        }
    }, [filter]);

    const handleToggle = (value) => {
        setActiveEdit(null);
        if (filter) {
            return null;
        } else if (value === active) {
            setActive(null);
        } else {
            setActive(value);
        }
    };

    const handleEditToggle = (value) => {
        setActive(null);
        toggle(null);
        if (value === activeEdit) {
            setActiveEdit(null);
        } else {
            setActiveEdit(value);
        }
    };

    return (
        <>
            <Card className="project-widget bg-light">
                <CardHeader className="bg-light">
                    <Row>
                        <Col md={9} className="align-self-center">
                            <div className="ml-2">
                                <strong> Budget :</strong>
                                <span className="ml-2"> {item.budget} </span>
                                <strong className="ml-3"> Posted time :</strong>
                                <span className="ml-2"> {item.postedTime} </span>
                                <strong className="ml-3"> Duration :</strong>
                                <span className="ml-2"> {item.duration} </span>
                                <strong className="ml-3"> Codi :</strong>
                                <span className="mx-2">{item.bid.codi.name}</span>
                            </div>
                        </Col>
                        <Col md={3}>
                            <div className="text-right font-24">
                                <span onClick={() => handleToggle('thread')}>
                                    <i
                                        className={`mdi ${
                                            active === 'thread' ? 'mdi-message-text' : 'mdi-message-text-outline'
                                        }`}></i>
                                </span>
                                <span className="ml-2" onClick={() => handleToggle('message')}>
                                    <i className={`mdi ${active === 'message' ? 'mdi-bell' : 'mdi-bell-outline'}`}></i>
                                </span>
                                <span className="ml-2" onClick={() => handleCollapse()}>
                                    <i
                                        className={`mdi ${
                                            active === 'all' ? 'mdi-chevron-double-up' : 'mdi-chevron-double-down'
                                        }`}></i>
                                </span>
                            </div>
                        </Col>
                    </Row>
                </CardHeader>
                <CardBody>
                    <Row>
                        <Col className="d-md-flex divider" md={4}>
                            <ClientWidget
                                item={item}
                                isOpen={active === 'clientDetail'}
                                toggle={() => handleToggle('clientDetail')}
                                isOpenEdit={activeEdit === 'client'}
                                toggleEdit={() => handleEditToggle('client')}
                            />
                        </Col>

                        <Col className="d-md-flex divider" md={4}>
                            <BidWidget
                                item={item}
                                isOpen={active === 'codiDetail'}
                                toggle={() => handleToggle('codiDetail')}
                                isOpenEdit={activeEdit === 'codi'}
                                toggleEdit={() => handleEditToggle('codi')}
                            />
                        </Col>
                        <Col className="d-md-flex" md={4}>
                            <DeliveryWidget
                                item={item}
                                isOpen={active === 'deliverDetail'}
                                toggle={() => handleToggle('deliverDetail')}
                                isOpenEdit={activeEdit === 'deliver'}
                                toggleEdit={() => handleEditToggle('deliver')}
                            />
                        </Col>
                    </Row>
                </CardBody>
            </Card>
            <Collapse isOpen={active === 'clientDetail'}>
                <ProjectDetailsWidget />
            </Collapse>
            <Collapse isOpen={active === 'codiDetail'}>
                <BidDetailsWidget />
            </Collapse>
            <Collapse isOpen={active === 'deliverDetail'}>
                <DeliveryDetailsWidget />
            </Collapse>
            <Collapse isOpen={active === 'all'}>
                <DefaultDetailsWidget />
            </Collapse>
            <Collapse isOpen={active === 'thread'}>
                <ThreadDetailsWidget />
            </Collapse>
            <Collapse isOpen={active === 'message'}>
                <ThreadDetailsWidget />
            </Collapse>
            <Collapse isOpen={activeEdit === 'client'}>
                <EditProjectWidget editToggle={(e) => handleEditToggle(e)} />
            </Collapse>
            <Collapse isOpen={activeEdit === 'codi'}>
                <EditBidWidget editToggle={(e) => handleEditToggle(e)} />
            </Collapse>
            <Collapse isOpen={activeEdit === 'deliver'}>
                <EditDeliveryWidget editToggle={(e) => handleEditToggle(e)} />
            </Collapse>
        </>
    );
}

export default ProjectWidget;
