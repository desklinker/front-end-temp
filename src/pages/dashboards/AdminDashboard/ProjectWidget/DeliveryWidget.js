import React from 'react';

function DeliveryWidget({ item, toggle, isOpen, toggleEdit, isOpenEdit }) {
    return (
        <div className="text-left w-100 ml-md-2">
            <p>
                <strong> Output deliver status :</strong>
                <span className="ml-2">{item.outputDelivery.status}</span>
                <span
                    className={`text-right position-absolute fixed-top mr-2 font-18 icon-arrow cursor-pointer ${
                        isOpenEdit ? 'text-dark font-weight-bold' : 'text-muted '
                    }`}
                    onClick={() => toggleEdit()}>
                    <i className="mdi mdi-square-edit-outline"></i>
                </span>
            </p>

            <p>
                <strong>Date : </strong>
                <span className="ml-2">{item.outputDelivery.updatedAt}</span>
            </p>
            <p>
                <strong> Payment status :</strong>
                <span className="ml-2">{item.outputDelivery.paymentStatus}</span>
            </p>
            <p>
                <strong> Revision or scope change update from coordinator :</strong>
                <span className="ml-2"> {item.outputDelivery.revision} </span>
            </p>
            <div
                className="text-right position-absolute fixed-bottom mr-2 font-24 text-primary icon-arrow"
                onClick={() => toggle()}>
                <i className={`mdi ${isOpen ? 'mdi-chevron-up' : 'mdi-chevron-down'}`}></i>
            </div>
        </div>
    );
}

export default DeliveryWidget;
