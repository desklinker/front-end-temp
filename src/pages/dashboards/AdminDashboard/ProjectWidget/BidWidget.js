import React from 'react';
import Rating from 'react-rating';

function BidWidget({ item, toggle, isOpen, toggleEdit, isOpenEdit }) {
    return (
        <div className="text-left w-100 ml-md-2">
            <p>
                <strong> Bid Status :</strong>
                <span className="ml-2">{item.bid.status}</span>
            </p>

            <p>
                <strong> Codi :</strong>
                <span className="mx-2">{item.bid.codi.name}</span>
                <Rating
                    initialRating={item.bid.codi.rating}
                    readonly
                    emptySymbol="mdi mdi-star-outline font-16 text-muted"
                    fullSymbol="mdi mdi-star font-16 text-warning"
                />
                <strong className="ml-2"> Projects :</strong>
                <span className="ml-2">{item.bid.codi.completedProjects}</span>
                <span
                    className={`text-right position-absolute fixed-top mr-2 font-18 icon-arrow cursor-pointer ${
                        isOpenEdit ? 'text-dark font-weight-bold' : 'text-muted'
                    }`}
                    onClick={() => toggleEdit()}>
                    <i className="mdi mdi-square-edit-outline"></i>
                </span>
            </p>
            <p>
                <strong> Professional division :</strong>
                <span className="ml-2">
                    (<strong> Tot :</strong> <span className="ml-2">{item.bid.profDivision.total}</span>)
                </span>
            </p>

            <div className="table-widget">
                <table className="mb-0 bg-transparent">
                    <tbody>
                        <tr>
                            <td className="font-weight-bold">Designer</td>
                            <td>
                                <span className="mr-2 font-weight-bold">: </span>
                                {item.bid.profDivision.designer.budget} ({item.bid.profDivision.designer.duration})
                            </td>
                        </tr>
                        <tr>
                            <td className="font-weight-bold">Drafter</td>
                            <td>
                                <span className="mr-2 font-weight-bold">: </span>
                                {item.bid.profDivision.drafter.budget} ( {item.bid.profDivision.drafter.duration})
                            </td>
                        </tr>
                        <tr>
                            <td className="font-weight-bold">Architect</td>
                            <td>
                                <span className="mr-2 font-weight-bold">: </span>
                                {item.bid.profDivision.architect.budget} ({item.bid.profDivision.architect.duration})
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div
                className="text-right position-absolute fixed-bottom mr-2 font-24 text-primary icon-arrow"
                onClick={() => toggle()}>
                <i className={`mdi ${isOpen ? 'mdi-chevron-up' : 'mdi-chevron-down'}`}></i>
            </div>
        </div>
    );
}

export default BidWidget;
