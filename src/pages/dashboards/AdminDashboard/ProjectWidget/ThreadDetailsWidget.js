import React from 'react';
import { Card, CardBody, Row, Col } from 'reactstrap';
import Comments from '../../../projects/Comments';

function ThreadDetailsWidget() {
    return (
        <Card>
            <CardBody>
                <Row>
                    <Col md={12}>
                        <Comments />
                    </Col>
                </Row>
            </CardBody>
        </Card>
    );
}

export default ThreadDetailsWidget;
