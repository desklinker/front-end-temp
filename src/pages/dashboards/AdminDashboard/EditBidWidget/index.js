import React, { useState } from 'react';
import { Card, CardBody } from 'reactstrap';
import OngoingForm from './OngoingForm';
import CompletedForm from './CompletedForm';

function EditBidWidget({ editToggle }) {
    const [item, setItem] = useState({
        bidStatus: 'ongoing',
        closedTime: '2020-01-03, 15:03',
        skill: { value: 'design', label: 'Design' },
    });

    const handleOngoingSubmit = (e, values) => {
        // console.log(values, formData);
        setItem({ ...item, bidStatus: 'completed' });
    };

    const handleCompletedSubmit = (e, values) => {
        // console.log(values, formData);
    };

    return (
        <Card className="project-widget">
            <CardBody>
                {item.bidStatus === 'ongoing' ? (
                    <OngoingForm
                        item={item}
                        submit={(e, values) => handleOngoingSubmit(e, values)}
                        editToggle={editToggle}
                    />
                ) : (
                    <CompletedForm
                        item={item}
                        submit={(e, values) => handleCompletedSubmit(e, values)}
                        editToggle={editToggle}
                    />
                )}
            </CardBody>
        </Card>
    );
}

export default EditBidWidget;
