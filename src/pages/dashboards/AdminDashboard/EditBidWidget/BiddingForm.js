import React, { useState } from 'react';
import { Button, Col, FormGroup, Input, Label, Row } from 'reactstrap';
import Select from 'react-select';
import { skills, timeFrameOptions } from '../data';
import { AvInput, AvForm } from 'availity-reactstrap-validation';
import Flatpickr from 'react-flatpickr';

function BiddingForm({ professional }) {
    const [formData, setFormData] = useState({});

    const updateValues = (field, fieldValue) => {
        setFormData({
            ...formData,
            [field]: fieldValue,
        });
    };

    const handleSubmit = (e, values) => {
        // console.log(values, formData);
    };

    const handleBidSubmit = (e, values) => {
        // console.log(values, formData);
    };

    return (
        <>
            <AvForm onValidSubmit={handleSubmit} model={{}}>
                <FormGroup row>
                    <Label sm={3}>Closed Time</Label>
                    <Col sm={9}>
                        <Flatpickr
                            className="form-control"
                            value={(formData && formData.postedTime) || ''}
                            onChange={(date) => {
                                updateValues('postedTime', date);
                            }}
                            options={{ enableTime: true, dateFormat: 'Y-m-d H:i' }}
                        />
                        <div className="text-right mt-1">
                            <Button type="submit" color="primary" size="sm">
                                Update
                            </Button>
                        </div>
                    </Col>
                </FormGroup>
            </AvForm>
            <hr />
            <AvForm onValidSubmit={handleBidSubmit} model={{}}>
                <FormGroup row>
                    <Label sm={3}>Skill</Label>
                    <Col sm={9}>
                        <Select
                            defaultValue={professional.skill}
                            isDisabled={true}
                            className="react-select"
                            classNamePrefix="react-select"
                            onChange={(value) => {
                                updateValues('skill', value);
                            }}
                            options={skills}
                        />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="exampleEmail3" sm={3}>
                        Bid Range
                    </Label>
                    <Col sm={9}>
                        <Row className="px-1">
                            <Col>
                                <AvInput
                                    name={`${professional.title}MinValue`}
                                    label=""
                                    required
                                    placeholder="Min value"
                                />
                            </Col>
                            <Col>
                                <AvInput
                                    name={`${professional.title}MaxValue`}
                                    label=""
                                    required
                                    placeholder="Max value"
                                />
                            </Col>
                        </Row>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="exampleEmail3" sm={3}>
                        Duration
                    </Label>
                    <Col sm={9}>
                        <Row className="px-1">
                            <Col>
                                <Input type="text" name="bid-duration" placeholder="Duration" />
                            </Col>
                            <Col>
                                <Select className="react-select" options={timeFrameOptions} />
                            </Col>
                        </Row>
                    </Col>
                </FormGroup>
                <Row>
                    <Col className="text-right">
                        <Button type="button" color="danger" className="mr-1" size="sm">
                            Stop Bid
                        </Button>
                        <Button type="submit" color="primary" size="sm">
                            Update & Restart
                        </Button>
                    </Col>
                </Row>
            </AvForm>
        </>
    );
}

export default BiddingForm;
