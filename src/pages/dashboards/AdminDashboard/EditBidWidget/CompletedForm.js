import React, { useEffect, useState } from 'react';
import { Button, Col, FormGroup } from 'reactstrap';
import { coordinators } from '../data';
import ProfessionalForm from './ProfessionalForm';
import map from 'lodash-es/map';
import chunk from 'lodash-es/chunk';

function CompletedForm({ editToggle }) {
    const [formData, setFormData] = useState({});
    const [professionals, setProfessionals] = useState(null);

    useEffect(() => {
        setProfessionals([
            {
                id: 1,
                name: { value: 1, label: 'Judith D Swilley' },
                skill: { value: 'design', label: 'Design' },
                bidValue: 100,
                duration: 2,
                timeFrame: 'months',
                bidClosedOn: '2020/01/25, 15:40',
            },
            {
                id: 2,
                title: 'Drafter',
                skill: { value: 'draft', label: 'Draft' },
                bidValue: 100,
                duration: 2,
                timeFrame: 'months',
                bidClosedOn: '2020/02/21, 15:40',
            },
            {
                id: 3,
                title: 'Architect',
                skill: { value: 'arch', label: 'Architecture' },
                bidValue: 100,
                duration: 2,
                timeFrame: 'months',
                bidClosedOn: '2020/01/06, 15:40',
            },
        ]);
        setFormData({ ...formData, coordinators: coordinators[0] });
    }, [formData]);

    return (
        <>
            <FormGroup>
                <Button className="close" onClick={editToggle}>
                    <span aria-hidden="true">&times;</span>
                </Button>
            </FormGroup>

            {professionals &&
                map(chunk(professionals, 3), (chunkedProfessions, pKey) => {
                    return (
                        <FormGroup row key={pKey}>
                            {map(chunkedProfessions, (professional, key) => {
                                return (
                                    <Col md={4} className={`px-2 ${key !== 0 && 'border-left'}`} key={professional.id}>
                                        <ProfessionalForm professional={professional} />
                                    </Col>
                                );
                            })}
                        </FormGroup>
                    );
                })}
        </>
    );
}

export default CompletedForm;
