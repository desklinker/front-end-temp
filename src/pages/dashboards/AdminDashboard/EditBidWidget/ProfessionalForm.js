import React, { useState } from 'react';
import { Button, Col, FormGroup, Input, Label, Row } from 'reactstrap';
import Select from 'react-select';
import { professionals, timeFrameOptions } from '../data';
import { AvField, AvForm } from 'availity-reactstrap-validation';

function ProfessionalForm({ professional }) {
    const [formData, setFormData] = useState({});

    const handleSubmit = (e, values) => {
        // console.log(values, formData);
    };

    const updateValues = (field, fieldValue) => {
        setFormData({
            ...formData,
            [field]: fieldValue,
        });
    };

    return (
        <AvForm onValidSubmit={handleSubmit} model={{}}>
            <Row>
                <Label sm={4}>Bidding closed on</Label>
                <Col sm={8}>
                    <AvField
                        name="bidClosedOn"
                        label=""
                        placeholder="Enter bid value"
                        value={professional.bidClosedOn}
                        disabled
                    />
                </Col>
            </Row>
            <FormGroup row>
                <Label sm={4}>Professional Name</Label>
                <Col sm={8}>
                    <Select
                        defaultValue={professional.skill}
                        className="react-select"
                        classNamePrefix="react-select"
                        onChange={(value) => {
                            updateValues('skill', value);
                        }}
                        options={professionals}
                    />
                </Col>
            </FormGroup>

            <Row>
                <Label for="exampleEmail3" sm={4}>
                    Bid Value
                </Label>
                <Col sm={8}>
                    <AvField name="bidValue" label="" required placeholder="Enter bid value" />
                </Col>
            </Row>

            <FormGroup row>
                <Label sm={4}>Duration</Label>
                <Col sm={8}>
                    <Row className="px-1">
                        <Col>
                            <Input type="text" name="bid-duration" id={`bid-duration`} placeholder="Duration" />
                        </Col>
                        <Col>
                            <Select className="react-select" options={timeFrameOptions} />
                        </Col>
                    </Row>
                </Col>
            </FormGroup>

            <Row>
                <Col className="text-right">
                    <Button type="submit" color="primary" size="sm">
                        Update
                    </Button>
                </Col>
            </Row>
        </AvForm>
    );
}

export default ProfessionalForm;
