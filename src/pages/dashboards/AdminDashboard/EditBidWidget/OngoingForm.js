import React, { useEffect, useState } from 'react';
import { Button, Col, FormGroup, Label, CardBody, Card } from 'reactstrap';
import Select from 'react-select';
import { coordinators, ongoingStatus } from '../data';
import { AvForm } from 'availity-reactstrap-validation';
import BiddingForm from './BiddingForm';
import map from 'lodash-es/map';
import chunk from 'lodash-es/chunk';

function OngoingForm({ editToggle, submit }) {
    const [formData, setFormData] = useState({});
    const [professionals, setProfessionals] = useState(null);

    const updateValues = (field, fieldValue) => {
        setFormData({
            ...formData,
            [field]: fieldValue,
        });
    };

    useEffect(() => {
        setProfessionals([
            {
                id: 1,
                title: 'Designer',
                skill: { value: 'design', label: 'Design' },
                bidValue: 100,
                duration: 2,
                timeFrame: 'months',
            },
            {
                id: 2,
                title: 'Designer 2',
                skill: { value: 'design', label: 'Design' },
                bidValue: 50,
                duration: 2,
                timeFrame: 'months',
            },
            {
                id: 3,
                title: 'Drafter',
                skill: { value: 'draft', label: 'Draft' },
                bidValue: 100,
                duration: 2,
                timeFrame: 'months',
            },
            {
                id: 4,
                title: 'Architect',
                skill: { value: 'arch', label: 'Architecture' },
                bidValue: 100,
                duration: 2,
                timeFrame: 'months',
            },
            {
                id: 5,
                title: 'Architect 2',
                skill: { value: 'arch', label: 'Architecture' },
                bidValue: 240,
                duration: 3,
                timeFrame: 'months',
            },
        ]);
        setFormData({ ...formData, coordinators: coordinators[0] });
    }, [formData]);

    return (
        <>
            <FormGroup>
                <Button className="close" onClick={editToggle}>
                    <span aria-hidden="true">&times;</span>
                </Button>
            </FormGroup>
            <AvForm onValidSubmit={submit} model={{}}>
                <FormGroup row className="border-bottom pb-2">
                    <Label sm={1}>Bid Status</Label>
                    <Col md={4}>
                        <Select
                            className="react-select"
                            classNamePrefix="react-select"
                            onChange={(members) => {
                                updateValues('bidStatus', members);
                            }}
                            defaultValue={ongoingStatus[0]}
                            options={ongoingStatus}
                        />
                    </Col>
                    <Col>
                        <Button type="submit" color="primary" size="sm" className="py-1">
                            Update
                        </Button>
                    </Col>
                </FormGroup>
            </AvForm>

            {professionals &&
                map(chunk(professionals, 3), (chunkedProfessions, pKey) => {
                    return (
                        <FormGroup row key={pKey}>
                            {map(chunkedProfessions, (professional) => (
                                <Col md={4} key={professional.id}>
                                    <Card>
                                        <CardBody>
                                            <BiddingForm professional={professional} />
                                        </CardBody>
                                    </Card>
                                </Col>
                            ))}
                        </FormGroup>
                    );
                })}
        </>
    );
}

export default OngoingForm;
