import React, { useState } from 'react';
import { Row, Col, Button, FormGroup } from 'reactstrap';
import { AvField, AvForm } from 'availity-reactstrap-validation';
import Select from 'react-select';
import Flatpickr from 'react-flatpickr';
import { timeFrameOptions } from '../data';

function EditProjectDetail({ toggle, type }) {
    const [formData, setFormData] = useState([]);

    const updateValues = (field, fieldValue) => {
        formData[field] = fieldValue;
        setFormData(formData);
    };

    const handleValidSubmit = (e, values) => {
        // console.log(values, formData);
    };

    return (
        <AvForm onValidSubmit={handleValidSubmit} model={{}}>
            <Row>
                <Col md={4}>
                    <AvField name="project" label="" placeholder="Enter project name" />
                </Col>
                <Col md={3}>
                    <FormGroup>
                        <Flatpickr
                            className="form-control"
                            value={(formData && formData.startDate) || ''}
                            onChange={(date) => {
                                updateValues('date', date);
                            }}
                            options={{ enableTime: true, dateFormat: 'Y-m-d H:i' }}
                        />
                    </FormGroup>
                </Col>
                <Col md={3}>
                    <FormGroup>
                        <AvField
                            name="duration"
                            label=""
                            required
                            id="duration"
                            placeholder="Enter duration"
                            type="number"
                            min={0}
                        />
                    </FormGroup>
                </Col>
                <Col md={2}>
                    <FormGroup>
                        <Select className="react-select" options={timeFrameOptions} />
                    </FormGroup>
                </Col>
            </Row>
            <Row>
                <Col md={12}>
                    <AvField type="textarea" name="outline" label="" placeholder="Enter outline" />
                </Col>
            </Row>

            <Row>
                <Col className="text-right">
                    <Button type="button" color="secondary" className="mr-1" onClick={() => toggle(type)} size="sm">
                        Cancel
                    </Button>
                    <Button type="submit" color="primary" size="sm">
                        Update
                    </Button>
                </Col>
            </Row>
        </AvForm>
    );
}

export default EditProjectDetail;
