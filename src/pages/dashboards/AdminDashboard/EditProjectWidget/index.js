import React, { useEffect, useState } from 'react';
import { Card, CardBody, Row, Col, Button, FormGroup, Label } from 'reactstrap';
import { AvField, AvForm } from 'availity-reactstrap-validation';
import Select from 'react-select';
import { coordinators, currencyOptions, projectFormData, timeFrameOptions } from '../data';

function EditProjectWidget({ editToggle }) {
    const [formData, setFormData] = useState([]);

    const updateValues = (field, fieldValue) => {
        setFormData({
            ...formData,
            [field]: fieldValue,
        });
    };

    useEffect(() => {
        setFormData(projectFormData);
    }, []);

    const handleValidSubmit = (e, values) => {
        // console.log(values, formData);
    };

    return (
        <Card className="project-widget">
            <CardBody>
                <AvForm onValidSubmit={handleValidSubmit} model={projectFormData}>
                    <Row>
                        <Col md={4}>
                            <AvField name="project" label="Project Name" placeholder="Enter project name" />
                        </Col>
                        <Col md={3}>
                            <FormGroup>
                                <Label>Project Category</Label>
                                <Select
                                    className="react-select"
                                    classNamePrefix="react-select"
                                    defaultValue={projectFormData.projectCategory}
                                    onChange={(category) => {
                                        updateValues('projectCategory', category);
                                    }}
                                    options={[
                                        { value: '1', label: 'Civil Engineering' },
                                        { value: '2', label: 'Mechanical Engineering' },
                                        { value: '3', label: 'Computer Science' },
                                    ]}
                                />
                            </FormGroup>
                        </Col>
                        <Col md={3}>
                            <AvField
                                name="budget"
                                label="Budget"
                                required
                                id="budget"
                                placeholder="Enter budget"
                                type="number"
                                min={0}
                            />
                        </Col>
                        <Col md={2}>
                            <FormGroup>
                                <Label>Currency</Label>
                                <Select className="react-select" options={currencyOptions} />
                            </FormGroup>
                        </Col>
                    </Row>

                    <FormGroup row>
                        <Col md={4}>
                            <Label>Duration</Label>
                            <Row>
                                <Col>
                                    <FormGroup>
                                        <AvField
                                            name="duration"
                                            label=""
                                            required
                                            id="duration"
                                            placeholder="Enter duration"
                                            type="number"
                                            min={0}
                                        />
                                    </FormGroup>
                                </Col>
                                <Col>
                                    <FormGroup>
                                        <Select className="react-select" options={timeFrameOptions} />
                                    </FormGroup>
                                </Col>
                            </Row>
                        </Col>
                        <Col>
                            <Label>Codi Name/ Names</Label>
                            <Select
                                isMulti
                                className="react-select"
                                classNamePrefix="react-select"
                                getOptionLabel={(x) => x.label}
                                getOptionValue={(x) => x.value}
                                value={formData.coordinators || ''}
                                onChange={(coordinators) => {
                                    updateValues('coordinators', coordinators);
                                }}
                                options={coordinators}
                            />
                        </Col>
                    </FormGroup>
                    <Row>
                        <Col className="text-right">
                            <Button
                                type="button"
                                color="secondary"
                                className="mr-1"
                                size="sm"
                                onClick={() => editToggle('client')}>
                                Cancel
                            </Button>
                            <Button type="submit" color="primary" size="sm">
                                Update
                            </Button>
                        </Col>
                    </Row>
                </AvForm>
            </CardBody>
        </Card>
    );
}

export default EditProjectWidget;
