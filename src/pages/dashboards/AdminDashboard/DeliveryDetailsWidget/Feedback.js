import React from 'react';
import FileWidget from '../../../../components/FileWidget';
import Rating from 'react-rating';

function Feedback() {
    const item = {
        satisfactoryLevel: 8.5,
        completeness: 8.5,
        costEfficient: 6,
        easy: 7.5,
        overallScore: 8,
    };
    return (
        <div className="pl-2">
            <FileWidget title="Output files" />

            <div className="border-top my-2 pt-3">
                <span className="font-weight-bolder text-muted">Feedback :</span>
                <table className="mb-0 bg-transparent mt-1 ml-2">
                    <tbody>
                        <tr>
                            <td className="font-weight-bold">Satisfactory level</td>
                            <td>
                                <span className="mr-2 font-weight-bold">: </span>
                                <Rating
                                    stop={10}
                                    initialRating={item.satisfactoryLevel}
                                    readonly
                                    emptySymbol="mdi mdi-star-outline font-16 text-muted"
                                    fullSymbol="mdi mdi-star font-16 text-warning"
                                />
                            </td>
                        </tr>
                        <tr>
                            <td className="font-weight-bold">Completeness</td>
                            <td>
                                <span className="mr-2 font-weight-bold">: </span>
                                <Rating
                                    stop={10}
                                    initialRating={item.completeness}
                                    readonly
                                    emptySymbol="mdi mdi-star-outline font-16 text-muted"
                                    fullSymbol="mdi mdi-star font-16 text-warning"
                                />
                            </td>
                        </tr>
                        <tr>
                            <td className="font-weight-bold">Cost efficient</td>
                            <td>
                                <span className="mr-2 font-weight-bold">: </span>
                                <Rating
                                    stop={10}
                                    initialRating={item.costEfficient}
                                    readonly
                                    emptySymbol="mdi mdi-star-outline font-16 text-muted"
                                    fullSymbol="mdi mdi-star font-16 text-warning"
                                />
                            </td>
                        </tr>
                        <tr>
                            <td className="font-weight-bold">Easy</td>
                            <td>
                                <span className="mr-2 font-weight-bold">: </span>
                                <Rating
                                    stop={10}
                                    initialRating={item.easy}
                                    readonly
                                    emptySymbol="mdi mdi-star-outline font-16 text-muted"
                                    fullSymbol="mdi mdi-star font-16 text-warning"
                                />
                            </td>
                        </tr>
                        <tr>
                            <td className="font-weight-bold">Overall score</td>
                            <td>
                                <span className="mr-2 font-weight-bold">: </span>
                                <Rating
                                    stop={10}
                                    initialRating={item.overallScore}
                                    readonly
                                    emptySymbol="mdi mdi-star-outline font-16 text-muted"
                                    fullSymbol="mdi mdi-star font-16 text-warning"
                                />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div className="border-top my-2 pt-3">
                <span className="font-weight-bolder text-muted">Client's past projects :</span>

                <table className="mb-0 bg-transparent table-bordered">
                    <thead className="text-center">
                        <tr>
                            <th>Project Title</th>
                            <th>Output Delivery Time</th>
                            <th>Payment Date</th>
                            <th>Client Rating</th>
                            <th>Codi Rating</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Landscape Design</td>
                            <td>Jan 06, 2020</td>
                            <td>Jan 10, 2020</td>
                            <td>8.5/10</td>
                            <td>8.5/10</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default Feedback;
