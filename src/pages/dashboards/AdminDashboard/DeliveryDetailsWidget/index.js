import React from 'react';
import { Card, CardBody, Row, Col } from 'reactstrap';
import Invoice from './Invoice';
import Feedback from './Feedback';
function DeliveryDetailsWidget({ isEdit, toggle }) {
    return (
        <Card>
            <CardBody>
                {isEdit && (
                    <span
                        className={`text-right position-absolute fixed-top mr-2 font-24 text-primary icon-arrow cursor-pointer`}
                        onClick={() => toggle()}>
                        <i className="mdi mdi-square-edit-outline"></i>
                    </span>
                )}
                <Row>
                    <Col md={8}>
                        <Invoice />
                    </Col>
                    <Col md={4}>
                        <Feedback />
                    </Col>
                </Row>
            </CardBody>
        </Card>
    );
}

export default DeliveryDetailsWidget;
