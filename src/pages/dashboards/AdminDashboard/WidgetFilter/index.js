import React from 'react';
import Col from 'reactstrap/es/Col';
import Row from 'reactstrap/es/Row';

function WidgetFilter({ toggle, sort, setSort, filter, setFilter }) {
    return (
        <Row>
            <Col>
                <div className="grid-filter-wrap widget-filter">
                    <span onClick={() => toggle()} className={`filter-item ${filter ? 'active' : ''}`}>
                        <i className="mdi mdi-filter-outline"></i>
                    </span>
                    <span onClick={() => setFilter(null)} className={`filter-item ${!filter ? 'active' : ''}`}>
                        <i className="mdi mdi-filter-remove-outline"></i>
                    </span>
                </div>
            </Col>
            <Col>
                <div className="grid-filter-wrap widget-filter">
                    <span onClick={() => setSort('asc')} className={`filter-item ${sort === 'asc' ? 'active' : ''}`}>
                        <i className="mdi mdi-sort-ascending"></i>
                    </span>
                    <span onClick={() => setSort('des')} className={`filter-item ${sort === 'des' ? 'active' : ''}`}>
                        <i className="mdi mdi-sort-descending"></i>
                    </span>
                </div>
            </Col>
        </Row>
    );
}

export default WidgetFilter;
