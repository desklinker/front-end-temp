import React, { useEffect, useState } from 'react';
import { Button, Col, FormGroup, Label, Row, Input } from 'reactstrap';
import Select from 'react-select';
import { coordinators, outputDeliverStatus, paymentStatus, projectCategories, projectStatus, skills } from '../data';
import CardBody from 'reactstrap/es/CardBody';
import Card from 'reactstrap/es/Card';
import Flatpickr from 'react-flatpickr';

const initialValues = {
    category: '',
    minBudget: '',
    maxBudget: '',
    postedTime: '',
    durationStart: '',
    durationEnd: '',
    projectStatus: '',
    outputDeliverStatus: '',
    paymentStatus: '',
    skill: '',
    codi: '',
};

function FilterForm({ setFilter, setOpen, filter }) {
    const [values, setValues] = useState({});

    const updateValues = (field, fieldValue) => {
        setValues({
            ...values,
            [field]: fieldValue,
        });
    };

    const handleInputChange = (e, field) => {
        if (e) {
            updateValues(field, e.target.value);
        }
    };

    const handleFilter = () => {
        setFilter(values);
        setOpen(false);
    };

    const handleSubmit = () => {
        handleFilter();
    };

    useEffect(() => {
        if (filter) {
            setValues(filter);
        } else {
            setValues(initialValues);
        }
    }, [filter]);

    return (
        <Card>
            <CardBody>
                <FormGroup row>
                    <Col sm={3}>
                        <Label>Category</Label>
                        <Select
                            placeholder="Select category..."
                            className="react-select"
                            classNamePrefix="react-select"
                            value={(values && values.category) || ''}
                            onChange={(value) => {
                                updateValues('category', value);
                            }}
                            options={projectCategories}
                        />
                    </Col>
                    <Col sm={3}>
                        <Label>Budget</Label>
                        <Row>
                            <Col>
                                <Input
                                    type="number"
                                    name="minBudget"
                                    label=""
                                    placeholder="Min value"
                                    min={0}
                                    value={values.minBudget || ''}
                                    onChange={(e) => handleInputChange(e, 'minBudget')}
                                />
                            </Col>
                            <Col>
                                <Input
                                    type="number"
                                    name="maxBudget"
                                    label=""
                                    placeholder="Max value"
                                    disabled={!values.minBudget}
                                    min={values.minBudget || 0}
                                    value={values.maxBudget || ''}
                                    onChange={(e) => handleInputChange(e, 'maxBudget')}
                                />
                            </Col>
                        </Row>
                    </Col>
                    <Col sm={3}>
                        <Label>Posted time</Label>
                        <Flatpickr
                            className="form-control"
                            value={values && values.postedTime}
                            onChange={(date) => {
                                updateValues('postedTime', date);
                            }}
                            options={{ mode: 'range' }}
                        />
                    </Col>
                    <Col sm={3}>
                        <Label>Duration</Label>
                        <Row>
                            <Col>
                                <Input
                                    type="number"
                                    name="durationStart"
                                    label=""
                                    placeholder="Min value"
                                    value={values.durationStart || ''}
                                    onChange={(e) => handleInputChange(e, 'durationStart')}
                                />
                            </Col>
                            <Col>
                                <Input
                                    type="number"
                                    name="durationEnd"
                                    label=""
                                    placeholder="Max value"
                                    disabled={!values.durationStart}
                                    min={values.durationStart || 0}
                                    value={values.durationEnd || ''}
                                    onChange={(e) => handleInputChange(e, 'durationEnd')}
                                />
                            </Col>
                        </Row>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col sm={3}>
                        <Label>Project status</Label>
                        <Select
                            placeholder="Select..."
                            value={(values && values.projectStatus) || ''}
                            className="react-select"
                            classNamePrefix="react-select"
                            onChange={(value) => {
                                updateValues('projectStatus', value);
                            }}
                            options={projectStatus}
                        />
                    </Col>
                    <Col sm={3}>
                        <Label>Output deliver status</Label>
                        <Select
                            placeholder="Select..."
                            value={(values && values.outputDeliverStatus) || ''}
                            className="react-select"
                            classNamePrefix="react-select"
                            onChange={(value) => {
                                updateValues('outputDeliverStatus', value);
                            }}
                            options={outputDeliverStatus}
                        />
                    </Col>
                    <Col sm={3}>
                        <Label>Payment status</Label>
                        <Select
                            placeholder="Select..."
                            value={(values && values.paymentStatus) || ''}
                            className="react-select"
                            classNamePrefix="react-select"
                            onChange={(value) => {
                                updateValues('paymentStatus', value);
                            }}
                            options={paymentStatus}
                        />
                    </Col>
                    <Col sm={3}>
                        <Label>Total number of participants</Label>
                        <Input
                            type="number"
                            name="noOfParticipants"
                            label=""
                            min={0}
                            value={(values && values.noOfParticipants) || ''}
                            placeholder="Total number of participants"
                            onChange={(e) => handleInputChange(e, 'noOfParticipants')}
                        />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col sm={4}>
                        <Label>Skill</Label>
                        <Select
                            placeholder="Select category..."
                            value={(values && values.skill) || ''}
                            className="react-select"
                            classNamePrefix="react-select"
                            onChange={(value) => {
                                updateValues('skill', value);
                            }}
                            options={skills}
                            isMulti={true}
                        />
                    </Col>
                    <Col sm={8}>
                        <Label>Coordinator</Label>
                        <Select
                            placeholder="Select..."
                            value={(values && values.codi) || ''}
                            className="react-select"
                            classNamePrefix="react-select"
                            onChange={(value) => {
                                updateValues('codi', value);
                            }}
                            options={coordinators}
                            isMulti={true}
                        />
                    </Col>
                </FormGroup>

                <Row>
                    <Col className="text-right">
                        <Button
                            type="button"
                            color="secondary"
                            className="mr-2"
                            onClick={() => {
                                setValues(initialValues);
                                setFilter(null);
                                setOpen(false);
                            }}>
                            Cancel
                        </Button>
                        <Button type="submit" color="primary" onClick={(e) => handleSubmit(e)}>
                            Filter
                        </Button>
                    </Col>
                </Row>
            </CardBody>
        </Card>
    );
}

export default FilterForm;
