import React, { useState } from 'react';
import { Card, CardBody, Row, Col, Button, FormGroup, Label } from 'reactstrap';
import { AvField, AvForm } from 'availity-reactstrap-validation';
import Select from 'react-select';
import Flatpickr from 'react-flatpickr';

function EditDeliveryWidget({ editToggle }) {
    const [formData, setFormData] = useState([]);

    const updateValues = (field, fieldValue) => {
        formData[field] = fieldValue;
        setFormData(formData);
    };

    const handleValidSubmit = (e, values) => {
        // console.log(values, formData);
    };

    return (
        <Card className="project-widget">
            <CardBody>
                <AvForm onValidSubmit={handleValidSubmit} model={{}}>
                    <Row>
                        <Col md={3}>
                            <FormGroup>
                                <Label>Output Delivery Status</Label>
                                <Select
                                    className="react-select"
                                    classNamePrefix="react-select"
                                    onChange={(status) => {
                                        updateValues('deliveryStatus', status);
                                    }}
                                    options={[
                                        { value: '1', label: 'Ongoing' },
                                        { value: '2', label: 'Success' },
                                        { value: '2', label: 'Failure' },
                                    ]}
                                />
                            </FormGroup>
                        </Col>
                        <Col md={3}>
                            <FormGroup>
                                <Label>Date</Label>
                                <Flatpickr
                                    className="form-control"
                                    value={(formData && formData.date) || ''}
                                    onChange={(date) => {
                                        updateValues('date', date);
                                    }}
                                    options={{ enableTime: true, dateFormat: 'Y-m-d H:i' }}
                                />
                            </FormGroup>
                        </Col>
                        <Col md={3}>
                            <FormGroup>
                                <Label>Payment Status</Label>
                                <Select
                                    className="react-select"
                                    classNamePrefix="react-select"
                                    onChange={(status) => {
                                        updateValues('paymentStatus', status);
                                    }}
                                    options={[
                                        { value: '1', label: 'Ongoing' },
                                        { value: '2', label: 'Success' },
                                        { value: '2', label: 'Failure' },
                                    ]}
                                />
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>
                            <AvField
                                type="textarea"
                                name="revision"
                                label="Revision or scope change update from coordinator "
                                placeholder="Enter revision or scope change update from coordinator"
                            />
                        </Col>
                    </Row>

                    <Row>
                        <Col className="text-right">
                            <Button
                                type="button"
                                color="secondary"
                                className="mr-1"
                                onClick={() => editToggle()}
                                size="sm">
                                Cancel
                            </Button>
                            <Button type="submit" color="primary" size="sm">
                                Update
                            </Button>
                        </Col>
                    </Row>
                </AvForm>
            </CardBody>
        </Card>
    );
}

export default EditDeliveryWidget;
