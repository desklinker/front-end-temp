import React from 'react';
import { Card, CardBody, Row, Col } from 'reactstrap';
import IndividualBid from './IndividualBid';
import GroupBid from './GroupBid';

function BidDetailsWidget({ isEdit, toggle }) {
    const bids = [
        { id: 'BID:3245', pid: 'PID:4251', value: '$ 150', duration: '15 days' },
        { id: 'BID:3245', pid: 'PID:4251', value: '$ 150', duration: '15 days' },
        { id: 'BID:3245', pid: 'PID:4251', value: '$ 150', duration: '15 days' },
        { id: 'BID:3245', pid: 'PID:4251', value: '$ 150', duration: '15 days' },
    ];

    const groupBids = [
        { id: 'GBID:3245', value: '$ 150', duration: '15 days' },
        { id: 'GBID:3245', value: '$ 150', duration: '15 days' },
        { id: 'GBID:3245', value: '$ 150', duration: '15 days' },
        { id: 'GBID:3245', value: '$ 150', duration: '15 days' },
    ];
    return (
        <Card className="project-widget">
            <CardBody>
                {isEdit && (
                    <span
                        className={`text-right position-absolute fixed-top mr-2 font-24 text-primary icon-arrow cursor-pointer`}
                        onClick={() => toggle()}>
                        <i className="mdi mdi-square-edit-outline"></i>
                    </span>
                )}
                <Row>
                    <Col md={6}>
                        <IndividualBid bids={bids} />
                    </Col>
                    <Col md={6}>
                        <GroupBid bids={groupBids} />
                    </Col>
                </Row>
            </CardBody>
        </Card>
    );
}

export default BidDetailsWidget;
