import React from 'react';
import { Table, Button, UncontrolledPopover, PopoverHeader, PopoverBody } from 'reactstrap';

function GroupBid({ bids }) {
    return (
        <>
            <strong>Group Bids :</strong>

            <Table className="table mb-0 bg-transparent" size="sm" bordered>
                <thead className="thead-dark">
                    <tr>
                        <th className="text-center">Bid ID</th>
                        <th className="text-center">Bid Value</th>
                        <th className="text-center">Duration</th>
                        <th className="text-center">View details</th>
                        <th className="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {bids.map((b, index) => {
                        return (
                            <tr key={index}>
                                <td className="text-center">{b.id}</td>
                                <td className="text-center">{b.value}</td>
                                <td className="text-center">{b.duration}</td>
                                <td className="text-center">
                                    <Button size="sm" color="success">
                                        Accept
                                    </Button>
                                </td>
                                <td className="text-center">
                                    <Button size="sm" color="secondary" id="BidPop1" outline>
                                        Details
                                    </Button>
                                    <UncontrolledPopover placement="right" target="BidPop1" trigger="focus">
                                        <PopoverHeader>Group Bid Details</PopoverHeader>
                                        <PopoverBody>
                                            Project 1 - BID:2132 <br />
                                            Project 1 - BID:2132 <br />
                                            Project 1 - BID:2132 <br />
                                        </PopoverBody>
                                    </UncontrolledPopover>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </Table>
        </>
    );
}

export default GroupBid;
