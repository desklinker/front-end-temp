import React from 'react';
import { Table, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

function IndividualBid({ bids }) {
    return (
        <>
            <strong>Individual Bids :</strong>

            <Table className="table mb-0 bg-transparent" size="sm" bordered>
                <thead className="thead-dark">
                    <tr>
                        <th className="text-center">Bid ID</th>
                        <th className="text-center">Professional ID</th>
                        <th className="text-center">Bid Value</th>
                        <th className="text-center">Duration</th>
                        <th className="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {bids.map((b, index) => {
                        return (
                            <tr key={index}>
                                <td className="text-center">{b.id}</td>
                                <td className="text-center">
                                    <Link to="/profile">{b.pid}</Link>
                                </td>
                                <td className="text-center">{b.value}</td>
                                <td className="text-center">{b.duration}</td>
                                <td className="text-center">
                                    <Button size="sm" color="success">
                                        Accept
                                    </Button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </Table>
        </>
    );
}

export default IndividualBid;
