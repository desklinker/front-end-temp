import React from 'react';
import { Row, Col } from 'reactstrap';

import '../styles/dashboard.css';
import { projects } from './data';
import SingleProject from '../../../components/SingleProject';

function ProfessionalDashboard() {
    return (
        <React.Fragment>
            <Row>
                <Col>
                    <div className="page-title-box mt-2">
                        <h4 className="page-title mb-0">Hi Vignesh, Let's Find a new job today !</h4>
                    </div>
                </Col>
            </Row>

            <Row className="mb-2">
                {projects.slice(0, 4).map((project, i) => {
                    return (
                        <Col md={4} lg={3} key={'proj-' + project.id} className="d-flex">
                            <SingleProject project={project} />
                        </Col>
                    );
                })}
            </Row>
        </React.Fragment>
    );
}

export default ProfessionalDashboard;
