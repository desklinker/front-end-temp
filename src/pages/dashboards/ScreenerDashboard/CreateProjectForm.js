import React, { Component } from 'react';
import moment from 'moment';
import { Row, Col, Card, CardBody, FormGroup, Label, Button, CustomInput } from 'reactstrap';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import Flatpickr from 'react-flatpickr';
import Select from 'react-select';

import FileUploader from '../../../components/FileUploader';
import Files from '../../projects/Files';

class CreateProjectForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            startDate: new Date(),
            timeFrameOptions: [
                { value: 'hours', label: 'Hours' },
                { value: 'days', label: 'Days' },
                { value: 'months', label: 'Months' },
            ],
            currencyOptions: [
                { value: 'USD', label: 'USD' },
                { value: 'SGD', label: 'SGD' },
                { value: '£', label: '£' },
                { value: 'AUD', label: 'AUD' },
            ],
            isRequested: false,
            isLinked: props.formData ? props.formData.isLinked : false,
            isOpen: false,
            formData: props.formData,
        };

        this.handleValidSubmit = this.handleValidSubmit.bind(this);
        this.updateValues = this.updateValues.bind(this);
    }

    /**
     * Update values
     */
    updateValues = (field, fieldValue) => {
        const state = { ...this.state.state };
        state[field] = fieldValue;
        this.setState(state);
    };

    /**
     * Handle the form submission
     */
    handleValidSubmit = (e, values) => {
        console.log({ ...values, ...this.state });
    };

    render() {
        const customStyles = {
            // For the select it self, not the options of the select
            control: (styles, state) => ({
                ...styles,
                backgroundColor: state.isDisabled ? '#e9ecef' : 'white',
                padding: '0.9px 8px',
            }),
            singleValue: (styles) => ({
                ...styles,
                color: '#343a40',
            }),
        };

        console.log(this.state.formData, this.state.startDate);
        return (
            <Card>
                <CardBody>
                    <AvForm onValidSubmit={this.handleValidSubmit} model={this.state.formData}>
                        <Row>
                            <Col>
                                <AvField
                                    name="title"
                                    label="Title"
                                    required
                                    placeholder="Enter project title"
                                    disabled
                                />
                                <AvField name="confirmTitle" placeholder="Enter project title" />
                                <FormGroup>
                                    <Label>Project category</Label>
                                    <Select
                                        onChange={(category) => {
                                            this.updateValues('category', category);
                                        }}
                                        options={[
                                            { value: '1', label: 'Civil Engineering' },
                                            { value: '2', label: 'Mechanical Engineering' },
                                            { value: '3', label: 'Computer Science' },
                                        ]}
                                        defaultValue={this.state.formData ? this.state.formData.category : ''}
                                        styles={customStyles}
                                        isDisabled={true}
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Select
                                        onChange={(category) => {
                                            this.updateValues('confirmCategory', category);
                                        }}
                                        options={[
                                            { value: '1', label: 'Civil Engineering' },
                                            { value: '2', label: 'Mechanical Engineering' },
                                            { value: '3', label: 'Computer Science' },
                                        ]}
                                    />
                                </FormGroup>

                                <AvField
                                    name="overview"
                                    label="Overview"
                                    placeholder="Enter some brief about project.."
                                    type="textarea"
                                    rows="5"
                                    disabled
                                />
                                <AvField
                                    name="confirmOverview"
                                    label=""
                                    placeholder="Enter some brief about project.."
                                    type="textarea"
                                    rows="5"
                                />

                                <Row>
                                    <Col md={6}>
                                        <AvField
                                            name="budget"
                                            label="Budget"
                                            placeholder="Enter project budget in selected currency"
                                            type="number"
                                            min={0}
                                            disabled
                                        />
                                        <AvField
                                            name="confirmBudget"
                                            label=""
                                            required
                                            placeholder="Enter project budget in selected currency"
                                            type="number"
                                            min={0}
                                        />
                                    </Col>
                                    <Col md={6}>
                                        <FormGroup>
                                            <Label>Currency</Label>
                                            <Select
                                                options={this.state.currencyOptions}
                                                defaultValue={this.state.formData ? this.state.formData.currency : ''}
                                                isDisabled
                                                styles={customStyles}
                                            />
                                        </FormGroup>
                                        <FormGroup>
                                            <Select
                                                options={this.state.currencyOptions}
                                                onChange={(currency) => {
                                                    this.updateValues('confirmCurrency', currency);
                                                }}
                                                styles={customStyles}
                                            />
                                        </FormGroup>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={12}>
                                        <FormGroup>
                                            <CustomInput
                                                id="isRequested"
                                                label="Request for quotation"
                                                type="checkbox"
                                                className="text-secondary"
                                            />
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={3}>
                                        <FormGroup>
                                            <AvField
                                                label="Duration"
                                                name="projectDuration"
                                                id="projectDuration"
                                                placeholder="Duration"
                                                disabled
                                            />
                                            <AvField
                                                label=""
                                                name="confirmProjectDuration"
                                                id="project-duration"
                                                placeholder="Duration"
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={3}>
                                        <FormGroup style={{ marginTop: '31px' }}>
                                            <Select
                                                options={this.state.timeFrameOptions}
                                                styles={customStyles}
                                                isDisabled
                                                defaultValue={
                                                    this.state.formData ? this.state.formData.durationType : ''
                                                }
                                            />
                                        </FormGroup>
                                        <FormGroup>
                                            <Select options={this.state.timeFrameOptions} styles={customStyles} />
                                        </FormGroup>
                                    </Col>
                                    <Col md={6}>
                                        <FormGroup>
                                            <Label>Start Date</Label>
                                            <Flatpickr
                                                className="form-control"
                                                value={this.state.startDate}
                                                onChange={(date) => {
                                                    this.updateValues('startDate', date);
                                                }}
                                                options={{ minDate: moment().format('YYYY-MM-DD') }}
                                                disabled={true}
                                            />
                                        </FormGroup>
                                        <FormGroup>
                                            <Flatpickr
                                                className="form-control"
                                                value={this.state.confirmStartDate}
                                                onChange={(date) => {
                                                    this.updateValues('confirmStartDate', date);
                                                }}
                                                options={{ minDate: moment().format('YYYY-MM-DD') }}
                                            />
                                        </FormGroup>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={12}>
                                        <FormGroup>
                                            <Label>Expected output</Label>
                                            <Select
                                                styles={customStyles}
                                                isMulti={true}
                                                onChange={(output) => {
                                                    this.updateValues('expectedOutput', output);
                                                }}
                                                options={[
                                                    { value: '1', label: 'CAD' },
                                                    { value: '2', label: 'Design' },
                                                    { value: '3', label: 'Architecture' },
                                                    { value: '4', label: 'Survey' },
                                                ]}
                                                defaultValue={this.state.formData && this.state.formData.expectedOutput}
                                                isDisabled
                                            />
                                        </FormGroup>
                                        <FormGroup>
                                            <Select
                                                styles={customStyles}
                                                isMulti={true}
                                                onChange={(output) => {
                                                    this.updateValues('expectedOutput', output);
                                                }}
                                                options={[
                                                    { value: '1', label: 'CAD' },
                                                    { value: '2', label: 'Design' },
                                                    { value: '3', label: 'Architecture' },
                                                    { value: '4', label: 'Survey' },
                                                ]}
                                            />
                                        </FormGroup>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={12}>
                                        <FormGroup>
                                            <CustomInput
                                                id="isLinked"
                                                name="isLinked"
                                                label="Linked to previous projects"
                                                type="checkbox"
                                                className="text-secondary"
                                                checked={this.state.isLinked}
                                                onChange={(e) => this.setState({ isLinked: e.target.checked })}
                                            />
                                        </FormGroup>
                                    </Col>
                                </Row>

                                {this.state.isLinked && (
                                    <Row>
                                        <Col md={12}>
                                            <FormGroup>
                                                <Select
                                                    isMulti={true}
                                                    styles={customStyles}
                                                    onChange={(projects) => {
                                                        this.updateValues('previousProjects', projects);
                                                    }}
                                                    options={[
                                                        { value: '1', label: 'Project #1' },
                                                        { value: '2', label: 'Project #2' },
                                                        { value: '3', label: 'Project #3' },
                                                    ]}
                                                    defaultValue={
                                                        this.state.formData && this.state.formData.previousProjects
                                                    }
                                                    isDisabled
                                                />
                                            </FormGroup>
                                            <FormGroup>
                                                <Select
                                                    isMulti={true}
                                                    styles={customStyles}
                                                    onChange={(projects) => {
                                                        this.updateValues('previousProjects', projects);
                                                    }}
                                                    options={[
                                                        { value: '1', label: 'Project #1' },
                                                        { value: '2', label: 'Project #2' },
                                                        { value: '3', label: 'Project #3' },
                                                    ]}
                                                />
                                            </FormGroup>
                                        </Col>
                                    </Row>
                                )}
                            </Col>
                            <Col>
                                <Files title="Project files" />
                                <FormGroup>
                                    <Label>Attachments</Label>
                                    <p className="text-muted font-14">Recommended files less than 250 Mb</p>
                                    <FileUploader
                                        onFileUpload={(files) => {
                                            this.updateValues('avatar', files);
                                        }}
                                    />
                                </FormGroup>

                                <AvField
                                    name="driveLink"
                                    label="Drive Link"
                                    placeholder="Insert if you have any Drive attachments"
                                    type="text"
                                    disabled
                                />
                                <AvField
                                    name="confirmDriveLink"
                                    label=""
                                    placeholder="Insert if you have any Drive attachments"
                                    type="text"
                                />
                                <Row className="mt-2">
                                    <Col className="text-right">
                                        <Button id="caret" color="success" type="submit">
                                            Submit
                                        </Button>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </AvForm>
                </CardBody>
            </Card>
        );
    }
}

export default CreateProjectForm;
