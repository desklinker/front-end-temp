import React from 'react';
import { Row, Col } from 'reactstrap';

import ProjectCard from './ProjectCard';
import { projects } from './data';

class ScreenerDashboard extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Row>
                    <Col>
                        <div className="page-title-box mt-2">
                            <h4 className="page-title mb-0">Hi Vignesh, Let's Find a new job today !</h4>
                        </div>
                    </Col>
                </Row>
                <Row className="mb-2">
                    {projects.map((project, i) => {
                        if (i < 4) {
                            return (
                                <Col md={4} lg={3} key={'proj-' + project.id} className="d-flex">
                                    <ProjectCard project={project} revision={true} />
                                </Col>
                            );
                        }else{
                            return null
                        }
                    })}
                </Row>
            </React.Fragment>
        );
    }
}

export default ScreenerDashboard;
