import cad from '../../../assets/images/projects/cad.jpg';
import design from '../../../assets/images/projects/design.jpg';

export const projects = [
    {
        id: 1,
        title: 'Ubold v3.0 - Redesign',
        state: 'Ongoing',
        image: cad,
        shortDesc:
            'With supporting text below as a natural lead-in to additional contenposuere erat a ante This card has supporting text below as a natural lead-in to additional content is a little bit longer ',
        overview:
            'With supporting text below as a natural lead-in to additional contenposuere erat a ante This card has supporting text below as a natural lead-in to additional content is a little bit longer',
        category: { value: '1', label: 'Civil Engineering' },
        budget: 1000,
        currency: { value: 'USD', label: 'USD' },
        isRequested: true,
        isLinked: true,
        projectDuration: 6,
        durationType: { value: 'days', label: 'Days' },
        startDate: '2020-05-20',
        expectedOutput: [
            { value: '2', label: 'Design' },
            { value: '3', label: 'Architecture' },
        ],
        previousProjects: [
            { value: '1', label: 'Project #1' },
            { value: '3', label: 'Project #3' },
        ],
        driveLink: 'http://sample.com/drive/',
    },
    {
        id: 2,
        title: 'Minton v3.0 - Redesign',
        state: 'Ongoing',
        image: design,
        shortDesc:
            'This card has supporting text below as a natural lead-in to additional content is a little bit longer',
        overview:
            'With supporting text below as a natural lead-in to additional contenposuere erat a ante This card has supporting text below as a natural lead-in to additional content is a little bit longer',
        category: { value: '1', label: 'Civil Engineering' },
        budget: 5000,
        currency: { value: 'USD', label: 'USD' },
        isRequested: true,
        isLinked: true,
        projectDuration: 6,
        durationType: { value: 'days', label: 'Days' },
        startDate: '2020-05-20',
        expectedOutput: [
            { value: '2', label: 'Design' },
            { value: '3', label: 'Architecture' },
        ],
        previousProjects: [
            { value: '1', label: 'Project #1' },
            { value: '3', label: 'Project #3' },
        ],
        driveLink: 'http://sample.com/drive/',
    },
    {
        id: 3,
        title: 'Hyper v2.1 - Angular and Django',
        state: 'Ongoing',
        image: cad,
        shortDesc: 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid',
        overview: 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid',
        category: { value: '3', label: 'Computer Science' },
        budget: 500,
        currency: { value: 'USD', label: 'USD' },
        isRequested: true,
        isLinked: true,
        projectDuration: 3,
        durationType: { value: 'months', label: 'months' },
        startDate: '2020-05-20',
        expectedOutput: [
            { value: '2', label: 'Design' },
            { value: '3', label: 'Architecture' },
        ],
        previousProjects: [
            { value: '1', label: 'Project #1' },
            { value: '3', label: 'Project #3' },
        ],
        driveLink: 'http://sample.com/drive/',
    },
];
