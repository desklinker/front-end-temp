import React from 'react';
import { Card, CardBody, Badge } from 'reactstrap';
import classNames from 'classnames';
import { useSelector } from 'react-redux';

import { Link } from 'react-router-dom';

const ProjectCard = (props) => {
    const project = props.project || {};
    const user = useSelector(state => state.Auth.user);

    return (
        <Card className="d-block w-100">
            {project.image && (
                <React.Fragment>
                    <img className="card-img-top" src={project.image} alt="" />
                    <div className="card-img-overlay">
                        {project.state != null && (
                            <div
                                className={classNames(
                                    'badge',
                                    {
                                        'badge-success': project.state === 'Completed',
                                        'badge-info': project.state === 'Ongoing',
                                        'badge-light': project.state === 'Planned',
                                    },
                                    'p-1'
                                )}>
                                {project.state !== 'Planned' ? project.state : 'Bidding on process'}
                            </div>
                        )}
                    </div>
                </React.Fragment>
            )}

            <CardBody className={project.image ? 'position-relative' : ''}>
                <h4 className="mt-0">
                    <Link to={`/projects/${project.id}`} className="text-title">
                        {project.title}
                    </Link>
                </h4>

                <p className="mb-1">
                    {project.image && project.field != null && (
                        <Badge color="primary" className="mr-1" style={{ fontWeight: '500' }}>
                            {project.field}
                        </Badge>
                    )}
                    {project.image && project.skill != null && user.role === 'prof' && (
                        <Badge color="primary" className="mr-1" style={{ fontWeight: '500' }}>
                            {project.skill}
                        </Badge>
                    )}
                    {project.image && project.duration != null && (
                        <Badge color="secondary" className="mr-1" style={{ fontWeight: '500' }}>
                            {project.duration}
                        </Badge>
                    )}
                </p>

                <div className="text-muted mt-2 mb-1">
                    {project.shortDesc && project.shortDesc.length > 100
                        ? `${project.shortDesc.substring(0, 100)}...`
                        : project.shortDesc}
                </div>
            </CardBody>
        </Card>
    );
};

export default ProjectCard;
