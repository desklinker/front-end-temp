import React, { Component } from 'react';
import moment from 'moment';
import { Row, Col, Card, CardBody, FormGroup, Label, Button, CustomInput } from 'reactstrap';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import Flatpickr from 'react-flatpickr';
import Select from 'react-select';

import FileUploader from '../../../components/FileUploader';

class CreateProjectForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            startDate: new Date(),
            endDate: new Date(),
            timeFrameOptions: [
                { value: 'hours', label: 'Hours' },
                { value: 'days', label: 'Days' },
                { value: 'months', label: 'Months' },
            ],
            isRequested: false,
            isOpen: false,
        };

        this.handleValidSubmit = this.handleValidSubmit.bind(this);
        this.updateValues = this.updateValues.bind(this);
    }

    /**
     * Update values
     */
    updateValues = (field, fieldValue) => {
        const state = { ...this.state.state };
        state[field] = fieldValue;
        this.setState(state);
    };

    /**
     * Handle the form submission
     */
    handleValidSubmit = (e, values) => {
        console.log({ ...values, ...this.state });
    };

    render() {
        return (
            <Card>
                <CardBody>
                    <Row>
                        <Col xs={12}>
                            <Button className="close" aria-label="Close" onClick={this.props.toggleForm}>
                                <span aria-hidden="true">&times;</span>
                            </Button>
                            <h3>Create Project</h3>
                        </Col>
                    </Row>
                    <AvForm onValidSubmit={this.handleValidSubmit}>
                        <Row>
                            <Col>
                                <AvField name="name" label="Name" required placeholder="Enter project name" />

                                <FormGroup>
                                    <Label>Project category</Label>
                                    <Select
                                        className="react-select"
                                        classNamePrefix="react-select"
                                        onChange={(members) => {
                                            this.updateValues('members', members);
                                        }}
                                        options={[
                                            { value: '1', label: 'Civil Engineering' },
                                            { value: '2', label: 'Mechanical Engineering' },
                                            { value: '3', label: 'Computer Science' },
                                        ]}
                                    />
                                </FormGroup>

                                <AvField
                                    name="overview"
                                    label="Overview"
                                    placeholder="Enter some brief about project.."
                                    type="textarea"
                                    rows="5"
                                />

                                <Row>
                                    <Col md={12}>
                                        <FormGroup>
                                            <CustomInput
                                                id="isRequested"
                                                label="Request for quotation"
                                                type="checkbox"
                                                className="text-secondary"
                                            />
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={3}>
                                        <FormGroup>
                                            <AvField
                                                label="Duration"
                                                name="project-duration"
                                                id="project-duration"
                                                placeholder="Duration"
                                            />
                                        </FormGroup>
                                    </Col>
                                    <Col md={3}>
                                        <FormGroup style={{ marginTop: '31px' }}>
                                            <Select className="react-select" options={this.state.timeFrameOptions} />
                                        </FormGroup>
                                    </Col>
                                    <Col md={6}>
                                        <FormGroup>
                                            <Label>Start Date</Label>
                                            <Flatpickr
                                                className="form-control"
                                                value={this.state.startDate}
                                                onChange={(date) => {
                                                    this.updateValues('startDate', date);
                                                }}
                                                options={{ minDate: moment().format('YYYY-MM-DD') }}
                                            />
                                        </FormGroup>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={12}>
                                        <FormGroup>
                                            <Label>Expected output</Label>
                                            <Select
                                                isMulti={true}
                                                className="react-select"
                                                classNamePrefix="react-select"
                                                onChange={(members) => {
                                                    this.updateValues('members', members);
                                                }}
                                                options={[
                                                    { value: '1', label: 'CAD' },
                                                    { value: '2', label: 'Design' },
                                                    { value: '3', label: 'Architecture' },
                                                    { value: '4', label: 'Survey' },
                                                ]}
                                            />
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </Col>
                            <Col>
                                <FormGroup>
                                    <Label>Attachments</Label>
                                    <p className="text-muted font-14">Recommended files less than 250 Mb</p>
                                    <FileUploader
                                        onFileUpload={(files) => {
                                            this.updateValues('avatar', files);
                                        }}
                                    />
                                </FormGroup>

                                <AvField
                                    name="driveLink"
                                    label="Drive Link"
                                    placeholder="Insert if you have any Drive attachments"
                                    type="text"
                                />
                                <Row className="mt-2">
                                    <Col className="text-right">
                                        <Button id="caret" color="success" type="submit">
                                            Submit to client
                                        </Button>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </AvForm>
                </CardBody>
            </Card>
        );
    }
}

export default CreateProjectForm;
