import React from 'react';
import { Row, Col, Collapse } from 'reactstrap';
import { connect } from 'react-redux';

import SingleProject from './SingleProject';
import cad from '../../../assets/images/projects/cad.jpg';
import design from '../../../assets/images/projects/design.jpg';
import './styles/dashboard.css';
import CreateProjectForm from './CreateProjectForm';
import SelectedProject from './SelectedProject';

const projects = [
    {
        id: 1,
        title: 'Ubold v3.0 - Redesign',
        state: 'Ongoing',
        image: cad,
        shortDesc:
            'With supporting text below as a natural lead-in to additional contenposuere erat a ante This card has supporting text below as a natural lead-in to additional content is a little bit longer ',
        totalTasks: null,
        totalComments: null,
        totalMembers: null,
        progress: null,
        field: 'Civil Engineering',
        skill: 'CAD',
        minBid: '$250',
        maxBid: '$500',
        duration: '6 Weeks',
    },
    {
        id: 2,
        title: 'Minton v3.0 - Redesign',
        state: 'Ongoing',
        image: design,
        shortDesc:
            'This card has supporting text below as a natural lead-in to additional content is a little bit longer',
        totalTasks: null,
        totalComments: null,
        totalMembers: null,
        progress: null,
        field: 'Civil Engineering',
        skill: 'Design',
        minBid: '$250',
        maxBid: '$500',
        duration: '6 Weeks',
    },
    {
        id: 3,
        title: 'Desk v2.1 - Angular and Django',
        state: 'Ongoing',
        image: cad,
        shortDesc: 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid',
        totalTasks: null,
        totalComments: null,
        totalMembers: null,
        progress: null,
        field: 'Civil Engineering',
        skill: 'CAD',
        minBid: '$250',
        maxBid: '$500',
        duration: '6 Weeks',
    },
];

class ExecutiveDashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isCreateProjectOpen: false,
            selected: projects[0],
        };
    }

    handleCreateProjectClick = () => {
        this.setState({
            isCreateProjectOpen: !this.state.isCreateProjectOpen,
        });
    };

    render() {
        return (
            <React.Fragment>
                <Row>
                    <Col>
                        <div className="page-title-box mt-2">
                            <div className="page-title mb-0">
                                <h4 className="mb-0">ABC Company (Pvt) Ltd</h4>
                                <p className="text-muted font-14">
                                    <span>
                                        <i className="mdi mdi-map-marker"></i> 3940 Richison Drive
                                    </span>
                                    <span className="ml-2">
                                        <i className="mdi mdi-phone-outline"></i> +1 456 9595 9594
                                    </span>
                                </p>
                            </div>
                        </div>
                    </Col>
                </Row>

                <Row className="mb-2">
                    <Col md={3}>
                        <div className="addProjectWrapperCard text-center" onClick={this.handleCreateProjectClick}>
                            <div className="addProjectText">
                                <i className="mdi mdi-briefcase-plus-outline"></i>
                                <h3>create project</h3>
                            </div>
                        </div>
                    </Col>
                    <Col md={9}>
                        <Row>
                            {projects.map((project, i) => {
                                if (i < 3) {
                                    return (
                                        <Col md={4} key={'proj-' + project.id}>
                                            <SingleProject
                                                project={project}
                                                revision={true}
                                                selected={this.state.selected}
                                                onClick={(project) => this.setState({ selected: project })}
                                            />
                                        </Col>
                                    );
                                }else{
                                    return null;
                                }
                            })}
                        </Row>
                    </Col>
                </Row>

                <hr />

                <Collapse isOpen={this.state.isCreateProjectOpen}>
                    <Row>
                        <Col>
                            <CreateProjectForm toggleForm={this.handleCreateProjectClick} />
                        </Col>
                    </Row>
                </Collapse>

                <Collapse isOpen={!this.state.isCreateProjectOpen}>
                    <SelectedProject project={this.state.selected} />
                </Collapse>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.Auth,
    };
};

export default connect(mapStateToProps)(ExecutiveDashboard);
