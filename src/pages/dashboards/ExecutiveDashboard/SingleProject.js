import React from 'react';
import { Card, CardBody, Progress, Badge } from 'reactstrap';
import classNames from 'classnames';

const SingleProject = (props) => {
    const { selected, onClick } = props;
    const project = props.project || {};

    return (
        <Card
            className={`d-block executive-card ${selected && selected.id === project.id && 'active-card'}`}
            onClick={() => onClick(project)}>
            {project.image && (
                <React.Fragment>
                    <img className="card-img-top" src={project.image} alt="" />
                    <div className="card-img-overlay">
                        {project.state != null && (
                            <div
                                className={classNames(
                                    'badge',
                                    {
                                        'badge-success': project.state === 'Completed',
                                        'badge-info': project.state === 'Ongoing',
                                        'badge-light': project.state === 'Planned',
                                    },
                                    'p-1'
                                )}>
                                {project.state !== 'Planned' ? project.state : 'Bidding on process'}
                            </div>
                        )}
                    </div>
                </React.Fragment>
            )}

            <CardBody className={project.image ? 'position-relative' : ''}>
                <h4 className="mt-0 text-title">{project.title}</h4>

                <p className="mb-1">
                    {project.image && project.field != null && (
                        <Badge color="primary" className="mr-1" style={{ fontWeight: '500' }}>
                            {project.field}
                        </Badge>
                    )}
                   
                    {project.image && project.duration != null && (
                        <Badge color="secondary" className="mr-1" style={{ fontWeight: '500' }}>
                            {project.duration}
                        </Badge>
                    )}
                </p>

                <div className="text-muted mt-2 mb-1">
                    {project.shortDesc && project.shortDesc.length > 100
                        ? `${project.shortDesc.substring(0, 100)}...`
                        : project.shortDesc}
                </div>

                <p className="mb-1">
                    {project.image && project.totalTasks != null && (
                        <span className="pr-2 text-nowrap mb-2 d-inline-block">
                            <i className="mdi mdi-format-list-bulleted-type text-muted mr-1"></i>
                            <b>{project.totalTasks}</b> Tasks
                        </span>
                    )}
                    {project.image && project.totalComments != null && (
                        <span className="text-nowrap mb-2 d-inline-block">
                            <i className="mdi mdi-comment-multiple-outline text-muted mr-1"></i>
                            <b>{project.totalComments}</b> Comments
                        </span>
                    )}
                </p>

                {project.image && project.progress != null && project.state !== 'Ongoing' && (
                    <React.Fragment>
                        {project.progress <= 30 && (
                            <div>
                                <p className="mt-3 mb-2 font-weight-bold">
                                    Progress <span className="float-right">{project.progress}%</span>
                                </p>
                                <Progress value={project.progress} color="warning" className="progress-sm" />
                            </div>
                        )}
                        {project.progress > 30 && project.progress < 100 && (
                            <div>
                                <p className="mt-3 mb-2 font-weight-bold">
                                    Progress <span className="float-right">{project.progress}%</span>
                                </p>
                                <Progress value={project.progress} color="success" className="progress-sm" />
                            </div>
                        )}
                        {project.progress === 100 && (
                            <span></span>
                            // <Progress value={project.progress} color="success" className="progress-sm" />
                        )}
                    </React.Fragment>
                )}
            </CardBody>
        </Card>
    );
};

export default SingleProject;
