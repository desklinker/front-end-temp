import React, { useState } from 'react';

import { Badge, Card, CardBody, Col, Row, Button } from 'reactstrap';
import classNames from 'classnames';
import Files from '../../projects/Files';
import FileAttachment from './FileAttachment';
import Comments from './Comments';
import CreateProjectForm from './CreateProjectForm';

function SelectedProject({ project }) {
    const [isEdit, setEdit] = useState(false);

    const toggle = () => setEdit(!isEdit);

    return (
        <React.Fragment>
            <Row>
                <Col md={8}>
                    {!isEdit ? (
                        <Card className="d-block">
                            <CardBody>
                                <>
                                    <Row>
                                        <Col md={6}>
                                            <img
                                                className="project-img"
                                                src={project.image}
                                                alt={`${project.title}-img`}
                                            />
                                        </Col>
                                        <Col md={6}>
                                            <Button className="close" aria-label="Close" onClick={toggle}>
                                                <i className="mdi mdi-square-edit-outline"></i>
                                            </Button>
                                            <h3 className="mt-0">{project.title}</h3>
                                            <div
                                                className={classNames(
                                                    'badge',
                                                    {
                                                        'badge-success': project.status === 'Completed',
                                                        'badge-info': project.status === 'Ongoing',
                                                        'badge-primary': project.status === 'Draft',
                                                    },
                                                    'mb-1'
                                                )}>
                                                {project.status}
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12} className="mt-3">
                                            <h5>Outline:</h5>

                                            <p className="text-muted mb-2">
                                                With supporting text below as a natural lead-in to additional
                                                contenposuere erat a ante. Voluptates, illo, iste itaque voluptas
                                                corrupti ratione reprehenderit magni similique? Tempore, quos delectus
                                                asperiores libero voluptas quod perferendis! Voluptate, quod illo rerum?
                                                Lorem ipsum dolor sit amet.
                                            </p>

                                            <p className="text-muted mb-4">
                                                Voluptates, illo, iste itaque voluptas corrupti ratione reprehenderit
                                                magni similique? Tempore, quos delectus asperiores libero voluptas quod
                                                perferendis! Voluptate, quod illo rerum? Lorem ipsum dolor sit amet.
                                                With supporting text below as a natural lead-in to additional
                                                contenposuere erat a ante.
                                            </p>
                                        </Col>
                                    </Row>
                                </>
                            </CardBody>
                        </Card>
                    ) : (
                        <CreateProjectForm />
                    )}
                    <Card>
                        <CardBody>
                            <h3 className="mb-1">{project.title}</h3>
                            <div className="mb-2">
                                <Badge color="secondary">{project.status}</Badge>
                            </div>
                            <Comments />
                        </CardBody>
                    </Card>
                </Col>

                <Col md={4}>
                    <Files title="Project files" />
                    <FileAttachment />
                </Col>
            </Row>
        </React.Fragment>
    );
}

export default SelectedProject;
