import React, { Component } from 'react';
import { Button, Card, CardBody, Col, FormGroup, Row } from 'reactstrap';
import { AvField, AvForm } from 'availity-reactstrap-validation';
import FileUploader from '../../../components/FileUploader';

class FileAttachment extends Component {
    constructor(props) {
        super(props);

        this.handleValidSubmit = this.handleValidSubmit.bind(this);
        this.updateValues = this.updateValues.bind(this);
    }

    /**
     * Update values
     */
    updateValues = (field, fieldValue) => {
        const state = { ...this.state.state };
        state[field] = fieldValue;
        this.setState(state);
    };

    /**
     * Handle the form submission
     */
    handleValidSubmit = (e, values) => {
        console.log({ ...values, ...this.state });
    };

    render() {
        return (
            <Card>
                <CardBody>
                    <Row>
                        <Col>
                            <AvForm onValidSubmit={this.handleValidSubmit}>
                                <Row>
                                    <Col>
                                        <FormGroup>
                                            <FileUploader
                                                onFileUpload={(files) => {
                                                    this.updateValues('avatar', files);
                                                }}
                                            />
                                        </FormGroup>

                                        <AvField
                                            name="driveLink"
                                            label=""
                                            placeholder="Insert if you have any Drive attachments"
                                            type="text"
                                        />
                                        <Row className="mt-2">
                                            <Col className="text-right">
                                                <Button id="caret" color="success" type="submit">
                                                    Submit
                                                </Button>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </AvForm>
                        </Col>
                    </Row>
                </CardBody>
            </Card>
        );
    }
}

export default FileAttachment;
