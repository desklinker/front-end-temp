import React from 'react';
import { connect } from 'react-redux';
import AdminDashboard from './AdminDashboard';
import ScreenerDashboard from './ScreenerDashboard';
import ExecutiveDashboard from './ExecutiveDashboard';
import ClientDashboard from './ClientDashboard';
import ProfessionalDashboard from './ProfessionalDashboard';
import CoodinatorDashboard from './CoodinatorDashboard';
import './styles/dashboard.css';
import { ADMIN, PROFESSIONAL, COORDINATOR, SCREENER, CLIENT } from '../../constants';

function Dashboard({ user, history, match, ...props }) {
  const role = user && user.role;

  switch (role) {
    case ADMIN:
      return <AdminDashboard />;

    case PROFESSIONAL:
      return <ProfessionalDashboard />;

    case COORDINATOR:
      return <CoodinatorDashboard />;

    case SCREENER:
      return <ScreenerDashboard />;

    case CLIENT:
      return <ClientDashboard history={history} user={user} />;

    default:
      return <ExecutiveDashboard />;
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.Auth.user,
  };
};

export default connect(mapStateToProps)(Dashboard);
