import React, { useState } from 'react';
import { Row, Col, Collapse } from 'reactstrap';
import SingleProject from '../../../components/SingleProject';

import Prograssbar from '../../../components/Progressbar';
import ConfirmModal from '../../../components/ConfirmModal';
import './styles/client.css';
import CreateProject from '../../projects/CreateProject';
import { useLoadProjects } from './hooks';
import { Modal } from '../../../components/Modal';

function ClientDashboard({ history, user }) {
  const [isShow, setShow] = useState(false);
  const [isSubmitting, setSubmitting] = useState(false);
  const [isConfirm, setConfirm] = useState(false);

  const { projects, onLoadProjects } = useLoadProjects({ user });

  const handleConfirmClick = () => {
    setShow(true);
    setConfirm(false);
    setSubmitting(false);
  };

  const toggleProgressbar = (value) => {
    if (value !== 'close') {
      setSubmitting(!isSubmitting);
    }
    if (!isSubmitting) {
      setShow(false);
    } else if (value === 'close') {
      setConfirm(true);
    }
  };

  return (
    <React.Fragment>
      <Row>
        <Col>
          <div className="page-title-box mt-2">
            <h4 className="page-title mb-0">Hi Vignesh, Let's work together on our next project !</h4>
          </div>
        </Col>
      </Row>

      {isSubmitting && (
        <Prograssbar isOpen={isSubmitting} toggle={toggleProgressbar} history={history} isConfirmOpen={isConfirm} />
      )}

      <Modal />
      
      {isConfirm && (
        <ConfirmModal
          isOpen={isConfirm}
          toggle={() => setConfirm(!isConfirm)}
          onClick={handleConfirmClick}
          text="Are you confirm cancel this uploading?"
          header="Confirm to Cancel"
        />
      )}

      <Collapse isOpen={isShow}>
        <Row>
          <Col>
            {isShow && (
              <CreateProject
                isShow={isShow}
                setShow={setShow}
                history={history}
                user={user}
                toggleProgressbar={toggleProgressbar}
                onLoad={onLoadProjects}
              />
            )}
          </Col>
        </Row>
      </Collapse>

      <Row className="mb-2">
        <Col md={4} lg={3}>
          <div className="addProjectWrapperCard text-center" onClick={() => setShow(!isShow)}>
            <div className="addProjectText">
              <i className="mdi mdi-briefcase-plus-outline"></i>
              <h3>create project</h3>
            </div>
          </div>
        </Col>
        {projects &&
          projects.map((project, key) => {
            return (
              <Col md={4} lg={3} key={`proj-${key}`} className="d-flex">
                <SingleProject project={project} />
              </Col>
            );
          })}
      </Row>

      {/* <Row>
        <Col md={12}>
          <h3 className="mb-3">Recently completed projects</h3>
        </Col>
        {recentProjects.map((project, i) => {
          if (i < 4) {
            return (
              <Col md={4} lg={3} key={'proj-' + project.id} className="d-flex">
                <SingleProject project={project} revision={true} />
              </Col>
            );
          } else {
            return null;
          }
        })}
      </Row> */}
    </React.Fragment>
  );
}

export default ClientDashboard;
