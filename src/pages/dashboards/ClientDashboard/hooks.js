import { useCallback, useState, useEffect } from 'react';
import { loadProjects } from '../../../actions/project';
import { useDispatch } from 'react-redux';

export function useLoadProjects({ user }) {
  const dispatch = useDispatch();
  const [projects, setProjects] = useState(null);
  const { userId } = user;

  const onLoadProjects = useCallback(async () => {
    try {
      const res = await dispatch(loadProjects(userId));
      setProjects(res.projects);
    } catch (e) {
      //
    }
  }, [dispatch, userId]);

  useEffect(() => {
    onLoadProjects();
  }, [onLoadProjects]);

  return { projects, onLoadProjects };
}
