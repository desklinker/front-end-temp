import avatar2 from '../../assets/images/users/avatar-2.jpg';
import avatar4 from '../../assets/images/users/avatar-4.jpg';
import avatar5 from '../../assets/images/users/avatar-5.jpg';
import avatar7 from '../../assets/images/users/avatar-7.jpg';

const users = [
    {
        id: 1,
        name: 'Steel Project',
        avatar: avatar2,
        lastMessage: 'Latest thread message by the user...',
        totalUnread: 3,
        lastMessageOn: '4:30am',
        email: 'support@coderthemes.com',
        phone: '+1 456 9595 9594',
        location: 'California, USA',
        languages: 'English, German, Spanish',
        groups: 'Work, Favourties',
    },
    {
        id: 2,
        name: 'Timber Project',
        avatar: avatar5,
        lastMessage: "Hey! a reminder for tomorrow's meeting?",
        totalUnread: 0,
        lastMessageOn: '5:30am',
        email: 'support@coderthemes.com',
        phone: '+1 456 9595 9594',
        location: 'New York, USA',
        languages: 'English, German, Spanish',
        groups: 'Work, Friends',
    },
    {
        id: 3,
        name: 'Concrete Project',
        avatar: avatar4,
        lastMessage: "Are we going to have this week's planning meeting?",
        totalUnread: 2,
        lastMessageOn: 'Thu',
        email: 'support@coderthemes.com',
        phone: '+1 456 9595 9594',
        location: 'New Jersey, USA',
        languages: 'English, German, Spanish',
        groups: 'Work, Favourties',
    },
];

const messages = [];

const defaultTo = {
    id: 9,
    name: 'Shreyu N',
    avatar: avatar7,
    email: 'support@coderthemes.com',
    phone: '+1 456 9595 9594',
    location: 'California, USA',
    languages: 'English, German, Spanish',
    groups: 'Work, Friends',
};

for (const user of users) {
    messages.push(
        {
            id: 1,
            message: {
                type: 'text',
                value: 'Hello!',
            },
            to: defaultTo,
            from: user,
            sendOn: '10:00',
        },
        {
            id: 2,
            message: {
                type: 'text',
                value: 'Hi, How are you? What about our next meeting?',
            },
            to: user,
            from: defaultTo,
            sendOn: '10:01',
        },
        {
            id: 3,
            message: {
                type: 'text',
                value: 'Yeah everything is fine',
            },
            to: defaultTo,
            from: user,
            sendOn: '10:01',
        },
        {
            id: 4,
            message: {
                type: 'text',
                value: 'Awesome!',
            },
            to: user,
            from: defaultTo,
            sendOn: '10:02',
        },
        {
            id: 5,
            message: {
                type: 'text',
                value: "Let's have it today if you are free",
            },
            to: defaultTo,
            from: user,
            sendOn: '10:03',
        },
        {
            id: 6,
            message: {
                type: 'text',
                value: 'Sure thing! let me know if 2pm works for you',
            },
            to: user,
            from: defaultTo,
            sendOn: '10:03',
        },
        {
            id: 7,
            message: {
                type: 'text',
                value: 'Sorry, I have another meeting scheduled at 2pm. Can we have it at 3pm instead?',
            },
            to: defaultTo,
            from: user,
            sendOn: '10:04',
        },
        {
            id: 8,
            message: {
                type: 'text',
                value: 'We can also discuss about the presentation talk format if you have some extra mins',
            },
            to: defaultTo,
            from: user,
            sendOn: '10:04',
        },
        {
            id: 9,
            message: {
                type: 'text',
                // tslint:disable-next-line: max-line-length
                value:
                    "3pm it is. Sure, let's discuss about presentation format, it would be great to finalize today. I am attaching the last year format and assets here..",
            },
            to: user,
            from: defaultTo,
            sendOn: '10:05',
        },
        {
            id: 10,
            message: {
                type: 'file',
                value: {
                    file: 'Hyper-admin.zip',
                    size: '2.3MB',
                },
            },
            to: user,
            from: defaultTo,
            sendOn: '10:05',
        }
    );
}

const countries = [
    { value: 'sin', label: 'Singapore' },
    { value: 'aus', label: 'Australia' },
];

const projectStatus = [
    { value: '1', label: 'Just posted' },
    { value: '2', label: 'Screened' },
    { value: '3', label: 'Bid on progress' },
    { value: '4', label: 'Project ongoing' },
    { value: '5', label: 'Project completed' },
    { value: '6', label: 'Pending for payment' },
    { value: '7', label: 'Revision on progress' },
    { value: '8', label: 'Project completed > 14 days' },
    { value: '9', label: 'Project completed < 14 days' },
    { value: '10', label: 'Terminated projects' },
];

const categories = [
    { value: '1', label: 'Civil Engineering' },
    { value: '2', label: 'Mechanical Engineering' },
    { value: '3', label: 'Computer Science' },
];

const outputDeliverStatus = [
    { value: '1', label: 'Drafts passed' },
    { value: '2', label: 'Pending client confirmation' },
    { value: '3', label: 'Final output passed' },
    { value: '4', label: 'Revisions on progress' },
    { value: '5', label: 'Scope changes on progress' },
];

const paymentStatus = [
    { value: '1', label: 'Pending' },
    { value: '2', label: 'Success' },
    { value: '3', label: 'Failed' },
];
const skills = [
    { value: 'cad', label: 'CAD' },
    { value: 'design', label: 'Design' },
    { value: 'arch', label: 'Architecture' },
    { value: 'draft', label: 'Draft' },
];

const coordinators = [
    { value: '1', label: 'Brandon Smith (2)' },
    { value: '2', label: 'Thomazin Ursula (6)' },
    { value: '3', label: 'Isabell Esabell (2)' },
    { value: '4', label: 'Jennat Marion (1)' },
];

export {
    users,
    messages,
    countries,
    paymentStatus,
    outputDeliverStatus,
    categories,
    projectStatus,
    skills,
    coordinators,
};
