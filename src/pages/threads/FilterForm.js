import React, { useEffect, useState } from 'react';
import { Button, Col, FormGroup, Label, Row, Input } from 'reactstrap';
import Select from 'react-select';
import { coordinators, outputDeliverStatus, paymentStatus, categories, projectStatus } from './data';
import CardBody from 'reactstrap/es/CardBody';
import Card from 'reactstrap/es/Card';
import Flatpickr from 'react-flatpickr';

const initialValues = {
    category: '',
    client: '',
    codi: '',
    professional: '',
    admin: '',
    minBudget: '',
    maxBudget: '',
    postedTime: '',
    durationStart: '',
    durationEnd: '',
    projectStatus: '',
    outputDeliverStatus: '',
    paymentStatus: '',
};

function FilterForm({ setFilterOpen }) {
    const [values, setValues] = useState({});

    const updateValues = (field, fieldValue) => {
        setValues({
            ...values,
            [field]: fieldValue,
        });
    };

    const handleInputChange = (e, field) => {
        if (e) {
            updateValues(field, e.target.value);
        }
    };

    const handleSubmit = () => {
        setFilterOpen(false);
        console.log(values, 'value');
    };

    useEffect(() => {
        setValues(initialValues);
    }, []);

    return (
        <Card>
            <CardBody>
                <FormGroup row>
                    <Col sm={3}>
                        <Label>Category</Label>
                        <Select
                            placeholder="Select category..."
                            className="react-select"
                            classNamePrefix="react-select"
                            value={(values && values.category) || ''}
                            onChange={(value) => {
                                updateValues('category', value);
                            }}
                            options={categories}
                        />
                    </Col>
                    <Col sm={3}>
                        <Label>Client</Label>
                        <Select
                            placeholder="Select..."
                            value={(values && values.client) || ''}
                            className="react-select"
                            classNamePrefix="react-select"
                            onChange={(value) => {
                                updateValues('client', value);
                            }}
                            options={coordinators}
                        />
                    </Col>
                    <Col sm={3}>
                        <Label>Coordinator</Label>
                        <Select
                            placeholder="Select..."
                            value={(values && values.codi) || ''}
                            className="react-select"
                            classNamePrefix="react-select"
                            onChange={(value) => {
                                updateValues('codi', value);
                            }}
                            options={coordinators}
                        />
                    </Col>
                    <Col sm={3}>
                        <Label>Professional</Label>
                        <Select
                            placeholder="Select..."
                            value={(values && values.professional) || ''}
                            className="react-select"
                            classNamePrefix="react-select"
                            onChange={(value) => {
                                updateValues('professional', value);
                            }}
                            options={coordinators}
                        />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col sm={3}>
                        <Label>Admin</Label>
                        <Select
                            placeholder="Select..."
                            value={(values && values.admin) || ''}
                            className="react-select"
                            classNamePrefix="react-select"
                            onChange={(value) => {
                                updateValues('admin', value);
                            }}
                            options={coordinators}
                        />
                    </Col>
                    <Col sm={3}>
                        <Label>Budget</Label>
                        <Row>
                            <Col>
                                <Input
                                    type="number"
                                    name="minBudget"
                                    label=""
                                    placeholder="Min value"
                                    min={0}
                                    value={values.minBudget || ''}
                                    onChange={(e) => handleInputChange(e, 'minBudget')}
                                />
                            </Col>
                            <Col>
                                <Input
                                    type="number"
                                    name="maxBudget"
                                    label=""
                                    placeholder="Max value"
                                    disabled={!values.minBudget}
                                    min={values.minBudget || 0}
                                    value={values.maxBudget || ''}
                                    onChange={(e) => handleInputChange(e, 'maxBudget')}
                                />
                            </Col>
                        </Row>
                    </Col>
                    <Col sm={3}>
                        <Label>Posted time</Label>
                        <Flatpickr
                            className="form-control"
                            value={values && values.postedTime}
                            onChange={(date) => {
                                updateValues('postedTime', date);
                            }}
                            options={{ mode: 'range', enableTime: true }}
                        />
                    </Col>
                    <Col sm={3}>
                        <Label>Duration</Label>
                        <Row>
                            <Col>
                                <Input
                                    type="number"
                                    name="durationStart"
                                    label=""
                                    placeholder="Min value"
                                    value={values.durationStart || ''}
                                    onChange={(e) => handleInputChange(e, 'durationStart')}
                                />
                            </Col>
                            <Col>
                                <Input
                                    type="number"
                                    name="durationEnd"
                                    label=""
                                    placeholder="Max value"
                                    disabled={!values.durationStart}
                                    min={values.durationStart || 0}
                                    value={values.durationEnd || ''}
                                    onChange={(e) => handleInputChange(e, 'durationEnd')}
                                />
                            </Col>
                        </Row>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col sm={3}>
                        <Label>Project status</Label>
                        <Select
                            placeholder="Select..."
                            value={(values && values.projectStatus) || ''}
                            className="react-select"
                            classNamePrefix="react-select"
                            onChange={(value) => {
                                updateValues('projectStatus', value);
                            }}
                            options={projectStatus}
                        />
                    </Col>
                    <Col sm={3}>
                        <Label>Output deliver status</Label>
                        <Select
                            placeholder="Select..."
                            value={(values && values.outputDeliverStatus) || ''}
                            className="react-select"
                            classNamePrefix="react-select"
                            onChange={(value) => {
                                updateValues('outputDeliverStatus', value);
                            }}
                            options={outputDeliverStatus}
                        />
                    </Col>
                    <Col sm={3}>
                        <Label>Payment status</Label>
                        <Select
                            placeholder="Select..."
                            value={(values && values.paymentStatus) || ''}
                            className="react-select"
                            classNamePrefix="react-select"
                            onChange={(value) => {
                                updateValues('paymentStatus', value);
                            }}
                            options={paymentStatus}
                        />
                    </Col>
                </FormGroup>

                <Row>
                    <Col className="text-right">
                        <Button
                            type="button"
                            color="secondary"
                            className="mr-2"
                            onClick={() => {
                                setValues(initialValues);
                                setFilterOpen(false);
                            }}>
                            Cancel
                        </Button>
                        <Button type="submit" color="primary" onClick={(e) => handleSubmit(e)}>
                            Filter
                        </Button>
                    </Col>
                </Row>
            </CardBody>
        </Card>
    );
}

export default FilterForm;
