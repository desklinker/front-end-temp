import React  from 'react';
import Comments from '../projects/Comments';
import { CardBody, Card, Badge, Button } from 'reactstrap';

class ThreadArea extends React.Component {
    render() {
        return (
            <Card>
                <CardBody>
                    <h3 className="mb-1">Project Title</h3>
                    <span className="text-muted">sub project of Main Project Name</span>
                    <div className="mb-2">
                        <Badge color="secondary">On going</Badge>
                    </div>
                    <Comments />
                    <div className="mb-3">
                        <p>Project members</p>
                        <Button className="font-13 mr-1" size="sm" color="primary" outline>
                            CODID: 3234
                        </Button>
                        <Button className="font-13 mr-1" size="sm" color="primary" outline>
                            EXE: JHON
                        </Button>
                        <Button className="font-13 mr-1" size="sm" outline>
                            EXE: RAM
                        </Button>
                    </div>
                </CardBody>
            </Card>
        );
    }
}

export default ThreadArea;
