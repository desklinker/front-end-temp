import React, { useState } from 'react';
import { Row, Col, Collapse } from 'reactstrap';

import PageTitle from '../../components/PageTitle';
import ChatUsers from './ChatUsers';

import { users } from './data';
import ThreadArea from './ThreadArea';
import FilterForm from './FilterForm';

// ChatApp
function Threads() {
    const [selectedUser, setSelectedUser] = useState(users[0]);
    const [isFiltersOpen, setFiltersOpen] = useState(false);

    const onUserChange = (user) => {
        setSelectedUser(user);
    };

    const toggle = () => setFiltersOpen(!isFiltersOpen);

    const search = (text) => {
        //
    };

    return (
        <React.Fragment>
            <PageTitle
                breadCrumbItems={[{ label: 'Contacts', path: '/contacts', active: true }]}
                title={'Threads'}
                isFilter
                toggle={toggle}
                isSearch
                search={search}
            />

            <Collapse isOpen={isFiltersOpen}>
                <FilterForm setFilterOpen={setFiltersOpen} />
            </Collapse>

            <Row>
                <Col xl={3} lg={3} md={12} className="order-lg-1 order-xl-1">
                    <ChatUsers onUserSelect={onUserChange} />
                </Col>

                <Col xl={9} lg={9} md={12} className="order-lg-2 order-xl-1">
                    <ThreadArea selectedUser={selectedUser} />
                </Col>
            </Row>
        </React.Fragment>
    );
}

export default Threads;
