import React, { useState } from 'react';
import { Card, Col, Row } from 'reactstrap';
import map from 'lodash-es/map';
import chunk from 'lodash-es/chunk';
import FileUploader from '../../components/FileUploader';

const FileItem = ({ project }) => {
    const [values, setValues] = useState({});

    const updateValues = (field, fieldValue) => {
        setValues({
            ...values,
            [field]: fieldValue,
        });
    };

    return (
        <>
            <div className="mb-2">
                {project &&
                    map(chunk(project.files, 3), (chunkedFiles, cKey) => {
                        return (
                            <Row>
                                {map(chunkedFiles, (item, key) => {
                                    return (
                                        <Col md={4} key={key}>
                                            <Card className="mb-1 shadow-none border">
                                                <div className="px-2 py-1">
                                                    <Row className="align-items-center">
                                                        <div className="col">
                                                            <a href="/" className="text-muted font-weight-bold">
                                                                Hyper-admin-design.zip
                                                            </a>
                                                            <p className="mb-0 text-muted">
                                                                {item.size && `${item.postedOn} | ${item.size}`}
                                                            </p>
                                                        </div>
                                                        <div className="col-auto">
                                                            <a href="/" className="text-muted mr-2">
                                                                <i className="dripicons-trash"></i>
                                                            </a>
                                                            <a href="/" className="text-muted">
                                                                <i className="dripicons-download"></i>
                                                            </a>
                                                        </div>
                                                    </Row>
                                                </div>
                                            </Card>
                                        </Col>
                                    );
                                })}
                            </Row>
                        );
                    })}
            </div>
            <FileUploader
                onFileUpload={(files) => {
                    updateValues('avatar', files);
                }}
            />
        </>
    );
};

export default FileItem;
