import React, { Fragment } from 'react';
import { Media } from 'reactstrap';
import classnames from 'classnames';
import { Link } from 'react-router-dom';

function ProjectItem({ index, activateProject, project, selectedProject }) {
    return (
        <Fragment key={index}>
            <Link
                to="#"
                key={index}
                className="text-body"
                onClick={(e) => {
                    activateProject(project);
                }}>
                <Media
                    className={classnames('mt-1', 'p-2', {
                        'bg-light': project.id === selectedProject.id,
                    })}>
                    <Media body>
                        <h5 className="mt-0 mb-0 font-14">
                            <span className="float-right text-muted font-12">{project.duration}</span>
                            {project.title}
                        </h5>
                        <p className="mt-1 mb-0 text-muted font-14">
                            <span className="w-75">{`${
                                project.shortDesc && project.shortDesc.substring(0, 45)
                            }...`}</span>
                        </p>
                    </Media>
                </Media>
            </Link>
        </Fragment>
    );
}

export default ProjectItem;
