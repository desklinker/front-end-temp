import React, { useState } from 'react';
import PageTitle from '../../components/PageTitle';
import { Col, Row } from 'reactstrap';
import ProjectList from './ProjectList';
import ProjectFiles from './ProjectFiles';

function FileManager() {
    const [selectedProject, setSelectedProject] = useState();

    const onProjectSelect = (project) => {
        setSelectedProject(project);
    };

    const search = (text) => {
        //
    };

    return (
        <>
            <PageTitle
                breadCrumbItems={[{ label: 'File Manager', path: '/file-manager', active: true }]}
                title={'File Manger'}
                isSearch
                search={search}
            />

            <Row>
                <Col xl={3} lg={3} md={12} className="order-lg-1 order-xl-1">
                    <ProjectList onProjectSelect={onProjectSelect} />
                </Col>

                <Col xl={9} lg={9} md={12} className="order-lg-2 order-xl-1">
                    <ProjectFiles project={selectedProject} />
                </Col>
            </Row>
        </>
    );
}

export default FileManager;
