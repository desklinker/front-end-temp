import React from 'react';
import Card from 'reactstrap/es/Card';
import CardBody from 'reactstrap/es/CardBody';
import { Badge } from 'reactstrap';
import FileItem from './FileItem';

function ProjectFiles({ project }) {
    return (
        <Card>
            <CardBody>
                <h3 className="mb-0">{project && `${project.title}`}</h3>
                <div className="text-muted">These files are related to selected project</div>
                <Badge color="secondary mb-3">{project ? project.status : 'Ongoing'}</Badge>

                <h4 className="header-title mb-2"> Screener</h4>
                <FileItem project={project} />
                <hr />

                <h4 className="header-title mb-2"> Coordinator</h4>
                <FileItem project={project} />
                <hr />

                <h4 className="header-title mb-2"> Professional</h4>
                <FileItem project={project} />
                <hr />
            </CardBody>
        </Card>
    );
}

export default ProjectFiles;
