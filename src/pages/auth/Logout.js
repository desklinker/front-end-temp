import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { poolData } from '../../config/env';
import { CognitoUserPool, CognitoUser } from 'amazon-cognito-identity-js';
import { logoutUser } from '../../redux/actions';

class Logout extends Component {
  /**
   * Redirect to login
   */
  componentDidMount() {
    const userPool = new CognitoUserPool(poolData);
    const userData = {
      Username: this.props.user?.username,
      Pool: userPool,
    };

    const cognitoUser = new CognitoUser(userData);
    cognitoUser.signOut();

    // emit the event
    this.props.logoutUser(this.props.history);
  }

  render() {
    return <React.Fragment></React.Fragment>;
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.Auth.user,
  };
};

export default withRouter(connect(mapStateToProps, { logoutUser })(Logout));
