import React from 'react';
import scrolldown from '../../../../assets/images/register/scrolldown.gif';
import scrolldownDark from '../../../../assets/images/register/scrolldown-dark.gif';

import FadeInUp from './FadeInUp';

function SlideFooter({ isMounted, isDark, delay }) {
    return (
        <div className="slide-footer">
            <FadeInUp isMounted={isMounted} delay={delay ? delay : 1}>
                <img src={isDark ? scrolldownDark : scrolldown} alt="" />
            </FadeInUp>
        </div>
    );
}

export default SlideFooter;
