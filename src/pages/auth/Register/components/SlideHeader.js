import React from 'react';
import { useHistory } from 'react-router-dom';

function SlideHeader() {
    const history = useHistory();

    const { hash } = history && history.location;

    return (
        <>
            <nav className={`navbar navbar-expand-sm navbar-slide ${hash.includes('#introSection/') ? 'bg-dark' : ''}`}>
                <div className="container-fluid mx-5">
                    <a className="navbar-brand" href="#introSection">
                        Brand
                    </a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar1">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbar1">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <a
                                    className={`nav-link ${hash === '#introSection/1' ? 'active' : ''}`}
                                    href="#introSection/1">
                                    About
                                </a>
                            </li>
                            <li className="nav-item">
                                <a
                                    className={`nav-link ${hash === '#introSection/2' ? 'active' : ''}`}
                                    href="#introSection/2">
                                    Contact us
                                </a>
                            </li>
                            <li className="nav-item">
                                <a
                                    className={`nav-link ${hash === '#introSection/3' ? 'active' : ''}`}
                                    href="#introSection/3">
                                    F.A.Q.
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </>
    );
}

export default SlideHeader;
