import React, { useMemo } from 'react';
import { motion } from 'framer-motion';

function FadeInUp({ isMounted, children, delay, yOffset = 24, duration = 0.5 }) {
    const easing = [0.42, 0, 0.58, 1];

    const transition = useMemo(
        () => ({
            duration,
            ease: easing,
        }),
        [duration, easing]
    );

    const fadeInUp = {
        hidden: { y: yOffset, opacity: 0, transition },
        animate: {
            y: 0,
            opacity: 1,
            transition: { ...transition, delay: delay },
        },
    };

    return (
        <motion.div initial="hidden" animate={isMounted ? 'animate' : 'hidden'} exit="hidden" variants={fadeInUp}>
            {children}
        </motion.div>
    );
}

export default FadeInUp;
