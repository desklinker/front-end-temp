import React from 'react';
import ContactUsImg from '../../../../assets/images/register/contact-us.svg';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import { FormGroup, Button } from 'reactstrap';
import SlideHeader from '../components/SlideHeader';

function ContactUsSlide({ active }) {
    const handleSubmit = (event, values) => {
        //
    };

    return (
        <div>
            <SlideHeader />
            <div className="content">
                <div className="row">
                    <div className="col-lg-5 offset-lg-1">
                        <h1 className="mb-2 text-primary font-weight-bold"> QUESTIONS? </h1>
                        <h3 className="mb-4"> CONTACT US</h3>
                        <p className="w-75">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book.
                        </p>

                        <div className="row">
                            <div className="col-lg-9">
                                <AvForm onValidSubmit={handleSubmit} className="contact-slide">
                                    <AvField type="email" name="email" label="" placeholder="E-mail" />

                                    <AvField type="textarea" name="revision" label="" placeholder="Your message" />
                                    <FormGroup className="text-left">
                                        <Button className="btn btn-danger">Send</Button>
                                    </FormGroup>
                                </AvForm>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-5">
                        <img src={ContactUsImg} alt="about-img" className="w-100" />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ContactUsSlide;
