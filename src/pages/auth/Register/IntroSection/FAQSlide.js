import React from 'react';
import SlideHeader from '../components/SlideHeader';

import { Card, CardBody, CardHeader, NavLink, UncontrolledCollapse } from 'reactstrap';

function FAQSlide({ active }) {
    return (
        <div>
            <SlideHeader />
            <div className="content pt-3">
                <div className="row mt-lg-5">
                    <div className="col-sm-12">
                        <div className="text-center">
                            <h3 className="">Frequently Asked Questions</h3>
                            <p className="text-muted mt-3">
                                Nisi praesentium similique totam odio obcaecati, reprehenderit, dignissimos rem
                                temporibus ea inventore alias!
                                <br /> Beatae animi nemo ea tempora, temporibus laborum facilis ut!
                            </p>

                            <button type="button" className="btn btn-success btn-sm mt-2">
                                <i className="mdi mdi-email-outline mr-1"></i> Email us your question
                            </button>
                            <button type="button" className="btn btn-info btn-sm mt-2 ml-1">
                                <i className="mdi mdi-twitter mr-1"></i> Send us a tweet
                            </button>
                        </div>
                    </div>
                </div>

                <div className="row pt-5">
                    <div className="col-lg-10 offset-lg-1">
                        <div className="content pt-3">
                            <div id="accordion" className="accordion question-according">
                                <Card className="mb-0">
                                    <CardHeader>
                                        <h5 className="m-0">
                                            <NavLink
                                                className="custom-accordion-title d-block pt-2 pb-2"
                                                id="groups1"
                                                href="#">
                                                Collapsible Group Item #1{' '}
                                                <span className="float-right">
                                                    <i className="mdi mdi-chevron-right font-18 accordion-arrow"></i>
                                                </span>
                                            </NavLink>
                                        </h5>
                                    </CardHeader>
                                    <UncontrolledCollapse toggler="#groups1">
                                        <CardBody>
                                            <div id="accordion" className="accordion custom-accordion">
                                                <Card className="mb-0">
                                                    <CardHeader>
                                                        <h5 className="m-0">
                                                            <NavLink
                                                                className="custom-accordion-title d-block pt-2 pb-2"
                                                                id="groups1q1"
                                                                href="#">
                                                                Q. Collapsible Group Item #1{' '}
                                                                <span className="float-right">
                                                                    <i className="mdi mdi-chevron-right font-18 accordion-arrow"></i>
                                                                </span>
                                                            </NavLink>
                                                        </h5>
                                                    </CardHeader>
                                                    <UncontrolledCollapse toggler="#groups1q1">
                                                        <CardBody>This is first collapse content</CardBody>
                                                    </UncontrolledCollapse>
                                                </Card>

                                                <Card className="mb-0">
                                                    <CardHeader>
                                                        <h5 className="m-0">
                                                            <NavLink
                                                                className="custom-accordion-title d-block pt-2 pb-2"
                                                                id="groups1q2"
                                                                href="#">
                                                                Q. Collapsible Group Item #1{' '}
                                                                <span className="float-right">
                                                                    <i className="mdi mdi-chevron-right font-18 accordion-arrow"></i>
                                                                </span>
                                                            </NavLink>
                                                        </h5>
                                                    </CardHeader>
                                                    <UncontrolledCollapse toggler="#groups1q2">
                                                        <CardBody>This is first collapse content</CardBody>
                                                    </UncontrolledCollapse>
                                                </Card>
                                            </div>
                                        </CardBody>
                                    </UncontrolledCollapse>
                                </Card>

                                <Card className="mb-0">
                                    <CardHeader>
                                        <h5 className="m-0">
                                            <NavLink
                                                className="custom-accordion-title d-block pt-2 pb-2"
                                                id="groups2"
                                                href="#">
                                                Collapsible Group Item #2{' '}
                                                <span className="float-right">
                                                    <i className="mdi mdi-chevron-right font-18 accordion-arrow"></i>
                                                </span>
                                            </NavLink>
                                        </h5>
                                    </CardHeader>
                                    <UncontrolledCollapse toggler="#groups2">
                                        <CardBody>This is second collapse content</CardBody>
                                    </UncontrolledCollapse>
                                </Card>

                                <Card className="mb-0">
                                    <CardHeader>
                                        <h5 className="m-0">
                                            <NavLink
                                                className="custom-accordion-title d-block pt-2 pb-2"
                                                id="groups3"
                                                href="#">
                                                Collapsible Group Item #3{' '}
                                                <span className="float-right">
                                                    <i className="mdi mdi-chevron-right font-18 accordion-arrow"></i>
                                                </span>
                                            </NavLink>
                                        </h5>
                                    </CardHeader>
                                    <UncontrolledCollapse toggler="#groups3">
                                        <CardBody>This is third collapse content</CardBody>
                                    </UncontrolledCollapse>
                                </Card>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default FAQSlide;
