import React from 'react';
import AboutImg from '../../../../assets/images/register/about.svg';
import SlideHeader from '../components/SlideHeader';

function AboutSlide({ active }) {
    return (
        <div>
            <SlideHeader />
            <div className="content pt-3 mb-5 normalScroll">
                <div className="row mt-lg-5">
                    <div className="col-lg-5 offset-lg-1">
                        <h1 className="mb-4"> About Us</h1>
                        <p className="w-75">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                        </p>
                    </div>
                    <div className="col-lg-5">
                        <img src={AboutImg} alt="about-img" className="w-100" />
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-10 offset-lg-1">
                        <div className="row mt-lg-5">
                            <div className="col-lg-4">
                                <div className="text-center p-3">
                                    <div className="avatar-sm m-auto">
                                        <span className="avatar-title bg-primary-lighten rounded-circle">
                                            <i className="uil uil-desktop text-primary font-24"></i>
                                        </span>
                                    </div>
                                    <h4 className="mt-3">Responsive Layouts</h4>
                                    <p className="text-muted mt-2 mb-0">
                                        Et harum quidem rerum as expedita distinctio nam libero tempore cum soluta nobis
                                        est cumque quo.
                                    </p>
                                </div>
                            </div>

                            <div className="col-lg-4">
                                <div className="text-center p-3">
                                    <div className="avatar-sm m-auto">
                                        <span className="avatar-title bg-primary-lighten rounded-circle">
                                            <i className="uil uil-vector-square text-primary font-24"></i>
                                        </span>
                                    </div>
                                    <h4 className="mt-3">Based on Bootstrap UI</h4>
                                    <p className="text-muted mt-2 mb-0">
                                        Temporibus autem quibusdam et aut officiis necessitatibus saepe eveniet ut sit
                                        et recusandae.
                                    </p>
                                </div>
                            </div>

                            <div className="col-lg-4">
                                <div className="text-center p-3">
                                    <div className="avatar-sm m-auto">
                                        <span className="avatar-title bg-primary-lighten rounded-circle">
                                            <i className="uil uil-presentation text-primary font-24"></i>
                                        </span>
                                    </div>
                                    <h4 className="mt-3">Creative Design</h4>
                                    <p className="text-muted mt-2 mb-0">
                                        Nam libero tempore, cum soluta a est eligendi minus id quod maxime placeate
                                        facere assumenda est.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div className="row mt-xl-5 mb-3">
                            <div className="col-lg-4">
                                <div className="text-center p-3">
                                    <div className="avatar-sm m-auto">
                                        <span className="avatar-title bg-primary-lighten rounded-circle">
                                            <i className="uil uil-desktop text-primary font-24"></i>
                                        </span>
                                    </div>
                                    <h4 className="mt-3">Responsive Layouts</h4>
                                    <p className="text-muted mt-2 mb-0">
                                        Et harum quidem rerum as expedita distinctio nam libero tempore cum soluta nobis
                                        est cumque quo.
                                    </p>
                                </div>
                            </div>

                            <div className="col-lg-4">
                                <div className="text-center p-3">
                                    <div className="avatar-sm m-auto">
                                        <span className="avatar-title bg-primary-lighten rounded-circle">
                                            <i className="uil uil-vector-square text-primary font-24"></i>
                                        </span>
                                    </div>
                                    <h4 className="mt-3">Based on Bootstrap UI</h4>
                                    <p className="text-muted mt-2 mb-0">
                                        Temporibus autem quibusdam et aut officiis necessitatibus saepe eveniet ut sit
                                        et recusandae.
                                    </p>
                                </div>
                            </div>

                            <div className="col-lg-4">
                                <div className="text-center p-3">
                                    <div className="avatar-sm m-auto">
                                        <span className="avatar-title bg-primary-lighten rounded-circle">
                                            <i className="uil uil-presentation text-primary font-24"></i>
                                        </span>
                                    </div>
                                    <h4 className="mt-3">Creative Design</h4>
                                    <p className="text-muted mt-2 mb-0">
                                        Nam libero tempore, cum soluta a est eligendi minus id quod maxime placeate
                                        facere assumenda est.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    );
}

export default AboutSlide;
