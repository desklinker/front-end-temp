import React from 'react';
import IntroSlide from './IntroSlide';
import AboutSlide from './AboutSlide';
import ContactUsSlide from './ContactUsSlide';
import FAQSlide from './FAQSlide';

function IntroSection({ active }) {
  return (
    <>
      <div className="slide">
        <IntroSlide active={active}/>
      </div>
      <div className="slide" >
        <AboutSlide active={active}/>
      </div>
      <div className="slide" >
        <ContactUsSlide active={active}/>
      </div>
      <div className="slide">
        <FAQSlide active={active}/>
      </div>
    </>
  );
}

export default IntroSection;
