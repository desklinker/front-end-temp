import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import SlideHeader from '../components/SlideHeader';
import SlideFooter from '../components/SlideFooter';

function IntroSlide({ active }) {
    const [isMounted, setMounted] = useState(false);

    const history = useHistory();

    useEffect(() => {
        if ((active && active.anchor === 'introSection') || history.location.hash === '#introSection' ) {
            setMounted(true);
        }

        return () => {
            setMounted(false);
        };
    }, [active, history.location.hash, history.location.pathname]);
 
    return (
        <>
            <SlideHeader />
            <div className="content img-slide">
                <div id="color-overlay"></div>
            </div>
            <SlideFooter isMounted={isMounted} />
        </>
    );
}

export default IntroSlide;
