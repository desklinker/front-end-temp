import React, { useRef, useCallback } from 'react';

import 'fullpage.js/vendors/scrolloverflow';
import ReactFullpage from '@fullpage/react-fullpage';
import { useHistory } from 'react-router-dom';

import FirstFeature from './features/FirstFeature';
import SecondFeature from './features/SecondFeature';
import ThirdFeature from './features/ThirdFeature';
import TitleSection from './TitleSection';
import OurWorkSection from './OurWorkSection';
import IntroSection from './IntroSection';

const anchors = ['introSection', 'titleSection', 'ourWorkSection', 'firstFeature', 'secondFeature', 'thirdFeature'];

function SliderComponent() {
  const history = useHistory();

  const { hash } = history && history.location;

  const intervalId = useRef(null);

  const handleAutoScroll = useCallback(
    (fullpageApi, index) => {
      if (hash.includes('/')) {
        clearInterval(intervalId.current);
      } else {
        if (intervalId.current) clearInterval(intervalId.current);
        // console.log(fullpageApi, 'fullpageApi');
        // if (index === 0) {
        //     intervalId.current = setInterval(() => {
        //         fullpageApi.moveSectionDown();
        //     }, 5000);
        // }
        // if (index === 1) {
        //     intervalId.current = setInterval(() => {
        //         fullpageApi.moveSectionDown();
        //     }, 10000);
        // }
        // else {
        //     intervalId.current = setInterval(() => {
        //         fullpageApi.moveSectionDown();
        //     }, 10000);
        // }
      }
    },
    [hash]
  );

  return (
    <ReactFullpage
      anchors={anchors}
      navigation
      scrollingSpeed={1000}
      scrollOverflow={true}
      navigationTooltips={anchors}
      controlArrows={false}
      normalScrollElements="#normalScroll"
      render={({ state, fullpageApi }) => {
        const active = fullpageApi && fullpageApi.getActiveSection();
        if (fullpageApi) {
          if (hash.includes('/')) {
            fullpageApi.setAllowScrolling(false);
          } else {
            fullpageApi.setAllowScrolling(true);
          }
          handleAutoScroll(fullpageApi, active.index);
        }
        return (
          <ReactFullpage.Wrapper>
            <div className="section" data-percentage="80" data-centered="true">
              <IntroSection active={active} />
            </div>
            <div className="section">
              <TitleSection active={active} />
            </div>
            <div className="section">
              <OurWorkSection active={active} />
            </div>
            <div className="section">
              <FirstFeature active={active} />
            </div>
            <div className="section">
              <SecondFeature active={active} />
            </div>
            <div className="section">
              <ThirdFeature active={active} />
            </div>
          </ReactFullpage.Wrapper>
        );
      }}
    />
  );
}

export default SliderComponent;
