import { useCallback, useState } from 'react';
import { useDispatch } from 'react-redux';
import { checkAvailability } from '../../../actions/users';

const queryString = require('query-string');

export function useCheckAvailability() {
  const dispatch = useDispatch();
  const [usernameExist, setUsernameExist] = useState(false);
  const [emailExist, setEmailExist] = useState(false);
  const [phoneExist, setPhoneExist] = useState(false);

  const onCheckAvailability = useCallback(
    async (type, value) => {
      const query = queryString.stringify({ [type]: [value] });
      try {
        const res = await dispatch(checkAvailability(type, query));

        if (type === 'username') {
          setUsernameExist(res);
        } else if (type === 'email') {
          setEmailExist(res);
        } else if (type === 'phoneNumber') {
          setPhoneExist(res);
        }
      } catch (error) {
        //
      }
    },
    [dispatch]
  );

  return { onCheckAvailability, usernameExist, emailExist, phoneExist };
}
