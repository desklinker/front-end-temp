import React from 'react';
import { Formik, Form } from 'formik';
import { object, string, ref } from 'yup';
import { Row, Col, FormGroup, Button, Input, FormFeedback, CustomInput, Alert } from 'reactstrap';
import PhoneInput from 'react-phone-input-2';
import { useCheckAvailability } from './hooks';
import { roles } from '../../../constants/roles';

const validationSchema = object().shape({
  firstName: string().required('First name field is required'),
  lastName: string().required('Last name field is required'),
  username: string().required('Username field is required'),
  email: string().email().required('Email field is required'),
  phoneNumber: string().required('Phone number field is required'),
  password: string().required('Password is required'),
  confirmPassword: string().oneOf([ref('password'), null], 'Passwords must match'),
});

function RegisterForm(props) {
  const { onCheckAvailability, emailExist, usernameExist, phoneExist } = useCheckAvailability();

  const handleCheckAvailability = (type, value) => {
    onCheckAvailability(type, value);
  };

  const handleSubmit = (values, actions) => {
    props.onSubmit(
      {
        ...values,
        phoneNumber: `+${values.phoneNumber}`,
        userRoleId: roles[values.role] && roles[values.role].id,
        groupName: roles[values.role] && roles[values.role].value,
      },
      actions
    );
  };

  const validate = (values, props) => {
    const errors = {};

    if (usernameExist) {
      errors.username = 'Already the username exist';
    }
    if (emailExist) {
      errors.email = 'Already the email exist';
    }
    if (phoneExist) {
      errors.phoneNumber = 'Already the phone number exist';
    }

    return errors;
  };

  return (
    <Formik
      validateOnBlur={false}
      enableReinitialize
      initialValues={{
        firstName: '',
        lastName: '',
        email: '',
        username: '',
        phoneNumber: '',
        password: '',
        confirmPassword: '',
      }}
      validate={validate}
      validationSchema={validationSchema}
      onSubmit={handleSubmit}>
      {({ errors, values, setFieldValue, handleChange, handleFocus, touched, handleBlur, isSubmitting }) => {
        return (
          <Form className="form-custom">
            <Alert color="danger" isOpen={!!errors.general}>
              <div>{errors.general}</div>
            </Alert>
            <FormGroup>
              <CustomInput
                className="text-muted font-weight-normal"
                type="radio"
                id="client"
                label="Do you give a job"
                checked={values.role === 'client'}
                onChange={() => setFieldValue('role', 'client')}
                inline
              />
              <CustomInput
                className="text-muted font-weight-normal"
                type="radio"
                id="professional"
                label="Do you want a job"
                checked={values.role === 'professional'}
                onChange={() => setFieldValue('role', 'professional')}
                inline
              />
            </FormGroup>

            <FormGroup>
              <Row className="px-1">
                <Col>
                  <Input
                    name="firstName"
                    type="text"
                    placeholder="First Name"
                    onChange={handleChange}
                    onFocus={handleFocus}
                    value={values.firstName}
                  />
                  {errors.firstName && touched.firstName && (
                    <FormFeedback className="d-block">{errors.firstName}</FormFeedback>
                  )}
                </Col>
                <Col>
                  <Input
                    name="lastName"
                    type="text"
                    placeholder="Last Name"
                    onChange={handleChange}
                    onFocus={handleFocus}
                    value={values.lastName}
                  />
                  {errors.lastName && touched.lastName && (
                    <FormFeedback className="d-block">{errors.lastName}</FormFeedback>
                  )}
                </Col>
              </Row>
            </FormGroup>
            <FormGroup>
              <Input
                name="username"
                type="text"
                placeholder="Username"
                onChange={(e) => {
                  handleChange(e);
                  handleCheckAvailability('username', e.target.value);
                }}
                onBlur={handleBlur}
                onFocus={handleFocus}
                value={values.username}
              />
              {errors.username && touched.username && (
                <FormFeedback className="d-block">{errors.username}</FormFeedback>
              )}
            </FormGroup>

            <FormGroup>
              <Input
                name="email"
                type="email"
                placeholder="Email address"
                onChange={(e) => {
                  handleChange(e);
                  handleCheckAvailability('email', e.target.value);
                }}
                onBlur={handleBlur}
                value={values.email || ''}
              />
              {errors.email && touched.email && <FormFeedback className="d-block">{errors.email}</FormFeedback>}
            </FormGroup>

            <FormGroup>
              <PhoneInput
                label={'Phone Number'}
                country={'us'}
                onlyCountries={['lk', 'sg', 'us', 'uk', 'my', 'in', 'cn', 'au']}
                onChange={(value) => {
                  setFieldValue('phoneNumber', value);
                  handleCheckAvailability('phoneNumber', `+${value}`);
                }}
                onBlur={handleBlur}
                inputProps={{
                  name: 'phoneNumber',
                }}
                value={values.phoneNumber || ''}
              />
              {errors.phoneNumber && touched.phoneNumber && (
                <FormFeedback className="d-block">{errors.phoneNumber}</FormFeedback>
              )}
            </FormGroup>
            <FormGroup>
              <Input
                name="password"
                type="password"
                placeholder="Password"
                onChange={handleChange}
                value={values.password || ''}
              />
              {errors.password && touched.password && (
                <FormFeedback className="d-block">{errors.password}</FormFeedback>
              )}
            </FormGroup>

            <FormGroup>
              <Input
                name="confirmPassword"
                type="password"
                placeholder="Confirm password"
                onChange={handleChange}
                value={values.confirmPassword || ''}
              />
              {errors.confirmPassword && touched.confirmPassword && (
                <FormFeedback className="d-block">{errors.confirmPassword}</FormFeedback>
              )}
            </FormGroup>

            <FormGroup>
              <div className="custom-control custom-checkbox">
                <Input type="checkbox" className="custom-control-input" id="checkbox-signup" />
                <label className="custom-control-label">
                  I accept{' '}
                  <a href="##" className="text-muted">
                    Terms and Conditions
                  </a>
                </label>
              </div>
            </FormGroup>
            <FormGroup className="mb-0 text-center">
              <Button className="btn btn-primary btn-block" type="submit">
                <i className="mdi mdi-account-circle"></i> {isSubmitting ? 'Loading' : 'Sign Up'}
              </Button>
            </FormGroup>
          </Form>
        );
      }}
    </Formik>
  );
}

export default RegisterForm;
