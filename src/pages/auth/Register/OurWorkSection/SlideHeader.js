import React from 'react';
import { useHistory } from 'react-router-dom';

function SlideHeader() {
    const history = useHistory();

    const { hash } = history && history.location;

    return (
        <>
            <nav className={`navbar navbar-expand-sm navbar-slide ${hash !== '#ourWorkSection' ? 'bg-dark' : ''}`}>
                <div className="container-fluid mx-5">
                    <a className="navbar-brand" href="#ourWorkSection">
                        Brand
                    </a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar1">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbar1">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <a
                                    className={`nav-link ${hash === '#ourWorkSection/1' ? 'active' : ''}`}
                                    href="#ourWorkSection/1">
                                    Civil
                                </a>
                            </li>
                            <li className="nav-item">
                                <a
                                    className={`nav-link ${hash === '#ourWorkSection/2' ? 'active' : ''}`}
                                    href="#ourWorkSection/2">
                                    Computer
                                </a>
                            </li>
                            <li className="nav-item">
                                <a
                                    className={`nav-link ${hash === '#ourWorkSection/3' ? 'active' : ''}`}
                                    href="#ourWorkSection/3">
                                    Mechanical
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </>
    );
}

export default SlideHeader;
