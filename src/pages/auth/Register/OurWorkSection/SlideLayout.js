import React from 'react';
import SlideHeader from './SlideHeader';

function SlideLayout({ isMounted, children }) {
    return (
        <div>
            <SlideHeader />
            <div className="content pt-3 mb-5">
               {children}
            </div>
        </div>
    );
}

export default SlideLayout;
