import React from 'react';
import OurWorkSlide from './OurWorkSlide';
import FieldInfoSlide from './FieldInfoSlide';

import Civil from '../../../../assets/images/landing/civil.svg';
import ItSupport from '../../../../assets/images/landing/computer.svg';
import Setting from '../../../../assets/images/landing/mechanic.svg';

function OurWorkSection({ active }) {
    return (
        <>
            <div className="slide">
                <OurWorkSlide active={active} />
            </div>
            <div className="slide">
                <FieldInfoSlide active={active} title="Civil" imgUrl={Civil} />
            </div>
            <div className="slide">
                <FieldInfoSlide active={active} title="Computer" imgUrl={ItSupport} />
            </div>
            <div className="slide">
                <FieldInfoSlide active={active} title="Mechanic" imgUrl={Setting} />
            </div>
        </>
    );
}

export default OurWorkSection;
