import React, { useState, useEffect } from 'react';
import { motion } from 'framer-motion';
import { Card, CardBody } from 'reactstrap';
import FadeInUp from '../components/FadeInUp';

import { ReactComponent as Civil } from '../../../../assets/images/landing/civil.svg';
import { ReactComponent as ItSupport } from '../../../../assets/images/landing/computer.svg';
import { ReactComponent as Setting } from '../../../../assets/images/landing/mechanic.svg';

function FieldComponent({ active, section, field, title, description, delay, icon, url }) {
    const [isMounted, setMounted] = useState(false);

    useEffect(() => {
        if (active && active.anchor === section) {
            setMounted(true);
        }
    }, [active, section]);

    return (
        <div className="row align-items-center">
            {(field === 'civil' || field === 'mechanical') && (
                <div className="col-lg-4 offset-lg-1">
                    <FadeInUp delay={delay} isMounted={isMounted}>
                        <motion.div whileHover={{ scale: 1.1 }} transition={{ duration: 0.5 }}>
                            {field === 'civil' ? <Civil className="w-100" /> : <Setting className="w-100" />}
                        </motion.div>
                    </FadeInUp>
                </div>
            )}

            <div className="col-lg-6 offset-lg-1 field-component">
                <FadeInUp delay={delay + 0.2} isMounted={isMounted}>
                    <Card className="hover-card">
                        <CardBody>
                            <a href={url} className="text-dark">
                                <FadeInUp delay={delay + 0.4} isMounted={isMounted}>
                                    <div className="avatar-sm d-inline mr-2">
                                        <span className="avatar-sm bg-info-lighten rounded-circle our-work-icon">
                                            <i className={`uil ${icon} text-info font-24`}></i>
                                        </span>
                                    </div>
                                    <h2 className="font-weight-normal d-inline">
                                        <span className="hover-link">{title}</span>
                                    </h2>
                                </FadeInUp>
                                <FadeInUp delay={delay + 0.6} isMounted={isMounted}>
                                    <p>{description}</p>
                                </FadeInUp>
                            </a>
                        </CardBody>
                    </Card>
                </FadeInUp>
            </div>
            <div className="col-lg-4 offset-lg-1">
                {field === 'computer' && (
                    <FadeInUp delay={delay} isMounted={isMounted}>
                        <motion.div whileHover={{ scale: 1.1 }} transition={{ duration: 0.5 }}>
                            <ItSupport className="w-100" />
                        </motion.div>
                    </FadeInUp>
                )}
            </div>
        </div>
    );
}

export default FieldComponent;
