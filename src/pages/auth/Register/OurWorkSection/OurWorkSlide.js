import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import FieldComponent from './FieldComponent';
import SlideFooter from '../components/SlideFooter';

function OurWorkSlide({ active }) {
    const [isMounted, setMounted] = useState(false);

    const history = useHistory();

    useEffect(() => {
        if (history.location.hash === '#ourWorkSection') {
            setMounted(true);
        }

        return () => {
            setMounted(false);
        };
    }, [active, history.location.hash]);

    return (
        <>
            <div className="intro-slide pr-5">
                <div className="container-fluid">
                    <FieldComponent
                        delay={0.4}
                        active={active}
                        section={'ourWorkSection'}
                        field={'civil'}
                        icon={'uil-constructor'}
                        title={'Civil'}
                        url={'#ourWorkSection/1'}
                        description="It is a long established fact that a reader will be distracted by the
                    readable content of a page when looking at its layout. It is a long
                    established fact that a reader will be distracted by the readable content of
                    a page when looking at its layout."
                    />

                    <FieldComponent
                        delay={1.6}
                        active={active}
                        section={'ourWorkSection'}
                        field={'computer'}
                        icon={'uil-desktop'}
                        title={'Computer'}
                        url={'#ourWorkSection/2'}
                        description="It is a long established fact that a reader will be distracted by the
                    readable content of a page when looking at its layout. It is a long
                    established fact that a reader will be distracted by the readable content of
                    a page when looking at its layout."
                    />

                    <FieldComponent
                        delay={2.2}
                        active={active}
                        section={'ourWorkSection'}
                        field={'mechanical'}
                        icon={'uil-wrench'}
                        title={'Mechanical'}
                        url={'#ourWorkSection/3'}
                        description="It is a long established fact that a reader will be distracted by the
                    readable content of a page when looking at its layout. It is a long
                    established fact that a reader will be distracted by the readable content of
                    a page when looking at its layout."
                    />
                </div>
            </div>
            <SlideFooter isMounted={isMounted} isDark />
        </>
    );
}

export default OurWorkSlide;
