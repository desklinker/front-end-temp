import React from 'react';
import SlideLayout from './SlideLayout';

function FieldInfoSlide({ isMounted, title, imgUrl }) {
    return (
        <SlideLayout>
            <div className="row mt-lg-5">
                <div className="col-lg-5 offset-lg-1">
                    <h1 className="mb-4"> {title}</h1>
                    <p className="w-75">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                        of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                        but also the leap into electronic typesetting, remaining essentially unchanged.
                    </p>
                </div>
                <div className="col-lg-5">
                    <img src={imgUrl} alt="about-img" className="w-100" />
                </div>
            </div>
        </SlideLayout>
    );
}

export default FieldInfoSlide;
