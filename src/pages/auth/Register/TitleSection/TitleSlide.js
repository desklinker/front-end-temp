import React from 'react';
import FadeInUp from '../components/FadeInUp';
import SlideLayout from './SlideLayout';
import SlideFooter from '../components/SlideFooter';

function TitleSlide({ isMounted }) {
    return (
        <>
            <SlideLayout isMounted={isMounted}>
                <FadeInUp delay={0.6} isMounted={isMounted} yOffset={124} duration={1}>
                    <div className="text-primary brand">DeskLinker</div>
                </FadeInUp>
                <FadeInUp delay={1} isMounted={isMounted} yOffset={36}>
                    <p className="brand-describe ml-2">
                        It is a long established fact that a reader will be distracted by the readable content of a page
                        when looking at its layout.
                    </p>
                </FadeInUp>
            </SlideLayout>
            <SlideFooter isMounted={isMounted} isDark />
        </>
    );
}

export default TitleSlide;
