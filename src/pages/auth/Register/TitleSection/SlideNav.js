import React from 'react';
import { useHistory } from 'react-router-dom';
import { motion } from 'framer-motion';

const container = {
    hidden: { opacity: 1, scale: 0 },
    visible: {
        opacity: 1,
        scale: 1,
        transition: {
            delay: 2,
            when: 'beforeChildren',
            staggerChildren: 0.2,
        },
    },
};

const itemVarient = {
    hidden: { y: 20, opacity: 0 },
    visible: {
        y: 0,
        opacity: 1,
        transition: {
            type: 'spring',
            stiffness: 300,
        },
    },
};

const menuItems = [
    {
        title: 'Easy',
        link: '#titleSection/1',
    },
    {
        title: 'Fast',
        link: '#titleSection/2',
    },
    {
        title: 'Free virtual company',
        link: '#titleSection/3',
    },
    {
        title: 'Task-oriented payments',
        link: '#titleSection/4',
    },
    {
        title: 'Secured privacy details',
        link: '#titleSection/5',
    },
    {
        title: 'Overhead payments exemption',
        link: '#titleSection/6',
    },
    {
        title: 'Broad range of professionals',
        link: '#titleSection/7',
    },
    {
        title: 'Project tracking',
        link: '#titleSection/8',
    },
    {
        title: 'Professionals Grading',
        link: '#titleSection/9',
    },
    {
        title: 'Branding',
        link: '#titleSection/10',
    },
];

function SlideNav({ isMounted }) {
    const history = useHistory();

    const { hash } = history && history.location;

    return (
        <motion.ul
            className="list-unstyled text-right"
            variants={container}
            initial="hidden"
            animate={isMounted ? 'visible' : 'hidden'}>
            {menuItems.map((item, index) => (
                <motion.li className="menu-item" key={index} variants={itemVarient}>
                    <a href={item.link} className={`menu-link ${hash === item.link ? 'active' : ''}`}>
                        <span>{item.title} </span>
                    </a>
                </motion.li>
            ))}
        </motion.ul>
    );
}

export default SlideNav;
