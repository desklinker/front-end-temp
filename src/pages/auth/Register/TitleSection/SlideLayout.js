import React from 'react';
import { Row, Col } from 'reactstrap';
import SlideNav from './SlideNav';

function SlideLayout({ isMounted, children }) {
    return (
        <>
            <div className="title-section  px-5">
                <a href="#titleSection" className="brand-logo">
                    Brand
                </a>
                <Row className="align-items-center">
                    <Col md={9}>{children}</Col>

                    <Col md={3} className="menu-list">
                        <SlideNav isMounted={isMounted} />
                    </Col>
                </Row>
            </div>
        </>
    );
}

export default SlideLayout;
