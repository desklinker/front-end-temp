import React from 'react';
import SlideLayout from './SlideLayout';

function EasySlide({ isMounted, title, imgUrl }) {
    return (
        <SlideLayout isMounted={isMounted}>
            <div className="row mt-lg-5">
                <div className="col-lg-10 offset-lg-1">
                    <h1 className="mb-4"> {title} </h1>
                    <img src={imgUrl} alt="about-img" className="w-75" />
                    <p className="mt-3 font-18">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                        of type and scrambled it to make a type specimen book.
                    </p>
                </div>
            </div>
        </SlideLayout>
    );
}

export default EasySlide;
