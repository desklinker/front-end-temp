import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import TitleSlide from './TitleSlide';
import EasySlide from './EasySlide';

import EasyImg from '../../../../assets/images/register/easy.svg';
import FastImg from '../../../../assets/images/register/fast.svg';
import VirtualImg from '../../../../assets/images/register/virtual.svg';
import PaymentImg from '../../../../assets/images/register/payment.svg';
import ObservationImg from '../../../../assets/images/register/observations.svg';
import SecurityImg from '../../../../assets/images/register/security.svg';
import TrackingImg from '../../../../assets/images/register/project-tracking.svg';
import FactorImg from '../../../../assets/images/register/success-factor.svg';

function TitleSection({ active, children }) {
    const [isMounted, setMounted] = useState(false);

    const history = useHistory();

    useEffect(() => {
        if ((active && active.anchor === 'titleSection') || history.location.hash === '#titleSection') {
            setMounted(true);
        }

        return () => {
            setMounted(false);
        };
    }, [active, history.location.hash]);

    return (
        <>
            <div className="slide">
                <TitleSlide isMounted={isMounted} />
            </div>
            <div className="slide">
                <EasySlide isMounted={isMounted} title="Easy" imgUrl={EasyImg} />
            </div>
            <div className="slide">
                <EasySlide isMounted={isMounted} title="Fast" imgUrl={FastImg} />
            </div>
            <div className="slide">
                <EasySlide isMounted={isMounted} title="Free virtual company" imgUrl={VirtualImg} />
            </div>
            <div className="slide">
                <EasySlide isMounted={isMounted} title="Task-oriented payments" imgUrl={PaymentImg} />
            </div>
            <div className="slide">
                <EasySlide isMounted={isMounted} title="Secured privacy details" imgUrl={SecurityImg} />
            </div>
            <div className="slide">
                <EasySlide isMounted={isMounted} title="Overhead payments exemption" imgUrl={PaymentImg} />
            </div>
            <div className="slide">
                <EasySlide isMounted={isMounted} title="Broad range of professionals" imgUrl={FactorImg} />
            </div>
            <div className="slide">
                <EasySlide isMounted={isMounted} title="Project tracking" imgUrl={TrackingImg} />
            </div>
            <div className="slide">
                <EasySlide isMounted={isMounted} title="Professionals Grading" imgUrl={FactorImg} />
            </div>
            <div className="slide">
                <EasySlide isMounted={isMounted} title="Branding" imgUrl={ObservationImg} />
            </div>
        </>
    );
}

export default TitleSection;
