import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import LoaderWidget from '../../../components/Loader';
import 'react-phone-input-2/lib/style.css';
import { useRegisterUser } from '../hooks';
import { LOGIN, ROOT } from '../../../constants/routes';
import RegisterForm from './RegisterForm';
import SliderComponent from './SliderComponent';

function Register(props) {
  const { onRegisterUser } = useRegisterUser(props.history);
  const { loading, error } = useSelector((state) => state.Auth);

  return (
    <div className="auth-fluid flex-row">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-3 bg-form" id="normalScroll">
            <div className="auth-fluid-form-box">
              <div className="align-items-center">
                <div className="card-body">
                  <div className="text-center text-lg-left mb-lg-4 mb-xl-5">
                    <Link to={ROOT} className="logo-dark">
                      <span>
                        <h3>DeskLinker</h3>
                      </span>
                    </Link>
                  </div>

                  {loading && <LoaderWidget />}

                  <h4 className="mt-0">Free Sign Up</h4>
                  <p className="text-muted mb-3">
                    Don't have an account? Create your account, it takes less than a minute
                  </p>

                  <RegisterForm onSubmit={onRegisterUser} error={error} />

                  <footer className="footer footer-alt">
                    <p className="text-muted">
                      Already have account?
                      <Link to={LOGIN} className="text-muted ml-1">
                        <b>Sign In</b>
                      </Link>
                    </p>
                  </footer>
                </div>
              </div>
            </div>
          </div>

          <div className="col-md-9 hidden-sm-down p-0">
            <SliderComponent />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Register;
