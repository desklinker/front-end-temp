import React, { useState, useEffect } from 'react';
import { motion } from 'framer-motion';
import featureThird from '../../../../assets/images/landing/widgets2.png';
import FadeInUp from '../components/FadeInUp';
import FeatureList from './FeatureList';

const lists = [
    { text: 'Built with latest Bootstrap', icon: 'uil-check-square' },
    { text: 'Extensive use of SCSS variables', icon: 'uil-edit' },
    { text: 'Fully responsive', icon: 'uil-mobile-android-alt' },
    { text: 'Well documented and structured code', icon: 'uil-code' },
    { text: 'Detailed Documentation', icon: 'uil-file-alt' },
];

function ThirdFeature({ active, loaded }) {
    const [isMounted, setMounted] = useState(false);

    useEffect(() => {
        if (active && active.anchor === 'thirdFeature') {
            setMounted(true);
        }

        return () => {
            setMounted(false);
        };
    }, [active, loaded]);

    return (
        <div className="feature-right">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-lg-6">
                        <div className="mt-4">
                            <motion.div whileHover={{ scale: 1.1 }} transition={{ duration: 0.5 }}>
                                <img src={featureThird} className="img-fluid shadow-lg" alt="" />
                            </motion.div>
                        </div>
                    </div>

                    <div className="col-lg-6">
                        <div className="row justify-content-center">
                            <div className="col-lg-8">
                                <div className="mt-4">
                                    <div className="mt-4">
                                        <FadeInUp delay={0.4} isMounted={isMounted}>
                                            <h4 className="mt-4">Other Features</h4>
                                        </FadeInUp>

                                        <FadeInUp delay={0.4} isMounted={isMounted}>
                                            <p className="text-muted mt-2 mb-4">
                                                Hyper is built using the latest tech and tools and provide an easy way
                                                to customize anything, including an overall color schemes, layout, etc
                                            </p>
                                        </FadeInUp>

                                        <FeatureList lists={lists} isMounted={isMounted} color={'primary'} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ThirdFeature;
