import React, { useState, useEffect } from 'react';
import { motion } from 'framer-motion';
import FadeInUp from '../components/FadeInUp';
import FeatureList from './FeatureList';
import featureSecond from '../../../../assets/images/landing/widgets2.png';

const lists = [
    { text: 'Many ui components covered', icon: 'uil-package' },
    { text: 'Multiple reusable widgets', icon: 'uil-archive-alt' },
    { text: 'Multiple charts included', icon: 'uil-chart-pie' },
];

function SecondFeature({ active }) {
    const [isMounted, setMounted] = useState(false);

    useEffect(() => {
        if (active && active.anchor === 'secondFeature') {
            setMounted(true);
        }

        return () => {
            setMounted(false);
        };
    }, [active]);

    return (
        <div className="feature-left">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-lg-6">
                        <div className="mt-4">
                            <motion.div whileHover={{ scale: 1.1 }} transition={{ duration: 0.5 }}>
                                <img src={featureSecond} className="img-fluid shadow-lg" alt="" />
                            </motion.div>
                        </div>
                    </div>

                    <div className="col-lg-6">
                        <div className="row justify-content-center">
                            <div className="col-lg-8">
                                <div className="mt-4">
                                    <FadeInUp delay={0.4} isMounted={isMounted}>
                                        <h4 className="mt-4">Simply beautiful design</h4>
                                    </FadeInUp>
                                    <FadeInUp delay={0.4} isMounted={isMounted}>
                                        <p className="text-muted mt-2 mb-5">
                                            The simplest and fastest way to build dashboard or admin panel
                                        </p>
                                    </FadeInUp>
                                    <FeatureList lists={lists} isMounted={isMounted} color={'success'} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default SecondFeature;
