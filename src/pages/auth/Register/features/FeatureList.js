import React from 'react';
import { motion } from 'framer-motion';

const variants = {
    hidden: { opacity: 0, y: '10%' },
    animate: {
        opacity: 1,
        transition: {
            delay: 1,
            duration: 0.8,
        },
    },
};

const itemVariants = {
    hidden: { y: 50, opacity: 0 },
    animate: {
        y: 0,
        delay: 1,
        opacity: 1,
    },
};

function FeatureList({ isMounted, lists, color }) {
    return (
        <motion.ul
            className="list-unstyled pl-2 f-17"
            variants={variants}
            initial="hidden"
            animate={isMounted ? 'animate' : 'hidden'}
            exit="hidden">
            {lists.map((item, key) => (
                <li
                    className="mb-3"
                    variants={itemVariants}
                    key={key}
                    initial="hidden"
                    animate={isMounted ? 'animate' : 'hidden'}
                    exit="hidden">
                    <i className={`icon-with-bg text-${color} bg-${color} f-20 mr-2 align-middle ${item.icon}`}></i>
                    {item.text}
                </li>
            ))}
        </motion.ul>
    );
}

export default FeatureList;
