import React, { useState, useEffect } from 'react';
import { motion } from 'framer-motion';
import featureFirst from '../../../../assets/images/landing/apps.png';
import FadeInUp from '../components/FadeInUp';
import FeatureList from './FeatureList';

const lists = [
    { text: 'Projects &amp; Tasks', icon: 'uil-briefcase' },
    { text: 'Ecommerce Application Pages', icon: 'uil-list-ul' },
    { text: 'Profile, pricing, invoice', icon: 'uil-clipboard-notes' },
    { text: 'Login, signup, forget password', icon: 'uil-dollar-sign' },
];

function FirstFeature({ active }) {
    const [isMounted, setMounted] = useState(false);

    useEffect(() => {
        if (active && active.anchor === 'firstFeature') {
            setMounted(true);
        }

        return () => {
            setMounted(false);
        };
    }, [active]);

    return (
        <div className="feature-right">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-lg-6">
                        <div className="mt-4">
                            <motion.div whileHover={{ scale: 1.1 }} transition={{ duration: 0.5 }}>
                                <img src={featureFirst} className="img-fluid shadow-lg" alt="" />
                            </motion.div>
                        </div>
                    </div>

                    <div className="col-lg-6">
                        <div className="row justify-content-center">
                            <div className="col-lg-8">
                                <div className="mt-4">
                                    <FadeInUp delay={0.4} isMounted={isMounted}>
                                        <h4 className="mt-4">Inbuilt applications and pages</h4>
                                    </FadeInUp>
                                    <FadeInUp delay={0.6} isMounted={isMounted}>
                                        <p className="text-muted mt-2 mb-5">
                                            Hyper comes with a variety of ready-to-use applications and pages that help
                                            to speed up the development
                                        </p>
                                    </FadeInUp>
                                    <FeatureList lists={lists} isMounted={isMounted} color={"primary"}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default FirstFeature;




