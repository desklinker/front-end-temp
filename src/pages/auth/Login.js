import React, { createRef, useEffect } from 'react';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';

import { Container, Row, Col, Card, CardBody, Label, FormGroup, Button, Alert } from 'reactstrap';
import { AvForm, AvField, AvGroup, AvInput, AvFeedback } from 'availity-reactstrap-validation';

import { initMenu, loginUser } from '../../redux/actions';
import { isUserAuthenticated } from '../../helpers/authUtils';
import LoaderWidget from '../../components/Loader';
import logo from '../../assets/images/logo.png';
import { DASHBOARD, REGISTER } from '../../constants/routes';

function Login({ history, ...props }) {
  const isMounted = createRef(null);
  const isAuthTokenValid = isUserAuthenticated();

  const handleValidSubmit = (event, values) => {
    props.loginUser(values.username, values.password, props.history);
  };

  useEffect(() => {
    isMounted.current = true;
    return () => {
      isMounted.current = false;
    };
  }, [isMounted]);

  if (isAuthTokenValid) {
    return <Redirect to={DASHBOARD} />;
  }

  const renderRedirectToRoot = () => {
    if (isAuthTokenValid) {
      return <Redirect to="/" />;
    }
  };

  return (
    <React.Fragment>
      {renderRedirectToRoot()}
      {(isMounted.current || !isAuthTokenValid) && (
        <div className="account-pages mt-5 mb-5">
          <Container>
            <Row className="justify-content-center">
              <Col lg={5}>
                <Card>
                  <div className="card-header pt-4 pb-4 text-center bg-primary">
                    <Link to="/">
                      {/*<h2 style={{color: '#fff'}}>DeskLinker</h2>*/}
                      <span>
                        <img src={logo} className="w-50" alt="logo" />
                      </span>
                    </Link>
                  </div>

                  <CardBody className="p-4 position-relative">
                    {/* preloader */}
                    {props.loading && <LoaderWidget />}

                    <div className="text-center w-75 m-auto">
                      <h4 className="text-dark-50 text-center mt-0 font-weight-bold">Sign In</h4>
                      <p className="text-muted mb-4">Enter your username and password to access admin panel.</p>
                    </div>

                    {props.error && (
                      <Alert color="danger" isOpen={props.error ? true : false}>
                        <div>{props.error}</div>
                      </Alert>
                    )}

                    <AvForm onValidSubmit={handleValidSubmit}>
                      <AvField name="username" label="Username" placeholder="Enter your username" required />

                      <AvGroup>
                        <Label for="password">Password</Label>
                        <a href="/account/forget-password" className="text-muted float-right">
                          <small>Forgot your password?</small>
                        </a>
                        <AvInput
                          type="password"
                          name="password"
                          id="password"
                          placeholder="Enter your password"
                          required
                        />
                        <AvFeedback>This field is invalid</AvFeedback>
                      </AvGroup>

                      <FormGroup>
                        <Button color="success">Submit</Button>
                      </FormGroup>
                    </AvForm>
                  </CardBody>
                </Card>
              </Col>
            </Row>

            <Row className="mt-1">
              <Col className="col-12 text-center">
                <p className="text-muted">
                  Don't have an account?{' '}
                  <Link to={REGISTER} className="text-muted ml-1">
                    <b>Register</b>
                  </Link>
                </p>
              </Col>
            </Row>
          </Container>
        </div>
      )}
    </React.Fragment>
  );
}

const mapStateToProps = (state) => {
  const { user, loading, error } = state.Auth;
  return { user, loading, error };
};

export default connect(mapStateToProps, { loginUser, initMenu })(Login);
