import React, { useState, useCallback } from 'react';
import { Container, Row, Col, Card, CardBody, FormGroup, Button, Alert } from 'reactstrap';

import emailImg from '../../assets/images/mail_sent.svg';
import { useVerifyAttribute, useResendConfirmationCode } from './hooks';
import OTPInput from '../../components/OTPInput';
import { getAuthUser } from '../../helpers/authUtils';
import { useSelector } from 'react-redux';

function PhoneNumberVerify({ history, match }) {
  const [verificationCode, setVerificationCode] = useState('');
  const { onVerifyAttribute, error } = useVerifyAttribute({ history, match });
  const user = useSelector((state) => state.Auth.user);

  const authUser = getAuthUser();

  const { resendConfirmationCode } = useResendConfirmationCode();

  const verifyPhoneNumber = useCallback(() => {
    onVerifyAttribute(verificationCode);
  }, [onVerifyAttribute, verificationCode]);

  const phoneNumber = (phoneNumber) => {
    return phoneNumber.slice(0, 3) + user.phoneNumber.slice(2).replace(/.(?=...)/g, '*');
  };
  return (
    <React.Fragment>
      <div className="account-pages mt-5 mb-5">
        <Container>
          <Row className="justify-content-center">
            <Col lg={5}>
              <Card>
                <div className="card-header pt-4 pb-4 text-center bg-primary">
                  <a href="/">
                    <h2 style={{ color: '#fff' }}>DeskLinker</h2>
                  </a>
                </div>

                <CardBody className="p-4 position-relative">
                  <div className="text-center m-auto">
                    <img src={emailImg} alt="" height="64" />

                    <h4 className="text-dark-50 text-center mt-4 font-weight-bold">Please check your phone</h4>
                    <p className="text-muted mb-4">
                      A cofirm message has been send to{' '}
                      <b>
                        {user.phoneNumber
                          ? phoneNumber(user.phoneNumber)
                          : authUser.phoneNumber && phoneNumber(authUser.phoneNumber)}
                      </b>
                      .
                    </p>

                    {error && (
                      <Alert color="danger" isOpen={error}>
                        <div>{error}</div>
                      </Alert>
                    )}

                    <OTPInput
                      length={4}
                      className="otpContainer"
                      inputClassName="otpInput"
                      isNumberInput
                      autoFocus
                      onChangeOTP={(value) => {
                        setVerificationCode(value);
                      }}
                    />

                    <FormGroup>
                      <Button color="primary" onClick={() => resendConfirmationCode('phone-number', match)}>
                        Resent Code
                      </Button>
                      <Button className="ml-2" color="primary" onClick={verifyPhoneNumber} disabled={!verificationCode}>
                        Confirm
                      </Button>
                    </FormGroup>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  );
}

export default PhoneNumberVerify;
