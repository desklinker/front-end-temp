import { useState, useEffect, useCallback, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { CognitoUserPool, CognitoUserAttribute } from 'amazon-cognito-identity-js';

import { PHONE_VERIFY, DASHBOARD, CONFIRM } from '../../constants/routes';
import { registerUser, registerUserFailed } from '../../redux/actions';
import { sendVerificationCode, create, confirmVerificationCode } from '../../actions/users';
import { region, userPoolId, poolData } from '../../config/env';

const aws = require('aws-sdk');
const userPool = new CognitoUserPool(poolData);
const cognitoIdServiceProvider = new aws.CognitoIdentityServiceProvider({
  apiVersion: '2016-04-18',
  region: region,
});

//Register new user
export function useRegisterUser(history) {
  const dispatch = useDispatch();

  const onRegisterUser = useCallback(
    async (values, actions) => {
      actions.setSubmitting(true);

      const { username, email, password, phoneNumber, groupName } = values;
      let attributeList = [];

      let dataEmail = {
        Name: 'email',
        Value: email,
      };

      let dataPhoneNumber = {
        Name: 'phone_number',
        Value: phoneNumber,
      };

      let attributeEmail = new CognitoUserAttribute(dataEmail);
      let attributePhoneNumber = new CognitoUserAttribute(dataPhoneNumber);

      attributeList.push(attributeEmail);
      attributeList.push(attributePhoneNumber);

      try {
        // Create new user in cognito user pool
        await dispatch(registerUser(username, password, groupName, attributeList, userPool));

        //Create new user in app database
        await dispatch(create(values));

        //Send email varification code
        dispatch(sendVerificationCode('email', username));
        history.push(CONFIRM.replace(':username', username));
      } catch (error) {
        actions.setFieldError('general', error.message);
      }

      actions.setSubmitting(false);
    },
    [dispatch, history]
  );

  useEffect(() => {
    return () => {
      dispatch(registerUserFailed(null));
    };
  }, [dispatch]);
  return { onRegisterUser };
}

//Confirm user with email verification code
export function useConfirmSignUp({ history, match }) {
  const [error, setError] = useState(null);
  const { username } = match.params;

  const dispatch = useDispatch();

  const confirmSignUp = useCallback(
    async (verificationCode, groupName) => {
      const res = await dispatch(confirmVerificationCode('email', verificationCode, match.params.username));

      if (!res) {
        setError('Sorry the verification code you entered was invalid, please try again');
        return;
      }

      const params = {
        UserPoolId: userPoolId,
        Username: match.params.username,
      };

      const userAttibuteParams = {
        ...params,
        UserAttributes: [
          {
            Name: 'email_verified',
            Value: 'true',
          },
        ],
      };

      cognitoIdServiceProvider.adminConfirmSignUp(params, async (err, data) => {
        if (err) {
          setError(err.message);
          return;
        }
        await cognitoIdServiceProvider.adminUpdateUserAttributes(userAttibuteParams, async (err, result) => {
          if (err) {
            setError(err.message);
            return;
          }
          await postConfirmationTrigger(username, groupName);

          dispatch(sendVerificationCode('phone-number', username));
          history.push(PHONE_VERIFY.replace(':username', username));
        });
      });
    },
    [dispatch, history, match.params.username, username]
  );

  return { confirmSignUp, error };
}

//Phone-number verify
export function useVerifyAttribute({ history, match }) {
  const [error, setError] = useState(null);
  const dispatch = useDispatch();

  const onVerifyAttribute = useCallback(
    async (verificationCode) => {
      try {
        const res = await dispatch(confirmVerificationCode('phone-number', verificationCode, match.params.username));

        if (!res) {
          setError('Sorry the verification code you entered was invalid, please try again');
          return;
        }

        var params = {
          UserAttributes: [
            {
              Name: 'phone_number_verified',
              Value: 'true',
            },
          ],
          UserPoolId: userPoolId,
          Username: match.params.username,
        };

        await cognitoIdServiceProvider.adminUpdateUserAttributes(params, function (err, result) {
          if (err) {
            setError(err.message);
            return;
          }
          history.push(DASHBOARD);
        });
      } catch (error) {}
    },
    [dispatch, history, match.params.username]
  );

  return { onVerifyAttribute, error };
}

export function useHover() {
  const [value, setValue] = useState(false);

  const ref = useRef(null);

  const handleMouseOver = () => setValue(true);
  const handleMouseOut = () => setValue(false);

  useEffect(() => {
    const node = ref.current;
    if (node) {
      node.addEventListener('mouseover', handleMouseOver);
      node.addEventListener('mouseout', handleMouseOut);

      return () => {
        node.removeEventListener('mouseover', handleMouseOver);
        node.removeEventListener('mouseout', handleMouseOut);
      };
    }
  }, []);

  return [ref, value];
}

export function useInterval(handler, interval) {
  const [intervalId, setIntervalId] = useState();
  useEffect(() => {
    const id = setInterval(handler, interval);
    setIntervalId(id);
    return () => clearInterval(id);
  }, [handler, interval]);
  return () => clearInterval(intervalId);
}

export function postConfirmationTrigger(username, groupName) {
  const params = {
    GroupName: groupName,
    UserPoolId: userPoolId,
    Username: username,
  };

  cognitoIdServiceProvider.adminAddUserToGroup(params).promise();
}

//Resend confirm code for all entities
export function useResendConfirmationCode() {
  const dispatch = useDispatch();

  const resendConfirmationCode = useCallback(
    async (verifyingEntity, match) => {
      try {
        await dispatch(sendVerificationCode(verifyingEntity, match.params.username));
      } catch (error) {}
    },
    [dispatch]
  );

  return { resendConfirmationCode };
}
