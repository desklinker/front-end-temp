import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Row, Col, Card, CardBody, FormGroup, Button, Input } from 'reactstrap';
import { isUserAuthenticated } from '../../helpers/authUtils';
import emailImg from '../../assets/images/mail_sent.svg';
import { useConfirmSignUp, useResendConfirmationCode } from './hooks';
import { useSelector } from 'react-redux';

function Confirm({ history, match }) {
  const [verificationCode, setVerificationCode] = useState(null);
  const user = useSelector((state) => state.Auth.user);

  const { confirmSignUp } = useConfirmSignUp({ history, match });
  const { resendConfirmationCode } = useResendConfirmationCode();

  const renderRedirectToRoot = () => {
    const isAuthTokenValid = isUserAuthenticated();
    if (isAuthTokenValid) {
      return <Redirect to="/" />;
    }
  };

  const isAuthTokenValid = isUserAuthenticated();

  return (
    <React.Fragment>
      {renderRedirectToRoot()}

      {!isAuthTokenValid && (
        <div className="account-pages mt-5 mb-5">
          <Container>
            <Row className="justify-content-center">
              <Col lg={5}>
                <Card>
                  <div className="card-header pt-4 pb-4 text-center bg-primary">
                    <a href="/">
                      <h2 style={{ color: '#fff' }}>DeskLinker</h2>
                    </a>
                  </div>

                  <CardBody className="p-4 position-relative">
                    <div className="text-center m-auto">
                      <img src={emailImg} alt="" height="64" />

                      <h4 className="text-dark-50 text-center mt-4 font-weight-bold">Please check your email</h4>
                      <p className="text-muted mb-4">
                        A email has been send to <b>{user && user.email}</b>. Please check for an email from company.
                      </p>

                      <FormGroup>
                        <Input
                          name="code"
                          id="code"
                          placeholder="Enter the verification code"
                          onChange={(e) => setVerificationCode(e.target.value)}
                        />
                      </FormGroup>

                      <FormGroup>
                        <Button color="primary" onClick={()=>resendConfirmationCode('email', match )}>
                          Resent Code
                        </Button>
                        <Button
                          className="ml-2"
                          color="primary"
                          onClick={() => confirmSignUp(verificationCode, user.role)}
                          disabled={!verificationCode}>
                          Confirm
                        </Button>
                      </FormGroup>
                    </div>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </Container>
        </div>
      )}
    </React.Fragment>
  );
}

export default Confirm;
