import React from 'react';

import PageTitle from '../../components/PageTitle';

import './styles/contacts.css';
import { useSelector } from 'react-redux';
import ClientContact from './ClientContact';
import OtherContact from './OtherContact';
import AdminContact from './AdminContact';
import { ADMIN, CLIENT } from '../../constants';

// ChatApp
function Contacts() {
  const user = useSelector((state) => state.Auth.user);

  return (
    <React.Fragment>
      <PageTitle breadCrumbItems={[{ label: 'Contacts', path: '/contacts', active: true }]} title={'Contacts'} />

      {user.role === CLIENT ? <ClientContact /> : user.role === ADMIN ? <AdminContact /> : <OtherContact />}
    </React.Fragment>
  );
}

export default Contacts;
