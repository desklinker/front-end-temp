// @flow
import React from 'react';
import { Table } from 'reactstrap';

class MutualProjects extends React.Component {
    render() {
        const subProjects = [
            { name: 'Sub Project 1', startDate: '03/05/2020', duration: '15 days', budget: '$250' },
            { name: 'Sub Project 2', startDate: '03/05/2020', duration: '15 days', budget: '$250' },
            { name: 'Sub Project 3', startDate: '03/05/2020', duration: '15 days', budget: '$250' },
            { name: 'Sub Project 4', startDate: '03/05/2020', duration: '15 days', budget: '$250' },
        ];

        return (
            <div>
                <h5 className="card-title mb-3">Mutual Projects</h5>

                <Table className="mb-0" size="sm" bordered>
                    <thead className="thead-dark">
                        <tr>
                            <th>Project</th>
                            <th>Start date</th>
                            <th>Duration</th>
                            <th>Budget</th>
                            <th>Status</th>
                            <th>Rating</th>
                        </tr>
                    </thead>
                    <tbody>
                        {subProjects.map((p, index) => {
                            return (
                                <tr key={index}>
                                    <td>
                                        <a href="/project/sub">{p.name}</a>
                                    </td>
                                    <td>{p.startDate}</td>
                                    <td>{p.duration}</td>
                                    <td>{p.budget}</td>
                                    <td>Completed</td>
                                    <td>4.5</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default MutualProjects;
