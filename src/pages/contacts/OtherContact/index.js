import React, { Component } from 'react';
import { Row, Col, CardBody, Card } from 'reactstrap';

import ChatUsers from './ChatUsers';

import { users } from './data';
import UserBox from '../../profile/ViewProfile/UserBox';
import UserInfoBox2 from '../../profile/ViewProfile/UserInfoBox2';
import MutualProjects from './MutualProjects';

import '../styles/contacts.css';

// ChatApp
class OtherContact extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedUser: users[1],
        };

        this.onUserChange = this.onUserChange.bind(this);
    }

    /**
     * On user change
     */
    onUserChange = (user) => {
        this.setState({ selectedUser: user });
    };

    render() {
        return (
            <Row>
                <Col lg={3} md={12} className="order-lg-1 order-xl-1">
                    <ChatUsers onUserSelect={this.onUserChange} />
                </Col>

                <Col lg={9} md={12} className="order-lg-1 order-xl-1">
                    <UserBox />
                    <UserInfoBox2 />
                    <Card>
                        <CardBody>
                            <MutualProjects />
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        );
    }
}

export default OtherContact;
