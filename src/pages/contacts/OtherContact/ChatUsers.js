import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Card, CardBody, Media, Collapse } from 'reactstrap';
import classnames from 'classnames';
import SimpleBar from 'simplebar-react';
import Select from 'react-select';

import { users } from './data';

import '../styles/contacts.css';

// ChatUsers
class ChatUsers extends Component {
    constructor(props) {
        super(props);

        this.state = {
            users: [...users],
            selectedUser: users[1],
            selectedGroup: 'All',
            isFiltersOpen: false,
        };

        this.filterUsers = this.filterUsers.bind(this);
        this.search = this.search.bind(this);
        this.activateUser = this.activateUser.bind(this);
    }

    /**
     * Filter users
     */
    filterUsers = (group) => {
        this.selectedGroup = group;
        this.setState({
            selectedGroup: group,
            users:
                group !== 'All'
                    ? [...users].filter((u) => u.groups.toLowerCase().indexOf(group.toLowerCase()) >= 0)
                    : [...users],
        });
    };

    /**
     * Search the user
     * @param {*} text
     */
    search(text) {
        this.setState({
            users: text ? [...users].filter((u) => u.name.indexOf(text.toUpperCase()) >= 0) : [...users],
        });
    }

    /**
     * Activates the user
     * @param {*} user
     */
    activateUser(user) {
        this.setState({ selectedUser: user });
        if (this.props.onUserSelect) {
            this.props.onUserSelect(user);
        }
    }

    render() {
        const skills = [
            { value: 'cad', label: 'CAD' },
            { value: 'design', label: 'Design' },
            { value: 'arch', label: 'Archtecture' },
        ];
        const grades = [
            { value: 'Interns', label: 'Inters' },
            { value: 'Freshers', label: 'Freshers' },
            { value: 'Juniors', label: 'Juniors' },
            { value: 'Seniors', label: 'Seniors' },
        ];
        const ratings = [
            { value: '4+', label: '4 and above' },
            { value: '2-4', label: '2 to 4' },
            { value: '-2', label: '2 and below' },
        ];
        const toggle = () => this.setState({ isFiltersOpen: !this.state.isFiltersOpen });

        return (
            <React.Fragment>
                <Card>
                    <CardBody className="p-0">
                        {/* <ul className="nav nav-tabs nav-bordered">
                            {groupFilters.map((group, index) => {
                                return (
                                    <li key={index} className="nav-item contact-grade-filter-li" onClick={e => this.filterUsers(group)}>
                                        <Link
                                            to="#"
                                            className={classnames('nav-link', {
                                                active: this.state.selectedGroup === group,
                                            })}>
                                            {group}
                                        </Link>
                                    </li>
                                );
                            })}
                        </ul>                         */}

                        <div className="tab-content">
                            <div className="tab-pane show active p-3">
                                <div className="app-search">
                                    <div className="form-group position-relative">
                                        <input
                                            type="text"
                                            className="form-control"
                                            placeholder="Search Professionals"
                                            onKeyUp={(e) => this.search(e.target.value)}
                                        />
                                        <span className="mdi mdi-magnify"></span>
                                    </div>
                                </div>

                                <div className="text-right text-muted">
                                    <a href="##" className="text-muted font-13" onClick={toggle}>
                                        filters
                                    </a>
                                </div>
                                <Collapse isOpen={this.state.isFiltersOpen}>
                                    <Select className="react-select mb-1" placeholder="skills.." options={skills} />
                                    <Select className="react-select mb-1" placeholder="grades.." options={grades} />
                                    <Select className="react-select mb-1" placeholder="ratings.." options={ratings} />
                                </Collapse>

                                <SimpleBar style={{ maxHeight: '550px', width: '100%' }}>
                                    {this.state.users.map((user, index) => {
                                        return (
                                            <Link
                                                to="#"
                                                key={index}
                                                className="text-body"
                                                onClick={(e) => {
                                                    this.activateUser(user);
                                                }}>
                                                <Media
                                                    className={classnames('mt-1', 'p-2', {
                                                        'bg-light': user.id === this.state.selectedUser.id,
                                                        'grade-1': user.grade === 'Subordinates',
                                                        'grade-2': user.grade === 'Interns',
                                                        'grade-3': user.grade === 'Freshers',
                                                        'grade-4': user.grade === 'Juniors',
                                                        'grade-5': user.grade === 'Seniors',
                                                    })}>
                                                    <img
                                                        src={user.avatar}
                                                        className="mr-2 rounded-circle"
                                                        height="48"
                                                        alt=""
                                                    />

                                                    <Media body>
                                                        <h5 className="mt-0 mb-0 font-14 user-name">
                                                            {/* <span className="float-right text-muted font-12">
                                                                {user.lastMessageOn}
                                                            </span> */}
                                                            {user.name}
                                                        </h5>
                                                        <div className="user-rating text-muted font-13">
                                                            <i className="uil-star"></i> {user.rating}
                                                        </div>
                                                        <p className="mt-1 mb-0 text-muted font-14">
                                                            Civil Engineer
                                                            {/* <span className="w-25 float-right text-right">
                                                                {user.totalUnread && (
                                                                    <span className="badge badge-danger-lighten">
                                                                        {user.totalUnread}
                                                                    </span>
                                                                )}
                                                            </span> */}
                                                            {/* <span className="w-75">{user.lastMessage}</span> */}
                                                        </p>
                                                    </Media>
                                                </Media>
                                            </Link>
                                        );
                                    })}
                                </SimpleBar>
                            </div>
                        </div>
                    </CardBody>
                </Card>
            </React.Fragment>
        );
    }
}

export default ChatUsers;
