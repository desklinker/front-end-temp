import React from 'react';
import PaginateTable from '../../../../components/PaginateTable';
import { Card, CardBody } from 'reactstrap';

const records = [
    {
        id: 1,
        name: 'Ulrikaumeko Jentle',
        phone: '(478) 8204063',
        email: 'ujentled@pagesperso-orange.fr',
        location: 'Sweden',
        rating: 2.5,
    },
    {
        id: 2,
        name: 'Peria Richens',
        phone: '(423) 7419337',
        email: 'prichense@google.it',
        location: 'Indonesia',
        rating: 3.5,
    },
    {
        id: 3,
        name: 'Sollie Ramelet',
        phone: '(600) 7539738',
        email: 'srameletf@hubpages.com',
        location: 'Brazil',
        rating: 2.5,
    },
    {
        id: 4,
        name: 'Hodge Diviney',
        phone: '(311) 8358001',
        email: 'hdivineyg@ycombinator.com',
        location: 'Belarus',
        rating: 4.5,
    },
    {
        id: 5,
        name: 'Roger Lidster',
        phone: '(146) 1370107',
        email: 'rlidsterh@redcross.org',
        location: 'Indonesia',
        rating: 4.5,
    },
    {
        id: 6,
        name: 'Kata MacKnight',
        phone: '(402) 3164595',
        email: 'kmacknight5@boston.com',
        location: 'China',
        rating: 3,
    },
    {
        id: 7,
        name: 'Mateo Kingzeth',
        phone: '(941) 2564726',
        email: 'mkingzeth6@google.com.hk',
        location: 'China',
        rating: 4,
    },
    {
        id: 8,
        name: 'Caprice Custed',
        phone: '(239) 8054555',
        email: 'ccustedi@godaddy.com',
        location: 'Russia',
        rating: 3.5,
    },
    {
        id: 9,
        name: 'Becki Filer',
        phone: '(128) 9346017',
        email: 'bfilerj@xrea.com',
        location: 'Madagascar',
        rating: 4.5,
    },
    {
        id: 10,
        name: 'Ezmeralda Piggrem',
        phone: '(532) 2561412',
        email: 'epiggrema@addtoany.com',
        location: 'Cambodia',
        rating: 3,
    },
];

const columns = [
    {
        dataField: 'name',
        text: 'Name',
        sort: true,
    },
    {
        dataField: 'phone',
        text: 'Phone',
        sort: true,
    },
    {
        dataField: 'location',
        text: 'Location',
        sort: true,
    },
    {
        dataField: 'email',
        text: 'Email',
        sort: true,
    },
    {
        dataField: 'rating',
        text: 'Rating',
        sort: true,
        formatter: 'ratingColumn',
    },
];

function ExecutivesTable() {
    return (
        <Card>
            <CardBody>
                <PaginateTable data={records} columns={columns} title="Executives Contacts" />
            </CardBody>
        </Card>
    );
}

export default ExecutivesTable;
