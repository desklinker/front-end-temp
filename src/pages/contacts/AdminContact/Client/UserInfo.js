import React from 'react';
import '../styles/profile.css';
import { Card, CardBody, Col, Row } from 'reactstrap';

function UserInfo() {
    return (
        <Card>
            <CardBody>
                <Row>
                    <Col md={4}>
                        <h4 className="header-title mt-0 mb-3">Basic Information</h4>
                        <div className="text-left">
                            <p className="text-muted">
                                <strong>Mobile :</strong>
                                <span className="ml-2">(+94) 77 123 4567</span>
                            </p>

                            <p className="text-muted">
                                <strong>Email :</strong> <span className="ml-2">vikki@gmail.com</span>
                            </p>

                            <p className="text-muted">
                                <strong>Country :</strong> <span className="ml-2">Singapore</span>
                            </p>

                            <p className="text-muted">
                                <strong>Skills :</strong>
                                <span className="ml-2"> CAD, Design, Architecture </span>
                            </p>
                        </div>
                    </Col>
                    <Col md={4}>
                        <h4 className="header-title mt-0 mb-3">Company Details</h4>
                        <div className="text-left">
                            <p>
                                <strong>Name :</strong>
                                <span className="ml-2"> ABC (Pvt)Ltd</span>
                            </p>

                            <p>
                                <strong>Address :</strong>
                                <span className="ml-2"> 3940 Richison Drive, Singapore</span>
                            </p>

                            <p>
                                <strong>Telephone Number :</strong>
                                <span className="ml-2">+1 456 9595 9594 </span>
                            </p>

                            <p>
                                <strong>Fax Number :</strong>
                                <span className="ml-2">+1 456 9595 9594 </span>
                            </p>
                        </div>
                    </Col>
                    <Col md={4}>
                        <h4 className="header-title mt-0 mb-3">Bank Details</h4>
                        <div className="text-left">
                            <p className="text-muted">
                                <strong>Bank Name :</strong>
                                <span className="ml-2">Commercial Bank (Pvt) Ltd</span>
                            </p>

                            <p className="text-muted">
                                <strong>Branch :</strong> <span className="ml-2">Wellawatte</span>
                            </p>

                            <p className="text-muted">
                                <strong>Account Number :</strong> <span className="ml-2">00212545212</span>
                            </p>
                        </div>
                    </Col>
                </Row>
            </CardBody>
        </Card>
    );
}

export default UserInfo;
