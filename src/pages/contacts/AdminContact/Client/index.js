import React from 'react';
import { Col, Row } from 'reactstrap';
import Users from '../Users';
import UserBox from '../../../profile/ViewProfile/UserBox';
import UserInfo from './UserInfo';
import ProjectsTable from './ProjectsTable';
import CoordinatorsTable from './CoordinatorsTable';
import ExecutivesTable from './ExecutivesTable';
import Threads from '../Threads';
import Templates from './Templates';
import ClientSettings from '../settings/ClientSettings';

function Client({ onUserChange, isSetting }) {
    return (
        <>
            {!isSetting ? (
                <>
                    <Row className="pr-1">
                        <Col lg={3} md={12} className="order-lg-1 order-xl-1 w-100">
                            <Users onUserSelect={onUserChange} />
                        </Col>

                        <Col lg={9} md={12} className="order-lg-1 order-xl-1">
                            <UserBox type="client" />
                            <UserInfo />
                            <CoordinatorsTable />
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <ProjectsTable />
                            <ExecutivesTable />
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Templates title="Templates" />
                        </Col>
                    </Row>
                    <Threads />
                </>
            ) : (
                <ClientSettings />
            )}
        </>
    );
}

export default Client;
