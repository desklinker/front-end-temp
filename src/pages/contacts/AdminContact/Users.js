import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Card, CardBody, Media, Collapse, Button } from 'reactstrap';
import classnames from 'classnames';
import SimpleBar from 'simplebar-react';
import { users as allUsers } from './data';
import '../styles/contacts.css';
import FilterForm from './FilterForm';

function Users(props) {
    const [users, setUsers] = useState(allUsers);
    const [selectedUser, setSelectedUser] = useState(users[1]);
    const [isFiltersOpen, setFiltersOpen] = useState(false);

    useEffect(() => {}, []);
    /**
     * Search the user
     * @param {*} text
     */
    const search = (text) => {
        setUsers(text ? [...users].filter((u) => u.name.indexOf(text.toUpperCase()) >= 0) : [...users]);
    };

    /**
     * Activates the user
     * @param {*} user
     */
    const activateUser = (user) => {
        setSelectedUser(user);
        if (props.onUserSelect) {
            props.onUserSelect(user);
        }
    };

    const toggle = () => setFiltersOpen(!isFiltersOpen);

    return (
        <React.Fragment>
            <Card>
                <CardBody className="p-0">
                    <div className="tab-content">
                        <div className="tab-pane show active p-3">
                            <div className="app-search d-inline-block text-right">
                                <div className="form-group d-inline-block position-relative">
                                    <input
                                        type="text"
                                        className="form-control"
                                        placeholder="Search Professionals"
                                        onKeyUp={(e) => search(e.target.value)}
                                    />
                                    <span className="mdi mdi-magnify"></span>
                                </div>

                                <Button className="ml-2 d-inline-block btn btn-secondary mb-1" onClick={toggle}>
                                    <i className="mdi mdi-filter-variant"></i>
                                </Button>
                            </div>
                            <Collapse isOpen={isFiltersOpen}>
                                <FilterForm setFilterOpen={setFiltersOpen} isSkill={props.isSkill} />
                            </Collapse>
                            <SimpleBar style={{ maxHeight: '600px', width: '100%' }}>
                                {users.map((user, index) => {
                                    return (
                                        <Link
                                            to="#"
                                            key={index}
                                            className="text-body"
                                            onClick={(e) => {
                                                activateUser(user);
                                            }}>
                                            <Media
                                                className={classnames('mt-1', 'p-2', {
                                                    'bg-light': user.id === selectedUser.id,
                                                    'grade-1': user.grade === 'Subordinates',
                                                    'grade-2': user.grade === 'Interns',
                                                    'grade-3': user.grade === 'Freshers',
                                                    'grade-4': user.grade === 'Juniors',
                                                    'grade-5': user.grade === 'Seniors',
                                                })}>
                                                <img
                                                    src={user.avatar}
                                                    className="mr-2 rounded-circle"
                                                    height="48"
                                                    alt=""
                                                />

                                                <Media body>
                                                    <h5 className="mt-0 mb-0 font-14 user-name">{user.name}</h5>
                                                    <div className="user-rating text-muted font-13">
                                                        <i className="uil-star"></i> {user.rating}
                                                    </div>
                                                    <p className="mt-1 mb-0 text-muted font-14">Civil Engineer</p>
                                                </Media>
                                            </Media>
                                        </Link>
                                    );
                                })}
                            </SimpleBar>
                        </div>
                    </div>
                </CardBody>
            </Card>
        </React.Fragment>
    );
}

export default Users;
