import React, {  useState } from 'react';
import { Row, Col,  Nav, NavItem, NavLink, TabContent, TabPane, Button } from 'reactstrap';

import { users } from './data';

import '../styles/contacts.css';
import classnames from 'classnames';
import Client from './Client';
import Coordinator from './Coordinator';
import Professional from './Professional';

// ChatApp
function AdminContact() {
    const [selectedUser, setSelectedUser] = useState(users[1]);
    const [activeTab, setActiveTab] = useState('1');
    const [isSetting, setSetting] = useState(false);

    const onUserChange = (user) => {
        setSelectedUser(user);
    };

    const toggle = (tab: string) => {
        if (activeTab !== tab) {
            setActiveTab(tab);
        }
    };

    const tabContents = [
        {
            id: '1',
            title: 'Client',
            icon: 'mdi mdi-home-variant',
        },
        {
            id: '2',
            title: 'Coordinator',
            icon: 'mdi mdi-account-circle',
        },
        {
            id: '3',
            title: 'Professional',
            icon: 'mdi mdi-settings-outline',
        },
    ];

    const handleToggle = () => {
        setSetting(!isSetting);
    };

    return (
        <>
            <Row>
                <Col md={9}>
                    <Nav tabs className="nav-bordered pb-1 mb-2">
                        {tabContents.map((tab, index) => {
                            return (
                                <NavItem key={index}>
                                    <NavLink
                                        href="#"
                                        className={classnames({ active: activeTab === tab.id })}
                                        onClick={() => toggle(tab.id)}>
                                        <i className={classnames(tab.icon, 'd-lg-none', 'd-block', 'mr-1')}></i>
                                        <span className="d-none d-lg-block">{tab.title}</span>
                                    </NavLink>
                                </NavItem>
                            );
                        })}
                    </Nav>
                </Col>
                <Col className="text-right">
                    <Button
                        className={`ml-2 d-inline-block btn ${isSetting ? 'btn-dark' : 'btn-secondary'}`}
                        onClick={() => handleToggle()}>
                        <i className="mdi mdi-settings"></i>
                    </Button>
                </Col>
            </Row>
            <Row>
                <Col>
                    <TabContent activeTab={activeTab}>
                        <TabPane tabId="1">
                            <Client onUserChange={onUserChange} isSetting={isSetting} selectedUser={selectedUser} />
                        </TabPane>
                        <TabPane tabId="2">
                            <Coordinator onUserChange={onUserChange} isSetting={isSetting} />
                        </TabPane>
                        <TabPane tabId="3">
                            <Professional onUserChange={onUserChange} isSetting={isSetting} />
                        </TabPane>
                    </TabContent>
                </Col>
            </Row>
        </>
    );
}

export default AdminContact;
