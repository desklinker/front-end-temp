import React from 'react';
import PaginateTable from '../../../../components/PaginateTable';
import { Card, CardBody } from 'reactstrap';

const records = [
    {
        id: 1,
        project: 'Project #1',
        budget: '$100',
        duration: '50 days',
        postedOn: '2020-01-05',
        biddingStatus: 'completed',
        outputDeliveryStatus: 'revisionsOnProgress',
        paymentStatus: 'pending',
    },
    {
        id: 2,
        project: 'Project #2',
        budget: '$300',
        duration: '30 days',
        postedOn: '2020-01-05',
        biddingStatus: 'ongoing',
        outputDeliveryStatus: 'pendingClientConfirmation',
        paymentStatus: 'success',
    },
    {
        id: 3,
        project: 'Project #3',
        budget: '$350',
        duration: '4 months',
        postedOn: '2020-01-08',
        biddingStatus: 'ongoing',
        outputDeliveryStatus: 'pendingClientConfirmation',
        paymentStatus: 'success',
    },
    {
        id: 4,
        project: 'Project #4',
        budget: '$250',
        duration: '2 months',
        postedOn: '2020-02-05',
        biddingStatus: 'completed',
        outputDeliveryStatus: 'revisionsOnProgress',
        paymentStatus: 'pending',
        executives: ['Ezmeralda Piggrem', 'Becki Filer'],
    },
    {
        id: 5,
        project: 'Project #5',
        budget: '$200',
        duration: '27 days',
        postedOn: '2020-01-05',
        biddingStatus: 'ongoing',
        outputDeliveryStatus: 'pendingClientConfirmation',
        paymentStatus: 'pending',
    },
    {
        id: 6,
        project: 'Project #6',
        budget: '$250',
        duration: '4 months',
        postedOn: '2020-03-05',
        biddingStatus: 'ongoing',
        outputDeliveryStatus: 'pendingClientConfirmation',
        paymentStatus: 'success',
        executives: ['Peria Richens'],
    },
    {
        id: 7,
        project: 'Project #7',
        budget: '$210',
        duration: '5 months',
        postedOn: '2020-01-05',
        biddingStatus: 'ongoing',
        paymentStatus: 'success',
        outputDeliveryStatus: 'scopeChangesOnProgress',
    },
    {
        id: 8,
        project: 'Project #8',
        budget: '$300',
        duration: '6 months',
        postedOn: '2020-01-10',
        biddingStatus: 'ongoing',
        paymentStatus: 'success',
        outputDeliveryStatus: 'scopeChangesOnProgress',
    },
    {
        id: 9,
        project: 'Project #9',
        budget: '$105',
        duration: '15 days',
        postedOn: '2020-01-05',
        biddingStatus: 'ongoing',
        outputDeliveryStatus: 'pendingClientConfirmation',
        paymentStatus: 'pending',
        executives: ['Peria Richens', 'Ulrikaumeko Jentle'],
    },
    {
        id: 10,
        project: 'Project #10',
        budget: '$230',
        duration: '20 days',
        postedOn: '2020-01-05',
        biddingStatus: 'ongoing',
        outputDeliveryStatus: 'pendingClientConfirmation',
        paymentStatus: 'pending',
    },
    {
        id: 11,
        project: 'Project #11',
        budget: '$350',
        duration: '20 days',
        postedOn: '2020-01-05',
        biddingStatus: 'ongoing',
        outputDeliveryStatus: 'pendingClientConfirmation',
        paymentStatus: 'pending',
        executives: ['Roger Lidster', 'Hodge Diviney'],
    },
    {
        id: 12,
        project: 'Project #12',
        budget: '$320',
        duration: '3 months',
        postedOn: '2020-01-05',
        biddingStatus: 'ongoing',
        outputDeliveryStatus: 'pendingClientConfirmation',
        paymentStatus: 'pending',
    },
    {
        id: 13,
        project: 'Project #13',
        budget: '$140',
        duration: '25 days',
        postedOn: '2020-01-05',
        biddingStatus: 'ongoing',
        outputDeliveryStatus: 'revisionsOnProgress',
        paymentStatus: 'pending',
    },
    {
        id: 14,
        project: 'Project #14',
        budget: '$120',
        duration: '3 months',
        postedOn: '2020-01-05',
        biddingStatus: 'ongoing',
        outputDeliveryStatus: 'draftsPassed',
        paymentStatus: 'success',
    },
    {
        id: 15,
        project: 'Project #15',
        budget: '$120',
        duration: '15 days',
        postedOn: '2020-01-05',
        biddingStatus: 'ongoing',
        outputDeliveryStatus: 'pendingClientConfirmation',
        paymentStatus: 'success',
    },
];

const columns = [
    {
        dataField: 'project',
        text: 'Project',
        sort: true,
    },
    {
        dataField: 'budget',
        text: 'Budget',
        sort: true,
    },
    {
        dataField: 'duration',
        text: 'Duration',
        sort: true,
    },
    {
        dataField: 'postedOn',
        text: 'Posted On',
        sort: true,
    },
    {
        dataField: 'biddingStatus',
        text: 'Bidding Status',
        sort: true,
        formatter: 'statusColumn',
    },
    {
        dataField: 'outputDeliveryStatus',
        text: 'Output Status',
        sort: true,
        formatter: 'statusColumn',
    },
    {
        dataField: 'paymentStatus',
        text: 'Payment Status',
        sort: true,
        formatter: 'statusColumn',
    },
];

function ProjectsTable() {
    return (
        <Card>
            <CardBody>
                <PaginateTable data={records} columns={columns} title="Projects" />
            </CardBody>
        </Card>
    );
}

export default ProjectsTable;
