import React from 'react';
import { Col, Row } from 'reactstrap';
import ChatUsers from './ChatUsers';
import Comments from './Comments';

function Threads({ onUserChange }) {
    return (
        <Row>
            <Col xl={3} lg={3} md={12} className="order-lg-1 order-xl-1">
                <ChatUsers onUserSelect={onUserChange} />
            </Col>

            <Col xl={9} lg={9} md={12} className="order-lg-2 order-xl-1">
                <Comments />
            </Col>
        </Row>
    );
}

export default Threads;
