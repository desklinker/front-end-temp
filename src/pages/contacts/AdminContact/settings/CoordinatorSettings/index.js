import React, { useState } from 'react';
import CardBody from 'reactstrap/es/CardBody';
import Card from 'reactstrap/es/Card';
import PaginateTable from '../../../../../components/PaginateTable';
import { columns, records } from './data';
import { Modal, ModalBody, ModalHeader } from 'reactstrap';
import Form from './Form';

function CoordinatorSettings() {
    const [selected, setSelected] = useState({});
    const [isOpen, setOpen] = useState(false);

    const handleSelected = (value) => {
        setSelected(value);
        toggle();
    };

    const toggle = () => {
        setOpen(!isOpen);
    };

    return (
        <>
            <Card>
                <CardBody>
                    <PaginateTable
                        data={records}
                        columns={columns}
                        setSelected={(value) => handleSelected(value)}
                        searchable
                    />
                </CardBody>
            </Card>

            <Modal isOpen={isOpen} toggle={toggle} className="modal-dialog-centered" size="lg">
                <ModalHeader toggle={toggle}>Edit</ModalHeader>
                <ModalBody>
                    <Form items={selected} toggle={toggle} />
                </ModalBody>
            </Modal>
        </>
    );
}

export default CoordinatorSettings;
