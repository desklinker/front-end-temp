import React, { useEffect, useState } from 'react';
import { Button, Col, CustomInput, Row } from 'reactstrap';
import { AvField, AvForm } from 'availity-reactstrap-validation';
import { projectCategories, skills } from '../../../../dashboards/AdminDashboard/data';
import Select from 'react-select';
import Label from 'reactstrap/es/Label';
import startCase from 'lodash-es/startCase';
import lowerCase from 'lodash-es/lowerCase';
import FormGroup from 'reactstrap/es/FormGroup';

const status = ['active', 'viewer', 'inActive', 'pastProjectViewer'];

function Form({ items, toggle }) {
    const [values, setValues] = useState({});

    const handleSubmit = () => {
        //
    };

    const updateValues = (field, fieldValue) => {
        setValues({
            ...values,
            [field]: fieldValue,
        });
    };

    useEffect(() => {
        if (items) {
            setValues(items);
        }
    }, [items]);

    return (
        <AvForm onValidSubmit={handleSubmit} className="w-100" model={items ? items : {}}>
            <Row>
                <Col md={4}>
                    <AvField name="name" label="Name" placeholder="Name" />
                </Col>
                <Col md={4}>
                    <Label>Category</Label>
                    <Select
                        placeholder="Select category..."
                        value={(values && values.category) || ''}
                        className="react-select mt-1"
                        classNamePrefix="react-select"
                        onChange={(value) => {
                            updateValues('category', value);
                        }}
                        options={projectCategories}
                    />
                </Col>
                <Col md={4}>
                    <Label>Category</Label>
                    <Select
                        placeholder="Select skill..."
                        value={(values && values.skill) || ''}
                        className="react-select mt-1"
                        classNamePrefix="react-select"
                        onChange={(value) => {
                            updateValues('skill', value);
                        }}
                        options={skills}
                    />
                </Col>
            </Row>
            <Row>
                {' '}
                <Col md={4}>
                    <AvField name="phone" label="Phone" placeholder="Phone" />
                </Col>
                <Col md={4}>
                    <AvField name="email" label="Email" placeholder="Email" type="email" />
                </Col>
                <Col md={4}>
                    <AvField name="grade" label="Grade" placeholder="Grade" type="number" />
                </Col>
            </Row>
            <Row>
                <Col md={4}>
                    <AvField name="rating" label="Rating" placeholder="Rating" type="number" step={0.5} />
                </Col>
                <Col md={4}>
                    <AvField
                        name="noOfActiveProjects"
                        label="No of Active Projects"
                        placeholder="No of active projects"
                        type="number"
                    />
                </Col>
                <Col md={4}>
                    <AvField
                        name="noOfCompletedProjects"
                        label="No of Completed Projects"
                        placeholder="No of completed projects"
                        type="number"
                    />
                </Col>
            </Row>
            <Row>
                <Col md={4}>
                    <AvField name="totalPayment" label="Total Payment" placeholder="Total payment" type="number" />
                </Col>
                <Col md={4}>
                    <AvField
                        name="terminatedProjects"
                        label="Terminated Projects"
                        placeholder="Terminated Projects"
                        type="number"
                    />
                </Col>
                <Col md={4}>
                    <AvField
                        name="gaveUpProjects"
                        label="Gave Up Projects"
                        placeholder="Gave up projects"
                        type="number"
                    />
                </Col>
            </Row>

            <Row>
                <Col>
                    <Label for="status">Status</Label>
                </Col>
            </Row>
            <Row className="text-muted">
                {status.map((item) => {
                    return (
                        <Col key={item}>
                            <FormGroup>
                                <CustomInput
                                    type="radio"
                                    id={item}
                                    className="text-muted"
                                    checked={values.activeStatus === item}
                                    onChange={(e) => updateValues('activeStatus', item)}
                                    label={startCase(lowerCase(item))}
                                />
                            </FormGroup>
                        </Col>
                    );
                })}
            </Row>
            <Row className="mt-2">
                <Col className="text-right">
                    <Button type="submit" color="primary" className="mr-2">
                        Update
                    </Button>
                    <Button color="secondary" onClick={toggle}>
                        Cancel
                    </Button>
                </Col>
            </Row>
        </AvForm>
    );
}

export default Form;
