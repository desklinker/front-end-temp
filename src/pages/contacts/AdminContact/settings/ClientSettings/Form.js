import React, { useEffect, useState } from 'react';
import { Button, Col, CustomInput, Row } from 'reactstrap';
import { AvField, AvForm } from 'availity-reactstrap-validation';
import Label from 'reactstrap/es/Label';
import FormGroup from 'reactstrap/es/FormGroup';
import startCase from 'lodash-es/startCase';
import lowerCase from 'lodash-es/lowerCase';

const status = ['active', 'inActive'];

function Form({ items, toggle }) {
    const [values, setValues] = useState({});

    const handleSubmit = () => {
        //
    };

    const updateValues = (field, fieldValue) => {
        setValues({
            ...values,
            [field]: fieldValue,
        });
    };

    useEffect(() => {
        if (items) {
            setValues(items);
        }
    }, [items]);


    return (
        <AvForm onValidSubmit={handleSubmit} className="w-100" model={items ? items : {}}>
            <Row>
                <Col md={6}>
                    <AvField name="phone" label="Phone" placeholder="Phone" />
                </Col>
                <Col md={6}>
                    <AvField name="email" label="Email" placeholder="Email" type="email" />
                </Col>
            </Row>
            <Row>
                <Col>
                    <Label for="status">Status</Label>
                </Col>
            </Row>
            <Row className="text-muted">
                {status.map((item) => {
                    return (
                        <Col key={item} md={3}>
                            <FormGroup>
                                <CustomInput
                                    type="radio"
                                    id={item}
                                    className="text-muted"
                                    checked={values.activeStatus === item}
                                    onChange={(e) => updateValues('activeStatus', item)}
                                    label={startCase(lowerCase(item))}
                                />
                            </FormGroup>
                        </Col>
                    );
                })}
            </Row>
            <Row>
                <Col className="text-right">
                    <Button type="submit" color="primary" className="mr-2">
                        Update
                    </Button>
                    <Button color="secondary" onClick={toggle}>
                        Cancel
                    </Button>
                </Col>
            </Row>
        </AvForm>
    );
}

export default Form;
