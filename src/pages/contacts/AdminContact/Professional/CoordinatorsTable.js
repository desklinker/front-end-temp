import React from 'react';
import PaginateTable from '../../../../components/PaginateTable';
import { Card, CardBody } from 'reactstrap';

const records = [
    {
        id: 1,
        name: 'Abagael Breslau',
        phone: '(461) 3636077',
        email: 'kmacknight5@boston.com',
        noOfProjects: 6,
    },
    {
        id: 2,
        name: 'Veradis Taber',
        phone: '(917) 2590629',
        email: 'mkingzeth6@google.com.hk',
        noOfProjects: 5,
    },
    {
        id: 3,
        name: 'Lindon Ceeley',
        phone: '(925) 9515307',
        email: 'ccustedi@godaddy.com',
        noOfProjects: 2,
    },
    {
        id: 4,
        name: 'Mendel Alfonzo',
        phone: '(295) 3668262',
        email: 'bfilerj@xrea.com',
        noOfProjects: 3,
    },
    {
        id: 5,
        name: 'Dorrie Tindley',
        phone: '(809) 2120936',
        email: 'kmacknight5@boston.com',
        noOfProjects: 2,
    },
    {
        id: 6,
        name: 'Kata MacKnight',
        phone: '(402) 3164595',
        email: 'kata@redcross.org',
        noOfProjects: 3,
    },
    {
        id: 7,
        name: 'Mateo Kingzeth',
        phone: '(941) 2564726',
        email: 'mateo@redcross.org',
        noOfProjects: 4,
    },
    {
        id: 8,
        name: 'Jayson Creaney',
        phone: '(969) 3198297',
        email: 'rlidsterh@redcross.org',
        noOfProjects: 3,
    },
    {
        id: 9,
        name: 'Elita Ortmann',
        phone: '(283) 1302865',
        email: 'bfilerj@xrea.com',
        noOfProjects: 2,
    },
    {
        id: 10,
        name: 'Tyne Hollidge',
        phone: '(529) 2241787',
        email: 'epiggrema@addtoany.com',
        noOfProjects: 3,
    },
];

const columns = [
    {
        dataField: 'name',
        text: 'Name',
        sort: true,
    },
    {
        dataField: 'phone',
        text: 'Phone',
        sort: true,
    },
    {
        dataField: 'email',
        text: 'Email',
        sort: true,
    },
    {
        dataField: 'noOfProjects',
        text: 'No of Projects',
        sort: true,
    },
];

function CoordinatorsTable() {
    return (
        <Card>
            <CardBody>
                <PaginateTable data={records} columns={columns} title="Coordinators Contacts" />
            </CardBody>
        </Card>
    );
}

export default CoordinatorsTable;
