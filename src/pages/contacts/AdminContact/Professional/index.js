import React, { useState } from 'react';
import { Col, Row } from 'reactstrap';
import Users from '../Users';
import UserBox from '../../../profile/ViewProfile/UserBox';
import UserInfo from '../UserInfo';
import ProjectsTable from './ProjectsTable';
import CoordinatorsTable from './CoordinatorsTable';
import Threads from '../Threads';
import UserBoxForm from '../forms/UserBoxForm';
import UserInfoForm from '../forms/UserInfoForm';
import ProfessionalSettings from '../settings/ProfessionalSettings';

function Professional({ onUserChange, isSetting }) {
    const [isEdit, setEdit] = useState(false);
    const [isInfoEdit, setInfoEdit] = useState(false);

    return (
        <>
            {!isSetting ? (
                <>
                    <Row className="d-flex">
                        <Col lg={3} md={12} className="order-lg-1 order-xl-1 w-100">
                            <Users onUserSelect={onUserChange} isSkill />
                        </Col>

                        <Col lg={9} md={12} className="order-lg-1 order-xl-1">
                            {!isEdit ? (
                                <UserBox setEdit={setEdit} />
                            ) : (
                                <UserBoxForm isEdit={isEdit} setEdit={setEdit} />
                            )}
                            {!isInfoEdit ? (
                                <UserInfo setEdit={setInfoEdit} />
                            ) : (
                                <UserInfoForm isEdit={isInfoEdit} setEdit={setInfoEdit} />
                            )}
                            <CoordinatorsTable />
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <ProjectsTable />
                        </Col>
                    </Row>
                    <Threads />
                </>
            ) : (
                <ProfessionalSettings />
            )}
        </>
    );
}

export default Professional;
