import React, { useEffect, useState } from 'react';
import { Button, Col, Row, Input } from 'reactstrap';
import Select from 'react-select';
import { countries, paymentStatus, skills, projectCategories } from '../../dashboards/AdminDashboard/data';

const initialValues = {
    country: '',
    noOfProjectPosted: '',
    noOfThreads: '',
    paymentStatus: '',
    noOfExecutive: '',
    totalBudget: '',
    rating: '',
    category: '',
};

function FilterForm({ setFilter, setFilterOpen, filter, isSkill }) {
    const [values, setValues] = useState({});

    const updateValues = (field, fieldValue) => {
        setValues({
            ...values,
            [field]: fieldValue,
        });
    };

    const handleInputChange = (e, field) => {
        if (e) {
            updateValues(field, e.target.value);
        }
    };

    const handleFilter = () => {
        setFilter(values);
        setFilterOpen(false);
    };

    const handleSubmit = () => {
        handleFilter();
    };

    useEffect(() => {
        if (filter) {
            setValues(filter);
        } else {
            setValues(initialValues);
        }
    }, [filter]);

    return (
        <>
            <Select
                placeholder="Select country..."
                className="react-select"
                classNamePrefix="react-select"
                value={(values && values.country) || ''}
                onChange={(value) => {
                    updateValues('country', value);
                }}
                options={countries}
            />
            <Select
                placeholder="Select category..."
                value={(values && values.category) || ''}
                className="react-select mt-1"
                classNamePrefix="react-select"
                onChange={(value) => {
                    updateValues('category', value);
                }}
                options={projectCategories}
            />
            {isSkill && (
                <Select
                    placeholder="Select skill..."
                    value={(values && values.skill) || ''}
                    className="react-select mt-1"
                    classNamePrefix="react-select"
                    onChange={(value) => {
                        updateValues('skill', value);
                    }}
                    options={skills}
                    isMulti={true}
                />
            )}

            <Input
                className="mt-1 text-left"
                type="number"
                name="totalBudget"
                label=""
                min={0}
                value={(values && values.totalBudget) || ''}
                placeholder="Total budget"
                onChange={(e) => handleInputChange(e, 'totalBudget')}
            />

            <Input
                className="mt-1"
                type="number"
                name="noOfProjectPosted"
                label=""
                placeholder="No of project posted"
                min={0}
                value={values.noOfProjectPosted || ''}
                onChange={(e) => handleInputChange(e, 'noOfProjectPosted')}
            />
            <Select
                placeholder="Select payment status..."
                value={(values && values.paymentStatus) || ''}
                className="react-select mt-1"
                classNamePrefix="react-select"
                onChange={(value) => {
                    updateValues('paymentStatus', value);
                }}
                options={paymentStatus}
            />
            <Input
                className="mt-1"
                type="number"
                name="rating"
                label=""
                min={0}
                value={(values && values.rating) || ''}
                placeholder="Enter rating which given by codi"
                onChange={(e) => handleInputChange(e, 'rating')}
            />
            <Input
                className="mt-1"
                type="number"
                name="noOfExecutive"
                label=""
                min={0}
                value={(values && values.noOfExecutive) || ''}
                placeholder="Number of executives"
                onChange={(e) => handleInputChange(e, 'noOfExecutive')}
            />

            <Row>
                <Col className="text-right mt-1">
                    <Button
                        type="button"
                        color="secondary"
                        className="mr-2 btn-sm"
                        onClick={() => {
                            setValues(initialValues);
                            setFilterOpen(false);
                        }}>
                        Clear
                    </Button>
                    <Button type="submit" color="primary" onClick={(e) => handleSubmit(e)} size="sm">
                        Filter
                    </Button>
                </Col>
            </Row>
        </>
    );
}

export default FilterForm;
