import React from 'react';
import { Row, Col, Card, CardBody, FormGroup, Button } from 'reactstrap';
import profileImg from '../../../../assets/images/users/avatar-1.jpg';
import { AvForm, AvField } from 'availity-reactstrap-validation';

function UserBoxForm(props) {
    const handleValidSubmit = (e, values) => {
        //
    };

    const { isEdit, setEdit } = props;
    // const { companyName, companyAddress, telephoneNumber, fax, serviceContactName, serviceContactPhone } = formData;

    // const defaultValues = {
    //     companyName: companyName,
    //     companyAddress: companyAddress,
    //     telephoneNumber: telephoneNumber,
    //     fax: fax,
    //     serviceContactName: serviceContactName,
    //     serviceContactPhone: serviceContactPhone,
    // };

    return (
        <Card className="">
            <CardBody className="profile-user-box">
                <FormGroup>
                    {isEdit && (
                        <Button className="close" onClick={() => setEdit(false)}>
                            <span aria-hidden="true">&times;</span>
                        </Button>
                    )}
                </FormGroup>

                <Row>
                    <Col lg={9} md={12}>
                        <div className="media">
                            <AvForm onValidSubmit={handleValidSubmit} className="w-100">
                                <Row>
                                    <Col md={3} className="text-center">
                                        <span className="">
                                            <img
                                                src={profileImg}
                                                style={{ width: '150px' }}
                                                alt=""
                                                className="rounded-circle img-thumbnail"
                                            />
                                        </span>
                                    </Col>
                                    <Col>
                                        <div className="media-body">
                                            <Row>
                                                <Col md={9}>
                                                    <AvField name="name" label="" placeholder="Coordinator name" />
                                                </Col>
                                                <Col md={3}>
                                                    <AvField
                                                        name="rating"
                                                        label=""
                                                        placeholder="Rating"
                                                        type="number"
                                                    />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={12}>
                                                    <AvField
                                                        type="textarea"
                                                        name="description"
                                                        label=""
                                                        placeholder="Description"
                                                    />
                                                </Col>
                                            </Row>
                                        </div>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col className="text-right">
                                        <Button type="submit" color="primary">
                                            Update
                                        </Button>
                                    </Col>
                                </Row>
                            </AvForm>
                        </div>
                    </Col>
                </Row>
            </CardBody>
        </Card>
    );
}

export default UserBoxForm;
