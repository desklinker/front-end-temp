import React, { useState } from 'react';
import '../styles/profile.css';
import { Card, CardBody, Col, Row, FormGroup, Button } from 'reactstrap';
import Select from 'react-select';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import { countries, skills } from '../../../dashboards/AdminDashboard/data';

function UserInfoForm({ setEdit }) {
    const [values, setValues] = useState({});

    const updateValues = (field, fieldValue) => {
        setValues({
            ...values,
            [field]: fieldValue,
        });
    };

    const handleValidSubmit = (e, values) => {
        //
    };

    return (
        <Card>
            <CardBody>
                <FormGroup>
                    <Button className="close" onClick={() => setEdit(false)}>
                        <span aria-hidden="true">&times;</span>
                    </Button>
                </FormGroup>
                <AvForm onValidSubmit={handleValidSubmit} className="w-100">
                    <Row>
                        <Col md={4}>
                            <h4 className="header-title mt-0 mb-3">Basic Information</h4>
                            <AvField name="mobile" label="" placeholder="Mobile" />
                            <AvField name="email" label="" placeholder="Email" type="email" />

                            <FormGroup>
                                <Select
                                    placeholder="Select country..."
                                    className="react-select"
                                    classNamePrefix="react-select"
                                    value={(values && values.country) || ''}
                                    onChange={(value) => {
                                        updateValues('country', value);
                                    }}
                                    options={countries}
                                />
                            </FormGroup>
                            <FormGroup>
                                <Select
                                    placeholder="Select skills"
                                    value={(values && values.skill) || ''}
                                    className="react-select mt-1"
                                    classNamePrefix="react-select"
                                    onChange={(value) => {
                                        updateValues('skill', value);
                                    }}
                                    options={skills}
                                    isMulti={true}
                                />
                            </FormGroup>
                        </Col>
                        <Col md={4}>
                            <h4 className="header-title mt-0 mb-3">Educational Qualifications</h4>
                            <AvField name="qualification" label="" placeholder="Qualification" />
                            <AvField name="university" label="" placeholder="University" />

                            <Row className="px-1">
                                <Col md={8}>
                                    <AvField name="yearOfCompletion" label="" placeholder="Year of Completion" />
                                </Col>
                                <Col md={4}>
                                    <AvField name="gpa" label="" placeholder="GPA" />
                                </Col>
                            </Row>
                        </Col>
                        <Col md={4}>
                            <h4 className="header-title mt-0 mb-3">Experience</h4>

                            <AvField name="expQualification" label="" placeholder="Qualification" />
                            <Row className="px-1">
                                <Col>
                                    <AvField name="from" label="" placeholder="From" />
                                </Col>
                                <Col>
                                    <AvField name="to" label="" placeholder="To" />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="border-top">
                        <Col md={4}>
                            <h4 className="header-title mt-2 mb-3">Bank Details</h4>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={4}>
                            <AvField name="bankName" label="" placeholder="Bank Name" />
                        </Col>
                        <Col>
                            <AvField name="branch" label="" placeholder="Branch" />
                        </Col>
                        <Col>
                            <AvField name="accountNumber" label="" placeholder="Account Number" />
                        </Col>
                    </Row>
                    <Row>
                        <Col className="text-right">
                            <Button type="submit" color="primary">
                                Update
                            </Button>
                        </Col>
                    </Row>
                </AvForm>
            </CardBody>
        </Card>
    );
}

export default UserInfoForm;
