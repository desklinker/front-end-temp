const projects = [
    {
        id: 1,
        title: 'Steel Project #2',
        state: 'Planned',
        status: 'Ongoing',
        shortDesc: 'With supporting text below as a natural lead-in to additional contenposuere erat a ante',
        field: 'Civil Engineering',
        duration: '6 Weeks',
        executives: [
            {
                id: 4,
                fullName: 'Mary',
                shortName: 'Mary',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: 'ABC (Pvt)Ltd',
                    address: '3940  Richison Drive',
                    phone: '+1 456 9595 9594',
                },
                username: 'mary',
                password: '123456',
            },
            {
                id: 5,
                fullName: 'Rachel',
                shortName: 'Rachel',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
        ],
        files: [
            {
                name: 'Client-design.zip',
                size: '2.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Dashboard-design.jpg',
                size: '3.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Admin-design.jpg',
                size: '2.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Admin-bug-report.mp4',
                size: '7.05 MB',
                postedOn: '2020-01-03, 15:03',
            },
        ],
    },
    {
        id: 2,
        title: 'Timber Project #2',
        state: 'Planned',
        status: 'Ongoing',
        shortDesc:
            'This card has supporting text below as a natural lead-in to additional content is a little bit longer',
        field: 'Civil Engineering',
        duration: '6 Weeks',
        executives: [
            {
                id: 1,
                fullName: 'Kimberly',
                shortName: 'Kimberly',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
            {
                id: 2,
                fullName: 'Madeleine',
                shortName: 'Madeleine',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
            {
                id: 3,
                fullName: 'Rebecca',
                shortName: 'Rebecca',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
            {
                id: 5,
                fullName: 'Rachel',
                shortName: 'Rachel',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
        ],
        files: [
            {
                name: 'Client-design.zip',
                size: '2.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Dashboard-design.jpg',
                size: '3.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Admin-design.jpg',
                size: '2.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Admin-bug-report.mp4',
                size: '7.05 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Codi-design.jpg',
                size: '6.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Dashboard-bug-report.mp4',
                size: '7.05 MB',
                postedOn: '2020-01-03, 15:03',
            },
        ],
    },
    {
        id: 3,
        title: 'Concrete Project #2',
        state: 'Planned',
        status: 'Past',
        shortDesc: 'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid',
        progress: '40',
        field: 'Civil Engineering',
        duration: '6 Weeks',
        executives: [
            {
                id: 3,
                fullName: 'Rebecca',
                shortName: 'Rebecca',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
            {
                id: 5,
                fullName: 'Rachel',
                shortName: 'Rachel',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
                username: 'rachel',
                password: '123456',
                isInformed: false,
            },
        ],
        files: [
            {
                name: 'Client-design.zip',
                size: '2.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Dashboard-design.jpg',
                size: '3.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Admin-design.jpg',
                size: '2.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Admin-bug-report.mp4',
                size: '7.05 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Codi-design.jpg',
                size: '6.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
        ],
    },
    {
        id: 4,
        title: 'Steel Project #1',
        state: 'Planned',
        status: 'Ongoing',
        shortDesc: "Some quick example text to build on the card title and make up the bulk of the card's content",
        field: 'Civil Engineering',
        duration: '6 Weeks',
        executives: [
            {
                id: 1,
                fullName: 'Kimberly',
                shortName: 'Kimberly',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
            {
                id: 2,
                fullName: 'Madeleine',
                shortName: 'Madeleine',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
        ],
        files: [
            {
                name: 'Dashboard-design.jpg',
                size: '3.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Admin-design.jpg',
                size: '2.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Admin-bug-report.mp4',
                size: '7.05 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Codi-design.jpg',
                size: '6.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Dashboard-bug-report.mp4',
                size: '7.05 MB',
                postedOn: '2020-01-03, 15:03',
            },
        ],
    },
    {
        id: 5,
        title: 'Concrete Project #1',
        state: 'Planned',
        status: 'Past',
        shortDesc: "Some quick example text to build on the card title and make up the bulk of the card's content",
        field: 'Civil Engineering',
        duration: '6 Weeks',
        executives: [
            {
                id: 1,
                fullName: 'Kimberly',
                shortName: 'Kimberly',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
            {
                id: 2,
                fullName: 'Madeleine',
                shortName: 'Madeleine',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
            {
                id: 3,
                fullName: 'Rebecca',
                shortName: 'Rebecca',
                email: 'support@coderthemes.com',
                phone: '+1 456 9595 9594',
                company: {
                    name: '',
                    address: '',
                    phone: '',
                },
            },
        ],
        files: [
            {
                name: 'Client-design.zip',
                size: '2.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Dashboard-design.jpg',
                size: '3.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Admin-design.jpg',
                size: '2.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Admin-bug-report.mp4',
                size: '7.05 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Codi-design.jpg',
                size: '6.5 MB',
                postedOn: '2020-01-03, 15:03',
            },
            {
                name: 'Dashboard-bug-report.mp4',
                size: '7.05 MB',
                postedOn: '2020-01-03, 15:03',
            },
        ],
    },
];

export { projects };
