import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Card, CardBody, Button, Media } from 'reactstrap';
import classnames from 'classnames';
import SimpleBar from 'simplebar-react';
import Select from 'react-select';
import RemoveModal from './RemoveModal';

function ExecutiveList(props) {
    const {
        executives,
        onExecutiveSelect,
        setFormShow,
        setShowProfileForm,
        isFormShow,
        removeExecutive,
        isEdit,
        isProfileEdit,
    } = props;

    const [selectedExecutive, setSelectedExecutive] = useState(executives[0]);
    const [pickedExecutive, setPickedExecutive] = useState(null);
    const [isOpen, setOpen] = useState(false);
    const [options, setOptions] = useState([
        { value: '0', label: 'Add new executive...' },
        { value: '7', label: 'Mario Speedwagon' },
        { value: '8', label: 'Anna Mull' },
        { value: '9', label: 'Bob Frapples' },
    ]);

    useEffect(() => {
        setSelectedExecutive(executives && executives[0]);
    }, [executives]);

    const activateUser = (executive) => {
        setSelectedExecutive(executive);
        if (onExecutiveSelect) {
            onExecutiveSelect(executive);
        }
    };

    const handleAddExecutive = () => {
        if (pickedExecutive && !pickedExecutive.value === 0) {
            setOptions(
                options.filter((option) => {
                    return option.value !== pickedExecutive.value;
                })
            );
        } else {
            setFormShow(true);
            setShowProfileForm(true);
        }
    };

    return (
        <React.Fragment>
            <Card>
                <CardBody className="p-3">
                    <div className="d-flex">
                        <Select
                            className="react-select w-100 mr-1"
                            onChange={(value) => setPickedExecutive(value)}
                            options={options}
                            defaultValue={options[0]}
                        />

                        <Button
                            color="secondary"
                            className="btn-icon"
                            size="sm"
                            onClick={handleAddExecutive}
                            disabled={isEdit && isProfileEdit}>
                            <span className="font-16">
                                <i className="mdi mdi-plus"></i>
                            </span>
                        </Button>
                    </div>
                    <SimpleBar style={{ maxHeight: '550px', width: '100%' }}>
                        {executives &&
                            selectedExecutive &&
                            executives.map((executive, index) => {
                                return (
                                    <Link
                                        to="#"
                                        key={index}
                                        className="text-body"
                                        onClick={(e) => {
                                            activateUser(executive);
                                            setFormShow(false);
                                            setShowProfileForm(false);
                                        }}>
                                        <Media
                                            className={classnames('mt-1', 'p-2', {
                                                'bg-light': executive.id === selectedExecutive.id && !isFormShow,
                                            })}>
                                            <Media body>
                                                <h5 className="mt-0 mb-0 font-14">
                                                    <Button
                                                        className="close float-right"
                                                        color="light"
                                                        onClick={() => setOpen(true)}>
                                                        <span className="float-right font-12">
                                                            <i className="mdi mdi-close"></i>
                                                        </span>
                                                    </Button>
                                                    {executive.fullName}
                                                </h5>
                                                <p className="mt-1 mb-0 text-muted font-14">
                                                    <span className="w-75">{executive.phone}</span>
                                                </p>
                                            </Media>
                                        </Media>
                                    </Link>
                                );
                            })}
                    </SimpleBar>

                    <RemoveModal
                        isOpen={isOpen}
                        remove={removeExecutive}
                        setOpen={setOpen}
                        selected={selectedExecutive && selectedExecutive.id}
                    />
                </CardBody>
            </Card>
        </React.Fragment>
    );
}

export default ExecutiveList;
