import React, { useEffect, useState } from 'react';
import { Row, Col } from 'reactstrap';

import ProjectList from './ProjectList';

import { projects as items } from './data';
import ExecutiveDetails from './ExecutiveDetails';
import ExecutiveProfile from './ExecutiveProfile';
import CreateExecutiveProfile from './CreateExecutiveProfile';
import ExecutiveList from './ExecutiveList';

import '../styles/contacts.css';
import CreateExecutive from './CreateExecutive';

function ClientContact() {
    const [projects, setProjects] = useState(items);
    const [selectedProject, setSelectedProject] = useState([]);
    const [executives, setExecutives] = useState([]);
    const [selectedExecutive, setSelectedExecutive] = useState(null);
    const [isFormShow, setFormShow] = useState(false);
    const [isShowProfileForm, setShowProfileForm] = useState(false);
    const [isEdit, setEdit] = useState(false);
    const [isProfileEdit, setProfileEdit] = useState(false);

    useEffect(() => {
        setSelectedProject(projects[0]);
        setExecutives(projects[0].executives);
        setSelectedExecutive(projects[0].executives && projects[0].executives[0]);
    }, [projects]);

    const onProjectSelect = (project) => {
        setSelectedProject(project);
        setExecutives(project.executives);
        setSelectedExecutive(project.executives && project.executives[0]);
    };

    const onExecutiveSelect = (executive) => {
        setSelectedExecutive(executive);
    };

    const removeExecutive = (executiveId) => {
        projects.forEach((project) => {
            if (project.id === selectedProject.id) {
                project.executives = project.executives.filter((exe) => exe.id !== executiveId);
            }
        });

        const index = projects.findIndex((project) => project.id === selectedProject.id);
        setProjects(projects);
        setSelectedProject(projects[index]);
        setExecutives(projects[index].executives);
        setSelectedExecutive(null);
    };

    return (
        <Row>
            <Col lg={3} md={12} className="order-lg-1 order-xl-1">
                <ProjectList onProjectSelect={onProjectSelect} />
            </Col>

            <Col lg={3} md={12} className="order-lg-1 order-xl-1">
                <ExecutiveList
                    executives={executives}
                    isEdit={isEdit}
                    isFormShow={isFormShow}
                    isProfileEdit={isProfileEdit}
                    isShowProfileForm={isShowProfileForm}
                    onExecutiveSelect={onExecutiveSelect}
                    removeExecutive={removeExecutive}
                    setFormShow={setFormShow}
                    setShowProfileForm={setShowProfileForm}
                    setEdit={setEdit}
                    setProfileEdit={setProfileEdit}
                />
            </Col>

            <Col lg={6} md={12} className="order-lg-1 order-xl-1">
                {(!isFormShow && !isShowProfileForm) || (!isFormShow && isShowProfileForm) ? (
                    <ExecutiveDetails info={selectedExecutive} setEdit={setEdit} setFormShow={setFormShow} />
                ) : (isFormShow && !isShowProfileForm) || (isFormShow && isShowProfileForm) ? (
                    <CreateExecutive
                        formData={selectedExecutive}
                        isEdit={isEdit}
                        setEdit={setEdit}
                        setFormShow={setFormShow}
                    />
                ) : null}

                {(!isFormShow && !isShowProfileForm) || (isFormShow && !isShowProfileForm) ? (
                    <ExecutiveProfile
                        info={selectedExecutive}
                        setShowProfileForm={setShowProfileForm}
                        setProfileEdit={setProfileEdit}
                    />
                ) : (!isFormShow && isShowProfileForm) || (isFormShow && isShowProfileForm) ? (
                    <CreateExecutiveProfile
                        formData={selectedExecutive}
                        isProfileEdit={isProfileEdit}
                        setProfileEdit={setProfileEdit}
                        setShowProfileForm={setShowProfileForm}
                    />
                ) : null}
            </Col>
        </Row>
    );
}

export default ClientContact;
