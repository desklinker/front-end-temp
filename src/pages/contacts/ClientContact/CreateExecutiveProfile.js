import React from 'react';
import { Row, Col, Card, CardBody, FormGroup, Button, CustomInput } from 'reactstrap';
import { AvForm, AvField } from 'availity-reactstrap-validation';

function CreateExecutiveProfile(props) {
    const { isProfileEdit, formData, setShowProfileForm, setProfileEdit } = props;

    const handleValidSubmit = (e, values) => {
        console.log({ ...values, ...this.state });
    };

    const defaultValues = {
        username: formData.username,
        password: formData.password,
        isInformed: true,
    };

    return (
        <React.Fragment>
            <Card>
                <CardBody>
                    {isProfileEdit && (
                        <FormGroup>
                            <button
                                type="button"
                                className="close"
                                aria-label="Close"
                                onClick={() => {
                                    setProfileEdit(false);
                                    setShowProfileForm(false);
                                }}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </FormGroup>
                    )}

                    <h4 className="header-title mt-0">Create Executive Profile</h4>
                    <hr />
                    <AvForm onValidSubmit={handleValidSubmit} model={isProfileEdit ? defaultValues : {}}>
                        <AvField
                            name="username"
                            label="Username"
                            required
                            placeholder="Enter username"
                            disabled={isProfileEdit}
                        />
                        <AvField
                            name="password"
                            label="Password"
                            required
                            type="password"
                            placeholder="Enter password"
                        />
                        <Row>
                            <Col md={12}>
                                <CustomInput
                                    id="isInformed"
                                    label="Inform executive username and password through whatsapp"
                                    type="checkbox"
                                    className="text-secondary"
                                />
                            </Col>
                        </Row>

                        <Row className="mt-2">
                            <Col className="text-right">
                                <Button type="submit" color={isProfileEdit ? 'primary' : 'success'} className="mr-1">
                                    {!isProfileEdit ? 'Submit' : 'Update'}
                                </Button>
                            </Col>
                        </Row>
                    </AvForm>
                </CardBody>
            </Card>
        </React.Fragment>
    );
}

export default CreateExecutiveProfile;
