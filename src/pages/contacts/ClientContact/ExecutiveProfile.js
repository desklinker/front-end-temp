import React from 'react';
import { Button, Card, CardBody } from 'reactstrap';

function ExecutiveProfile(props) {
    const { info, setShowProfileForm, setProfileEdit } = props;

    return (
        <React.Fragment>
            {info && (
                <Card>
                    <CardBody>
                        <Button
                            className="close"
                            aria-label="Close"
                            onClick={() => {
                                setShowProfileForm(true);
                                setProfileEdit(true);
                            }}>
                            <i className="mdi mdi-square-edit-outline"></i>
                        </Button>
                        <h4 className="header-title mt-0">Executive Profile Creation</h4>
                        <hr />

                        <div className="text-left">
                            <p className="text-muted">
                                <strong>Username :</strong> <span className="ml-2">{info.username}</span>
                            </p>

                            <p className="text-muted">
                                <strong>Password :</strong> <span className="ml-2">{info.password}</span>
                            </p>
                        </div>
                    </CardBody>
                </Card>
            )}
        </React.Fragment>
    );
}

export default ExecutiveProfile;
