import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Card, CardBody, Media, ButtonGroup, Button } from 'reactstrap';
import classnames from 'classnames';
import SimpleBar from 'simplebar-react';

import { projects } from './data';

// ChatProjects
class ProjectList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            projects: [...projects],
            selectedProject: projects[0],
            selectedGroup: 'All',
            isOnGoing: true,
            isPast: false,
        };

        this.filterProjects = this.filterProjects.bind(this);
        this.search = this.search.bind(this);
        this.activateProject = this.activateProject.bind(this);
    }

    /**
     * Filter projects
     */
    filterProjects = (group) => {
        this.selectedGroup = group;
        this.setState({
            selectedGroup: group,
            projects:
                group !== 'All'
                    ? [...projects].filter((u) => u.groups.toLowerCase().indexOf(group.toLowerCase()) >= 0)
                    : [...projects],
        });
    };

    /**
     * Search the project
     * @param {*} text
     */
    search(text) {
        this.setState({
            projects: text
                ? [...projects].filter((u) => u.name.toLowerCase().indexOf(text.toLowerCase()) >= 0)
                : [...projects],
        });
    }

    /**
     * Activates the project
     * @param {*} project
     */
    activateProject(project) {
        this.setState({ selectedProject: project });
        if (this.props.onProjectSelect) {
            this.props.onProjectSelect(project);
        }
    }

    handleThreadFilterBtnClick = (e) => {
        if (e.target.value === 'onGoing') {
            this.setState({
                isOnGoing: true,
                isPast: false,
            });
        } else if (e.target.value === 'past') {
            this.setState({
                isOnGoing: false,
                isPast: true,
            });
        }
    };

    render() {
        return (
            <React.Fragment>
                <Card>
                    <CardBody className="p-0">
                        <div className="tab-content">
                            <div className="tab-pane show active p-3">
                                <div className="app-search">
                                    <div className="form-group position-relative">
                                        <input
                                            type="text"
                                            className="form-control"
                                            placeholder="Search projects"
                                            onKeyUp={(e) => this.search(e.target.value)}
                                        />
                                        <span className="mdi mdi-magnify"></span>
                                    </div>
                                    <ButtonGroup className="threadFilterButton">
                                        <Button
                                            color="light"
                                            onClick={this.handleThreadFilterBtnClick}
                                            className={this.state.isOnGoing ? 'activeBtn' : ''}
                                            value="onGoing">
                                            On Going
                                        </Button>
                                        <Button
                                            color="light"
                                            onClick={this.handleThreadFilterBtnClick}
                                            className={this.state.isPast ? 'activeBtn' : ''}
                                            value="past">
                                            Past
                                        </Button>
                                    </ButtonGroup>
                                </div>

                                <SimpleBar style={{ maxHeight: '550px', width: '100%' }}>
                                    {this.state.projects.map((project, index) => {
                                        return (
                                            <Link
                                                to="#"
                                                key={index}
                                                className="text-body"
                                                onClick={(e) => {
                                                    this.activateProject(project);
                                                }}>
                                                <Media
                                                    className={classnames('mt-1', 'p-2', {
                                                        'bg-light': project.id === this.state.selectedProject.id,
                                                    })}>
                                                    <Media body>
                                                        <h5 className="mt-0 mb-0 font-14">
                                                            <span className="float-right text-muted font-12">
                                                                {project.duration}
                                                            </span>
                                                            {project.title}
                                                        </h5>
                                                        <p className="mt-1 mb-0 text-muted font-14">
                                                            <span className="w-75">
                                                                {`${
                                                                    project.shortDesc &&
                                                                    project.shortDesc.substring(0, 45)
                                                                }...`}
                                                            </span>
                                                        </p>
                                                    </Media>
                                                </Media>
                                            </Link>
                                        );
                                    })}
                                </SimpleBar>
                            </div>
                        </div>
                    </CardBody>
                </Card>
            </React.Fragment>
        );
    }
}

export default ProjectList;
