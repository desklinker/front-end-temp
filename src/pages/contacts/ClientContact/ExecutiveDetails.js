import React from 'react';
import { Card, CardBody, Button } from 'reactstrap';

function ExecutiveDetails(props) {
    const { info, setEdit, setFormShow } = props;

    return (
        <React.Fragment>
            <Card>
                <CardBody>
                    <Button
                        className="close"
                        aria-label="Close"
                        onClick={() => {
                            setFormShow(true);
                            setEdit(true);
                        }}>
                        <i className="mdi mdi-square-edit-outline"></i>
                    </Button>
                    <h4 className="header-title mt-0">Executive Details</h4>
                    <hr />

                    <div className="text-left">
                        <p className="text-muted">
                            <strong>Full Name :</strong> <span className="ml-2">{info && info.fullName}</span>
                        </p>

                        <p className="text-muted">
                            <strong>Short Name :</strong> <span className="ml-2">{info && info.shortName}</span>
                        </p>

                        <p className="text-muted">
                            <strong>WhatsApp Number :</strong>
                            <span className="ml-2">{info && info.phone}</span>
                        </p>

                        <p className="text-muted">
                            <strong>Email :</strong> <span className="ml-2">{info && info.email}</span>
                        </p>

                        <p className="text-muted">
                            <strong>Company Details :</strong>
                        </p>

                        <p className="text-muted ml-3">
                            <strong>Name :</strong>
                            <span className="ml-2"> {info && info.company && info.company.name} </span>
                        </p>

                        <p className="text-muted ml-3">
                            <strong>Address :</strong>
                            <span className="ml-2"> {info && info.company && info.company.address} </span>
                        </p>

                        <p className="text-muted ml-3">
                            <strong>Telephone Number :</strong>
                            <span className="ml-2"> {info && info.company && info.company.phone} </span>
                        </p>
                    </div>
                </CardBody>
            </Card>
        </React.Fragment>
    );
}

export default ExecutiveDetails;
