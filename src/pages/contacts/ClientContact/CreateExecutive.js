import React from 'react';
import { Row, Col, Card, CardBody, FormGroup, Button } from 'reactstrap';
import { AvForm, AvField } from 'availity-reactstrap-validation';

function CreateExecutive(props) {
    const handleValidSubmit = (e, values) => {
        //
    };

    const { formData, isEdit, setFormShow, setEdit } = props;
    const { fullName, shortName, phone, email, company } = formData;

    const defaultValues = {
        fullName: fullName,
        shortName: shortName,
        phone: phone,
        email: email,
        companyName: company.name,
        companyPhone: company.phone,
        companyAddress: company.address,
    };

    return (
        <React.Fragment>
            <Card>
                <CardBody>
                    <FormGroup>
                        {isEdit && (
                            <Button
                                className="close"
                                onClick={() => {
                                    setFormShow(false);
                                    setEdit(false);
                                }}>
                                <span aria-hidden="true">&times;</span>
                            </Button>
                        )}
                    </FormGroup>
                    <h4 className="header-title mt-0">Create New Executive</h4>
                    <hr />
                    <AvForm onValidSubmit={handleValidSubmit} model={isEdit ? defaultValues : {}}>
                        <AvField
                            name="fullName"
                            label="Full Name"
                            required
                            placeholder="Enter full name"
                            disabled={isEdit}
                        />
                        <AvField
                            name="shortName"
                            label="Short Name"
                            required
                            placeholder="Enter short name"
                            disabled={isEdit}
                        />
                        <Row>
                            <Col md={6}>
                                <AvField
                                    name="phone"
                                    label="WhatsApp Number"
                                    required
                                    placeholder="Enter whatsApp number"
                                />
                            </Col>

                            <Col md={6}>
                                <AvField name="email" label="Email" required placeholder="Enter email" />
                            </Col>
                        </Row>

                        <Row>
                            <Col md={12}>
                                <h4 className="mt-0">Company Details: </h4>
                                <hr />
                            </Col>
                        </Row>
                        <Row>
                            <Col md={6}>
                                <AvField
                                    name="companyName"
                                    label="Company Name"
                                    required
                                    placeholder="Enter company name"
                                />
                            </Col>

                            <Col md={6}>
                                <AvField
                                    name="companyPhone"
                                    label="Telephone Number"
                                    required
                                    placeholder="Enter telephone number"
                                />
                            </Col>
                        </Row>

                        <AvField name="companyAddress" label="Address" required placeholder="Enter company address" />

                        <Row className="mt-2">
                            <Col className="text-right">
                                <Button type="submit" color={isEdit ? 'primary' : 'success'} className="mr-1">
                                    {!isEdit ? 'Submit' : 'Update'}
                                </Button>
                            </Col>
                        </Row>
                    </AvForm>
                </CardBody>
            </Card>
        </React.Fragment>
    );
}

export default CreateExecutive;
