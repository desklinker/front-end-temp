import React from 'react';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';

function RemoveModal({ isOpen, setOpen, remove, selected }) {
    return (
        <Modal isOpen={isOpen} size="sm" centered>
            <ModalHeader>Confirm Delete</ModalHeader>
            <ModalBody>
                <p>Are you sure want to delete this contact?</p>
            </ModalBody>
            <ModalFooter>
                <Button size="sm" color="secondary" onClick={() => setOpen(false)}>
                    Cancel
                </Button>
                <Button
                    size="sm"
                    color="danger"
                    onClick={() => {
                        remove(selected);
                        setOpen(false);
                    }}>
                    Delete
                </Button>
            </ModalFooter>
        </Modal>
    );
}

export default RemoveModal;
