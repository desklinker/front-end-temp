// @flow
import React, { Component, Suspense } from 'react';
import { Container } from 'reactstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { motion } from 'framer-motion';

import { changeLayout } from '../redux/actions';
import * as layoutConstants from '../constants/layout';
// import ThemeCustomizer from '../components/ThemeCustomizer';
import Spinner from '../components/Spinner';
import OurWork from '../components/OurWork';
import { CLIENT, EXECUTIVE } from '../constants';

// code splitting and lazy loading
// https://blog.logrocket.com/lazy-loading-components-in-react-16-6-6cea535c0b52
const Topbar = React.lazy(() => import('../components/Topbar'));
const Navbar = React.lazy(() => import('../components/Navbar'));
const Footer = React.lazy(() => import('../components/Footer'));
// const RightSidebar = React.lazy(() => import('../components/RightSidebar'));

const loading = () => <div className="text-center"></div>;
const containerLoading = () => (
  <div className="text-center">
    <Spinner className="text-primary m-2" size="sm" />
  </div>
);

const variants = {
  active: {
    y: 0,
    transition: { duration: 0.5, ease: [0.42, 0, 0.58, 1] },
  },
  hidden: { y: '-100%', transition: { duration: 0.5, ease: [0.42, 0, 0.58, 1] } },
};

type HorizontalLayoutProps = {
  changeLayout: PropTypes.func,
  children: PropTypes.object,
  layout: PropTypes.object,
};

type HorizontalLayoutState = {
  isMenuOpened: boolean,
};

class HorizontalLayout extends Component<HorizontalLayoutProps, HorizontalLayoutState> {
  constructor(props) {
    super(props);

    this.openMenu = this.openMenu.bind(this);

    this.state = {
      isMenuOpened: false,
      show: true,
      scrollPos: 0,
    };
    this.handleScroll = this.handleScroll.bind(this);
  }

  componentDidMount = () => {
    window.addEventListener('scroll', this.handleScroll);
    this.props.changeLayout(layoutConstants.LAYOUT_HORIZONTAL);
  };

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll() {
    const { scrollPos } = this.state;
    if (document.body.getBoundingClientRect().top < -80) {
      this.setState({
        scrollPos: document.body.getBoundingClientRect().top,
        show: document.body.getBoundingClientRect().top > scrollPos,
      });
    } else {
      this.setState({
        show: true,
        scroll: 0,
      });
    }
  }

  /**
   * Opens the menu - mobile
   */
  openMenu = (e) => {
    e.preventDefault();
    this.setState({ isMenuOpened: !this.state.isMenuOpened });
  };

  render() {
    // get the child view which we would like to render
    const { children, user } = this.props;
    const role = user.role || null;
    return (
      <div className="app">
        <div className="wrapper">
          <div className="content-page">
            <div className="content">
              <motion.div
                id="navbar"
                animate={this.state.show ? 'active' : 'hidden'}
                variants={variants}
                transition={{ duration: 1 }}>
                <Suspense fallback={loading()}>
                  <Topbar
                    isMenuOpened={this.state.isMenuOpened}
                    openLeftMenuCallBack={this.openMenu}
                    navCssClasses="topnav-navbar"
                    {...this.props}
                  />
                </Suspense>

                <Suspense fallback={loading()}>
                  <Navbar isMenuOpened={this.state.isMenuOpened} {...this.props} />
                </Suspense>
              </motion.div>
              <Container fluid className="p-topbar">
                <Suspense fallback={containerLoading()}>{children}</Suspense>
              </Container>

              {(role === CLIENT || role === EXECUTIVE) && <OurWork {...this.props} />}
            </div>

            <Suspense fallback={loading()}>
              <Footer {...this.props} />
            </Suspense>

            {/* <Suspense fallback={loading()}>
                            <RightSidebar {...this.props}>
                                <ThemeCustomizer />
                            </RightSidebar>
                        </Suspense> */}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    layout: state.Layout,
    user: state.Auth.user,
  };
};
export default connect(mapStateToProps, { changeLayout })(HorizontalLayout);
