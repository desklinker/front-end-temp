// @flow
import { all, fork, put, takeEvery, call} from 'redux-saga/effects';
import { UPLOAD_FILE } from './constants';

import { uploadFileSuccess, uploadFileFailed } from './actions';

const delay = time => new Promise(resolve => setTimeout(resolve, time));


function* upload({ payload: { status, percent, formData } }) {
    try {
        yield call(delay, 2000);
        yield put(uploadFileSuccess(100));
      

    } catch (error) {
        let message;
        switch (error.status) {
            case 500:
                message = 'Internal Server Error';
                break;
            case 401:
                message = 'Invalid credentials';
                break;
            default:
                message = error;
        }
        yield put(uploadFileFailed(message));
    }
}

export function* watchUploadFile(): any {
    yield takeEvery(UPLOAD_FILE, upload);
}

function* progressSaga(): any {
    yield all([fork(watchUploadFile)]);
}

export default progressSaga;
