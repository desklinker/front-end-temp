import { UPLOAD_FILE, UPLOAD_FILE_SUCCESS, UPLOAD_FILE_FAILED } from './constants';

type ProgressAction = { type: string, payload: {} | string };

export const uploadFile = (percent: number, formData: {}): ProgressAction => ({
    type: UPLOAD_FILE,
    payload: { percent, formData },
});

export const uploadFileSuccess = (percent: number, data: string): ProgressAction => ({
    type: UPLOAD_FILE_SUCCESS,
    payload: {percent, data},
});

export const uploadFileFailed = (error: string): ProgressAction => ({
    type: UPLOAD_FILE_FAILED,
    payload: error,
});
