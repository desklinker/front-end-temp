// @flow
import { UPLOAD_FILE, UPLOAD_FILE_SUCCESS, UPLOAD_FILE_FAILED } from './constants';

const INIT_STATE = {
    percent: 0,
    loading: false,
};

type ProgressAction = { type: string, payload: {} | string };
type State = { percent?: number, status?: string, loading?: boolean };

const Progress = (state: State = INIT_STATE, action: ProgressAction) => {
    const { payload, type } = action;
    switch (type) {
        case UPLOAD_FILE:
            return {
                ...state,
                percent: payload.percent,
                formData: payload.formData,
                loading: true,
            };
        case UPLOAD_FILE_SUCCESS:
            return { ...state, percent: payload.percent, data: payload.data, loading: false, error: null };
        case UPLOAD_FILE_FAILED:
            return { ...state, error: payload, loading: false };
        default:
            return { ...state };
    }
};

export default Progress;
