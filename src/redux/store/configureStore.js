import { createBrowserHistory } from 'history';
import createSagaMiddleware, { END } from 'redux-saga';
import { routerMiddleware } from 'connected-react-router';
import { persistReducer, persistStore } from 'redux-persist';
import { applyMiddleware, compose, createStore } from 'redux';

import sagas from '../sagas';
import axios from '../../lib/axios';
import { rootPersistConfig } from '../../config/persist';
import createAxiosMiddleware from '../../middleware/axios';
import createRootReducer from '../reducers';

export const history = createBrowserHistory();

/**
 * Create store instance
 *
 * @return {Promise<Object>}
 */
async function configureStore() {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [sagaMiddleware, createAxiosMiddleware(axios), routerMiddleware(history)];
  const store = createStore(createRootReducer(history), {}, composeEnhancers(applyMiddleware(...middlewares)));

  const persist = await persistStore(store);

  store.runSaga = sagaMiddleware.run(sagas);
  store.close = () => store.dispatch(END);

  // Hot reload reducers (requires Webpack or Browserify HMR to be enabled)
  if (module.hot) {
    module.hot.accept('../reducers', () => {
      // This fetch the new state of the above reducers.
      const nextRootReducer = import('../reducers');
      store.replaceReducer(persistReducer(rootPersistConfig, nextRootReducer));
    });
  }

  return { store, persist };
}

export default configureStore;
