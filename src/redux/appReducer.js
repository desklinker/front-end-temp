import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import { connectRouter } from 'connected-react-router';

import Auth from './auth/reducers';
import AppMenu from './appMenu/reducers';
import Progress from './progress/reducers';
import Layout from './layout/reducers';
import { authPersistConfig } from '../config/persist';

export default (history) =>
  combineReducers({
    router: connectRouter(history),
    Auth: persistReducer(authPersistConfig, Auth),
    AppMenu,
    Layout,
    Progress,
  });
