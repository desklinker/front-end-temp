import axios from 'axios';
import { userMgtUrl } from '../../config/env';
import { getAuthUser } from '../../helpers/authUtils';

export const fetchUser = async () => {
  const authUser = await getAuthUser();
  // `axios` function returns promise, you can use any ajax lib, which can
  // return promise, or wrap in promise ajax call
  return axios.get(`${userMgtUrl}/users/username/${authUser.username}/basic-details`, {
    headers: {
        DL_AUTH: `${authUser.idToken}`,
      }
  });
};
