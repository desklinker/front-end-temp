// @flow
import {
  LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILED,
  LOGOUT_USER,
  REGISTER_USER,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_FAILED,
  FORGET_PASSWORD,
  FORGET_PASSWORD_SUCCESS,
  FORGET_PASSWORD_FAILED,
} from './constants';

type AuthAction = { type: string, payload: {} | string };

export const loginUser = (username: string, password: string, history: any): AuthAction => ({
  type: LOGIN_USER,
  payload: { username, password, history },
});

export const loginUserSuccess = (user: string): AuthAction => ({
  type: LOGIN_USER_SUCCESS,
  payload: user,
});

export const loginUserFailed = (error: string): AuthAction => ({
  type: LOGIN_USER_FAILED,
  payload: error,
});

export const registerUser = (
  username: string,
  password: string,
  role: string,
  attributeList: {},
  userPool: any
): AuthAction => ({
  type: REGISTER_USER,
  payload: { username, password, role, attributeList, userPool },
});

export const registerUserSuccess = (user: {}): AuthAction => ({
  type: REGISTER_USER_SUCCESS,
  payload: user,
});

export const registerUserFailed = (error: string): AuthAction => ({
  type: REGISTER_USER_FAILED,
  payload: error,
});

export const logoutUser = (history: any): AuthAction => ({
  type: LOGOUT_USER,
  payload: { history },
});

export const forgetPassword = (username: string): AuthAction => ({
  type: FORGET_PASSWORD,
  payload: { username },
});

export const forgetPasswordSuccess = (passwordResetStatus: string): AuthAction => ({
  type: FORGET_PASSWORD_SUCCESS,
  payload: passwordResetStatus,
});

export const forgetPasswordFailed = (error: string): AuthAction => ({
  type: FORGET_PASSWORD_FAILED,
  payload: error,
});
