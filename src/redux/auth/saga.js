/* eslint-disable require-yield */
// @flow
import axios from 'axios';
import { channel } from 'redux-saga';
import { all, call, fork, put, takeEvery, take } from 'redux-saga/effects';

import {
  LOGIN_USER,
  USER_PHONE_VERIFY,
  LOGOUT_USER,
  REGISTER_USER,
  FORGET_PASSWORD,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAILED,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_FAILED,
} from './constants';
import { LOGIN, PHONE_VERIFY } from '../../constants/routes';
import { notificationMgtUrl } from '../../config/env';

import {
  loginUserSuccess,
  loginUserFailed,
  registerUserSuccess,
  registerUserFailed,
  forgetPasswordFailed,
} from './actions';
import { fetchUser } from './api';
import { getCognitoAuthConfig, formattedResponse, formattedResponseUser } from '../../helpers/authUtils';
import get from 'lodash/get';

const loginChannel = channel();
const registerChannel = channel();

/**
 * Login the user
 * @param {*} payload - username and password
 */
function* login({ payload: { username, password, history } }) {
  const { cognitoUser, authenticationDetails } = getCognitoAuthConfig({ username, password });

  cognitoUser.authenticateUser(authenticationDetails, {
    onSuccess: function (result) {
      const response = formattedResponse(result);

      if (response.phoneNumberVerified) {
        loginChannel.put({
          type: LOGIN_USER_SUCCESS,
          payload: response,
        });     
      } else {
        loginChannel.put({
          type: USER_PHONE_VERIFY,
          payload: response,
        });

        history.push(PHONE_VERIFY.replace(':username', response.username));
      }
    },

    onFailure: function (err) {
      loginChannel.put({
        type: LOGIN_USER_FAILED,
        payload: err.message,
      });
    },
  });
}

/**
 * Logout the user
 * @param {*} payload
 */
function* logout({ payload: { history } }) {
  try {
    yield call(() => {
      history.push(LOGIN);
    });
  } catch (error) {}
}

/**
 * Register the user
 */
function* register({ payload: { username, password, role, attributeList, userPool } }) {
  userPool.signUp(username, password, attributeList, null, function (err, result) {
    if (!err) {
      registerChannel.put({
        type: REGISTER_USER_SUCCESS,
        payload: {
          username: username,
          email: get(attributeList[0], 'Value'),
          phoneNumber: get(attributeList[1], 'Value'),
          role: role,
        },
      });
    } else {
      registerChannel.put({
        type: REGISTER_USER_FAILED,
        payload: err.message,
      });
    }
  });
}

/**
 * forget password
 */
function* forgetPassword({ payload: { username } }) {
  try {
    // const response = yield call(fetchJSON, '/users/password-reset', options);
    // yield put(forgetPasswordSuccess(response.message));
  } catch (error) {
    let message;
    switch (error.status) {
      case 500:
        message = 'Internal Server Error';
        break;
      case 401:
        message = 'Invalid credentials';
        break;
      default:
        message = error;
    }
    yield put(forgetPasswordFailed(message));
  }
}

export function* watchLoginChannel() {
  while (true) {
    const action = yield take(loginChannel);
    if (action.type === LOGIN_USER_SUCCESS) {
      try {
        //fetch user detail
        const response = yield call(fetchUser);
        const payload = formattedResponseUser(response, action);
        yield put(loginUserSuccess(payload));
      } catch (error) {
        window.localStorage.clear();
        yield put(loginUserFailed(error.message));
      }
    } else if (action.type === USER_PHONE_VERIFY) {
      yield call(axios.post, `${notificationMgtUrl}/v1/verifications/phone-number/send/${action.payload.username}`);
    } else {
      yield put(loginUserFailed(action.payload));
    }
  }
}

export function* watchRegisterChannel() {
  while (true) {
    const action = yield take(registerChannel);
    if (action.type === REGISTER_USER_SUCCESS) {
      yield put(registerUserSuccess(action.payload));
    } else {
      yield put(registerUserFailed(action.payload));
    }
  }
}

export function* watchLoginUser(): any {
  yield takeEvery(LOGIN_USER, login);
}

export function* watchLogoutUser(): any {
  yield takeEvery(LOGOUT_USER, logout);
}

export function* watchRegisterUser(): any {
  yield takeEvery(REGISTER_USER, register);
}

export function* watchForgetPassword(): any {
  yield takeEvery(FORGET_PASSWORD, forgetPassword);
}

function* authSaga(): any {
  yield all([
    fork(watchLoginUser),
    fork(watchLogoutUser),
    fork(watchRegisterUser),
    fork(watchForgetPassword),
    fork(watchLoginChannel),
    fork(watchRegisterChannel),
  ]);
}

export default authSaga;
