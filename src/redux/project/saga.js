import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import { CREATE_PROJECT } from './constants';
import { createProjectSuccess } from './actions';

/**
 * Initilizes the menu
 */
function* createProject() {
    try {
        yield put(createProjectSuccess({}));
    } catch (error) {}
}

/**
 * Watchers
 */
export function* watchCreateProject() {
    yield takeEvery(CREATE_PROJECT, createProject);
}


function* projectSaga() {
    yield all([fork(createProject)]);
}

export default projectSaga;
