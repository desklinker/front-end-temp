import {
  CREATE_PROJECT,
  CREATE_PROJECT_SUCCESS,
  CREATE_PROJECT_FAILED,
  UPLOAD_FILES,
  UPLOAD_FILES_FAILED,
  UPLOAD_FILES_SUCCESS,
} from './constants';

export const createProject = () => ({
  type: CREATE_PROJECT,
  payload: {},
});

export const createProjectSuccess = () => ({
  type: CREATE_PROJECT_SUCCESS,
  payload: {},
});

export const createProjectFailed = () => ({
  type: CREATE_PROJECT_FAILED,
  payload: {},
});

export const uploadFiles = () => ({
  type: UPLOAD_FILES,
  payload: {},
});

export const uploadFilesSuccess = () => ({
  type: UPLOAD_FILES_SUCCESS,
  payload: {},
});

export const uploadFilesFailed = () => ({
  type: UPLOAD_FILES_FAILED,
  payload: {},
});
