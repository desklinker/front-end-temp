import { CREATE_PROJECT, CREATE_PROJECT_SUCCESS, CREATE_PROJECT_FAILED } from './constants';

const project = (state, action) => {
  switch (action.type) {
    case CREATE_PROJECT:
      return action.payload;
    case CREATE_PROJECT_SUCCESS:
      return { ...state, ...action.payload };
    case CREATE_PROJECT_FAILED:
      return { ...state };
    default:
      return state;
  }
};

export default project;
