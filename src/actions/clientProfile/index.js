import { HTTP_REQUEST } from '../../middleware/axios';
import { userMgtUrl } from '../../config/env';

export function storeCompanyDetails(userId, values) {
  return {
    [HTTP_REQUEST]: {
      method: 'POST',
      url: `${userMgtUrl}/clients/${userId}/company`,
      data: values,
    },
  };
}

export function loadCompanyDetails(userId) {
  return {
    [HTTP_REQUEST]: {
      method: 'GET',
      url: `${userMgtUrl}/clients/${userId}/company`,
    },
  };
}

export function updateCompanyDetails(userId, values) {
  return {
    [HTTP_REQUEST]: {
      method: 'PUT',
      url: `${userMgtUrl}/clients/${userId}/company`,
      data: values,
    },
  };
}

export function loadBankDetails(userId) {
  return {
    [HTTP_REQUEST]: {
      method: 'GET',
      url: `${userMgtUrl}/users/${userId}/bank-details`,
    },
  };
}

export function loadBillingDetails(userId) {
  return {
    [HTTP_REQUEST]: {
      method: 'GET',
      url: `${userMgtUrl}/users/${userId}/billing-address-details`,
    },
  };
}

export function storeBankDetails(userId, values) {
  return {
    [HTTP_REQUEST]: {
      method: 'POST',
      url: `${userMgtUrl}/users/${userId}/bank-details`,
      data: values,
    },
  };
}

export function updateBankDetails(userId, values) {
  return {
    [HTTP_REQUEST]: {
      method: 'PUT',
      url: `${userMgtUrl}/users/${userId}/bank-details`,
      data: values,
    },
  };
}

export function storeBillingDetails(userId, values) {
  return {
    [HTTP_REQUEST]: {
      method: 'POST',
      url: `${userMgtUrl}/users/${userId}/billing-address-details`,
      data: values,
    },
  };
}

export function updateBillingDetails(userId, values) {
  return {
    [HTTP_REQUEST]: {
      method: 'PUT',
      url: `${userMgtUrl}/users/${userId}/billing-address-details`,
      data: values,
    },
  };
}
