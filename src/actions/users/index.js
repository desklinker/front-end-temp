import { HTTP_REQUEST } from '../../middleware/axios';
import { notificationMgtUrl, userMgtUrl } from '../../config/env';
import { LOGIN_USER_SUCCESS } from '../../redux/auth/constants';

export function loadItems(query) {
  return {
    [HTTP_REQUEST]: {
      url: `users?${query}`,
    },
  };
}

export function loadItem(id) {
  return {
    [HTTP_REQUEST]: {
      method: 'GET',
      url: `${userMgtUrl}/users/users/${id}`,
    },
  };
}

export function create(values) {
  const { role } = values;
  return {
    [HTTP_REQUEST]: {
      data: values,
      method: 'POST',
      url: `${userMgtUrl}/users/${role}`,
    },
  };
}

export function update(id, values) {
  return {
    [HTTP_REQUEST]: {
      data: values,
      method: 'PUT',
      url: `${userMgtUrl}/users/${id}`,
    },
  };
}

export function upload(file, id) {
  const formData = new FormData();
  formData.append('image', file);

  return {
    [HTTP_REQUEST]: {
      data: formData,
      method: 'POST',
      url: `users/${id}/profile`,
      headers: { 'Content-Type': 'multipart/form-data' },
    },
  };
}

export function sendVerificationCode(verifyingEntity, username) {
  return {
    [HTTP_REQUEST]: {
      method: 'POST',
      url: `${notificationMgtUrl}/v1/verifications/${verifyingEntity}/send/${username}`,
    },
  };
}

export function confirmVerificationCode(verifyingEntity, code, username) {
  return {
    [HTTP_REQUEST]: {
      method: 'GET',
      url: `${notificationMgtUrl}/v1/verifications/${verifyingEntity}/verify/${code}/${username}`,
    },
  };
}

export function checkAvailability(fieldName, query) {
  const type = fieldName !== 'phoneNumber' ? fieldName : 'phone-number';

  return {
    [HTTP_REQUEST]: {
      method: 'GET',
      url: `${userMgtUrl}/users/availability/${type}?${query}`,
    },
  };
}

export function confirmUserDetails(userId) {
  return {
    [HTTP_REQUEST]: {
      method: 'POST',
      url: `${userMgtUrl}/users/confirm-user/${userId}`,
    },
  };
}

export function loadUserDetails(user) {
  return {
    [HTTP_REQUEST]: {
      method: 'GET',
      url: `${userMgtUrl}/users/username/${user.username}/basic-details`,
      onSuccess: (res) => {
        return {
          payload: {
            ...user,
            userConfirmed: res.userConfirmed,
          },
          type: LOGIN_USER_SUCCESS,
        };
      },
    },
  };
}

export function loadBasicDetails(username) {
  return {
    [HTTP_REQUEST]: {
      method: 'GET',
      url: `${userMgtUrl}/users/username/${username}/basic-details`,
    },
  };
}
