import { HTTP_REQUEST } from '../../middleware/axios';
import { userMgtUrl } from '../../config/env';

export function fetchFields() {
  return {
    [HTTP_REQUEST]: {
      method: 'GET',
      url: `${userMgtUrl}/users/fields`,
    },
  };
}

export function fetchCountry(query) {
  return {
    [HTTP_REQUEST]: {
      method: 'GET',
      url: `${userMgtUrl}/users/countries?${query}`,
    },
  };
}

export function storeExperienceDetails(userId, values) {
  return {
    [HTTP_REQUEST]: {
      method: 'POST',
      url: `${userMgtUrl}/users/${userId}/work-experience-details`,
      data: values,
    },
  };
}

export function loadExperience(userId) {
  return {
    [HTTP_REQUEST]: {
      method: 'GET',
      url: `${userMgtUrl}/users/${userId}/work-experience-details`,
    },
  };
}

export function updateExperienceDetails(userId, wedid, values) {
  return {
    [HTTP_REQUEST]: {
      method: 'PUT',
      url: `${userMgtUrl}/users/${userId}/work-experience-details/${wedid}`,
      data: values,
    },
  };
}

export function removeExperienceDetails(userId, wedid) {
  return {
    [HTTP_REQUEST]: {
      method: 'DELETE',
      url: `${userMgtUrl}/users/${userId}/work-experience-details/${wedid}`,
    },
  };
}

export function storeCompanyDetails(values) {
  return {
    [HTTP_REQUEST]: {
      method: 'POST',
      url: `${userMgtUrl}/users/work-experience-company-details`,
      data: values,
    },
  };
}

export function updateCompanyDetails(companyId, values) {
  return {
    [HTTP_REQUEST]: {
      method: 'PUT',
      url: `${userMgtUrl}/users/work-experience-company-details/${companyId}`,
      data: values,
    },
  };
}

export function updateFieldDetails(userId, values) {
  return {
    [HTTP_REQUEST]: {
      method: 'PUT',
      url: `${userMgtUrl}/users/${userId}/field`,
      data: values,
    },
  };
}

export function storeSkillDetails(userId, values) {
  return {
    [HTTP_REQUEST]: {
      method: 'POST',
      url: `${userMgtUrl}/users/${userId}/skills`,
      data: values,
    },
  };
}

export function loadFieldAndSkills(userId) {
  return {
    [HTTP_REQUEST]: {
      method: 'GET',
      url: `${userMgtUrl}/users/${userId}/fieldAndSkills`,
    },
  };
}

export function storeQualificationDetails(userId, values) {
  return {
    [HTTP_REQUEST]: {
      method: 'POST',
      url: `${userMgtUrl}/users/${userId}/qualifications`,
      data: values,
    },
  };
}

export function loadPreSignedUrl(query) {
  return {
    [HTTP_REQUEST]: {
      method: 'GET',
      url: `${userMgtUrl}/uploads/presignedUrl?${query}`,
    },
  };
}

export function updateQualificationDetails(userId, qdid, values) {
  return {
    [HTTP_REQUEST]: {
      method: 'PUT',
      url: `${userMgtUrl}/users/${userId}/qualifications/${qdid}`,
      data: values,
    },
  };
}

export function removeQualification(userId, qdid) {
  return {
    [HTTP_REQUEST]: {
      method: 'DELETE',
      url: `${userMgtUrl}/users/${userId}/qualifications/${qdid}`,
    },
  };
}

export function storeBankDetails(userId, values) {
  return {
    [HTTP_REQUEST]: {
      method: 'POST',
      url: `${userMgtUrl}/users/${userId}/bank-details`,
      data: values,
    },
  };
}

export function updateBankDetails(userId, values) {
  return {
    [HTTP_REQUEST]: {
      method: 'PUT',
      url: `${userMgtUrl}/users/${userId}/bank-details`,
      data: values,
    },
  };
}

export function storeUniversity(values) {
  return {
    [HTTP_REQUEST]: {
      method: 'POST',
      url: `${userMgtUrl}/users/universities`,
      data: values,
    },
  };
}

export function loadQualification(userId) {
  return {
    [HTTP_REQUEST]: {
      method: 'GET',
      url: `${userMgtUrl}/users/${userId}/qualifications`,
    },
  };
}

export function loadBankDetail(userId) {
  return {
    [HTTP_REQUEST]: {
      method: 'GET',
      url: `${userMgtUrl}/users/${userId}/bank-details`,
    },
  };
}
