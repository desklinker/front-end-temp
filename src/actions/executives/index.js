import { userMgtUrl } from '../../config/env';
import { HTTP_REQUEST } from '../../middleware/axios';

export function loadItems(userId, query) {
  return {
    [HTTP_REQUEST]: {
      url: `${userMgtUrl}/clients/${userId}/executives?${query}`,
    },
  };
}

export function create(userId, values) {
  return {
    [HTTP_REQUEST]: {
      data: values,
      method: 'POST',
      url: `${userMgtUrl}/clients/${userId}/executives`,
    },
  };
}
