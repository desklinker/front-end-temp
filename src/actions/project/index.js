import { HTTP_REQUEST } from '../../middleware/axios';
import { projectMgtUrl } from '../../config/env';
import { queryStringify } from '../../helpers';

export function storeProject(values, query) {
  return {
    [HTTP_REQUEST]: {
      method: 'POST',
      url: `${projectMgtUrl}/projects?${query}`,
      data: values,
    },
  };
}

export function updateProject(projectId, values, query) {
  return {
    [HTTP_REQUEST]: {
      method: 'PUT',
      url: `${projectMgtUrl}/projects/${projectId}?${query}`,
      data: values,
    },
  };
}

export function removeUploadFile(projectId, uploadId) {
  return {
    [HTTP_REQUEST]: {
      method: 'DELETE',
      url: `${projectMgtUrl}/projects/${projectId}/uploads/${uploadId}`,
    },
  };
}

export function removeProject(projectId) {
  return {
    [HTTP_REQUEST]: {
      method: 'DELETE',
      url: `${projectMgtUrl}/projects/${projectId}`,
    },
  };
}

export function loadProjects(userId) {
  const query = queryStringify({ clientId: userId, statusId: [1, 2] });
  return {
    [HTTP_REQUEST]: {
      method: 'GET',
      url: `${projectMgtUrl}/projects?${query}`,
    },
  };
}

export function loadProject(projectId) {
  return {
    [HTTP_REQUEST]: {
      method: 'GET',
      url: `${projectMgtUrl}/projects/${projectId}`,
    },
  };
}

export function loadUploadFiles(projectId) {
  return {
    [HTTP_REQUEST]: {
      method: 'GET',
      url: `${projectMgtUrl}/projects/${projectId}/uploads`,
    },
  };
}
