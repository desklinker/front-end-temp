import { HTTP_REQUEST } from '../../middleware/axios';

export function fetchData(mgtUrl, url, query) {
  return {
    [HTTP_REQUEST]: {
      method: 'GET',
      url: `${mgtUrl}/${url}?${query}`,
    },
  };
}
