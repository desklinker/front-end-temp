// @flow
import React from 'react';
import Routes from './routes/Routes';
import 'fullpage.js/vendors/scrolloverflow';
import AWS from 'aws-sdk';
import Auth from '@aws-amplify/auth';
import Storage from '@aws-amplify/storage'
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import useStore, { history } from './redux/store';

// Themes
import './assets/scss/Saas.scss';
import { region, accessSecretKey, accessKeyId, amplifyAuth, amplifyStorage } from './config/env';

// For Dark import Saas-Dark.scss
// import './assets/scss/Saas-Dark.scss';

// For Modern import Modern.scss
// import './assets/scss/Modern.scss';
// For modern dakr import Modern-Dark.scss
// import './assets/scss/Modern-Dark.scss';

// For Creative demo import Modern.scss
// import './assets/scss/Creative.scss';
// For Creative dark demo import Modern.scss
// import './assets/scss/Creative-Dark.scss';

// configure fake backend
// configureFakeBackend();

Auth.configure(amplifyAuth);
Storage.configure(amplifyStorage);

/*
 * Configure the SDK to use anonymous identity
 */
AWS.config.update({
  region: region,
  accessKeyId: accessKeyId,
  secretAccessKey: accessSecretKey,
});

type AppProps = {};

/**
 * Main app component
 */
function App(props: AppProps) {

  const { store, persist, isReady } = useStore();

  
  if (!isReady) {
    return "We're getting things ready...";
  }

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persist}>
        <Routes history={history} />
      </PersistGate>
    </Provider>
  );
}

export default App;
