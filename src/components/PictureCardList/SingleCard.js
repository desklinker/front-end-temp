import React from 'react';
import { Card, CardImg, CardImgOverlay } from 'reactstrap';

function SingleCard({item, onClick}) {


    return (
        <Card onClick={()=>onClick(item)} className="cursor-pointer">
            <CardImg top width="100%"  src={item.src} alt="Card image cap" className="card-img"/>
            <CardImgOverlay>
                <div className="text-white">{item.caption}</div>
                <h3 className="text-white">{item.header}</h3>
            </CardImgOverlay>
        </Card>
    );
}

export default SingleCard;
