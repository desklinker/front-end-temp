import React, { useEffect, useRef, useState } from 'react';
import { Col, Card, CardBody, Collapse } from 'reactstrap';
import ReactOwlCarousel from 'react-owl-carousel2';
import SingleCard from './SingleCard';
import { useOutsideClick } from './hooks';

function PictureCardList({ cards, cardEndRef }) {
    const [isOpen, setOpen] = useState(false);
    const [selected, setSelected] = useState(null);

    const ref = useRef(null);
    const options = {
        items: 5,
        nav: true,
        rewind: true,
        navText: ['<i class="uil-angle-left" ></i>', '<i class="uil-angle-right" ></i>'],
    };

    useOutsideClick(ref, () => {
        setOpen(false);
        setSelected(null);
    });

    useEffect(() => {
        const current = cardEndRef.current;
        return () => {
            if (selected) {
                current.scrollIntoView({
                    behavior: 'smooth',
                    block: 'end',
                });
            }
        };
    }, [cardEndRef, selected]);

    return (
        <div>
            <div ref={ref} className="picture-card">
                <ReactOwlCarousel options={options}>
                    {cards.map((card, i) => {
                        return (
                            <Col md={12} key={'card-' + i}>
                                <SingleCard
                                    item={card}
                                    onClick={(item) => {
                                        setSelected(item);
                                        setOpen(true);
                                    }}
                                />
                            </Col>
                        );
                    })}
                </ReactOwlCarousel>
                {selected && (
                    <Collapse isOpen={isOpen}>
                        <Card className="h-100">
                            <CardBody>
                                <h4>{selected.header}</h4>
                                <p>{selected.description}</p>
                            </CardBody>
                        </Card>
                    </Collapse>
                )}
            </div>
        </div>
    );
}

export default PictureCardList;
