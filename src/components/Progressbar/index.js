import React, { useState, useEffect, useCallback } from 'react';
import { Card, CardHeader, CardBody } from 'reactstrap';
import SweetAlert from 'react-bootstrap-sweetalert';
import { DASHBOARD } from '../../constants/routes';

function Progressbar({ isOpen, toggle, history, isConfirmOpen }) {
    const [progress, setProgress] = useState(0);
    const [isSubmitted, setSubmitted] = useState(false);

    const randNumber = useCallback(() => {
        setProgress(progress + Math.floor(Math.random() * 10 + 1));
    }, [progress]);

    useEffect(() => {
        let timer;
        if (!isConfirmOpen) {
            if (progress < 100) {
                timer = setTimeout(() => {
                    randNumber();
                }, 500);
            } else {
                setSubmitted(true);
                history.push(DASHBOARD);
            }

            return () => {
                clearTimeout(timer);
            };
        }
    }, [progress, isConfirmOpen, randNumber, history]);

    return (
        <>
            {!isSubmitted && isOpen ? (
                <Card size="lg">
                    <CardHeader>
                        Uploading...
                        <button type="button" className="close" aria-label="Close" onClick={() => toggle('close')}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </CardHeader>
                    <CardBody className=" py-3">
                        <p className="font-18">
                            Project #1 is uploading
                            <strong>
                                <span className="ml-2">{`${progress > 100 ? 100 : progress}%`}</span>
                            </strong>
                        </p>
                        <div className="progress my-4 submit-progress">
                            <div
                                className="progress-bar progress-bar-success progress-bar-striped"
                                role="progressbar"
                                aria-valuenow={progress}
                                aria-valuemin="0"
                                aria-valuemax="100"
                                style={{ width: `${progress}%` }}>
                                {/* {progress}% */}
                            </div>
                        </div>
                    </CardBody>
                </Card>
            ) : (
                <SweetAlert success title="Success Data!" onConfirm={toggle} showConfirm={false} timeout={2000}>
                    Successfully created your project.
                </SweetAlert>
            )}
        </>
    );
}

export default Progressbar;
