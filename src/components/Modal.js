import React, { useState, useEffect, useCallback } from 'react';
import upperCase from 'lodash-es/upperCase';

import PropTypes from 'prop-types';
import { Button, Modal as BaseModal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';

import { useHistory } from 'react-router-dom';
import { modalService } from '../services';

const propTypes = {
  id: PropTypes.string,
};

const defaultProps = {
  id: 'default-modal',
};

function Modal() {
  const history = useHistory();
  const [modals, setModals] = useState([]);

  useEffect(() => {
    // subscribe to dashboard component modal
    const subscription = modalService.onModal().subscribe((modal) => {
      if (modal) {
        // add modal to local state if not empty
        setModals((modals) => [...modals, modal]);
      } else {
        // clear modals when empty modal received
        setModals([]);
      }
    });
    // clear alerts on location change
    const historyUnlisten = history.listen(() => {
      modalService.clearModal();
    });

    // clean up function that runs when the component unmounts
    return () => {
      // unsubscribe & unlisten to avoid memory leaks
      subscription.unsubscribe();
      historyUnlisten();
    };
  }, [history]);

  const removeModal = useCallback((modal) => {
    // remove modal
    setModals((modals) => modals.filter((x) => x !== modal));
  }, []);

  return (
    <>
      {modals.map((modal, index) => {
        return (
          <BaseModal isOpen size="md" centered key={index}>
            <ModalHeader>{upperCase(modal.header)}</ModalHeader>
            <ModalBody>
              <p>{modal.text}</p>
            </ModalBody>
            <ModalFooter>
              <Button size="sm" color="secondary" onClick={() => removeModal(modal)}>
                Cancel
              </Button>
              <Button
                size="sm"
                color="danger"
                onClick={() => {
                  modal.onConfirm();
                  removeModal(modal);
                }}>
                Confirm
              </Button>
            </ModalFooter>
          </BaseModal>
        );
      })}
    </>
  );
}

Modal.propTypes = propTypes;
Modal.defaultProps = defaultProps;
export { Modal };
