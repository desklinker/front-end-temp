import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useField } from 'formik';
import { FormGroup, Input as BaseInput, FormFeedback } from 'reactstrap';

const generator = require('generate-password');

function TextField({ passGenerator, label, ...props }) {
  const [field, meta, helpers] = useField(props.name);
  const [isRevealPassword, setRevealPassword] = useState(false);
  const { setValue } = helpers;

  const generatePassword = () => {
    return generator.generate({
      length: 10,
      numbers: true,
      symbols:true,
      excludeSimilarCharacters: true,
      strict: true
    });
  };

  return (
    <FormGroup>
      {label && <label htmlFor={field.name}>{label}</label>}

      <BaseInput {...props} {...field} type={props.type === 'password' && isRevealPassword ? 'text' : props.type} />

      {props.type === 'password' && (
        <span className="custom-icon" onClick={() => setRevealPassword(!isRevealPassword)}>
          {isRevealPassword ? <i className="mdi mdi-eye"></i> : <i className=" mdi mdi-eye-off"></i>}
        </span>
      )}

      {passGenerator && (
        <span
          className="text-muted cursor-pointer"
          onClick={() => {
            const pass = generatePassword();
            setValue(pass);
          }}>
          Generate password
        </span>
      )}

      {meta.touched && meta.error && <FormFeedback className="d-block">{meta.error}</FormFeedback>}
    </FormGroup>
  );
}

TextField.prototype = {
  disabled: PropTypes.bool,
  label: PropTypes.string,
  passGenerator: PropTypes.bool,
};

export default TextField;
