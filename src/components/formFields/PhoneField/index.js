import React from 'react';
import { useField } from 'formik';
import PropTypes from 'prop-types';
import { FormGroup, FormFeedback } from 'reactstrap';
import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/style.css'

function PhoneField({ label, onCheckAvailability, ...props }) {
  const [field, meta, helpers] = useField(props.name);
  const { value, touched, error, onBlur } = meta;
  const { setValue } = helpers;

  return (
    <FormGroup>
      {label && <label htmlFor={field.name}>{label}</label>}
      <PhoneInput
        country={'us'}
        onlyCountries={['lk', 'sg', 'us', 'uk', 'my', 'in', 'cn', 'au']}
        onChange={(val) => {
          setValue(val);
          onCheckAvailability(field.name, `+${val}`);
        }}
        onBlur={onBlur}
        inputProps={{
          name: 'phoneNumber',
        }}
        value={value || ''}
      />
      {touched && error && <FormFeedback className="d-block">{error}</FormFeedback>}
    </FormGroup>
  );
}

PhoneField.prototype = {
  onCheckAvailability: PropTypes.func,
  label: PropTypes.string,
};

export default PhoneField;
