import React, { useCallback, useEffect, useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { useField } from 'formik';
import { FormGroup, FormFeedback } from 'reactstrap';
import { fetchData } from '../../../actions/dropDown';
import get from 'lodash/get';
import AsyncSelect from 'react-select/async';
import AsyncCreatableSelect from 'react-select/async-creatable';

const queryString = require('query-string');

function Dropdown({
  label,
  resource,
  listKey,
  listLabel,
  relation,
  relationId,
  clearField,
  setOpen,
  addNew,
  mgtUrl,
  url,
  ...props
}) {
  const [defaultOptions, setDefaultOptions] = useState(null);
  const [field, meta, helpers] = useField(props.name);
  const dispatch = useDispatch();
  const mountedRef = useRef(true);

  // load options using API call
  // using listkey as listLabel in AsyncCreatableSelect
  const loadOptions = useCallback(
    async (inputValue) => {
      const query = queryString.stringify({ [relation]: relationId, keyword: inputValue ? inputValue : '' });
      const title = props.isCreatable ? 'value' : 'title';
      const key = props.isCreatable ? 'label' : 'id';

      try {
        if (!relation || (relation && relationId)) {
          const res = await dispatch(fetchData(mgtUrl, url, query));
          let options = res[resource]
            ? res[resource].map((list) => ({ [key]: get(list, listKey), [title]: get(list, listLabel) }))
            : [];

          if (addNew) {
            options.unshift({ id: 0, title: `Add New University` });
          }
          return options;
        }
      } catch (e) {
        //
      }
    },
    [addNew, dispatch, listKey, listLabel, mgtUrl, props.isCreatable, relation, relationId, resource, url]
  );

  const loadDefaultOptions = useCallback(
    (inputValue) => {
      loadOptions(inputValue).then((res) => {
        setDefaultOptions(res);
      });
    },
    [loadOptions]
  );

  const onInputChange = (inputValue, { action }) => {
    // if (action === 'input-change') {
    //   setInputValue(inputValue);
    // }
    // if (action === 'menu-close') {
    //   loadDefaultOptions(inputValue);
    // }
  };

  useEffect(() => {
    loadDefaultOptions();

    return () => {
      mountedRef.current = false;
    };
  }, [loadDefaultOptions]);

  return (
    <FormGroup>
      {label && <label htmlFor={field.name}>{label}</label>}
      {!props.isCreatable ? (
        <AsyncSelect
          {...field}
          {...props}
          defaultOptions={defaultOptions}
          getOptionLabel={(e) => e.title}
          getOptionValue={(e) => e.id}
          loadOptions={loadOptions}
          onChange={(value) => {
            if (value && value.id === 0) {
              setOpen();
            } else {
              clearField && clearField();
            }
            helpers.setValue(value);
          }}
          onInputChange={onInputChange}
          // inputValue={inputValue}
          // defaultValue={inputValue}
        />
      ) : (
        <AsyncCreatableSelect
          {...field}
          {...props}
          defaultOptions={defaultOptions}
          loadOptions={loadOptions}
          onChange={(value) => {
            console.log(value);
            clearField && clearField();
            helpers.setValue(value);
          }}
          onInputChange={onInputChange}
        />
      )}
      {meta.touched && meta.error && <FormFeedback className="d-block">{meta.error}</FormFeedback>}
    </FormGroup>
  );
}

Dropdown.prototype = {
  disabled: PropTypes.bool,
  label: PropTypes.string,
};

export default Dropdown;
