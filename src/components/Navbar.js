import React, { useEffect, useState } from 'react';
import AppMenu from './AppMenu';

import { useSelector } from 'react-redux';
import { Collapse } from 'reactstrap';
import GlobalSearch from './GlobalSearch';
import SweetAlert from 'react-bootstrap-sweetalert';
import NotificationDropdown from './NotificationDropdown';

const Notifications = [
    {
        id: 1,
        text: 'Caleb Flakelar commented on Admin',
        subText: '1 min ago',
        icon: 'mdi mdi-comment-account-outline',
        bgColor: 'primary',
    },
    {
        id: 2,
        text: 'New user registered.',
        subText: '5 min ago',
        icon: 'mdi mdi-account-plus',
        bgColor: 'info',
    },
    {
        id: 3,
        text: 'Cristina Pride',
        subText: 'Hi, How are you? What about our next meeting',
        icon: 'mdi mdi-comment-account-outline',
        bgColor: 'success',
    },
    {
        id: 4,
        text: 'Caleb Flakelar commented on Admin',
        subText: '2 days ago',
        icon: 'mdi mdi-comment-account-outline',
        bgColor: 'danger',
    },
    {
        id: 5,
        text: 'Caleb Flakelar commented on Admin',
        subText: '1 min ago',
        icon: 'mdi mdi-comment-account-outline',
        bgColor: 'primary',
    },
    {
        id: 6,
        text: 'New user registered.',
        subText: '5 min ago',
        icon: 'mdi mdi-account-plus',
        bgColor: 'info',
    },
    {
        id: 7,
        text: 'Cristina Pride',
        subText: 'Hi, How are you? What about our next meeting',
        icon: 'mdi mdi-comment-account-outline',
        bgColor: 'success',
    },
    {
        id: 8,
        text: 'Caleb Flakelar commented on Admin',
        subText: '2 days ago',
        icon: 'mdi mdi-comment-account-outline',
        bgColor: 'danger',
    },
];

type NavbarProps = {
    isMenuOpened?: boolean,
};

const Navbar = (props: NavbarProps) => {
    const progress = useSelector((state) => state.Progress);
    const { percent, loading } = progress;

    const [isOpen, setOpen] = useState(false);

    useEffect(() => {
        if (percent >= 100 && !loading) {
            setOpen(true);
        }
    }, [percent, loading]);

    const toggle = () => {
        setOpen(!isOpen);
    };
    return (
        <React.Fragment>
            <div className="topnav">
                <div className="container-fluid">
                    <nav className="navbar navbar-dark navbar-expand-lg topnav-menu">
                        <Collapse isOpen={props.isMenuOpened} className="navbar-collapse" id="topnav-menu-content">
                            <AppMenu mode={'horizontal'} />
                            <GlobalSearch />
                            <div className="notification-list">
                                <NotificationDropdown notifications={Notifications} />
                            </div>
                        </Collapse>
                    </nav>
                </div>
            </div>

            {percent > 0 && loading && (
                <div className="float-none progress nav-progress">
                    <div
                        className="progress-bar"
                        role="progressbar"
                        style={{ width: `${percent}%` }}
                        aria-valuenow={percent}
                        aria-valuemin="0"
                        aria-valuemax="100"></div>
                </div>
            )}
            {isOpen && (
                <SweetAlert success title="Success Data!" onConfirm={toggle} showConfirm={false} timeout={2000}>
                    Successfully created your project.
                </SweetAlert>
            )}
        </React.Fragment>
    );
};

export default Navbar;
