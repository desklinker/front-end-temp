import React, { Fragment, useState } from 'react';
import { DropdownMenu, DropdownToggle, Dropdown } from 'reactstrap';
import SimpleBar from 'simplebar-react';
import { Link } from 'react-router-dom';
import upperFirst from 'lodash-es/upperFirst';

const groupList = {
    projects: [
        {
            id: 1,
            text: 'Caleb Flakelar commented on Admin',
            url: '#',
        },
        {
            id: 2,
            text: 'New user registered.',
            url: '#',
        },
        {
            id: 3,
            text: 'Cristina Pride',
            url: '#',
        },
        {
            id: 4,
            text: 'Caleb Flakelar commented on Admin',
            url: '#',
        },
    ],
    contacts: [
        {
            id: 5,
            text: 'Caleb Flakelar commented on Admin',
            url: '#',
        },
        {
            id: 6,
            text: 'New user registered.',
            url: '#',
        },
        {
            id: 7,
            text: 'Cristina Pride',
            url: '#',
        },
        {
            id: 8,
            text: 'Caleb Flakelar commented on Admin',
            url: '#',
        },
    ],
};

function GlobalSearch() {
    const notifyContainerStyle = {
        maxHeight: '300px',
    };

    const search = (value) => {
        //
    };
    const [popoverOpen, setPopoverOpen] = useState(false);

    const toggle = () => {
        setPopoverOpen(!popoverOpen);
    };

    return (
        <>
            <div className="nav-search form-inline ml-auto">
                <div className="form-group position-relative">
                    <Dropdown isOpen={popoverOpen} toggle={toggle}>
                        <DropdownToggle
                            data-toggle="dropdown"
                            tag="button"
                            className="nav-link dropdown-toggle arrow-none btn btn-link"
                            aria-expanded={popoverOpen}>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Search..."
                                id="Popover1"
                                onChange={(e) => search(e.target.value)}
                            />
                        </DropdownToggle>
                        <DropdownMenu right className="dropdown-lg">
                            <div>
                                <SimpleBar style={notifyContainerStyle}>
                                    {Object.keys(groupList).map((key, index) => {
                                        return (
                                            <Fragment key={index}>
                                                <div className="dropdown-item noti-title bg-light">
                                                    <h5 className="m-0">{upperFirst(key)}</h5>
                                                </div>
                                                {groupList[key] &&
                                                    groupList[key].map((item, i) => {
                                                        return (
                                                            <Link
                                                                to={item.url}
                                                                className="dropdown-item notify-item"
                                                                key={i + '-noti'}>
                                                                <span className="notify-details">{item.text}</span>
                                                            </Link>
                                                        );
                                                    })}
                                            </Fragment>
                                        );
                                    })}
                                </SimpleBar>
                            </div>
                        </DropdownMenu>
                    </Dropdown>
                </div>
            </div>
        </>
    );
}

export default GlobalSearch;
