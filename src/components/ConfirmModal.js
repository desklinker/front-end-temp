import React from 'react';
import upperCase from 'lodash-es/upperCase';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';

function ConfirmModal({ isOpen, header, text, toggle, onClick }) {
    return (
        <Modal isOpen={isOpen} size="md" centered>
            <ModalHeader>{upperCase(header)}</ModalHeader>
            <ModalBody>
                <p>{text}</p>
            </ModalBody>
            <ModalFooter>
                <Button size="sm" color="secondary" onClick={toggle}>
                    Cancel
                </Button>
                <Button size="sm" color="danger" onClick={() => onClick()}>
                    Confirm
                </Button>
            </ModalFooter>
        </Modal>
    );
}

export default ConfirmModal;
