import React, { useState, useMemo } from 'react';
import classNames from 'classnames';
import moment from 'moment';

import Truncate from 'react-truncate';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { PROJECT_VIEW } from '../../constants/routes';
import { startCase, camelCase } from 'lodash/string';
import { Card, CardBody, Progress, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import './styles/single-project.css';
import { projectImage } from '../../helpers';

const SingleProject = ({ project, revision }) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const user = useSelector((state) => state.Auth.user);

  const { pid, fieldId, startDateTime, endDateTime, projectStatus, overview, title, field } = project;
  const toggleDropdown = (e) => {
    e.preventDefault();
    setDropdownOpen(!dropdownOpen);
  };

  const duration = useMemo(
    () => moment.unix(endDateTime).startOf('day').diff(moment.unix(startDateTime).startOf('day'), 'days'),
    [endDateTime, startDateTime]
  );

  return (
    <Card className="hover-card d-block w-100">
      <Link to={PROJECT_VIEW.replace(':id', pid)} className="text-title cursor-pointer">
        {fieldId && (
          <React.Fragment>
            <img className="card-img-top" src={projectImage(fieldId)} alt="" />
            <div className="card-img-overlay">
              {projectStatus?.status != null && (
                <div
                  className={classNames(
                    'badge',
                    {
                      'badge-success': projectStatus.status === 'Completed',
                      'badge-info': projectStatus.status === 'PENDING_APPROVAL',
                      'badge-light': projectStatus.status === 'Planned',
                      'badge-secondary': projectStatus.status === 'DRAFT',
                    },
                    'p-1'
                  )}>
                  {startCase(camelCase(projectStatus?.status))}
                </div>
              )}

              <div className="float-right d-inline help-dropdown">
                <Dropdown isOpen={dropdownOpen} toggle={toggleDropdown}>
                  <DropdownToggle
                    data-toggle="dropdown"
                    tag="button"
                    className="dropdown-toggle arrow-none btn font-18"
                    onClick={(e) => toggleDropdown(e)}
                    aria-expanded={dropdownOpen}>
                    <i className="mdi mdi-dots-vertical noti-icon"></i>
                  </DropdownToggle>
                  <DropdownMenu right className="font-12">
                    <DropdownItem>Help</DropdownItem>
                    <DropdownItem>Support</DropdownItem>
                  </DropdownMenu>
                </Dropdown>
              </div>
            </div>
          </React.Fragment>
        )}

        <CardBody className={fieldId ? 'position-relative' : ''}>
          <h4 className="mt-0 text-break">
            <Truncate lines={1} ellipsis="...">
              {title}
            </Truncate>
          </h4>

          <p className="mb-1">
            {project.field != null && <span className="mr-1 badge badge-primary-lighten">{field.name}</span>}
            {project.skill != null && user.role === 'prof' && (
              <span className="mr-1 badge badge-primary-lighten">{project.skill}</span>
            )}
            {duration != null && <span className="mr-1 badge badge-secondary-lighten">{`${duration} Days`}</span>}
            {project.minBid != null && project.maxBid != null && user.role === 'prof' && (
              <span color="success" className="mr-1 badge badge-success-lighten">
                {project.minBid} - {project.maxBid}
              </span>
            )}
            {project.maxBid != null && user.role === 'codi' && (
              <span color="success" className="mr-1 badge badge-success-lighten">
                {project.maxBid}
              </span>
            )}
            {revision === true && user.role === 'client' && (
              <span color="secondary" className="mr-1 badge badge-secondary-lighten">
                10 days ago
              </span>
            )}
          </p>

          <div className="text-muted mt-2 mb-1 text-break">
            <Truncate lines={2} ellipsis="...">
              {overview}
            </Truncate>

            {/* <div className="text-muted mt-2 mb-1 text-break">
            {truncate(overview, {
              length: 48,
            })} */}
          </div>

          <p className="mb-1">
            {project.totalTasks != null && (
              <span className="pr-2 text-nowrap mb-2 d-inline-block">
                <i className="mdi mdi-format-list-bulleted-type text-muted mr-1"></i>
                <b>{project.totalTasks}</b> Tasks
              </span>
            )}
            {project.totalComments != null && (
              <span className="text-nowrap mb-2 d-inline-block">
                <i className="mdi mdi-comment-multiple-outline text-muted mr-1"></i>
                <b>{project.totalComments}</b> Comments
              </span>
            )}
          </p>

          {project.progress != null && project.state !== 'Ongoing' && (
            <React.Fragment>
              {project.progress <= 30 && (
                <div>
                  <p className="mt-3 mb-2 font-weight-bold">
                    Progress <span className="float-right">{project.progress}%</span>
                  </p>
                  <Progress value={project.progress} color="warning" className="progress-sm" />
                </div>
              )}
              {project.progress > 30 && project.progress < 100 && (
                <div>
                  <p className="mt-3 mb-2 font-weight-bold">
                    Progress <span className="float-right">{project.progress}%</span>
                  </p>
                  <Progress value={project.progress} color="success" className="progress-sm" />
                </div>
              )}
              {project.progress === 100 && (
                <span></span>
                // <Progress value={project.progress} color="success" className="progress-sm" />
              )}
            </React.Fragment>
          )}
          <p className="card-text">
            <small className="text-muted">{moment.unix(startDateTime).format('YYYY-MM-DD, HH:mm')}</small>
          </p>
        </CardBody>
      </Link>
    </Card>
  );
};

export default SingleProject;
