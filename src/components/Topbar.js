// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import ProfileDropdown from './ProfileDropdown';
import { showRightSidebar } from '../redux/actions';
import profilePic from '../assets/images/users/avatar-1.jpg';
import logoSm from '../assets/images/logo_sm.png';
import { ADMIN, COORDINATOR, PROFESSIONAL, CLIENT, EXECUTIVE, SCREENER, PROFILE_VIEW } from '../constants';

const ProfileMenus = [
  {
    label: 'My Profile',
    icon: 'uil uil-user',
    redirectTo: PROFILE_VIEW,
  },
  {
    label: 'Settings',
    icon: 'uil uil-cog',
    redirectTo: '/',
  },
  // {
  //     label: 'Support',
  //     icon: 'uil uil-life-ring',
  //     redirectTo: '/',
  // },
  // {
  //     label: 'Lock Screen',
  //     icon: 'uil uil-lock-alt',
  //     redirectTo: '/',
  // },
  {
    label: 'Logout',
    icon: 'uil uil-exit',
    redirectTo: '/account/logout',
  },
];

type TopbarProps = {
  showRightSidebar: PropTypes.func,
  hideLogo?: boolean,
  navCssClasses?: string,
  openLeftMenuCallBack?: PropTypes.func,
};

class Topbar extends Component<TopbarProps> {
  constructor(props) {
    super(props);

    this.handleRightSideBar = this.handleRightSideBar.bind(this);
  }

  /**
   * Toggles the right sidebar
   */
  handleRightSideBar = () => {
    this.props.showRightSidebar();
  };

  render() {
    const hideLogo = this.props.hideLogo || false;
    const navCssClasses = this.props.navCssClasses || '';
    const containerCssClasses = !hideLogo ? 'container-fluid' : '';
    const { firstName, lastName, role } = this.props.user.user;
    let userTitle;

    if (role === ADMIN) {
      userTitle = 'Administrator';
    } else if (role === COORDINATOR) {
      userTitle = 'Coordinator';
    } else if (role === PROFESSIONAL) {
      userTitle = 'Professional';
    } else if (role === CLIENT) {
      userTitle = 'Client';
    } else if (role === EXECUTIVE) {
      userTitle = 'Executive';
    } else if (role === SCREENER) {
      userTitle = 'Screener';
    }

    return (
      <React.Fragment>
        <div className={`navbar-custom ${navCssClasses}`}>
          <div className={containerCssClasses}>
            {!hideLogo && (
              <Link to="/" className="topnav-logo">
                <span className="topnav-logo-lg">
                  <h3>DeskLinker</h3>
                  {/*<img src={logo} alt="logo" height="50" />*/}
                </span>
                <span className="topnav-logo-sm">
                  <img src={logoSm} alt="logo" height="16" />
                </span>
              </Link>
            )}

            <ul className="list-unstyled topbar-right-menu float-right mb-0">
              {/* <li className="notification-list topbar-dropdown">
                                <button
                                    className="nav-link dropdown-toggle arrow-none btn btn-link"
                                    onClick={this.handleRightSideBar}>
                                    <i className="mdi mdi-settings-outline noti-icon"></i>
                                </button>
                            </li> */}
              {/* <li className="notification-list topbar-dropdown d-none d-lg-block">
                                <LanguageDropdown />
                            </li> */}
              {/*<li className="notification-list">*/}
              {/*    <NotificationDropdown notifications={Notifications} />*/}
              {/*</li>*/}
              <li className="notification-list">
                <ProfileDropdown
                  profilePic={profilePic}
                  menuItems={ProfileMenus}
                  username={`${firstName} ${lastName}`}
                  userTitle={userTitle}
                />
              </li>
            </ul>

            <button className="button-menu-mobile open-left disable-btn" onClick={this.props.openLeftMenuCallBack}>
              <i className="mdi mdi-menu"></i>
            </button>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.Auth,
});

export default connect(mapStateToProps, { showRightSidebar })(Topbar);
