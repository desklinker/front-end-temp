import React from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import startCase from 'lodash-es/startCase';
import lowerCase from 'lodash-es/lowerCase';
import Rating from 'react-rating';
import { Col, Row } from 'reactstrap';
import './styles/paginateTable.css';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';

const badges = {
    pendingClientConfirmation: 'badge-danger-lighten',
    draftsPassed: 'badge-info-lighten',
    finalOutputPassed: 'badge-success-lighten',
    active: 'badge-success-lighten',
    revisionsOnProgress: 'badge-secondary-lighten',
    scopeChangesOnProgress: 'badge-primary-lighten',
    pending: 'badge-info-lighten',
    success: 'badge-success-lighten',
    failed: 'badge-danger-lighten',
    ongoing: 'badge-info-lighten',
    completed: 'badge-success-lighten',
};

function PaginateTable({ data, columns, title, setSelected, searchable }) {
    const { SearchBar } = Search;

    const customTotal = (from, to, size) => (
        <span className="react-bootstrap-table-pagination-total ml-2">
            Showing {from} to {to} of {size} Results
        </span>
    );

    const StatusColumn = (cell, row, rowIndex, extraData) => {
        return <span className={`badge ${badges[cell]}`}>{startCase(lowerCase(cell))}</span>;
    };

    const BadgeColumn = (cell, row, rowIndex, extraData) => {
        return <span className={`badge badge-primary-lighten`}>{cell}</span>;
    };

    const BooleanColumn = (cell, row, rowIndex, extraData) => {
        return (
            <>
                {cell === 'Yes' ? (
                    <span className="badge badge-success-lighten">
                        {' '}
                        <i className="dripicons-checkmark"></i>
                    </span>
                ) : cell === 'No' ? (
                    <span className="badge badge-danger-lighten">
                        <i className="dripicons-cross"></i>
                    </span>
                ) : (
                    ''
                )}
            </>
        );
    };

    const MultiDataColumn = (cell, row, rowIndex, extraData) => {
        return <span>{cell && cell.map((item) => `${item}`).join(', ')}</span>;
    };

    const RatingColumn = (cell, row, rowIndex, extraData) => {
        return (
            <span>
                <Rating
                    initialRating={cell || 0}
                    readonly
                    emptySymbol="mdi mdi-star-outline font-16 text-muted"
                    fullSymbol="mdi mdi-star font-16 text-warning"
                />
            </span>
        );
    };

    const NameColumn = (cell, row, rowIndex, extraData) => {
        return (
            <>
                <img src={row.avatar} alt={cell} className="mr-2 rounded-circle" />
                {cell}
            </>
        );
    };

    const ActionColumn = (cell, row, rowIndex, extraData) => {
        return (
            <>
                <span onClick={() => setSelected(row)} className="cursor-pointer">
                    <i className="mdi mdi-square-edit-outline"></i>
                </span>
            </>
        );
    };

    const renderColumn = (type) => {
        switch (type) {
            case 'booleanColumn':
                return <BooleanColumn />;

            case 'multiDataColumn':
                return <MultiDataColumn />;

            case 'eatingColumn':
                return <RatingColumn />;

            case 'nameColumn':
                return <NameColumn />;

            case 'actionColumn':
                return <ActionColumn />;

            case 'statusColumn':
                return <StatusColumn />;

            case 'badgeColumn':
                return <BadgeColumn />;

            default:
                return null;
        }
    };
    const paginationOptions = {
        paginationSize: 5,
        pageStartIndex: 1,
        firstPageText: 'First',
        prePageText: 'Back',
        nextPageText: 'Next',
        lastPageText: 'Last',
        nextPageTitle: 'First page',
        prePageTitle: 'Pre page',
        firstPageTitle: 'Next page',
        lastPageTitle: 'Last page',
        showTotal: true,
        paginationTotalRenderer: customTotal,
        sizePerPageList: [
            {
                text: '5',
                value: 5,
            },
            {
                text: '10',
                value: 10,
            },
            {
                text: '25',
                value: 25,
            },
        ],
    };

    const defaultSorted = [
        {
            dataField: 'id',
            order: 'asc',
        },
    ];

    return (
        <>
            <h4 className="header-title">{title}</h4>

            {!searchable ? (
                <BootstrapTable
                    bootstrap4
                    keyField="id"
                    data={data}
                    columns={columns.map((column) => {
                        if (column.formatter) {
                            return {
                                ...column,
                                formatter: renderColumn(column.formatter),
                            };
                        } else {
                            return column;
                        }
                    })}
                    defaultSorted={defaultSorted}
                    pagination={paginationFactory(paginationOptions)}
                    wrapperClasses="table-responsive"
                />
            ) : (
                <ToolkitProvider
                    bootstrap4
                    keyField="id"
                    data={data}
                    columns={columns.map((column) => {
                        if (column.formatter) {
                            return {
                                ...column,
                                formatter: renderColumn(column.formatter),
                            };
                        } else {
                            return column;
                        }
                    })}
                    search>
                    {(props) => (
                        <React.Fragment>
                            <Row>
                                <Col>
                                    <SearchBar {...props.searchProps} />
                                </Col>
                            </Row>

                            <BootstrapTable
                                {...props.baseProps}
                                defaultSorted={defaultSorted}
                                pagination={paginationFactory(paginationOptions)}
                                wrapperClasses="table-responsive"
                            />
                        </React.Fragment>
                    )}
                </ToolkitProvider>
            )}
        </>
    );
}

export default PaginateTable;
