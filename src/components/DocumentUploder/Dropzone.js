import React, { useMemo, useState, useEffect } from 'react';
import { useDropzone } from 'react-dropzone';

const baseStyle = {
  border: ' 2px dashed #ccc',
  background: '#fafafa',
  padding: '40px',
  borderRadius: '6px',
  cursor: 'pointer',
  outline: 'none',
  minHeight: '150px',
  textAlign: 'center',
};

const activeStyle = {
  borderColor: '#2196f3',
};

const acceptStyle = {
  borderColor: '#00e676',
};

const rejectStyle = {
  borderColor: '#ff1744',
};

function Dropzone({ onUpload }) {
  const [files, setFiles] = useState([]);

  const { getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject } = useDropzone({
    accept: 'image/jpeg,image/jpg,image/png,.pdf',
    onDrop: (acceptedFiles) => {
      acceptedFiles.forEach((file) => onUpload(file));
      setFiles(acceptedFiles);
    },
    multiple: false,
  });

  const fileList = files.map((file) => (
    <li key={file.name}>
      {file.name} - {file.size} bytes
    </li>
  ));

  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach((file) => URL.revokeObjectURL(file.preview));
    },
    [files]
  );

  const style = useMemo(
    () => ({
      ...baseStyle,
      ...(isDragActive ? activeStyle : {}),
      ...(isDragAccept ? acceptStyle : {}),
      ...(isDragReject ? rejectStyle : {}),
    }),
    [isDragActive, isDragReject, isDragAccept]
  );

  return (
    <>
      <div {...getRootProps({ style })}>
        <input {...getInputProps()} />
        <i className="h2 text-muted dripicons-cloud-upload"></i>
        <h5 className="text-muted">Drop file here or click to upload.</h5>
      </div>
      <aside>
        <ul>{fileList}</ul>
      </aside>
    </>
  );
}

export default Dropzone;
