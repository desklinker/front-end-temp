import React from 'react';
import Dropzone from './Dropzone';

function DocumentUploader({ onUpload }) {
  return (
      <Dropzone onUpload={onUpload} />
  );
}

export default DocumentUploader;
