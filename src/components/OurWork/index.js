import React, { useEffect, useState, useRef } from 'react';
import { ReactComponent as Civil } from '../../assets/images/landing/civil.svg';
import { ReactComponent as ItSupport } from '../../assets/images/landing/computer.svg';
import { ReactComponent as Setting } from '../../assets/images/landing/mechanic.svg';
import { Card, CardBody, Row, Col } from 'reactstrap';
import { useHistory } from 'react-router-dom';
import './styles/our-work.css';
import { ROOT, DASHBOARD } from '../../constants/routes';
import { FadeIn } from './animate';
import { motion } from 'framer-motion';

const variants = {
    open: { y: 0, height: 'auto' },
    closed: { y: '100%', height: 0 },
};

function OurWork(props) {
    const history = useHistory();
    const [isOpen, setOpen] = useState(true);
    const [pathname, setPathname] = useState(window.location.pathname);

    const el = useRef(null);

    const scrollToBottom = () => {
        if (el.current) {
            el.current.scrollIntoView({
                behavior: 'smooth',
                block: 'end',
            });
        }
    };

    useEffect(() => {
        scrollToBottom();

        return history.listen((location) => {
            if (location.pathname) {
                setPathname(location.pathname);
            }
        });
    }, [history]);

    if (!(pathname === ROOT || pathname === DASHBOARD)) {
        return null;
    }

    const toggle = () => {
        setOpen(!isOpen);
    };

    return (
        <div className="our-work mt-5 border-top">
            <Row>
                <Col className="align-self-center position-absolute text-center" style={{ zIndex: 1 }}>
                    <span
                        className="avatar-sm rounded-circle our-work-icon toggle-button cursor-pointer"
                        onClick={toggle}>
                        <motion.i
                            className="uil text-secondary font-24 uil-angle-up"
                            animate={{ rotate: isOpen ? 180 : 0 }}></motion.i>
                    </span>
                </Col>
            </Row>

            <motion.div
                className="container-fluid"
                animate={isOpen ? 'open' : 'closed'}
                variants={variants}
                transition={{ duration: 0.9 }}>
                <div className="row mt-2  pt-3 align-items-center">
                    <div className="col-lg-4 offset-lg-1">
                        <FadeIn entered={isOpen}>
                            <Civil className="w-100" />
                        </FadeIn>
                    </div>
                    <div className="col-lg-6 offset-lg-1">
                        <Card className="hover-card">
                            <CardBody>
                                <div className="avatar-sm d-inline mr-2">
                                    <span className="avatar-sm bg-danger-lighten rounded-circle our-work-icon">
                                        <i className="uil uil-constructor text-danger font-24"></i>
                                    </span>
                                </div>
                                <h1 className="font-weight-normal d-inline">
                                    <span className="hover-link">Civil</span>
                                </h1>
                                <p className="text-muted mt-3">
                                    It is a long established fact that a reader will be distracted by the readable
                                    content of a page when looking at its layout. The point of using Lorem Ipsum is that
                                    it has a more-or-less normal distribution of letters, as opposed to using 'Content
                                    here, content here', making it look like readable English. Many desktop publishing
                                    packages and web page editors now use Lorem Ipsum as their default model text
                                </p>
                            </CardBody>
                        </Card>
                    </div>
                </div>

                <div className="row pb-3 pt-3 align-items-center">
                    <div className="col-lg-6">
                        <Card className="hover-card">
                            <CardBody>
                                <div className="avatar-sm d-inline mr-2">
                                    <span className="avatar-sm bg-primary-lighten rounded-circle our-work-icon">
                                        <i className="uil uil-desktop text-primary font-24"></i>
                                    </span>
                                </div>
                                <h1 className="font-weight-normal d-inline">
                                    <span className="hover-link">Computer</span>
                                </h1>
                                <p className="text-muted mt-3">
                                    It is a long established fact that a reader will be distracted by the readable
                                    content of a page when looking at its layout. The point of using Lorem Ipsum is that
                                    it has a more-or-less normal distribution of letters, as opposed to using 'Content
                                    here, content here', making it look like readable English.Many desktop publishing
                                    packages and web page editors now use Lorem Ipsum as their default model text.
                                </p>
                            </CardBody>
                        </Card>
                    </div>
                    <div className="col-lg-4  offset-lg-1 offset-right-lg-1">
                        <FadeIn entered={isOpen}>
                            <ItSupport className="w-100" />
                        </FadeIn>
                    </div>
                </div>

                <div className="row mt-2  pt-3 pb-5 align-items-center">
                    <div className="col-lg-4 offset-lg-1">
                        <FadeIn entered={isOpen}>
                            <Setting className="w-100" />
                        </FadeIn>
                    </div>
                    <div className="col-lg-6 offset-lg-1">
                        <Card className="hover-card">
                            <CardBody>
                                <div className="avatar-sm d-inline mr-2">
                                    <span className="avatar-sm bg-info-lighten rounded-circle our-work-icon">
                                        <i className="uil uil-wrench text-info font-24"></i>
                                    </span>
                                </div>
                                <h1 className="font-weight-normal d-inline">
                                    <span className="hover-link">Mechanical</span>
                                </h1>
                                <p className="text-muted mt-3">
                                    It is a long established fact that a reader will be distracted by the readable
                                    content of a page when looking at its layout. The point of using Lorem Ipsum is that
                                    it has a more-or-less normal distribution of letters, as opposed to using 'Content
                                    here, content here', making it look like readable English. Many desktop publishing
                                    packages and web page editors now use Lorem Ipsum as their default model text.
                                </p>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </motion.div>

            <div ref={el}></div>
        </div>
    );
}

export default OurWork;
