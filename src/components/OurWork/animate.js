import React from 'react';
import { Transition } from 'react-transition-group';


const duration = 500;

const defaultStyle = {
    transition: `transform ${duration}ms, opacity ${duration}ms ease`,
    opacity: 1,
};

const transitionStyles = {
    entering: { transform: 'scale(0.5)', opacity: 0 },
    entered: { transform: 'scale(1.0)', opacity: 1 },
    exiting: { opacity: 0 },
    exited: { opacity: 0 },
};

export const FadeIn = ({ entered, children }) => (
    <Transition in={entered} timeout={duration} appear>
        {(state) => (
            <div
                style={{
                    ...defaultStyle,
                    ...transitionStyles[state],
                }}>
                {children}
            </div>
        )}
    </Transition>
);
