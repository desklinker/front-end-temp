// @flow
import React from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Breadcrumb, BreadcrumbItem, Button } from 'reactstrap';
import PropTypes from 'prop-types';

type PageTitleProps = {
    breadCrumbItems: PropTypes.object,
    title: string,
    search: func,
    isSearch: boolean,
    isFilter: boolean,
};

/**
 * PageTitle
 */
const PageTitle = (props: PageTitleProps) => {
    return (
        <Row>
            <Col>
                <div className="page-title-box">
                    <div className="page-title-right">
                        <Breadcrumb>
                            <BreadcrumbItem>
                                <Link to="/">Desk Linker</Link>
                            </BreadcrumbItem>
                            {props.breadCrumbItems.map((item, index) => {
                                return item.active ? (
                                    <BreadcrumbItem active key={index}>
                                        {item.label}
                                    </BreadcrumbItem>
                                ) : (
                                    <BreadcrumbItem key={index}>
                                        <Link to={item.path}>{item.label}</Link>
                                    </BreadcrumbItem>
                                );
                            })}
                        </Breadcrumb>
                    </div>
                    <h4 className="page-title">{props.title}</h4>
                </div>
            </Col>
            <Col className="align-self-center text-right title-search">
                {props.isSearch && (
                    <div className="app-search page-title-box mt-2 d-inline-block">
                        <form>
                            <div className="form-group position-relative">
                                <input
                                    type="text"
                                    className="form-control"
                                    placeholder="Search..."
                                    onKeyUp={(e) => props.search(e.target.value)}
                                />
                                <span className="mdi mdi-magnify"></span>
                            </div>
                        </form>
                    </div>
                )}
                {props.isFilter && (
                    <div className="page-title-box mt-2 d-inline-block">
                        <div className="btn-group">
                            <Button className="ml-2 d-inline-block btn btn-secondary" onClick={props.toggle}>
                                <i className="mdi mdi-filter-variant"></i>
                            </Button>
                        </div>
                    </div>
                )}
                { props.view && <div className="page-title-box mt-2 d-inline-block">
                    <div className="btn-group ml-2">
                        <Button
                            color={props.view === 'card' ? 'secondary' : 'link'}
                            onClick={() => props.setView('card')}>
                            <i className="dripicons-view-apps"></i>
                        </Button>
                    </div>
                    <div className="btn-group d-inline-block">
                        <Button
                            color={props.view === 'list' ? 'secondary' : 'text-muted'}
                            onClick={() => props.setView('list')}>
                            <i className="dripicons-checklist"></i>
                        </Button>
                    </div>
                </div>}

            </Col>
        </Row>
    );
};

export default PageTitle;
