import React from 'react';
import { Card } from 'reactstrap';
import SimpleBar from 'simplebar-react';
import { Link } from 'react-router-dom';

const FileWidget = ({ title, maxHeight }) => {
    const items = [
        {
            name: 'Client-design.zip',
            size: '2.5 MB',
        },
        {
            name: 'Dashboard-design.jpg',
            size: '3.5 MB',
        },
        {
            name: 'Admin-design.jpg',
            size: '2.5 MB',
        },
        {
            name: 'Admin-bug-report.mp4',
            size: '7.05 MB',
        },
        { name: 'Codi-design.jpg', size: '6.5 MB' },
        {
            name: 'Dashboard-bug-report.mp4',
            size: '7.05 MB',
        },
    ];

    return (
        <>
            <div className="position-relative mb-1 text-right">
                <strong className="float-left">{title && `${title} :`}</strong>
                <span>
                    <a href="/" className="text-muted">
                        <i className="mdi mdi-download"></i> Download All
                    </a>
                </span>
            </div>
            <SimpleBar style={{ maxHeight: maxHeight ? maxHeight : '145px' }}>
                {items.map((item, index) => {
                    return (
                        <Card className="mb-1 shadow-none border" key={`key_${index}`}>
                            <div className="p-1">
                                <a href="/" className="text-muted font-weight-bold">
                                    {item.name}
                                </a>
                                <span className="ml-2">{item.size && `(${item.size})`}</span>
                                <span className="float-right">
                                    <a href="/" className="text-muted mr-2">
                                        <i className="dripicons-trash"></i>
                                    </a>
                                    <a href="/" className="text-muted">
                                        <i className="dripicons-download"></i>
                                    </a>
                                </span>
                            </div>
                        </Card>
                    );
                })}
            </SimpleBar>
            <p className="mb-0 mt-2">
                <strong className="text-muted">Drive Link :</strong>
                <span className="ml-2">
                    <Link to="www.sample.com" target="_blank">
                        http://www.sample.com
                    </Link>
                </span>
            </p>
        </>
    );
};

export default FileWidget;
