import React from 'react';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';

function RemoveConfirmModal({ isOpen, toggle, remove, selected, name }) {
    return (
        <Modal isOpen={isOpen} size="sm" centered>
            <ModalHeader>Confirm Delete</ModalHeader>
            <ModalBody>
                <p>{`Are you sure want to delete this ${name ? name : 'item'}?`}</p>
            </ModalBody>
            <ModalFooter>
                <Button size="sm" color="secondary" onClick={toggle}>
                    Cancel
                </Button>
                <Button
                    size="sm"
                    color="danger"
                    onClick={() => {
                        if (!selected) {
                            remove();
                        } else {
                            remove(selected);
                        }
                        toggle();
                    }}>
                    Delete
                </Button>
            </ModalFooter>
        </Modal>
    );
}

export default RemoveConfirmModal;
