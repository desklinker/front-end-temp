import { CookieStorage } from 'amazon-cognito-identity-js';

export const poolData = {
  UserPoolId: process.env.REACT_APP_USER_POOL_ID,
  ClientId: process.env.REACT_APP_USER_POOL_APP_CLIENT_ID,
  Storage: new CookieStorage({ domain: 'localhost' }),
};
