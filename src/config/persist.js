import Storage from 'redux-persist/lib/storage';

export const rootPersistConfig = {
  key: 'root',
  storage: Storage,
};

export const authPersistConfig = {
  key: 'auth',
  storage: Storage,
};