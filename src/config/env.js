module.exports = {
  timeout: 30000,
  name: process.env.REACT_APP_NAME,

  // URLs
  baseUrl: `${process.env.REACT_APP_API_URL}`,

  //Management based api base url
  notificationMgtUrl: `${process.env.REACT_APP_NOTIFICATION_MGT_URL}`,
  userMgtUrl: `${process.env.REACT_APP_USER_MGT_URL}`,
  projectMgtUrl: `${process.env.REACT_APP_PROJECT_MGT_URL}`,
  configMgtUrl: `${process.env.REACT_APP_CONFIG_MGT_URL}`,

  //aws api gateway
  apiGateway: {
    REGION: 'API_REGION',
    URL: 'API_URL',
  },

  //aws cognito config
  region: process.env.REACT_APP_REGION,
  userPoolId: process.env.REACT_APP_USER_POOL_ID,
  identityPoolId: process.env.REACT_APP_IDENTITY_POOL_ID,

  poolData: {
    UserPoolId: process.env.REACT_APP_USER_POOL_ID,
    ClientId: process.env.REACT_APP_USER_POOL_APP_CLIENT_ID,
  },

  accessSecretKey: process.env.REACT_APP_AWS_SECRET_KEY,
  accessKeyId: process.env.REACT_APP_AWS_ACCESS_KEY_ID,

  amplifyAuth: {
    identityPoolId: process.env.REACT_APP_IDENTITY_POOL_ID,
    region: process.env.REACT_APP_REGION,
    userPoolId: process.env.REACT_APP_USER_POOL_ID,
    userPoolWebClientId: process.env.REACT_APP_USER_POOL_APP_CLIENT_ID,
  },

  amplifyStorage: {
    AWSS3: {
      bucket: process.env.REACT_APP_S3_BUCKET_NAME,
      region: process.env.REACT_APP_REGION,
    },
  },
};
