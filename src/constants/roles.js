export const ADMIN = 'ADMIN';
export const COORDINATOR = 'COORDINATOR';
export const SCREENER = 'SCREENER';

export const CLIENT = 'CLIENT';
export const EXECUTIVE = 'EXECUTIVE';
export const PROFESSIONAL = 'PROFESSIONAL';

export const roles = {
  client: {
    id: 1,
    value: 'CLIENT',
  },
  professional: {
    id: 2,
    value: 'PROFESSIONAL',
  },
  executive: {
    id: 3,
    value: 'CLIENT_EXECUTIVE',
  },
};
