export const ROOT = '/';
export const DASHBOARD = '/dashboard';

export const FORBIDDEN = '/forbidden';
export const ACCOUNT = '/account';
export const LOGIN = '/account/login';
export const LOGOUT = '/account/logout';
export const REGISTER = '/account/register';

export const PHONE_VERIFY = '/account/phone-verify/:username';
export const CONFIRM = '/account/confirm/:username';
export const FORGET_PASSWORD = '/account/forget-password';

export const PROJECT = '/projects';
export const PROJECT_VIEW = '/projects/:id';
export const MY_PROJECT = '/my-projects';

export const PROFILE = '/profile';
export const PROFILE_VIEW = '/profile/view';
export const PROFILE_CREATE = '/profile/create';


export const THREAD = '/threads';
export const FILE_MANAGER = '/file-manager';
export const FINANCE = '/finance';
export const EARNING = '/earnings';
export const CONTACT = '/contacts';


export const PAGE_NOT_FOUND = '/error-404';
export const SERVER_ERROR = '/error-500';