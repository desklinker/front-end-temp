export const DRAFT = 'ADMIN';
export const COORDINATOR = 'COORDINATOR';
export const SCREENER = 'SCREENER';

export const CLIENT = 'CLIENT';
export const EXECUTIVE = 'EXECUTIVE';
export const PROFESSIONAL = 'PROFESSIONAL';