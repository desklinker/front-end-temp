import { Subject } from 'rxjs';

const subject = new Subject();

export const modalService = {
  setModal: ({ header, text, onConfirm }) => subject.next({ header, text, onConfirm }),
  clearModal: () => subject.next(),
  onModal: () => subject.asObservable(),
};
